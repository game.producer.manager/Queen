import {Layer} from "../util/canvas";
import {Player} from "../player/player"
import {DrawPoint} from "drawpoint/dist-esm";

/**
 * All parts should go in this namespace
 * @namespace Part
 */

export class BodyPart {
	/**
	 * Location of the part, prepend with + to mean allow other parts to also occupy this location,
	 *  - to mean restrict any other part from being here
	 */
	loc: string;
	parentPart: BodyPart;
	/**
	 * Drawing layer
	 */
	layer: Layer;
	/**
	 * Whether this part should be drawn on the other side as well
	 */
	reflect: boolean;
	/**
	 * Whether this part should not be drawn if there are clothes covering its location
	 */
	coverConceal: string[];
	/**
	 * Whether it's possible to wear anything over this part (mutually exclusive with converconceal)
	 */
	uncoverable: boolean;
	/**
	 * List of "{part group} {location}" strings that specify which parts of which part groups this part
	 * should be drawn above. For example, having "decorativeParts torso" will ensure this part gets drawn
	 * above any parts in that location. Specifying only a location will mean to be above any part in that
	 * location regardless of group.
	 */
	aboveParts: string[];
	/**
	 * Opposite to aboveParts.
	 */
	belowParts: string[];

	side: string;

	constructor(...data: object[]);

	/**
	 * Set the stroke pattern for this part
	 *
	 */
	stroke(ctx:CanvasRenderingContext2D, ex:object): string;

	/**
	 * Set the fill pattern for this part
	 */
	fill(ctx:CanvasRenderingContext2D, ex:object): string;

	/**
	 * Set how thick the stroke line should be
	 */
	getLineWidth(avatar: Player): number;

	/**
	 * Calculate drawpoints associated with this part and return it in the sequence to be drawn.
	 * @this Calculated dimensions of the player owning the part
	 * @param ex Exports from draw that should hold draw points calculated up to now;
	 * additional draw points defined by this part should be defined on ex
	 * @param mods Combined modifiers of the part and the Player owning the part
	 * @param calculate
	 * @param part The body part itself
	 * @return List of draw points (or convertible to draw point objects)
	 */
	calcDrawPoints(ex: object, mods: object, calculate: boolean, part: BodyPart): DrawPoint[];
}

export enum SideValue {
	RIGHT = 0,
	LEFT = 1
}

interface BodyPartConstructor {
	new(data: []): BodyPart;
}

export const Part: {

    /**
     * Right side of the body for anything taking side
     * @readonly
     * @type {number}
     */
	RIGHT: SideValue.RIGHT,
    /**
     * Left side of the body for anything taking side
     * @readonly
     * @type {number}
     */
	LEFT: SideValue.LEFT,
    /**
     * Give me a base body part and a side it's supposed to be on
     * I'll return to you a body part specific to that side
     * @memberof module:da.Part
     * @param {BodyPart} PartPrototype Prototype to instantiate with
     * @param {...object} userData Overriding data
     * @returns {BodyPart}
     */
	create(PartPrototype: BodyPartConstructor, ...userData: object[]): BodyPart;
}

/**
 * Get a side string in a well defined format
 * @param side A side string
 * @returns Either the side string or null if unrecognized
 */
export function getSideLocation(side: string): string;

/**
 * Check whether two parts conflict
 * @param partA
 * @param partB
 * @returns True if the two parts have conflicting locations
 */
export function partConflict(partA: BodyPart, partB: BodyPart): boolean;

export function getChildLocation(parentLoc: string, child: string): { childSide: string, childLoc: string };

export function getSideValue(side: string | SideValue): SideValue;

export function getAttachedLocation(partPrototype: object): string;
