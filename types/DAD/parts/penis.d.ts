import {BodyPart} from "./part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DecorativePart} from "../decorative_parts/decorative_part";
import {Player} from "../player/player"


declare class Penis extends BodyPart {
	constructor(...data: object[]);

	getLineWidth(avatar: Player): number;
}


export class PenisHuman extends Penis {
	constructor(...data: object[]);

	stroke(): string;
	calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}


declare class PenisHead extends DecorativePart {
	constructor(...data: object[]);
	getLineWidth(avatar: Player): number;
}


export class PenisHeadHuman extends PenisHead {
	constructor(...data: object[]);

	fill(ctx: CanvasRenderingContext2D, ex: object): string;
    stroke(ctx: CanvasRenderingContext2D, ex: object): string;
    calcDrawPoints(ex: object, mods: object, calculate: boolean): DrawPoint[];
}
