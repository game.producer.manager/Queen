export function getBaseColors(avatar: object): object;

// export ex.baseStroke, ex.baseFill, and ex.baseLipColor
export function configureBaseColors(ex: object): void;
export function initCanvas(canvas, ctx, config, avatar, ex, layer, clear, customDrawingFunction): void;
