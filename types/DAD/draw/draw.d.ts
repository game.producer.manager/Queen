import * as dp from 'drawpoint/dist';
import {Player} from '../player/player';

//export as namespace da;

/* Copyright 2016 Johnson Zhong */

export const defaultConfig: {
    nameColor          : "#000",
    genderColor        : "#000",
    heightColor        : "#000",
    heightBarColor     : "#000",
    printAdditionalInfo: true,
    printVitals        : true,
    printHeight        : true,
    renderShoeSideView : true,
    offsetX            : 0,
    offsetY            : 0,
}

export class DrawUserConfig {
	nameColor?: string;
	genderColor?: string;
	heightColor?: string;
	heightBarColor?: string;
	printAdditionalInfo?: boolean;
	printVitals?: boolean;
	printHeight?: boolean;
	renderShoeSideView?: boolean;
	offsetX?: number;
	offsetY?: number;
}

export class DrawingExports {
	avatar: Player;
	cx: number;
	cy: number;
	height: number;
	width: number;
	canvasGroup: HTMLElement[];
	clip: object;
	ctx: CanvasRenderingContext2D;
	ctxGroup: CanvasRenderingContext2D[];
}

/**
 * Draw an avatar to a canvasGroup
 * @param canvasGroupObj HTML DOM element holding all the canvases, gotten with
 * da.getCanvasGroup
 * @param avatar Player object to draw
 * @param userConfig Configuration object to override defaultConfig
 * @param customDrawingFunction {function} Custom function for drawing on the base layer
 * @returns Exports
 */
export function draw(canvasGroupObj: HTMLElement, avatar:object, userConfig?: DrawUserConfig, customDrawingFunction?: Function): Promise<object>;

export function renderBase(ctxGroup, ex): void;

export function renderParts(ctxGroup, ex, config): void;

export function drawPart(ex, parts, partIndex, layer, side, drawnParts): dp.DrawPoint[];

/**
 * draw all avatar body parts on this layer and side (but not stroke or fill it)
 * returns all drawpoints for this layer and side
 */
export function drawPartsLayer(ex, layer, side, ctx): dp.DrawPoint[];
export function connectEndPoints(firstPoint, lastPoint, deflection?:number): dp.Point;

export function coverNipplesIfHaveNoBreasts(ex, ctx, part): boolean;

export function drawFocusedWindow(focusedCanvas, ex, userConfig): void;
