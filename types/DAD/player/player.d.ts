import {BodyPart, BodyPartConstructor} from "../parts/part";
import {Clothing} from "../clothes/clothing";

/**
 * Player holds all data necessary for draw to render it.
 * It is also meant to be extended by the user of the library to include gameplay statistics.
 * The gameplay statistics can then be linked to the calculation of drawing dimensions.
 */
export class Player {
	static defaultStats(): object;

	static defaultMods(): object;

	static defaultVitals(): object;

	static defaultClothes(): Clothing[];


	/**
	 * @constructor
	 * @param {object} data Properties of the Player object (override the default values)
	 */
	constructor(data: object);

	toString(): string;

	/**
	 * Keep stats within boundaries
	 */
	clampStats(): void;

	/**
	 * Keep vitals within max vitals
	 */
	clampVitals(): void;


	// property getters
	/**
	 * Get modified statistic
	 * @param param Name of statistic
	 * @returns  Modified statistic value
	 */
	get(param: string): number;

	/**
	 * Get difference of statistic relative to the average
	 * @param param Name of statistic
	 * @returns Modified statistic value relative to average
	 */
	diff(param: string): number;

	/**
	 * Get modified dimensions
	 * @param param Name of dimension
	 * @returns Modified dimension value
	 */
	getDim(param: string): number;

	getBaseVitals(param: string): number;

	/**
	 * Get modifier
	 * @param  param Name of modifier
	 * @returns Modifier value
	 */
	getMod(param: string): number;

	/**
	 * Get statistical description of a dimension
	 * @param param Name of dimension
	 * @returns Statistical dimension description with low, high, stdev, avg
	 */
	getDimDesc(param: string): object;

	/**
	 * Calculate all components of the avatar, making it ready for drawing/interacting.
	 * This is needed because there are derived stats that this function resolves.
	 * It is called autonmatically every time the avatar is drawn, but if you interact with it in an RPG
	 * system, you should call it after every modification
	 */
	calcAll(): void;

	private _clampMods(): void;

	/**
	 * Calculate each dimension with 'this' set as the player inside the function
	 * This is automatically called when drawing, so it's rarely called manually.
	 */
	private _calcDimensions(): void;

	/**
	 * Calculate max vitals; this should be called after clamping mods
	 * This is automatically called when drawing, so it's rarely called manually
	 */
	private _calcVitals(): void;

	// body part interaction

	/**
	 * Attach a new body part, replacing any conflicting parts if necessary
	 * @param newPart New body part to be added
	 * @param parts Part group of the Player to attach to
	 * @returns Either the part that was removed, or null if nothing was removed
	 */
	attachPart(newPart: BodyPart, parts?: BodyPart[]): BodyPart;

	/**
	 * Either returns a reference to the part in a specific location or null if the part doesn't
	 * exist
	 * @param location Where the part is located
	 * @param parts Part group to search in
	 * @param siblingIndex Index relative to other parts in same location in same group
	 * @returns Part in this location or null
	 */
	getPartInLocation(location: string, parts?: BodyPart[], siblingIndex?: number): BodyPart;

	removeSpecificPart(partPrototype: BodyPartConstructor, parts?: BodyPart[], siblingIndex?: number): BodyPart;

	/**
	 * Return whether a part is covered by clothing
	 * Depends on the part to specify what locations are considered coverable
	 * with part.coverConceal
	 * @param part
	 * @returns
	 */
	checkPartCoveredByClothing(part: BodyPart): boolean;

	/**
	 * Remove a part at a specific location
	 * @param loc Where the part is located
	 * @param parts Part group of the Player to remove from
	 * @param siblingIndex Index relative to other parts in same location in same group
	 * @returns Part removed or null if nothing was removed
	 */
	removePart(loc: string, parts?: BodyPart[], siblingIndex?: number): BodyPart;

	private doRemovePart(oldPart, parts): void;

	replaceHair(newHair: object): void;

	// expression manipulation
	/**
	 * Apply an expression, replacing the current expression
	 * @param expression All expression are kept in Expression @see Expression.create()
	 */
	applyExpression(expression: object): void;
	removeExpression(): void;

	// clothing manipulation
	/**
	 * Find the clothes that occupies a certain body location
	 * @param location Unmodified body location
	 * @returns list of Clothes objects that cover that body location
	 */
	getClothingInLocation(location: string): Clothing[];

	/**
	 * Find currently worn clothing that conflicts with given clothing
	 * @param clothing The
	 * @returns list of Clothes objects that conflict with this clothing
	 */
	getConflictingClothing(clothing: Clothing): Clothing[]

	/**
	 * Wear a Clothing item
	 * @param  clothing Clothing to be worn
	 * @returns null if failed to wear clothing, or the list of removed
	 * conflicting clothes
	 */
	wearClothing(clothing: Clothing): Clothing[]
	/**
	 * Remove an article of clothing
	 * @param clothing
	 * @returns Whether clothing was successfully removed
	 */
	removeClothing(clothing: Clothing): boolean;
	/**
	 * Remove any clothing of article that can be removed
	 * @returns {Array} Array of clothing that was removed
	 */
	removeAllClothing(): Clothing[];

	// item manipulation
	wieldItem(item): void;
	removeItem(item): void;

	addTattoo(tattoo): void;
	removeTattoo(tattoo): void;

	heightAdjust(): void;

	isFemale(): boolean;
	isMale(): boolean;

	/**
	 * Provide equivalent values for missing drawpoints
	 * @param ex
	 */
	fillMissingDrawpoints(ex): void;
}
