import {DecorativePart} from "./decorative_part";

declare class FacialHair extends DecorativePart {
	constructor(...data: object[]);

	stroke(): string;
	clipFill(): void;
	fill(ignore:any, ex:object): string;
}


export class Mustache extends FacialHair {
	constructor(...data: object[]);

	stroke(): string;
	clipFill(): void;
	fill(ignore: any, ex: object): string;
}
