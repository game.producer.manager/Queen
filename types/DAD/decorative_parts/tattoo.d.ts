import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

export class Tattoo {
	constructor(...data: object[]);

    /**
     * Return the starting location for drawing the item
     * @param ex Drawing exports
     */
	renderTattooLocation(ex: DrawingExports): DrawPoint;
}

/**
 * Very similar to Items
 */
export namespace Tattoos {
	export function getRender(tattoo: Tattoo): HTMLImageElement;

	export function loadTattoo(tattoo: Tattoo): void;

	interface TattoConstructor {
		new(...data: object[])
	}
	export function create(Item: TattoConstructor, ...data: object[]): Tattoo;
}
