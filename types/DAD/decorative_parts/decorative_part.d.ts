import {Player} from "../player/player";
/**
 * Base class for non-combat parts for show
 */
export class DecorativePart {
	constructor(...data: object[]);
	stroke(ctx: CanvasRenderingContext2D, ex: object): string;
	fill(ignore: any, ex: object): string;

    // how thick the stroke line should be
	getLineWidth(avatar: Player): number;
}
