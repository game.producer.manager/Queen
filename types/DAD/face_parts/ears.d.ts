import {FacePart} from "./face_part";
import {Player} from "../player/player";
import {DrawingExports} from "../draw/draw";
import {DrawPoint} from "drawpoint/dist-esm";

declare class Ears extends FacePart {
	constructor(...data: object[]);

	clipFill(): void;
	clipStroke(): void;
	fill(ignore: any, ex: object): string;
	getLineWidth(avatar: Player): number;
}


export class EarsHuman extends Ears {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}


export class EarsElf extends Ears {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
