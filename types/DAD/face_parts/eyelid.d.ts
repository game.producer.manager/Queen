import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {FacePart} from "./face_part";

declare class Eyelid extends FacePart {
	constructor(...data: object[]);
}


export class EyelidHuman extends Eyelid {
	constructor(...data: object[]);

	fill(ignore: any, ex: DrawingExports): string;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
