import {FacePart} from "./face_part";
import {
	DrawPoint,
    extractPoint,
    clamp,
    breakPoint,
    continueCurve,
    adjust,
    rotatePoints,
    endPoint
} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class Eyelash extends FacePart {
	constructor(...data: object[]);
}

export class EyelashHuman extends Eyelash {
	constructor(...data: object[]);

	fill(ignore, ex: DrawingExports): string;
    calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
