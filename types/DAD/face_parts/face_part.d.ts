import {none} from "drawpoint/dist-esm";
import {Layer} from "../util/canvas";
import {Player} from "../player/player";

/**
 * Base class for parts that are on the face (head)
 */
export class FacePart {
	constructor(...data: object[]);

	stroke(ctx: CanvasRenderingContext2D, ex: object): string;
	fill(ctx: CanvasRenderingContext2D, ex: object): string;

    // how thick the stroke line should be
	getLineWidth(avatar: Player): number;
}
