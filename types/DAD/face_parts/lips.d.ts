import {FacePart} from "./face_part";
import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class LipShading extends ShadingPart {
	constructor(...data: object[]);

	fill(): string;
    calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Lips extends FacePart {
	constructor(...data: object[]);
}


export class LipsHuman extends Lips {
	constructor(...data: object[]);

	fill(ignore: any, ex: DrawingExports): string;
	clipFill(): void;
    calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
