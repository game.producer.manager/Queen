import {HairPart} from "./hair_part";

export class AsianBunBack extends HairPart {

	constructor(...data: object[]);

    // hair has special draw methods
	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object, ignore: any, extraColors: any): void;
}

export class AsianBunFront extends HairPart {
	constructor(...data: object[]);

	renderHairPoints(ctx: CanvasRenderingContext2D, ex: object): void;
}
