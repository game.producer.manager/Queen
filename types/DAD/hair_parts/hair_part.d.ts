import {Layer} from "../util/canvas";

/**
 * Base class for all hair parts; should go in a Player's hairParts
 */
export class HairPart {
	layer: Layer;
	reflect: boolean;
	coverConceal: [];
	uncoverable: boolean;
	constructor(...data: object[]);

	stroke(ignore, ex): string;

	fill(ignore, ex): string;
	getLineWidth(): number;
}


/**
 * Where all hairs go
 * @namespace Hair
 */
export const Hair: {

    /**
     * Create a HairPart instance
     * @param Part Prototype of hair part
     * @param data Overriding data for this particular part
     * @returns Instantiated hair part
     */
	create(Part: HairPart, ...data: []): HairPart,

    hairBack     : {
        loc       : "back hair",
        layer     : Layer.BACK,
        belowParts: ["parts groin"],
    },
	hairAboveEars: {
		loc: "ears hair",
		layer: Layer.BELOW_HAIR,
		aboveParts: ["ears"],
	},
	hairMedium: {
		loc: "medium hair",
		layer: Layer.GENITALS,
		aboveParts: ["chest", "neck"],
	},
	hairFront: {
		loc: "front hair",
		layer: Layer.HAIR,
	}
};
