import {DrawPoint, Point} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export function calcDressShirt(ex: DrawingExports): {
	collarbone: DrawPoint,
	cusp: DrawPoint,
	top: Point,
	bot: DrawPoint,
	outBot: DrawPoint,
	outMid: DrawPoint,
	breastBot: DrawPoint,
	breastTip: DrawPoint
	outTop: DrawPoint
};

/**
 * ClothingPart drawn classes/components
 */
export class DressShirtBasePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class DressShirtCollarPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class DressShirtButtonPart extends ClothingPart {
	constructor(...data: object[]);
	buttonStroke: string;
	buttonFill: string;
	buttonRadius: number;
	buttonThickness: number;
	buttonCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class DressShirtBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class LongSleeveCuffPart extends ClothingPart {
	constructor(...data: object[]);

	cuffPattern: string;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Base Clothing classes
 */
export class DressShirt extends Clothing {
	constructor(...data: object[]);

	stomachCoverage: number;
	neckCoverage: number;

	liningWidth: number;
	liningPattern: string;

	collarCoverage: number;
	collarHeight: number;
	collarWidth: number;
	collarPattern: string;

	// parting of the top and bottom in shirt in cm
	topParted: number;
	botParted: number;

	/**
	 * How tightly it clings to the body
	 */
	cling: number;
}


/**
 * Concrete Clothing classes
 */
export class WomenDressShirt extends DressShirt {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}


export class MenDressShirt extends DressShirt {
	constructor(...data: object[]);
	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
