import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class AnkletPart extends ClothingPart {
	constructor(...data: object[]);
	/**
	 * Where along the leg (0 is ankle, 1 is knee) to start the bottom
	 */
	startAlongLeg: number;
}

export class ChainAnkletPart extends AnkletPart {
	constructor(...data: object[]);
	chainWidth: number;
	chainDash: number[];

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class BandedAnkletPart extends AnkletPart {
	constructor(...data: object[]);

	legCoverage: number;

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/**
 * Base Clothing classes
 */
export class Anklet extends Clothing {
	constructor(...data: object[]);
}


/**
 * Concrete Clothing classes
 */
export class ChainAnklet extends Anklet {
	constructor(...data: object[]);

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class BandedAnklet extends Anklet {
	constructor(...data: object[]);

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
