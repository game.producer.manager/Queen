declare interface RollADieOptions {
	/**
	 * The element to render die animation on.
	 */
	element: HTMLElement;
	/**
	 * The number of dice to use.
	 */
	numberOfDice: number;
	/**
	 * Called when animation is finished. Returns an array of the values from throw.
	 */
	callback: (dices: number[]) => void;
	/**
	 * Roll the die without sound)
	 */
	noSound?: boolean;
	/**
	 * Time in milliseconds to delay before removing animations
	 */
	delay?: number;
	/**
	 * Values to show on die face. When provided, it overrides library generated values.
	 */
	values?: number[];
}

/**
 * Simple 3D dice roll animator by CSS3 Animation.
 */
declare function rollADie(params: RollADieOptions): void;
