App.Data.JobData["JUNGLE01"] = {
	ID: "JUNGLE", TITLE: "Explore the Jungle", GIVER: "JUNGLE",
	RATING: 1, // of 5
	// Can't do late at night
	PHASES: [0, 1, 2, 3],
	// Can do as many times as wanted in day.
	DAYS: 0,
	COST: [
		{TYPE: "TIME", VALUE: 1},
		{TYPE: "STAT", NAME: "Energy", VALUE: 2}
	],
	REQUIREMENTS: [
		// Check to make sure player is equipped the worn machete
		{TYPE: "EQUIPPED", NAME: "worn machete"},
	],
	INTRO:
		// This is run from a room, so no NPC teaser text.
		"",
	START:
		"You pull out your trusty @@.item-quest;worn machete@@ and begin hacking away at the underbrush.",
	SCENES: [
		{
			// Just check the players navigation skill and store the result.
			ID: "SCENE01",
			CHECKS: [
				{TAG: "A", TYPE: "SKILL", NAME: "Navigating", DIFFICULTY: 50}
			],
			TEXT: [
				{"A": 50, TEXT: "You spend what seems like hours traipsing through the jungle, @@not really sure@@ which way you are heading."},
				{"A": 99, TEXT: "You do your best to clear a straight path, trying to follow the sun, however, your managed to @@get turned around someplace@@… how unfortunate."},
				{"A": 500, TEXT: "You do your best to clear a straight path, trying to follow the sun. @@It seems to have worked@@ and you managed to survey a large part of the jungle."}
			]
		},
		{   // If pass check 'A' and If counter is at MAX (3) and the cove hasn't been found yet.
			ID: "SCENE04a",
			TRIGGERS: [
				{TYPE: "TAG", NAME: "A", VALUE: 100, CONDITION: "gte"},
				{TYPE: "COUNTER", NAME: "JUNGLE_COUNTER", VALUE: 3, CONDITION: "gte"},
				{TYPE: "FLAG_SET", NAME: "COVE_FOUND", VALUE: false}
			],
			POST: [
				// Clear counter, remember we just found something, set flag for finding the cove (used by jungle.twee)
				{TYPE: "JOB_FLAG", NAME: "JUNGLE_COUNTER", VALUE: 0},
				{TYPE: "JOB_FLAG", NAME: "JUNGLE_REWARD", VALUE: 1},
				{TYPE: "JOB_FLAG", NAME: "COVE_FOUND", VALUE: 1}
			],
			TEXT: "Along your adventure you seemed to have stumbled across a trail that doesn't look natural. It's cleverly disguised but leads towards the leeward side of the island and a small hidden cove.",
		},
		{   // If pass check 'A' and If counter is at MAX (3) and if we haven't just found the cove (reward flag) and the ruins hasn't been found yet.
			ID: "SCENE04a",
			TRIGGERS: [
				{TYPE: "TAG", NAME: "A", VALUE: 100, CONDITION: "gte"},
				{TYPE: "COUNTER", NAME: "JUNGLE_COUNTER", VALUE: 3, CONDITION: "gte"},
				{TYPE: "FLAG_SET", NAME: "JUNGLE_REWARD", VALUE: false},
				{TYPE: "FLAG_SET", NAME: "RUINS_FOUND", VALUE: false},
				{TYPE: "FLAG_SET", NAME: "COVE_FOUND", VALUE: true}
			],
			POST: [
				// Clear counter, remember we just found something, set flag for finding the cove (used by jungle.twee)
				{TYPE: "JOB_FLAG", NAME: "JUNGLE_COUNTER", VALUE: 0},
				{TYPE: "JOB_FLAG", NAME: "JUNGLE_REWARD", VALUE: 1},
				{TYPE: "JOB_FLAG", NAME: "RUINS_FOUND", VALUE: 1}
			],
			TEXT: "As you're hacking across the jungle, you stumble across what appears to be a large stone stature. You cautiously sneak up closer to inspect it. As you approach the fallen statue, you choke back a gasp as you realize that it's part of a larger structure, almost buried under the foliage. You are tempted to explore it immediately, but for now you return to the jungle clearing to plan your next move.",
		},
		{
			// On a skill success we want to increment our success counter. Don't process if we just hit max (3) and set the reward counter.
			ID: "SCENE04b",
			TRIGGERS: [
				{TYPE: "TAG", NAME: "A", VALUE: 100, CONDITION: "gte"},
				{TYPE: "COUNTER", NAME: "JUNGLE_COUNTER", VALUE: 2, CONDITION: "lte"},
				{TYPE: "FLAG_SET", NAME: "JUNGLE_REWARD", VALUE: false}
			],
			POST: [
				// Increment counter.
				{TYPE: "COUNTER", NAME: "JUNGLE_COUNTER", VALUE: 1}
			],
			TEXT: "You didn't find anything today, but you have a feeling that you're getting closer…",
		},
		{   // Just unset the flag that shows we found a path and make sure we clear the counter.
			ID: "SCENE04c",
			TRIGGERS: [
				{TYPE: "FLAG_SET", NAME: "JUNGLE_REWARD", VALUE: true}
			],
			POST: [
				{TYPE: "JOB_FLAG", NAME: "JUNGLE_REWARD", VALUE: undefined}
			],
		}

	],
};
