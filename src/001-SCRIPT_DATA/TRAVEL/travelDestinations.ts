namespace App.Data.Travel {
	export function disableAtLateNight(player: Entity.Player) {
		return player.Phase >= Phase.LateNight ? false : true;
	}
}
