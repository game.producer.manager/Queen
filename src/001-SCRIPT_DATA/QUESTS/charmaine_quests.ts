App.Data.Quests["CHARMAINE_CUSTOMER_SERVICE"] = {
	ID: "CHARMAINE_CUSTOMER_SERVICE", TITLE: "Customer Service - The Saucy Slattern",
	GIVER: "Charmaine",
	PRE: [
		{TYPE: "QUEST_FLAG", NAME: "CHARMAINE_JOB_1", VALUE: "COMPLETED", CONDITION: "eq"},
		{TYPE: "QUEST_FLAG", NAME: "CHARMAINE_JOB_2", VALUE: "COMPLETED", CONDITION: "eq"},
		{TYPE: "QUEST_FLAG", NAME: "CHARMAINE_JOB_3", VALUE: "COMPLETED", CONDITION: "eq"},
		{TYPE: "QUEST_FLAG", NAME: "CHARMAINE_JOB_4", VALUE: "COMPLETED", CONDITION: "eq"}
	],
	CHECKS: [
		{TYPE: "TRACK_CUSTOMERS", NAME: "SaucySlattern", VALUE: 20, CONDITION: "gte"}
	],
	ON_ACCEPT: [
		{TYPE: "TRACK_CUSTOMERS", NAME: "SaucySlattern"}
	],
	REWARD: [
		{TYPE: "MONEY", VALUE: 1000},
		{TYPE: "NPC_STAT", NAME: "Mood", VALUE: 15},
		{TYPE: "SLOT"},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/RARE_HANDMAIDEN", VALUE: 1},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/RARE_BREATH_MINT", VALUE: 1},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/RARE_ANAL_ANGEL", VALUE: 1},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/RARE_BOOBJITSU_MATRIARCH", VALUE: 1},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/UNCOMMON_CONCUBINE", VALUE: 1},
		{TYPE: "ITEM_CHOICE", NAME: "REEL/LEGENDARY_WHORE", VALUE: 1}
	],
	INTRO:
		"NPC_NAME greets you warmly with a smile and says s(Well, if it isn't PLAYER_NAME? The boys here at the Lass sure can't \
        get enough of you, can they?)\n\n\
        She seems to consider something for a moment and then grabs you by the arm, pulling you in close.\n\n\
        s(You've become pretty popular here and I'm thinkin' there's a way we could both benefit,) she says. The tone of her voice \
        makes you sure that any sort of agreement is definitely going to favor her, but still you're intrigued.\n\n\
        NPC_NAME leads you over to the main floor, looking out upon the clientele gathered for the days festivities. There's a literal \
        throbbing mass of horny men and dozens of girls working the crowd, in more ways than one.\n\n\
        s(Tell you what, you work for me for a bit all proper like - fuck my customers, and fuck them //good//. Drive them crazy. Make \
        it so they can't shut up about the whores here at the @@.location-name;Saucy Slattern@@ and I'll give you a cut of the house \
        profits, and maybe a little present. What do you say to that?)\n\n\
        It's an interesting deal, and obviously it's not the first time you've worked for NPC_NAME. You know she pays well and keeps \
        her word."
	,
	MIDDLE: "NPC_NAME says s(Hey, PLAYER_NAME, keeping your holes busy I hope? The words starting to get around, just keep at it.)",
	FINISH:
		"NPC_NAME says s(Nicely done, PLAYER_NAME. Just this morning I heard a chaplain talking to a baker outside his stall about \
        the class of sluts we have here and I'm sure we owe a lot of that to you.)\n\n\
        It's dubious praise, but considering your circumstances, you'll take it.\n\n\
        s(Like I promised, here's your cut of the take, oh and also, I threw in a little something extra that'd be of particular use \
        to a working girl like yourself.)"
	,
	JOURNAL_ENTRY: "NPC_NAME has asked you to work at her establishment, whoring for her customers and satisfying them so that \
        they'll spread the word about the experience.",
	JOURNAL_COMPLETE: "You completed NPC_NAME's dubious task, screwing and sucking your way through her customer base and \
        helping increase the notoriety of the @@.location-name;Saucy Slattern@@. "+
		"NPC_NAME was so grateful she gave you a fair sized cut of the profits and overall increased your whoring ability."
};
