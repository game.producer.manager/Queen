App.Data.EffectLib["BLACK_HAIR_DYE"] = {
    FUN : (p) => { p.HairColor = 'black'; App.Avatar._DrawPortrait(); },
    VALUE : 100,
        KNOWLEDGE : [ "Dye Hair Black" ]
};
App.Data.EffectLib["BROWN_HAIR_DYE"] = {
    FUN : (p) => { p.HairColor = 'brown'; App.Avatar._DrawPortrait(); },
    VALUE : 100,
        KNOWLEDGE : [ "Dye Hair Brown" ]
};
App.Data.EffectLib["RED_HAIR_DYE"] = {
    FUN : (p) => { p.HairColor = 'red'; App.Avatar._DrawPortrait(); },
    VALUE : 100,
        KNOWLEDGE : [ "Dye Hair Red" ]
};
App.Data.EffectLib["BLOND_HAIR_DYE"] = {
    FUN : (p) => { p.HairColor = 'blond'; App.Avatar._DrawPortrait(); },
    VALUE : 100,
        KNOWLEDGE : [ "Dye Hair Blond" ]
};
/** THE LOVERS - TAROT CARD */
App.Data.EffectLib["THE_LOVERS"] = {
    FUN : (p) => {p.AddItem("DRUGS", "siren elixir", 0);},
    VALUE : 500, KNOWLEDGE : [ "Add Item++++" ]
};
/** THE EMPRESS - TAROT CARD */
App.Data.EffectLib["THE_EMPRESS"] = {
    FUN : (p) => {p.AddItem("LOOT_BOX", "common food loot box", 0);},
    VALUE : 500, KNOWLEDGE : [ "Add Item++++" ]
};

App.Data.EffectLib.VOODOO_LACTATION = {
    FUN : (p) => { p.AdjustBodyXP("Lactation", 300, 75); },
    VALUE : 0,
    KNOWLEDGE : [ "Voodoo induced lactation+" ]
};

App.Data.EffectLib.VOODOO_BUST_FIRMNESS_NORMAL = {
	FUN: (p) => {
		const curXp = p.GetStatXP("BODY", "BustFirmness");
		if (curXp < 0) {
			p.AdjustBodyXP("BustFirmness", 1 - curXp, 70);
		} else {
			p.AdjustBodyXP("BustFirmness", curXp, 70);
		}
	},
    VALUE : 0,
    KNOWLEDGE : [ "Voodoo induced breast firmness+" ]
};

App.Data.EffectLib.VOODOO_BUST_FIRMNESS_FULL = {
	FUN: (p) => {
		const curXp = p.GetStatXP("BODY", "BustFirmness");
		if (curXp < 0) {
			p.AdjustBodyXP("BustFirmness", 10 - curXp);
		} else {
			p.AdjustBodyXP("BustFirmness", curXp);
		}
	},
    VALUE : 0,
    KNOWLEDGE : [ "Voodoo induced breast firmness++" ]
};
