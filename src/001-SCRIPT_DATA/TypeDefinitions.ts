declare namespace App{
	const enum Gender {
		Female = 0,
		Male = 1,
		Shemale = 2
	}
}
declare namespace App.Data {

	type SkillBonusData = Partial<Record<SkillStr, [number, number, number]>>;

	interface ItemDesc {
		Name: string;
		InMarket?: boolean;
		Quest?: boolean;
		Value?: number;
		PriceCoefficient?: number;
	}

	type CosmeticsType = "hair tool" | "hair products" | "hair treatment" | "basic makeup" | "expensive makeup";
	type ItemSubTypeStr = "food" | "potion" | "misc" | "MISC_LOOT" | "LOOT_BOX" | "QUEST" | ClothingType | CosmeticsType;

	interface InventoryItemDesc extends ItemDesc {
		ShortDesc: string;
		LongDesc: string;
		Type: ItemSubTypeStr;
	}

	interface ConsumableItemDesc extends InventoryItemDesc {
		Message?: string;
		/**
		 * Number of uses that are sold in a single item
		 * @default 1
		 */
		Charges?: number;
		Effects?: string[];
	}

	interface DrugItemDesc extends ConsumableItemDesc {
	}

	interface CosmeticsItemDesc extends ConsumableItemDesc {
		SkillBonus: SkillBonusData;
	}

	interface FoodItemDesc extends ConsumableItemDesc {
	}

	type ClothingSlot = "Neck" | "Bra" | "Nipples" | "Corset" | "Panty" | "Wig" | "Hat" |
		"Stockings" | "Shirt" | "Pants" | "Penis" | "Dress" | "Costume" | "Shoes" | "Butt" | "Weapon" |
		"Mascara";

	type ClothingStyle = "COMMON" | "UNCOMMON" | "RARE" | "LEGENDARY";
	type ClothingType = "ACCESSORY" | "CLOTHING" | "ONE PIECE" | "WEAPON";

	type HairColor = "black" | "blond" | "brown" | "red" | "lavender";
	interface ClothingItemDesc extends InventoryItemDesc {
		Slot: ClothingSlot;
		Restrict?: ClothingSlot[];
		Color: string;
		Style: ClothingStyle;
		Type: ClothingType;
		Category?: Clothes.CategoryStr[];
		WearEffect?: string[];
		ActiveEffect?: string[];
		InMarket?: boolean;
		Locked?: boolean;
		HairLength?: number; // for wigs
		HairStyle?: string; // for wigs
		HairBonus?: number; // for wigs
		Meta?: string[];
	}

	const enum ReelRank {
		Common = "COMMON",
		Uncommon = "UNCOMMON",
		Rare = "RARE",
		Legendary = "LEGENDARY"
	}

	type ReelRankStr = "COMMON" | "UNCOMMON" | "RARE" | "LEGENDARY";
	type ReelWildcard = "BEAUTY" | "FEM" | "PERV";
	type ReelAction = Whoring.SexActStr | ReelWildcard;

	interface ReelDesc extends ItemDesc {
		RANK: ReelRankStr;
		VALUE: number;
		CSS: string;
		DATA: ReelAction[];
	}

	interface BeautyProcedureDesc {
		NAME: string;
		DIFFICULTY: number;
		STYLE: number;
		SHORT: string;
	}


	interface HairStyleDesc extends BeautyProcedureDesc {
		RESOURCE1: number; // RES1 = ACCESSORIES, RES2 = PRODUCT
		RESOURCE2: number;
		MIN: number;
		MAX: number;
	}

	interface MakeupStylesDesc extends BeautyProcedureDesc { // RES1 = BASIC MAKEUP, RES2 = EXPENSIVE MAKEUP
		RESOURCE1: number;
		RESOURCE2: number;
	}

	interface NPCDesc {
		Name: string;
		Mood: number;
		DailyMood: number;
		Lust: number;
		DailyLust: number;
		Gender: 0 | 1 | 2;
		Title: string;
		LongDesc: string;
		Store?: string;
	}

	interface StoreInventoryItemDesc {
		CATEGORY: "COMMON" | "RARE";
		TYPE: ItemTypeStr;
		QTY: number;
		MAX: number;
		PRICE: number;
		MOOD: number;
		LOCK: 0 | 1;
		TAG: string;
	}

	interface StoreDesc {
		ID: string;
		NAME: string;
		OPEN: DayPhase[];
		RESTOCK: number;
		MAX_RARES?: number;
		UNLOCK_FLAG?: string;
		UNLOCK_FLAG_VALUE?: string | number;
		INVENTORY: StoreInventoryItemDesc[];
	}

	interface ItemTypeDescMap {
		"DRUGS": DrugItemDesc;
		"FOOD": FoodItemDesc;
		"COSMETICS": CosmeticsItemDesc;
		"MISC_CONSUMABLE": ConsumableItemDesc;
		"MISC_LOOT": ConsumableItemDesc;
		"CLOTHES": ClothingItemDesc;
		"WEAPON": ClothingItemDesc;
		"QUEST": InventoryItemDesc;
		"LOOT_BOX": ConsumableItemDesc;
		"REEL": ReelDesc;
	}

	type ItemCategoryAny = keyof ItemTypeDescMap;
	type ItemNameTemplate<T extends keyof ItemTypeDescMap> = `${T}/${string}`;
	type ItemNameTemplateAny = ItemNameTemplate<ItemCategoryAny>;

	//#region Effects

	type Items = App.Items.Clothing | App.Items.Consumable | App.Items.QuestItem | App.Items.Reel;
	type EffectAction = (p: App.Entity.Player, o: Items | null) => string | void;

	interface EffectDescBase {
		VALUE: number;
		KNOWLEDGE: string[];
	}

	interface EffectDesc extends EffectDescBase {
		FUN: EffectAction;
	}

	type VirtualSkillStr = 'Sharpness' | 'Damage Resistance';
	type WearSkill = SkillStr | VirtualSkillStr | CoreStatStr | BodyStatStr | DerivedBodyStatStr;
	type WearEffectAction = (s: WearSkill, o: Items.Clothing) => number;
	interface ClothingWearEffectDesc extends EffectDescBase {
		FUN: WearEffectAction;
	}
	//#endregion

	//#region Loot boxes
	interface LootTableItem {
		Type: ItemTypeStr | "COINS";
		Chance: number;
		Min?: number;
		Max?: number;
		MaxCount?: number;
		Free?: boolean;
		Filter?: (obj: object, category: ItemTypeStr, pool: number) => object;
	}

	interface LootItem {
		category: ItemTypeStr;
		tag: string;
		min: number;
		max: number;
	}
	//#endregion

	namespace Tasks {
		class CheckResult {
			RESULT: number;
			VALUE: number;
			MOD: number;
		}

		type CheckResults = Record<string, CheckResult>;

		type TaskCheckFunc = (player: App.Entity.Player, task: App.Scene, checks: CheckResults) => number;

		namespace Checks {
			interface Base {
				TAG: string;
				OPT?: "NO_SCALING" | "RANDOM" | "RAW";
			}

			interface BaseNumeric extends Base {
				VALUE?: number;
				DIFFICULTY: number;
			}

			interface Stat extends BaseNumeric {
				TYPE: "STAT";
				NAME: CoreStatStr;
			}

			interface Skill extends BaseNumeric {
				TYPE: "SKILL";
				NAME: SkillStr;
			}

			interface Body extends BaseNumeric {
				TYPE: "BODY";
				NAME: BodyStatStr;
			}

			interface Meta extends BaseNumeric {
				TYPE: "META";
				NAME: "BEAUTY" | "DANCE_STYLE";
			}

			interface Func extends Base {
				TYPE: "FUNC";
				VALUE: number;
				FUN: TaskCheckFunc;
			}

			type Any = Stat | Skill | Body | Meta | Func;
		}

		namespace Posts {
			interface Slot {
				TYPE: "SLOT";
			}

			interface Option<T> {
				Opt?: T;
			}

			interface BaseNumeric {
				VALUE: number;
				/** check name to read factor value from */
				FACTOR?: string;
				OPT?: 'RANDOM';
			}

			interface BaseFlag {
				NAME: string;
				VALUE: TaskFlag | undefined;
				OP?: "SET" | "ADD"
			}

			interface NamedNumeric<T> extends BaseNumeric {
				NAME: T;
			}

			interface Consumable extends NamedNumeric<string> {
				TYPE: ConsumableItemTypeStr;
			}

			interface Clothing extends NamedNumeric<string> {
				TYPE: EquipmentItemTypeStr;
				WEAR?: "WEAR";
			}

			interface Item extends NamedNumeric<string> {
				TYPE: "ITEM";
			}

			interface PickItem {
				TYPE: "PICK_ITEM";
				NAME: ItemTypeStr;
				VALUE: {price: number; meta_key?: string;}
				WEAR?: "WEAR";
			}

			interface ItemChoice extends NamedNumeric<string> {
				TYPE: "ITEM_CHOICE";
			}

			interface BodyXp extends NamedNumeric<BodyStatStr> {
				TYPE: "BODY_XP";
			}

			interface StatXp extends NamedNumeric<CoreStatStr> {
				TYPE: "STAT_XP";
			}

			interface SkillXp extends NamedNumeric<SkillStr> {
				TYPE: "SKILL_XP";
			}

			interface Stat extends NamedNumeric<CoreStatStr> {
				TYPE: "STAT";
			}

			interface Body extends NamedNumeric<BodyStatStr> {
				TYPE: "BODY";
			}

			interface Skill extends NamedNumeric<SkillStr> {
				TYPE: "SKILL";
			}

			interface NpcStat extends NamedNumeric<Entity.NPCStat> {
				TYPE: "NPC_STAT";
				NPC?: string | string[];
			}

			interface JobFlag extends BaseFlag {
				TYPE: "JOB_FLAG";
			}

			interface QuestFlag extends BaseFlag {
				TYPE: "QUEST_FLAG";
			}

			interface Quest {
				TYPE: "QUEST";
				NAME: string;
				VALUE: "START" | "COMPLETE";
			}

			interface Counter {
				TYPE: "COUNTER";
				NAME: string;
				VALUE: number | undefined;
				OPT?: number;
			}

			interface LootBox {
				TYPE: "LOOT_BOX";
				NAME: string;
				VALUE: number;
			}

			interface Money extends BaseNumeric {
				TYPE: "MONEY";
			}

			interface Tokens extends BaseNumeric {
				TYPE: "TOKENS";
			}

			interface Store {
				TYPE: "STORE";
				NAME: string;
				VALUE: string;
				OPT?: "LOCK" | "UNLOCK";
			}

			interface ResetStore {
				TYPE: "RESET_SHOP";
				NAME: string;
			}

			interface CorruptWillpower {
				TYPE: "CORRUPT_WILLPOWER";
				NAME: "LOW" | "MEDIUM" | "HIGH";
				VALUE: number;
				OPT: number;
			}

			interface SailDays {
				TYPE: "SAIL_DAYS";
				VALUE: number;
			}

			interface TrackCustomers {
				TYPE: "TRACK_CUSTOMERS";
				NAME: string;
			}

			interface TrackProgress {
				TYPE: "TRACK_PROGRESS";
				NAME: string;
				VALUE?: number;
				/** Multiply by MOD value from named check */
				FACTOR?: string;
				OP?: "SET" | "ADD" | "MULTIPLY"
			}
			interface SetClothingLock {
				TYPE: "SET_CLOTHING_LOCK";
				SLOT: ClothingSlot;
				VALUE: boolean;
			}

			interface GameState {
				TYPE: "GAME_STATE";
				NAME: "DAY_PHASE";
				VALUE: number;
			}

			interface BodyEffect {
				TYPE: "BODY_EFFECT";
				NAME: string;
				VALUE: boolean;
			}

			/** Apply an effect from EffectLib */
			interface Effect {
				TYPE: "EFFECT";
				NAME: string;
			}


			type Any = BodyXp | SkillXp | StatXp | Body | Skill | Stat | Slot | Consumable | Clothing | Item | ItemChoice | PickItem | NpcStat | Counter |
				JobFlag | QuestFlag | Quest | Store | ResetStore | LootBox | Money | Tokens | CorruptWillpower | SailDays | TrackCustomers | TrackProgress |
				SetClothingLock | GameState | BodyEffect | Effect;
		}

		type Condition = "gte" | "lte" | "gt" | "lt" | "eq" | "ne" |
			">=" | "<=" | ">" | "<" | "==" | "!=" |
			boolean;

		//#region Requirements
		namespace Requirements {
			interface Base {
				ALT_TITLE?: string;
			}

			interface BaseNumeric extends Base {
				VALUE: number;
				CONDITION: Condition;
			}

			type FlagValue = number | string | boolean | number[] | string[];

			interface BaseFlag<FlagType> extends Base {
				NAME: string;
				VALUE: FlagType;
				CONDITION?: Condition;
			}

			interface BaseBoolean extends Base {
				NAME: string;
				VALUE: boolean;
			}

			interface Stat extends BaseNumeric {
				TYPE: "STAT";
				NAME: CoreStatStr;
			}

			interface Skill extends BaseNumeric {
				TYPE: "SKILL";
				NAME: SkillStr;
			}

			interface Body extends BaseNumeric {
				TYPE: "BODY";
				NAME: BodyStatStr;
			}

			interface Meta extends BaseNumeric {
				TYPE: "META";
				NAME: "BEAUTY";
			}

			interface Money extends BaseNumeric {
				TYPE: "MONEY";
			}

			interface Tokens extends BaseNumeric {
				TYPE: "TOKENS";
			}

			interface NPCStat extends BaseNumeric {
				TYPE: "NPC_STAT";
				NAME: Entity.NPCStat;
				OPTION?: string;
			}

			interface DaysPassed extends BaseNumeric {
				TYPE: "DAYS_PASSED";
				NAME?: string;
			}

			interface QuestFlag extends BaseFlag<FlagValue> {
				TYPE: "QUEST_FLAG";
			}

			interface Quest extends BaseFlag<QuestState> {
				TYPE: "QUEST";
			}

			interface Job extends BaseFlag<FlagValue> {
				TYPE: "JOB_FLAG";
			}

			interface Item extends BaseNumeric {
				TYPE: "ITEM";
				NAME: ItemNameTemplateAny;
				/** Do not show this in the quest requirements until player finds the item */
				HIDDEN?: boolean;
			}

			interface Equipped extends Base {
				TYPE: "EQUIPPED";
				NAME: string;
			}

			interface NameOrSLot {
				SLOT?: ClothingSlot;
				NAME?: string;
			}

			interface IsWearingBase {
				TYPE: "IS_WEARING";
				VALUE?: boolean;
			}

			type IsWearing = IsWearingBase & RequireAtLeastOne<NameOrSLot>;

			interface StyleCategory extends BaseNumeric {
				TYPE: "STYLE_CATEGORY";
				NAME: Data.Clothes.CategoryStr;
			}

			interface Style extends BaseNumeric {
				TYPE: "STYLE";
			}

			interface HairStyle extends BaseBoolean {
				TYPE: "HAIR_STYLE";
			}

			interface HairColor extends BaseBoolean {
				TYPE: "HAIR_COLOR";
			}

			interface PortName extends BaseBoolean {
				TYPE: "PORT_NAME";
			}

			interface InPort extends BaseNumeric {
				TYPE: "IN_PORT";
			}

			interface TrackProgress extends BaseNumeric {
				TYPE: "TRACK_CUSTOMERS" | "TRACK_PROGRESS";
				NAME: string;
			}

			type Any = Body | Stat | Skill | Meta | Money | Tokens |
				NPCStat | DaysPassed | Quest | QuestFlag | Job |
				Item | Equipped | IsWearing | StyleCategory |
				Style | HairStyle | HairColor |
				PortName | InPort | TrackProgress;
		}
		//#endregion

		//#region Triggers
		namespace Triggers {
			interface BaseNumeric {
				VALUE: number;
				CONDITION: Condition;
			}

			interface NPCStat extends BaseNumeric {
				TYPE: "NPC_STAT";
				NAME: Entity.NPCStat;
			}

			interface Random100 extends BaseNumeric {
				TYPE: "RANDOM-100";
			}

			interface Counter extends BaseNumeric {
				TYPE: "COUNTER";
				NAME: string;
				OPT?: "RANDOM";
			}

			interface StatCore extends BaseNumeric {
				TYPE: "STAT_CORE";
				NAME: CoreStatStr;
				OPT?: "RANDOM";
			}

			interface StatBody extends BaseNumeric {
				TYPE: "STAT_BODY";
				NAME: BodyStatStr;
				OPT?: "RANDOM";
			}

			interface StatSkill extends BaseNumeric {
				TYPE: "STAT_SKILL";
				NAME: SkillStr;
				OPT?: "RANDOM";
			}

			interface FlagSet {
				TYPE: "FLAG_SET";
				NAME: string;
				VALUE: boolean;
			}

			interface Flag extends BaseNumeric {
				TYPE: "FLAG";
				NAME: string;
			}

			interface Tag extends BaseNumeric {
				TYPE: "TAG";
				NAME: string;
			}

			interface HasItem {
				TYPE: "HAS_ITEM";
				NAME: string;
			}

			interface TrackProgress {
				TYPE: "TRACK_PROGRESS";
				NAME: string;
				VALUE: number;
				CONDITION?: Condition;
			}

			type Any = NPCStat | NPCStat | Random100 | Counter | StatCore | StatBody | StatSkill | FlagSet | Flag | Tag | HasItem | TrackProgress;
		}
		//#endregion

		namespace Costs {
			interface BaseNumeric {
				VALUE: number;
			}

			interface Numeric<T> extends BaseNumeric {
				TYPE: T;
			}

			interface Stat extends Numeric<'STAT'> {
				NAME: CoreStatStr;
			}

			interface Skill extends Numeric<'SKILL'> {
				NAME: SkillStr;
			}

			interface Body extends Numeric<'BODY'> {
				NAME: BodyStatStr;
			}

			interface Money extends BaseNumeric {
				TYPE: "MONEY";
			}

			interface Tokens extends BaseNumeric {
				TYPE: "TOKENS";
			}

			interface Item {
				TYPE: "ITEM";
				NAME: string;
				VALUE: number;
			}

			interface Time {
				TYPE: "TIME";
				VALUE: number;
			}

			type Any = Stat | Skill | Body | Money | Tokens | Item | Time;
			type CostType = Any['TYPE'];
		}

		interface Task {
			/** Job or quest identifier */
			ID: string;
			TITLE: string;
			/** Id of the NPC who gives the task */
			GIVER: string;
			/**
			 *  Is job hidden until requirements are met.
			 * @default false
			*/
			HIDDEN?: boolean;
		}

		class ConditionalTextFragment {
			TEXT: string;
			[key: string]: any;
		}

		interface JobSceneContent {
			POST?: NonEmptyArray<Posts.Any>;
			TEXT?: string | NonEmptyArray<ConditionalTextFragment | string>;
		}

		interface StaticJobSceneData {
			ID: string;
			TRIGGERS?: NonEmptyArray<Triggers.Any>;
			TRIGGERS_ANY?: NonEmptyArray<Triggers.Any>;
			CHECKS?: NonEmptyArray<Checks.Any>;
		}

		type JobScene = StaticJobSceneData & RequireAtLeastOne<JobSceneContent>;

		interface Job extends Task {
			PAY?: number;
			TOKENS?: number;
			RATING: 1 | 2 | 3 | 4 | 5;
			PHASES: NonEmptyArray<DayPhase>;
			/** Minimal number of days until the job can be repeated, can be 0 which means the same day */
			DAYS: number;
			COST?: NonEmptyArray<Costs.Any>;
			REQUIREMENTS?: NonEmptyArray<Requirements.Any>;
			INTRO: string;
			START?: string;
			SCENES: NonEmptyArray<JobScene>;
			JOB_RESULTS?: NonEmptyArray<ConditionalTextFragment>;
			END?: string;
		}

		interface Quest extends Task {
			/** Flags that are required to trigger quest. */
			PRE?: NonEmptyArray<Requirements.Any>;
			/** Flags that are set when quest is completed. */
			POST?: NonEmptyArray<Posts.Any>;
			CHECKS?: NonEmptyArray<Requirements.Any>;
			ON_ACCEPT?: NonEmptyArray<Posts.Any>;
			ON_DAY_PASSED?: (this: App.Quest, player: App.Entity.Player) => void;
			REWARD?: NonEmptyArray<Posts.Any>;
			INTRO: string;
			MIDDLE: string;
			FINISH: string;
			JOURNAL_ENTRY: string;
			JOURNAL_COMPLETE: string;
		}
	}


	// #region Synergy
	interface SynergyElementChoice<T extends StatTypeStr> {
		TYPE: T;
		NAME: StatTypeStrMap[T];
		BONUS: number;
	}

	type BodySynergyElement = SynergyElementChoice<StatType.BODY>;

	type SynergyElement = SynergyElementChoice<"BODY"> |
		SynergyElementChoice<"SKILL"> |
		SynergyElementChoice<"STAT">;

	export type SkillSynergyData = Partial<Record<Skill | CoreStat, SynergyElement[]>>;

	//#endregion

	interface StatLevelingRecord {
		ADJECTIVE: string;
		COLOR: number;
	}

	interface MinimalStatLevelingData {
		[x: number]: StatLevelingRecord;
	}

	interface NoneStatLevelingData {
		NONE: {COST: 0, STEP: 0}
	}

	interface FixedStatLevelingRecord {
		COST: number;
		STEP: number;
	}

	interface FixedStatLevelingData {
		FIXED: FixedStatLevelingRecord;
	}

	interface ExplicitStatLevelingRecord extends StatLevelingRecord {
		COST: number;
		STEP: number;
	}

	interface ExplicitStatLevelingData {
		[x: number]: ExplicitStatLevelingRecord;
	}

	type LevelingCostFunction = (level: number) => number;

	export interface StatConfig {
		MIN: number;
		MAX: number;
		START: number;
		LEVELING_COST?: (level: number) => number;
		LEVELING: MinimalStatLevelingData | ExplicitStatLevelingData | FixedStatLevelingData | NoneStatLevelingData;
	}

	export interface BodyStatConfig extends StatConfig {
		CM_MIN: number;
		CM_MAX: number;
	}

	export interface SkillStatConfig extends StatConfig {
		ALTNAME?: string;
	}

	export interface CoreStatConfig extends StatConfig {
	}

	interface StatConfigMap {
		[StatType.STAT]: CoreStatConfig;
		[StatType.SKILL]: SkillStatConfig;
		[StatType.BODY]: BodyStatConfig;
	}

	interface StatConfigStrMap {
		'STAT': CoreStatConfig;
		'SKILL': SkillStatConfig;
		'BODY': BodyStatConfig;
	}

	//#region Items
	export enum ItemType {
		DRUGS = "DRUGS",
		FOOD = "FOOD",
		COSMETICS = "COSMETICS",
		MISC_CONSUMABLE = "MISC_CONSUMABLE",
		MISC_LOOT = "MISC_LOOT",
		CLOTHES = "CLOTHES",
		WEAPON = "WEAPON",
		QUEST = "QUEST",
		LOOT_BOX = "LOOT_BOX",
		REEL = "REEL"
	}

	export type ItemTypeStr = keyof typeof ItemType;
	export type ShopItemTypesStr = Exclude<ItemTypeStr, "QUEST">;
	export type EquipmentItemTypeStr = "CLOTHES" | "WEAPON";
	export type InventoryItemTypeStr = Exclude<ItemTypeStr, EquipmentItemTypeStr>;
	export type ConsumableItemTypeStr = Exclude<InventoryItemTypeStr, "QUEST" | "REEL">;
	//#endregion

	namespace Clothes {
		type CategoryStr = "Sexy Dancer" | "Gothic Lolita" | "Sissy Lolita" | "Daddy's Girl" | "Bimbo" | "Pirate Slut" | "Pet Girl" |
			"BDSM" | "Naughty Nun" | "High Class Whore" | "Slutty Lady" | "Ordinary";

	}
	namespace Whoring {
		type SexActStr = "HAND" | "ASS" | "BJ" | "TITS";
		type SexualBodyPart = "Ass" | "Bust" | "Face" | "Lips" | "Penis";
		type BonusSource = Clothes.CategoryStr | SexualBodyPart | SexualBodyPart;
		type BonusCategory = "Perversion" | "Femininity" | "Beauty";

		export interface WhoringLocationDesc {
			/** Shows in the UI as location. */
			DESC: string;
			/** Sex act weighted table. */
			WANTS: SexActStr[];
			/** Minimum payout rank. */
			MIN_PAY: number;
			/** Maximum payout rank. */
			MAX_PAY: number;
			/** Bonus payout, weighted table */
			BONUS: BonusSource[];
			BONUS_CATS: BonusCategory[];
			NAMES: string[];
			TITLE: string | null;
			RARES: string[];
			NPC_TAG: string;
			MARQUEE?: string;
		}
	}

	//#region Events

	type EventCheckFunc = (player: Entity.Player) => boolean;

	interface EventDesc {
		/** A unique ID for the event. */
		ID: string;
		/** The passage you are traversing from */
		FROM: string;
		FORCE?: boolean;
		/** Number of times the event can repeat, 0 means forever. */
		MAX_REPEAT: number;
		/** Minimum day the event shows up on */
		MIN_DAY: number;
		/** Maximum day the event shows up on, 0 means forever */
		MAX_DAY: number;
		/** Interval between repeats on this event. */
		COOL: number;
		/** Time phases the event is valid for */
		PHASE: DayPhase[];
		/** Override passage that the player is routed to. */
		PASSAGE: string;
		/** Condition function that is called to check if the event fires. */
		CHECK: EventCheckFunc;
	}
	//#endregion

	namespace Combat {
		interface Enemy {
			Name: string;
			Title: string;
			Health: number;
			MaxHealth: number;
			Energy: number;
			Attack: number;
			Defense: number;
			MaxStamina: number;
			Stamina: number;
			Speed: number;
			Moves: string;
			Gender: App.Gender;
			Portraits?: string[];
			Bust?: number;
			Ass?: number;
		}

		type MoveUnlockTest = (player: App.Combat.Combatant) => boolean;

		interface Move {
			Name: string;
			Description: string;
			Icon: string;
			Stamina: number;
			Combo: number;
			Speed: number;
			Damage: number;
			Unlock: MoveUnlockTest;
			Miss: string[][];
			Hit: string[][];
		}

		interface Style {
			Engine: App.Combat.Engines.EngineConstructor;
			moves: Record<string, Move>;
		}

		namespace EncounterLoots {
			interface Base {
				Chance: number;
				Min: number;
				Max: number;
			}

			interface Coins extends Base {
				Type: 'Coins';
			}

			interface Random extends Base {
				Type: 'Random';
			}

			interface Item extends Base {
				Type: ItemTypeStr;
				Tag: string;
			}

			type Any = Coins | Random | Item;
		}

		interface EncounterDesc {
			Enemies: string[],
			Fatal: boolean;
			WinPassage: string;
			LosePassage: string;
			Intro?: string;
			LootMessage?: string;
			Loot?: EncounterLoots.Any[];
			WinHandler?: () => void;
			LoseHandler?: () => void;
			FleeHandler?: () => void;
		}

		interface ClubEncounter {
			Title: string;
			WinsRequired: number;
			MaxWins: number;
			Encounter: string;
		}
	}

	interface ModDesc {
		level: number;
		encounter: string;
		name: string;
		symbol: string;
		color: string;
	}

	interface TarotCardDesc {
		Name: string;
		Css: string;
		Chat: string;
		Msg: string;
		Effects: string[];
	}

	namespace Travel {
		type ConditionCheckResult = boolean | string;
		type DynamicText = string | ((player: Entity.Player) => string);

		interface BasicConditionalDestination {
			/** The destination completely hidden from the player if false
			 * @default true
			 */
			available?: (player: Entity.Player) => boolean;
		}
		interface ConditionalDestination extends BasicConditionalDestination{
			destination: DynamicText;
			text?: DynamicText;

			enabled?: (player: Entity.Player) => boolean;
		}

		interface LockedDestinationWithNote extends BasicConditionalDestination{
			text: DynamicText;
			note: DynamicText;
		}

		type Destination = string | ConditionalDestination | LockedDestinationWithNote;

		export type Destinations = NonEmptyArray<Destination>;
	}
}
