// Relic parts - found in dungeons

//Common Parts
App.Data.MiscLoot["broken rune"] = {
	Name: "broken rune",
	ShortDesc: "<span class='item-common'>a broken rune</span>",
	LongDesc: "A small, flat oval piece of stone with part of a rune etched into it. It's broken now, but \
	perhaps there may still be some magic left in it.",
	Type: "MISC_LOOT",
	Value: 20,
	InMarket: false,
};

//Uncommon Parts
App.Data.MiscLoot["old arrowhead"] = {
	Name: "old arrowhead",
	ShortDesc: "<span class='item-uncommon'>an old arrowhead</span>",
	LongDesc: "This arrowhead is crafted from an unusual silvery metal. The most amazing thing about it is \
	that despite being neglected for so long, the edge and point are still sharp.",
	Type: "MISC_LOOT",
	Value: 40,
	InMarket: false,
};

//Rare Parts
App.Data.MiscLoot["glowing crystal"] = {
	Name: "glowing crystal",
	ShortDesc: "<span class='item-rare'>a faintly glowing crystal</span>",
	LongDesc: "This crystal gives off a faint, but cool white light. Upon closer inspection it has tiny intricate \
	glyphs carved onto its many face. Evidently, this is not a natural phenomina.",
	Type: "MISC_LOOT",
	Value: 80,
	InMarket: false,
};

//Legendary Parts
App.Data.MiscLoot["stone tablet"] = {
	Name: "stone tablet",
	ShortDesc: "<span class='item-legendary'>an ancient stone tablet</span>",
	LongDesc: "This ancient stone tablet is inscribed with incomprehensible glyphs and figures in a \
	language that must be long dead. Is it a diary? An alchemical formula? A pie recipe? You have \
	no idea!",
	Type: "MISC_LOOT",
	Value: 160,
	InMarket: false,
};
