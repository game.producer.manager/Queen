// Social Media Links
var RedditLink = "https://www.reddit.com/r/QueenOfTheSeas/";

//Sugarcube config
console.log("Sugarcube Config…");
Config.history.controls = false;
Config.history.maxStates = 1;
Config.ui.updateStoryElements = false; // we draw buttons ourselves
// and remove the ones provided by SugarCube
document.querySelector("div#ui-bar-body nav#menu[role='navigation']")?.remove();
Config.saves.version = App.Data.Game.Version;

console.log("Loading QoS Internal engines…");
// Player shim
setup.player = new App.Entity.Player();
// Jobs
setup.jobEngine = new App.JobEngine();
// Initialize Avatar Engine
App.Avatar = new App.Entity.AvatarEngine();
// Event Engine
setup.eventEngine = new App.EventEngine();
// Load gambling classes
setup.CoffinGame = new App.Gambling.Coffin();
// Load Combat Engine
setup.Combat = new App.Combat.CombatEngine();
// Load Spectator Engine
setup.Spectator = new App.Combat.SpectatorEngine();
// Load notification engine
setup.Notifications = new App.Notifications.Engine();
// Load Loot engine
setup.Loot = new App.Loot();
// Audio
setup.Audio = new App.Audio();
console.log("Finished loading QoS engines!");

// Funnel navigation to the event engine
Config.navigation.override = function (passageName) {
    if (App.EventHandlers.HasPlayerState() == false) return; //ignore these checks if there is no player state.
    return setup.eventEngine.CheckEvents(setup.player, State.passage, passageName);
};

Setting.addHeader("Display and UI");
Setting.addToggle("displayAvatar", {
    label: "Display the PC Avatar in mirrors and Portrait in UI",
    default: true,
    onInit: App.Avatar.SettingHandler,
    onChange: App.Avatar.SettingHandler
});

Setting.addToggle("displayNPC", {
    label: "Display NPC Avatars and Portraits in UI",
    default: true,
    onInit: App.Avatar.NPCSettingHandler,
    onChange: App.Avatar.NPCSettingHandler
});

Setting.addToggle("displayBodyScore", {
    label: "Display numerical body parameters",
    default: false,
    onChange: App.PR.handleDisplayBodyScoreChanged
});

Setting.addToggle("inlineItemDetails", {
    label    : "Inline detailed information in inventory and shops",
    desc     : "Toggles displaying additional information in the inventory and shop interfaces.",
    default  : false
});

// Setting for showing numbers along with the meters
Setting.addToggle("displayMeterNumber", {
	label: "Show numbers in (pseudo-)graphical meters",
	desc: "Controls printing number values for each meter bar in addition to the graphical representation",
	default: false,
	onInit: App.PR.handleMetersNumberValueSettingChanged,
	onChange: App.PR.handleMetersNumberValueSettingChanged
});

Setting.addList("theme", {
	label: "Theme",
	desc: "Color theme. //Reloading might be required//",
	list: ["dark", "light", "grey", "custom"],
	default: "dark",
	onInit: () => App.UI.loadTheme(settings.theme),
	onChange: () => App.UI.loadTheme(settings.theme)
});

Setting.addHeader("Gameplay")

Setting.addToggle("alternateControlForRogue", {
    label : "Alternate Control Scheme for Rogue-like elements",
    desc : "Use this if your device does not have a numpad. You may have to exit/enter Rogue-like again for it to take effect.",
    default: false
});

Setting.addToggle("autosaveAtSafePlaces", {
	label: "Automatic saving",
	desc: "Automatically save game at selected safe locations",
	default: false,
	onInit: App.PR.handleAutosaveChanged,
	onChange: App.PR.handleAutosaveChanged
});

Setting.addRange("bgmVolume", {
	label    : "BGM Volume",
	desc     : "Background music volume",
	min      : 0,
	max      : 10,
	step     : 1,
    onInit   : setup.Audio.VolumeInit.bind(setup.Audio),
	onChange : setup.Audio.VolumeAdjust.bind(setup.Audio),
    default: 2
});

Setting.addToggle("fastAnimations", {
    label: "Fast Animations",
    default: false
});

// Setting up a basic list control for the settings property 'units'
Setting.addList("units", {
    label   : "Unit system",
    desc    : "Choose the unit system.",
    list    : ["Imperial", "Metric"],
    default : "Imperial",
    onInit  : App.unitSystem.unitSettingChangedHandler,
    onChange: App.unitSystem.unitSettingChangedHandler
});


// SugarCube event handlers
App.EventHandlers.Init();

Config.saves.onLoad = App.EventHandlers.onLoad;
Config.saves.onSave = App.EventHandlers.onSave;
Config.saves.isAllowed = function() {
    return !tags().includes("no-saving");
};

// Setup some divs that will not get refreshed on passage navigation.
if ($('#hiddenCanvas').length == 0) {
	$("<canvas id='hiddenCanvas'></div>").appendTo(document.body);
}

// temp

App.UI.PassageRenderManager.instance.registerHandler(App.UI.rPostProcess);
