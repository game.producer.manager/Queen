App.registerPassageDisplayNames({
	GirlfriendRoom: (player) => player.GirlfriendName + "'s Room",
	GovernorStudy: (player) => App.Quest.IsCompleted(player, "DADDYSGIRL") ? "@@.state-girlinness;Daddy's room@@" : "Study",
	IslaFarm: "Farmlands",
	IslaHarbor: "The Docks",
	IslaMansion: "Governors Mansion",
	IslaStore: "General Store",
	IslaTavern: "Tavern",
	IslaTown: "Town Square",
	IslaVillageLane: "Village Lane",
});

App.registerTravelDestination({
	IslaHarbor: [
		{
			destination: "Deck",
			text: "Back to The Mermaid"
		},
		"IslaTown", "IslaTavern"
	],
	IslaTavern: ["IslaHarbor"],
	IslaTown: [
		"IslaHarbor", "IslaVillageLane",
		{
			destination: "IslaStore",
			enabled: (player) => player.Phase < 3
		},
		{
			destination: "IslaMansion",
			enabled: (player) => player.Phase < 3 || App.Quest.IsCompleted(player, "THEBACKDOOR")
		}
	],
	IslaVillageLane: ["IslaTown", "IslaFarm"],
	IslaFarm: [
		"IslaVillageLane",
		{
			destination: "",
			text: "It's too embarassing to go home",
			enabled: () => false
		}
	],
	IslaStore: ["IslaTown"],
	IslaMansion: ["IslaTown",
		{
			// guarded until night
			available: (player) => player.Phase < App.Phase.Night && !App.Quest.IsCompleted(player, "DADDYSGIRL"),
			text: (player) => player.GirlfriendName + "'s Room",
			note: (player) => `As you attempt to gain entry into @@.state-girlinness;${player.GirlfriendName}'s Room@@ the steward steps forward and blocks your way.

			@@.npc;Jarvis@@ says, <span class="npcText">Excuse me, but I can't allow you to enter any further than this. \
			The young miss isn't about but I dare say she wouldn't like you rifling through her things."</span>

			//Perhaps if there was some way to distract him…//`
		},
		{
			// guarded until night
			available: (player) => player.Phase < App.Phase.Night && !App.Quest.IsCompleted(player, "DADDYSGIRL"),
			text: "Study",
			note: `You move forward toward's the @@.location-name;Governor's Study@@, but the steward steps forward and blocks your way.

			@@.npc;Jarvis@@ says, <span class="npcText">"Sorry miss, but the Governor isn't taking any visitors now. \
			He's quite distraught over the disappearance of his daughter. Please come back some other time… or don't."</span>

			//How can you convince him to let you through without exposing your embarassing identity?//`
		},
		{
			available: (player) => player.Phase >= App.Phase.Night || App.Quest.IsCompleted(player, "DADDYSGIRL"),
			destination: "GovernorStudy"
		},
		{
			available: (player) => player.Phase >= App.Phase.Night || App.Quest.IsCompleted(player, "DADDYSGIRL"),
			destination: "GirlfriendRoom"
		}
	],
	GovernorStudy: ["IslaMansion"],
	GirlfriendRoom: [ "IslaMansion"]
});
