Macro.add("jobButton", {
	handler() {
		const job = this.args[0] as App.Job;
		const NPC = temporary()['MPC'] as App.Entity.NPC;
		const button = App.UI.appendNewElement('button', this.output, );
		if (job.Available(setup.player, NPC)) {
			button.textContent = "Accept";
			button.onclick = () => {
				App.UI.replace("JobUI", App.UI.pJobResults(setup.player, NPC, job));
			};
		} else if (job.OnCoolDown(setup.player)) {
			button.textContent = "Cooldown";
		} else {
			button.textContent = "Locked";
			$(this.output).wiki(job.ReqString(setup.player, NPC));
		}
	}
});

Macro.add("printJobResult", {
	handler() {
		$(this.output).wiki(App.UI.pJobResults(setup.player, temporary()['MPC'] as App.Entity.NPC, this.args[0] as App.Job))
	}
});
