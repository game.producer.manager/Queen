Macro.add("marketLink", {
	handler() {
		const SE = App.StoreEngine;
		const npc = setup.player.GetNPC(this.args[0] as string);
		if (SE.HasStore(npc) && SE.IsOpen(setup.player, npc) && setup.player.Phase < App.Phase.LateNight) {
			if (setup.player.CoreStats["Energy"] < 1) {
				App.UI.appendFormattedText(this.output, {text: 'Shop - Too tired.', style: 'state-disabled'});
			} else {
				const link = App.UI.passageLink("Shop", "Shop", () => {
					const ST = SE.OpenStore(setup.player, npc);
					ST.GenerateMarket();
					variables().MenuAction = npc;
					setup.player.AdjustStat("Energy", -1);
					setup.player.NextPhase(1);
					if (!tags().includes("custom-menu")) {
						variables().GameBookmark = passage();
					}
				});
				this.output.append(link);
				App.UI.appendFormattedText(this.output,
					' - [', {text: "Time 1", style: 'item-time'}, ' ', {text: "Energy 1", style: 'item-energy'}, ']'
				);
			}
		} else {
			App.UI.appendNewElement('span', this.output, 'Shop - closed', ['state-disabled']);
		}
	}
});
