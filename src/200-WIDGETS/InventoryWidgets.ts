namespace App.UI.Widgets {
	export namespace Inventory {
		class MessageViewer {
			#element: HTMLElement;
			constructor(element: HTMLElement) {
				this.#element = element;
			}

			showMessage(m: string) {
				replace(this.#element, m);
			}

			get element() {
				return this.#element;
			}
		}

		const warrobeItemComparer = (left: Items.Clothing, right: Items.Clothing): number => {
			if (left.Rank !== right.Rank) {
				return right.Rank - left.Rank; // higher ranked on top
			}
			return left.Name.localeCompare(right.Name);
		};

		//#region inventory tabs
		abstract class InventoryTab extends App.UI.Widgets.Tab {
			#tableElement: HTMLTableElement;
			#tableBody: HTMLTableSectionElement;
			#logger: MessageViewer;

			constructor(name: string, longName: string) {
				super(name, name.capitalizeFirstLetter(), `Inventory - ${longName}`);
				this.#tableElement = document.createElement('table');
				this.#tableElement.classList.add('inventory-table');
				this.#logger = new MessageViewer(document.createElement('div'));
			}

			/** @override */
			render() {
				const res = new DocumentFragment();
				const theader = appendNewElement('thead', this.#tableElement);
				const dummyRow = appendNewElement('tr', theader);
				const dummyCell = appendNewElement('th', dummyRow);
				dummyCell.colSpan = 999;
				dummyCell.style.fontWeight = "normal";
				dummyCell.append(this.#logger.element);
				// res.append(this.#logger.element);
				this.#tableBody = appendNewElement('tbody', this.#tableElement);
				res.append(this.#tableElement);
				this.update();
				return res;
			}

			public update() {
				replace(this.#tableBody, this.content());
			}

			protected static addCell(row: HTMLTableRowElement): HTMLTableCellElement {
				const res = document.createElement('td');
				row.append(res);
				return res;
			}

			protected static addButtonCell(row: HTMLTableRowElement, caption: string, handler: () => void): void {
				const cell = InventoryTab.addCell(row);
				const button = document.createElement('button');
				button.classList.add('inventory');
				button.textContent = caption;
				button.onclick = handler;
				cell.append(button);
			}

			protected static makeHeaderRow(cellWidths: string[]): HTMLTableRowElement {
				const res = document.createElement('tr');
				res.classList.add('section-header');
				for (const w of cellWidths) {
					const cell = document.createElement('td');
					cell.style.width = w;
					res.append(cell);
				}
				return res;
			}

			protected addExamineButtonCell(row: HTMLTableRowElement, item: Items.InventoryItem | Items.Clothing): void {
				InventoryTab.addButtonCell(row, "Examine", this.examineItem.bind(this, item));
			}

			protected addUseButtonCell(row: HTMLTableRowElement, item: Items.Consumable): void {
				InventoryTab.addButtonCell(row, "Use", this.useItem.bind(this, item));
			}

			private examineItem(item: Items.InventoryItem | Items.Clothing) {
				this.#logger.showMessage(item.Examine(setup.player));
			}

			private useItem(item: Items.Consumable) {
				this.logger.showMessage(setup.player.UseItem(item.Id));
				Passages.StoryCaption.updateGameScore();
				this.update();
			}

			protected get logger() {
				return this.#logger;
			}

			protected abstract content(): Node;
		}

		abstract class TossableInventoryTab extends InventoryTab {
			#types: string[];

			constructor(name: string, types: Data.ItemSubTypeStr[], longName: string) {
				super(name, longName);
				this.#types = types;
			}

			protected listItems(): Items.InventoryItem[] {
				return setup.player.GetItemByTypes(this.#types, true);
			}

			protected addTossButton(row: HTMLTableRowElement, item: Items.InventoryItem): void {
				InventoryTab.addButtonCell(row, "Toss", this.throwItem.bind(this, item));
			}

			private throwItem(item: Items.Consumable) {
				this.logger.showMessage(`You threw away ${setup.player.TakeItem(item.Id).Description}.`);
				this.update();
			}
		}

		export class ConsumableTab extends TossableInventoryTab {
			#favoritesEnabled: boolean;

			constructor(name: string, types: Data.ItemSubTypeStr[], longName: string) {
				super(name, types, longName);
				this.#favoritesEnabled = Quest.IsCompleted(setup.player, "BETTER_LOCKER");
			}

			/** @override */
			protected content() {
				const res = new DocumentFragment();
				const cellWidth: string[] = [];
				if (this.#favoritesEnabled) {
					cellWidth.push("1em");
				}
				// counts, toss, desc, use, examine
				cellWidth.push("4em", "5em", "100%", "5em", "5em");
				res.append(InventoryTab.makeHeaderRow(cellWidth));
				for (const item of this.listItems() as Items.Consumable[]) {
					const row = appendNewElement('tr', res);
					// favorite button
					if (this.#favoritesEnabled) {
						const favoriteCell = InventoryTab.addCell(row);
						favoriteCell.append(this.favoriteItemButton(item))
					}
					// count
					const countCell = InventoryTab.addCell(row);
					countCell.style.textAlign = "right";
					countCell.textContent = String(item.Charges());
					// throw button
					super.addTossButton(row, item);
					// desc
					const desc = InventoryTab.addCell(row);
					$(desc).wiki(PR.PrintItem(item, setup.player));

					super.addUseButtonCell(row, item);
					super.addExamineButtonCell(row, item);
				}
				return res;
			}

			private favoriteItemButton(item: Items.Consumable) {
				const res = document.createElement('a');
				res.innerHTML = PR.GetItemFavoriteIcon(item.IsFavorite());;
				res.onclick = () => this.favoriteItem(item);
				return res;
			}

			private favoriteItem(item: Items.Consumable) {
				this.logger.showMessage(`You placed ${item.Description} ${item.ToggleFavorite() ? "in a special box" : "back to the common locker space"}.`);
				this.update();
			}
		}

		export class MiscLootTab extends TossableInventoryTab {
			#enableToss: boolean;
			constructor(name: string, types: Data.ItemSubTypeStr[], longName: string, enableToss = false) {
				super(name, types, longName);
				this.#enableToss = enableToss;
			}

			content(): Node {
				const res = new DocumentFragment();
				const cellWidth: string[] = [];
				cellWidth.push("4em"); // counts
				if (this.#enableToss) {
					cellWidth.push("5em");
				}
				cellWidth.push("100%", "5em", "5em"); // desc, empty or use, examine
				res.append(InventoryTab.makeHeaderRow(cellWidth));
				for (const item of super.listItems() as Exclude<Items.InventoryItem, Items.Reel>[]) {
					const row = appendNewElement('tr', res);
					// count
					const countCell = InventoryTab.addCell(row);
					countCell.style.textAlign = "right";
					countCell.textContent = String(item.Charges());
					if (this.#enableToss) {
						super.addTossButton(row, item);
					}
					// desc
					const desc = InventoryTab.addCell(row);
					$(desc).wiki(PR.PrintItem(item, setup.player));
					if (item instanceof Items.Consumable) {
						super.addUseButtonCell(row, item);
					} else {
						// empty
						appendNewElement('td', row);
					}
					super.addExamineButtonCell(row, item);
				}
				return res;
			}
		}

		export class GearTab extends InventoryTab {
			constructor() {
				super("gear", "Clothing and Weapons");
			}

			content(): Node {
				const res = new DocumentFragment();
				res.append(InventoryTab.makeHeaderRow(["9em", "100%", "5em"]));
				const eq = setup.player.Equipment;
				const slots = Object.keys(eq) as Data.ClothingSlot[];
				for (const slot of slots) {
					let items: Items.Clothing[] = [];
					const eqInSlot = eq[slot];
					if (eqInSlot) {
						items.push(eqInSlot);
					}
					items.push(...setup.player.WardrobeItemsBySlot(slot));
					items.sort(warrobeItemComparer);
					if (items.length) {
						let slotPrinted = false;
						for (const item of items) {
							const row = document.createElement('tr');
							res.append(row);
							const slotCell = InventoryTab.addCell(row);
							if (!slotPrinted) {
								slotCell.classList.add('inventory-slot-name');
								slotCell.textContent = slot;
								slotPrinted = true;
							}
							const descCell = InventoryTab.addCell(row);
							if (item == eqInSlot) {
								descCell.append('(');
								if (item.IsLocked()) {
									appendNewElement('span', descCell, "locked", ['state-negative']);
								} else {
									appendNewElement('span', descCell, "worn", ['inventory-item-worn']);
								}
								descCell.append(') ');
							}
							$(descCell).wiki(PR.PrintItem(item, setup.player));
							this.addExamineButtonCell(row, item);
						}
					}
				}
				return res;
			}
		}

		export class CollarSwapTable extends InventoryTab {
			constructor() {
				super("collar", "Collar Swap");
			}

			content(): Node {
				const res = new DocumentFragment();
				const items = setup.player.WardrobeItemsBySlot('Neck');
				const eqItem = setup.player.Equipment['Neck'];
				items.sort(warrobeItemComparer);
				res.append(InventoryTab.makeHeaderRow(['5em', '100%', '5em']));
				if (eqItem) {
					const wornRow = document.createElement('tr');
					const wornCell = InventoryTab.addCell(wornRow);
					appendNewElement('span', wornCell, "Worn", ['inventory-item-worn']);
					const descCell = InventoryTab.addCell(wornRow);
					$(descCell).wiki(PR.PrintItem(eqItem, setup.player));
					this.addExamineButtonCell(wornRow, eqItem);
					res.append(wornRow);
				}
				for (const item of items) {
					const row = document.createElement('tr');
					res.append(row);
					InventoryTab.addButtonCell(row, "Swap", this.swapCollar.bind(this, item));
					const descCell = InventoryTab.addCell(row);
					$(descCell).wiki(PR.PrintItem(item, setup.player));
					this.addExamineButtonCell(row, item);
				}
				return res;
			}

			private swapCollar(collar: Items.Clothing): void {
				const current = setup.player.GetEquipmentInSlot("Neck");
				const currentIsLocked = current ? current.IsLocked() : false;
				setup.player.spendMoney(300, Entity.Spending.Jobs);
				setup.player.Wear(collar, currentIsLocked);
				this.update();
				// update avatar if shown
				if (settings.displayAvatar) {
					App.Avatar._DrawPortrait();
				}
			}
		}
		// #endregion
	}
}

Macro.add("Inventory", {
	skipArgs: true,
	handler() {
		const ins = App.UI.Widgets.Inventory;
		const tabs = new App.UI.Widgets.TabWidget();
		tabs.addTab(new ins.ConsumableTab("food", ['food'], "Food and Drink"));
		tabs.addTab(new ins.ConsumableTab("potions", ['potion'], "Potions and Drugs"));
		tabs.addTab(new ins.MiscLootTab("loot", ["QUEST", "LOOT_BOX", 'MISC_LOOT'], "Loot Items"));
		tabs.addTab(new ins.MiscLootTab("misc",
			["misc", "hair tool", "hair treatment", "basic makeup", "expensive makeup"], "Miscellaneous Items", true));
		tabs.addTab(new ins.GearTab());
		tabs.selectTabByIndex(0);

		this.output.append(tabs.element);
	}
});

Macro.add("DrawCollarTable", {
	skipArgs: true,
	handler() {
		this.output.append(new App.UI.Widgets.Inventory.CollarSwapTable().render());
	}
});
