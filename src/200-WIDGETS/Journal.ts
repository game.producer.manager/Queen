namespace App.UI.Widgets {

	//'active' 'JournalTabs' 'activeQuests' 'Active Quests'
	export function pQuestListTab(player: Entity.Player, questState: "active" | "completed"): string {
		const QL = App.Quest.List(questState === "active" ? ["active", "cancomplete"] : questState, player);
		if (QL.length === 0) {
			return "";
		}
		const entryType: QuestStage = questState === "active" ? "JOURNAL_ENTRY" : "JOURNAL_COMPLETE";

		if (questState === "completed") {
			QL.sort((l, r) => r.CompletedOn(player) - l.CompletedOn(player));
		} else {
			QL.sort((l, r) => r.AcceptedOn(player) - l.AcceptedOn(player));
		}

		let res = "";
		for (const q of QL) {
			if ((entryType == 'JOURNAL_ENTRY' && q.JournalEntry == "HIDDEN") ||
				(entryType == 'JOURNAL_COMPLETE' && q.JournalCompleteEntry == "HIDDEN")) {
				continue;
			}

			const NPC = player.GetNPC(q.Giver);
			const acceptDay = q.AcceptedOn(player);
			const completeDay = q.CompletedOn(player);
			if (questState === "active") {
				res += `<div style="float:left">Day ${acceptDay}`;
				if (player.Day !== acceptDay) {
					res += ` (${player.Day - acceptDay} days ago)`;
				}
				res += '</div>';
			} else {
				res += `<div style="float:left">Day ${completeDay} (`;
				res += (completeDay === acceptDay ? 'same day ' : `in ${completeDay - acceptDay} days`) + " after accepting)</div>";
			}
			res += `<div style="float:right">[ ${NPC.pName()} - ${q.Title()} ]</div> <br/>`;
			res += PR.pQuestDialog(q.ID(), entryType, player, NPC);
			if (questState == "active" && q.Checks !== undefined) {
				res += `<br/>${PR.pQuestRequirements(q.ID(), player)}<br/>`;
			}
			res += '<hr/>';
		}
		return res;
	}

	class JournalTab extends App.UI.Widgets.Tab {
		private func_: () => string;
		constructor(id: string, name: string, contentFunction: () => string) {
			super(id, name, `Journal - ${name}`);
			this.func_ = contentFunction;
		}
		render() {
			const res = new DocumentFragment();
			appendNewElement('hr', res);
			$(res).wiki(this.func_());
			return res;
		}
	}

	export class JournalTabActiveQuests extends JournalTab {
		constructor() {
			super("quests-active", "Active Quests", () => pQuestListTab(setup.player, "active"));
		}
	}

	export class JournalTabCompletedQuests extends JournalTab {
		constructor() {
			super("quests-completed", "Completed Quests", () => pQuestListTab(setup.player, "completed"));
		}
	}

	export class JournalTabGameStats extends JournalTab {
		constructor() {
			super("game-stats", "Game statistic", () => pGameStats(setup.player));
		}
	}

	export class JournalTabMemory extends App.UI.Widgets.Tab {
		constructor() {
			super("memory", "Memory", "Journal - Memory");
		}

		render() {
			const setStyle = (e: HTMLElement) => {
				e.style.color = "gold";
				e.style.fontWeight = "bold";
				e.style.fontSize = "larger";
				e.style.lineHeight = "40px";
			}
			const res = new DocumentFragment();
			appendNewElement('hr', res);
			const headerStyle = "color:gold;font-weight:bold;font-size:larger;line-height:40px;";
			setStyle(appendNewElement('span', res, "Clothing"));
			const clothingContainer = appendNewElement('div', res);
			setStyle(appendNewElement('span', res, "Food"));
			const foodContainer = appendNewElement('div', res);
			setStyle(appendNewElement('span', res, "Drugs and Potions"));
			const drugsContainer = appendNewElement('div', res);
			PR.PrintMemory(clothingContainer, foodContainer, drugsContainer);
			return res;
		}
	}
}

Macro.add("Journal", {
	skipArgs: true,
	handler() {
		const tabs = new App.UI.Widgets.TabWidget();
		tabs.addTab(new App.UI.Widgets.JournalTabActiveQuests());
		tabs.addTab(new App.UI.Widgets.JournalTabCompletedQuests());
		tabs.addTab(new App.UI.Widgets.JournalTabGameStats());
		tabs.addTab(new App.UI.Widgets.JournalTabMemory());
		tabs.selectTabByIndex(0);

		this.output.append(tabs.element);
	}
});
