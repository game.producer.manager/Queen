function questDialog(quest: App.Quest, npcId: string): Node {
	const res = new DocumentFragment();
	const npc = setup.player.GetNPC(npcId);
	const qState = quest.state(setup.player, npc);
	const scenes = quest.PlayScenes(setup.player, npc);
	const end = quest.PrintEnd(setup.player, npc);
	for (const s of scenes) {
		$(res).wiki(s.Print());
		App.UI.appendNewElement('br', res);
	}
	if (end !== "") {
		$(res).wiki(end);
	}

	if (qState === "available") {
		const acceptButton = App.UI.appendNewElement('button', res, "Accept", ['cmd-button']);
		acceptButton.onclick = () => {
			quest.CompleteScenes(setup.player);
			App.UI.replace("#QuestUI", questList(npcId));
		};
		res.append('  ');
		const declineButton = App.UI.appendNewElement('button', res, "Decline", ['cmd-button']);
		declineButton.onclick = () => {
			App.UI.replace("#QuestUI", questList(npcId));
		};
	} else if (qState === "active") {
		const okayButton = App.UI.appendNewElement('button', res, "Okay, fine", ['cmd-button']);
		okayButton.onclick = () => {
			App.UI.replace("#QuestUI", questList(npcId));
		};
	} else { // "cancomplete"
		const rewards = App.PR.pTaskRewards(quest);
		if (rewards.length) {
			App.UI.appendNewElement('span', res, "Quest Rewards", ['quest-state-cancomplete']);
			$(res).wiki(rewards.join('\n') + '\n');
		}
		const completeButton = App.UI.appendNewElement('button', res, "Complete Quest", ['cmd-button']);
		completeButton.onclick = () => {
			App.PR.SetTaskRewardChoices(quest);
			quest.CompleteScenes(setup.player);
			if (App.Quest.GetFlag(setup.player, "GAME_WON") == 1) {
				Engine.play("GameWon");
			}
			Engine.play(variables().GameBookmark);
		};
	}

	return res
}

function questList(npcId: string): Node {
	const res = new DocumentFragment();
	const npc = setup.player.GetNPC(npcId);
	const ql = App.Quest.List("any", setup.player, npcId);
	$(res).wiki(`You approach ${npc.pName()} to ask if there are any 'special tasks' that need to be done.`);
	for (const q of ql) {
		const qDiv = App.UI.appendNewElement('div', res);
		const qState = q.state(setup.player, npc);
		if (qState === "completed") {
			App.UI.appendNewElement('span', qDiv, q.Title(), ['state-disabled']);
			App.UI.appendNewElement('span', qDiv, " (COMPLETED)", ['state-neutral']);
		} else {
			const questLink = App.UI.appendNewElement('a', qDiv, q.Title());
			questLink.onclick = () => {
				App.UI.replace("#QuestUI", questDialog(q, npcId));
			};
			if (qState === "active") {
				App.UI.appendNewElement('span', qDiv, " (IN PROGRESS)", ['item-time']);
			} else if (qState === "cancomplete") {
				App.UI.appendNewElement('span', qDiv, " (IN PROGRESS)", ['quest-state-cancomplete']);
			}
		}
	}
	$(res).wiki(App.UI.pInteractLinkStrip());
	return res;
}

Macro.add("questList", {
	handler() {
		this.output.append(questList(variables().MenuAction as string));
	}
});
