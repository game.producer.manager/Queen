Macro.add("DrawAvatar", {
	skipArgs: true,
	handler() {
		const elementId = 'avatarUI';
		App.UI.appendNewElement('div', this.output).id = elementId;
		App.Avatar.DrawCanvas(elementId, 800, 360);
	}
});
