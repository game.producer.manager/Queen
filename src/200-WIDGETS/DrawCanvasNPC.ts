Macro.add("DrawCanvasNPC", {
	handler() {
		const elementId = 'npcUI';
		const div = App.UI.appendNewElement('div', this.output);
		div.id = elementId;
		div.style.display = 'inline-block';
		App.Avatar.DrawCanvasNPC(this.args[0], elementId, 560, 266);
	}
});
