namespace App.UI.Widgets {
	export function storeInventory(player: Entity.Player, npc: Entity.NPC): HTMLTableElement {
		const store = App.StoreEngine.OpenStore(player, npc);

		const restockingText = store.OwnerMood() >= 80 ?
			(store.DaysUntilRestocking() > 0 ? `Restocking in ${store.DaysUntilRestocking()} days` : "Fresh supplies arrived this morning!")
			: null;

		const missingItemsForQuests = Quest.allMissingItems(player);

		const inventoryTable = makeElement('table', undefined, ['store-inventory-table']);
		const colgroup = appendNewElement('colgroup', inventoryTable);
		appendNewElement('col', colgroup, undefined, ['name']);
		appendNewElement('col', colgroup, undefined, ['quantity']);
		appendNewElement('col', colgroup, undefined, ['price']);
		appendNewElement('col', colgroup, undefined, ['buy']);
		appendNewElement('col', colgroup, undefined, ['examine']);

		const tHeader = appendNewElement('thead', inventoryTable);
		const header = appendNewElement('tr', tHeader);
		const caption = appendNewElement('td', header);
		caption.colSpan = 3;
		caption.style.textAlign = "center";
		const shopName = appendNewElement('span', caption, store.GetName(), ['shopName']);
		if (restockingText) {
			if (settings.inlineItemDetails) {
				appendNewElement('td', header, restockingText).colSpan = 2;
			} else {
				appendNewElement('span', shopName, restockingText, ['tooltip']);
			}
		}

		const makeStoreBuyButtons = (item: Entity.StoreInventoryItem) => {
			const buyButton = makeElement('button', "Buy", ['store-button']);
			buyButton.onclick = () => {
				store.BuyItems(item, 1);
				Engine.play("Shop"); // FIXME
			};
			let noBuyAllButton = false;
			if (item.QTY < 1) {
				buyButton.classList.add('disabled-store-button');
				noBuyAllButton = true;
			}
			if (player.MaxItemCapacity(item)) {
				buyButton.classList.add('disabled-store-button');
				buyButton.textContent = "Full";
				noBuyAllButton = true;
			}
			if (item.TYPE === "CLOTHES") { // no "buy all" button
				if (player.OwnsWardrobeItem(item)) {
					buyButton.textContent = "Owned";
					buyButton.classList.add('disabled-store-button');
					noBuyAllButton = true;
				}
			}

			if (item.QTY < 2 || player.Money < store.GetPrice(item) * 2) {
				noBuyAllButton = true;
			}

			if (noBuyAllButton) {
				return buyButton;
			}

			const buyAllButton = makeElement('button', "Buy all", ['store-button']);
			buyAllButton.onclick = () => {
				store.BuyItems(item);
				Engine.play("Shop"); // FIXME
			}

			const res = new DocumentFragment();
			res.append(buyButton);
			res.append(" ");
			res.append(buyAllButton);
			return res;
		};

		const makeStoreExamineButton = (item: Entity.StoreInventoryItem) => {
			const button = makeElement('button', "Examine", ['store-button']);
			button.onclick = () => {
				UI.replace('#storeExamine', store.PrintItemLong(item));
			}
			return button;
		};

		const appendSection = (container: HTMLTableSectionElement, name: string, styleName: string, inv: Entity.StoreInventoryItem[]): void => {
			const headerRow = appendNewElement('tr', container, undefined, ['section-header']);
			const nameCell = appendNewElement('td', headerRow, undefined, ['name']);
			appendNewElement('span', nameCell, name, [styleName]);
			appendNewElement('td', headerRow, "Quantity", ['quantity']);
			appendNewElement('td', headerRow, "Price", ['price']);
			appendNewElement('td', headerRow, undefined, ['buy']);
			appendNewElement('td', headerRow, undefined, ['examine']);

			if (inv.length === 0) {
				const row = appendNewElement('tr', container);
				const note = appendNewElement('td', row);
				note.colSpan = 5;
				appendNewElement('span', note, "Nothing for sale!", ['attention']);
				return;
			}

			for (const itm of inv) {
				const row = appendNewElement('tr', container);
				const n = appendNewElement('td', row);
				if (missingItemsForQuests.hasOwnProperty(Items.MakeId(itm.TYPE, itm.TAG))) {
					appendNewElement('span', n, "(!) ", ['item-required-for-quest']);
				}
				$(n).wiki(store.PrintItem(itm));
				const q = appendNewElement('td', row, `${itm.QTY}`);
				q.style.textAlign = 'center';
				const p = appendNewElement('td', row);
				p.style.textAlign = 'right';
				$(p).wiki(`${store.GetPrice(itm)}`);
				const b = appendNewElement('td', row);
				b.style.textAlign = 'left';
				b.append(makeStoreBuyButtons(itm));
				const e = appendNewElement('td', row);
				e.style.textAlign = 'center';
				e.append(makeStoreExamineButton(itm));
			}
		}

		const inventory = appendNewElement('tbody', inventoryTable);
		appendSection(inventory, "General Items", 'item-category-general', store.GetCommonInventory());
		appendSection(inventory, "Rare Items", 'item-category-rare', store.GetRareInventory());
		const footer = appendNewElement('tfoot', inventoryTable);
		const fRow = appendNewElement('tr', footer);
		const cExamine = appendNewElement('td', fRow);
		cExamine.colSpan = 5;
		const examine = appendNewElement('span', cExamine);
		examine.id = "storeExamine";

		return inventoryTable;
	}
}

Macro.add("storeInventory", {
	handler() {
		this.output.append(App.UI.Widgets.storeInventory(setup.player, variables().MenuAction as App.Entity.NPC));
	}
});
