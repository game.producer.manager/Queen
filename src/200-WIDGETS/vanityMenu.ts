namespace App.UI.Widgets {

	interface BeatyResource {
		name: string;
		short: string;
		id: string;
	}
	abstract class VanityTable {
		#player: Entity.Player;
		#groupName: string;
		#table: HTMLTableElement;
		#tbody: HTMLTableSectionElement;
		#resources: BeatyResource[];
		#resourceAvailable: number[];
		#foot: HTMLTableSectionElement | null;

		constructor(player: Entity.Player, groupName: string, name: string, resources: BeatyResource[]) {
			this.#player = player;
			this.#groupName = groupName;
			this.#resources = resources;

			this.#table = App.UI.makeElement('table', undefined, ['vanity-table']);
			const header = App.UI.appendNewElement('thead', this.#table);
			header.append(this.vanityTableHeader(name, resources.map(r => r.short)));
			this.#tbody = App.UI.appendNewElement('tbody', this.#table);
			this.#resourceAvailable = this.#resources.map(r => setup.player.GetItemCharges(r.id));
			this.#foot = null;
		}

		get player() {
			return this.#player;
		}

		render(): Node {
			this.update();
			return this.#table;
		}

		protected get availableResources() {
			return this.#resourceAvailable;
		}

		protected vanityTableHeader(name: string, resourceNames: string[]): HTMLTableRowElement {
			const res = document.createElement('tr');
			const radioCol = App.UI.appendNewElement('th', res);
			radioCol.style.width = "1.5em";
			radioCol.style.textAlign = "center";
			const nameCol = App.UI.appendNewElement('th', res, name);
			nameCol.style.width = "100%";
			for (const rn of resourceNames) {
				const col = App.UI.appendNewElement('th', res, rn);
				col.style.fontSize = "smaller";
			}
			return res;
		}

		protected makeVanityTableRow(value: string, checked: boolean, enabled: boolean, desc: string,
			resources: number[]): HTMLTableRowElement {
			const row = document.createElement('tr');
			const cRadio = App.UI.appendNewElement('td', row);
			if (enabled) {
				const inp = App.UI.appendNewElement('input', cRadio);
				inp.type = "radio";
				inp.checked = checked;
				inp.name = this.#groupName;
				inp.value = value;
			}

			const cDesc = App.UI.appendNewElement('td', row);
			$(cDesc).wiki(desc);

			for (let i = 0; i < resources.length; ++i) {
				const rn = resources[i];
				const avail = this.availableResources[i];
				if (rn === 0) {
					App.UI.appendNewElement('td', row, '-');
				} else {
					App.UI.appendNewElement('td', row, String(rn), avail >= rn ? []: ['state-disabled']);
				}
			}
			return row;
		}

		update(): void {
			this.#resourceAvailable = this.#resources.map(r => setup.player.GetItemCharges(r.id));
			App.UI.replace(this.#tbody, this.renderBody());
			if (this.#foot) {
				this.#foot.remove()
				this.#foot = null;
			}
			const foot = this.renderFooter();
			if (foot.length) {
				this.#foot = document.createElement('tfoot');
				this.#foot.append(...foot);
				this.#table.append(this.#foot);
			}
		}

		get selectedStyle(): string {
			const selector = `input[name="${this.#groupName}"]:checked`
			const elem = document.querySelector(selector);
			if (elem) {
				return (elem as HTMLInputElement).value;
			}
			throw "Could not find selected input element";
		}

		protected abstract renderBody(): DocumentFragment;
		protected abstract renderFooter(): HTMLTableRowElement[];
	}

	export class VanityHairTable extends VanityTable {
		constructor(player: Entity.Player) {
			super(player, "radiogroup-hairstyle", "Hair Style", [
				{id: "hair tool", name: "Accessories", short: "Acc."},
				{id: "hair treatment", name: "Products", short: "Prod."}
			]);
		}

		renderBody(): DocumentFragment {
			const res = new DocumentFragment();

			const wig = this.player.GetEquipmentInSlot("Wig");
			const wigs = this.player.WardrobeItemsBySlot("Wig");
			if (wig) {
				res.append(this.makeWigRow(wig, true));
			}
			wigs.forEach(wig => {res.append(this.makeWigRow(wig, false));});

			const hair = App.Data.Lists.HairStyles;
			const hL = this.player.GetStat("BODY", "Hair");

			const styleName = (hs: Data.HairStyleDesc, enabled: boolean): string => {
				if (!enabled) {
					if (hL < hs.MIN) {
						return `@@.state-negative;⇓Hair@@ ${hs.SHORT}`;
					}
					if (hL > hs.MAX) {
						return `@@.state-negative;⇑Hair@@ ${hs.SHORT}`;
					}
					return `@@.state-disabled;${hs.SHORT}@@`;
				}
				return `${App.PR.ColorizeString(hs.STYLE, "⇑Style")} ${hs.SHORT}`;
			};

			for (const hs of hair) {
				let enabled = hs.RESOURCE1 <= this.availableResources[0] && hs.RESOURCE2 <= this.availableResources[1]
					&& hL <= hs.MAX && hL >= hs.MIN;
				const selected = !wig && this.player.LastUsedHair === hs.SHORT;
				res.append(this.makeVanityTableRow(hs.NAME, selected, enabled, styleName(hs, enabled), [hs.RESOURCE1, hs.RESOURCE2]));
			}

			return res;
		}

		renderFooter(): HTMLTableRowElement[] {
			const row = document.createElement('tr');
			App.UI.appendNewElement('td', row);
			const text = App.UI.appendNewElement('td', row, "Accessories & Products Owned");
			text.style.textAlign = "right";
			App.UI.appendNewElement('td', row, String(this.availableResources[0]));
			App.UI.appendNewElement('td', row, String(this.availableResources[1]));
			return [row];
		}

		private makeWigRow(wig: App.Items.Clothing, worn: boolean): HTMLTableRowElement {
			return this.makeVanityTableRow(wig.Id, worn, true,
				`${App.PR.ColorizeString(wig.HairBonus(), "⇑Wig")} ${wig.Description}`, [0, 0]);
		}
	}

	export class VanityMakeupTable extends VanityTable {
		constructor(player: Entity.Player) {
			super(player, "makeup", "Makeup Styles", [
				{id: "basic makeup", name: "Basic makeup", short: "Bas."},
				{id: "expensive makeup", name: "Expensive makeup", short: "Exp."}
			]);
		}

		renderBody(): DocumentFragment {
			const res = new DocumentFragment();
			for (const ms of App.Data.Lists.MakeupStyles) {
				const enabled = ms.RESOURCE1 < this.availableResources[0] && ms.RESOURCE2 < this.availableResources[1];
				res.append(this.makeVanityTableRow(ms.NAME, this.player.MakeupStyle === ms.SHORT, enabled,
					`${App.PR.ColorizeString(ms.STYLE, "⇑Style")} ${ms.SHORT}`, [ms.RESOURCE1, ms.RESOURCE2]));
			}
			return res;
		}

		renderFooter(): HTMLTableRowElement[] {
			const row = document.createElement('tr');
			App.UI.appendNewElement('td', row);
			const text = App.UI.appendNewElement('td', row, "Basic & Expensive Makeup Owned");
			text.style.textAlign = "right";
			App.UI.appendNewElement('td', row, String(this.availableResources[0]));
			App.UI.appendNewElement('td', row, String(this.availableResources[1]));
			return [row];
		}
	}
}

Macro.add("vanityMenu", {
	skipArgs: true,
	handler() {
		const res = new DocumentFragment();

		const hairStyles = new App.UI.Widgets.VanityHairTable(setup.player);
		const makeups = new App.UI.Widgets.VanityMakeupTable(setup.player);

		const tables = App.UI.appendNewElement('div', res);
		tables.style.marginBottom = "1ex";
		tables.style.marginTop = "1ex";
		tables.style.display = "flex";
		tables.append(hairStyles.render(), makeups.render());

		const actionsDiv = App.UI.appendNewElement('div', res);
		const energy = setup.player.GetStat("STAT", "Energy");
		App.UI.appendFormattedText(actionsDiv, {text: "Actions", style: "action-general"}, ": ");
		const actions: (string|Node)[] = [];

		const optionalAction = (enabled: boolean, handler: () => void, ...text: (string|App.UI.FormattedFragment)[]) => {
			if (enabled) {
				const link = document.createElement('a');
				App.UI.appendFormattedText(link, ...text);
				link.onclick = handler;
				actions.push(link);
			} else {
				const span = document.createElement('span');
				App.UI.appendFormattedText(span, ...text);
				actions.push(span);
				span.classList.add('state-disabled');
			}
		};

		optionalAction(energy > 1, () => {
			setup.player.AdjustStat("Energy", -1);
			setup.player.DoStyling(hairStyles.selectedStyle, makeups.selectedStyle);
			Engine.play(passage());
		}, "Do Makeup and Hair ", {text: "(1 Energy)", style: "item-energy"});

		optionalAction(energy > 1 && setup.player.MakeupStyle !== "plain faced", () => {
			setup.player.MakeupStyle = "plain faced";
			setup.player.MakeupBonus = 0;
			setup.player.AdjustStat("Energy", -1);
			Engine.play(passage());
		}, "Remove Makeup ", {text: "(1 Energy)", style: "item-energy"});

		const us = App.unitSystem;
		const P = App.PR;
		const hairLength = setup.player.GetStat("BODY", "Hair");
		const trimHair = document.createElement('a');
		const newHairLength = Math.max(5, hairLength - 5);
		optionalAction(energy > 1 && hairLength > 5, () => {
			setup.player.Clothing.TakeOffSlot("Wig");
			setup.player.AdjustStat('Energy', -1);
			setup.player.AdjustBody('Hair', newHairLength - hairLength);
			Engine.play(passage());
		}, `Trim hair ${us.lengthString(P.statValueToCM(hairLength, 'Hair'), false)} → ${us.lengthString(P.statValueToCM(newHairLength, 'Hair'), false)}`, {text: " (1 Energy)", style: "item-energy"});

		actionsDiv.append(App.UI.generateLinksStrip(actions));
		this.output.append(res);
	}
});
