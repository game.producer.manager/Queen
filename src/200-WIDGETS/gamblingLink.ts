Macro.add("gamblingLink", {
	handler() {
		const gameName = `Play ${this.args[0]}`;
		const link = this.args[1] as string;
		const player = setup.player;
		const energy = player.GetStat('STAT', 'Energy');
		if (energy < 1 || player.Phase >= App.Phase.LateNight) {
			App.UI.appendNewElement('span', this.output, `[${gameName}]`, ['state-disabled']);
		} else {
			this.output.append(App.UI.passageLink(gameName, link, () => {
				player.AdjustStat("Energy", -1);
				player.NextPhase(1);
				if (!tags().includes('custom-menu')) {
					variables().GameBookmark = passage();
				}
			}));
		}
	}
});
