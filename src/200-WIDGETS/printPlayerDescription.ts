Macro.add("printPlayerDescription", {
	skipArgs: true,
	handler() {
		const mirrorContainer = document.createElement('div');
		mirrorContainer.id = 'mirrorContainer';
		if (settings.displayAvatar) {
			const elementId = 'avatarUI';
			App.UI.appendNewElement('div', this.output).id = elementId;
			App.Avatar.DrawCanvas(elementId, 800, 360);
		}

		const textContainer = settings.displayAvatar ? App.UI.appendNewElement('div', mirrorContainer) : mirrorContainer;
		const P = App.PR;
		const pl = setup.player;

		$(textContainer).wiki(`<p>You are ${P.pHeight(pl)} and ${P.pFitness(pl)}</p>\
		<p>You have a ${P.pFace(pl)} ${P.pHair(pl)} ${P.pEyes(pl)} You have ${P.pLips(pl)}</p>\
		<p>${P.pBust(pl)} ${pl.GetStat("BODY", "Bust") > 7 ? P.pBustFirmness(pl) : ""} ${pl.GetStat("BODY", "Lactation") > 0 ? P.pLactation(pl) : ""}</p>\
		<p>${P.pAss(pl)} ${P.pHips(pl)} ${P.pWaist(pl)} and ${P.pFigure(pl)}.</p>\
		<p>You have ${P.pPenis(pl)} and ${P.pBalls(pl)} between your legs. ${pl.IsFuta ? P.pFutaStatus(pl) : ""}</p>\

		<p>You consider your natural beauty to be ${P.pBeauty(pl)}.\nThe fetish appeal of your body is ${P.pFetish(pl)}.</p>`);

		this.output.append(mirrorContainer);
	}
});
