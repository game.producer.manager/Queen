Macro.add("printStyleDescription", {
	skipArgs: true,
	handler() {
		const elem = document.createElement('span');
		elem.id = "StyleDesc";
		const qe = $(elem);
		const p = App.PR;
		qe.wiki(`${p.pHair(setup.player)} ${p.pMakeup(setup.player)}. The style and fashion of your clothing is ${p.pClothing(setup.player)}. Your hygiene and styling is ${App.PR.pStyle(setup.player)}.`);
		this.output.append(elem);
	}
});
