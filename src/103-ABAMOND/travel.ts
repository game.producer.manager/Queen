namespace App {
	registerPassageDisplayNames({
		AB_BlackSwamp: "The Black Swamp",
		AB_ForestClearing: "Forest Clearing",
		AB_ForestTrail: "Forest Trail",
		AB_GlutezonGate: () => visited("AB_GlutezonGate") ? "Glutezon Village Front Gate" : "Valley Below",
		AB_GlutezonQueen: () => visited("AB_GlutezonQueen") ? "Glutezon Queen's Chamber" : "Long Hall",
		AB_GlutezonShop: "Village Trader",
		AB_GlutezonVillage: "Village Yard",
		AB_HillsideTrail: "Hillside Trail",
		AB_MamazonCroplands: "Back Gate",
		AB_MamazonGate: () => visited("AB_MamazonGate") ? "Mamazon Village Front Gate" : "Small Village",
		AB_MamazonQueen: () => visited("AB_MamazonQueen") ? "Mamazon Queen's Chamber" : "Stone Hall",
		AB_MamazonShop: "Village Trader",
		AB_MamazonVillage: "Village Green",
		AB_MysteriousLanding: "Mysterious Landing",
		AB_OvergrownCairn: "Overgrown Cairn",
		AB_Plateau: "Hillside Plateau",
		AB_Shanty: "Small Shanty",
		AB_SwampClearing: "Bobola Clearing",
		AB_SwampRiver: "Swamp River",
		Abamond: "Lonely Pier",
		FarBeach: "Far Beach",
		HiddenCove: "Hidden Cove",
		Jungle: "Into the Jungle",
		OceanCave: "Explore the caves",
		Ruins: "Into the Ruins",
		TreasureRoom: "Treasure Room",
	});

	registerTravelDestination({
		Abamond: [
			{
				text: "Back to The Mermaid",
				destination: "Deck"
			},
			"FarBeach", "Jungle"
		],
		FarBeach: ["Abamond", "OceanCave"],
		Jungle: ["Abamond", "AB_BlackSwamp",
			{
				available: (player) => JobEngine.instance.HasJobFlag(player, "COVE_FOUND"),
				destination: "HiddenCove"
			},
			{
				available: (player) => JobEngine.instance.HasJobFlag(player, "RUINS_FOUND"),
				destination: "Ruins"
			}
		],
		AB_BlackSwamp: ["Jungle", "AB_SwampRiver"],
		AB_SwampRiver: ["AB_BlackSwamp", "AB_MysteriousLanding",
			{
				available: (player) => Quest.IsCompleted(player, "BOBOLA_SAP_2") && !Quest.IsCompleted(player, "BOBOLA_SAP_1") &&
					!player.GetItemByName('bucket of bobola sap'),
				destination : "AB_SwampClearing"
			}
		],
		AB_SwampClearing: ["AB_SwampRiver"],
		AB_MysteriousLanding: ["AB_SwampRiver", "AB_Shanty"],
		AB_Shanty: ["AB_MysteriousLanding"],
		OceanCave: ["FarBeach"], // TODO the explore link
		HiddenCove: ["Jungle", "AB_Plateau"],
		Ruins: ["Jungle", "TreasureRoom"],
		TreasureRoom: ["Ruins"],
		AB_Plateau: ["HiddenCove", "AB_HillsideTrail", "AB_ForestTrail"],
		AB_HillsideTrail: ["AB_Plateau", "AB_OvergrownCairn"],
		AB_ForestTrail: ["AB_Plateau", "AB_ForestClearing"],
		AB_OvergrownCairn: ["AB_HillsideTrail", "AB_GlutezonGate"],
		AB_ForestClearing: ["AB_ForestTrail", "AB_MamazonGate"],
		AB_GlutezonGate: ["AB_OvergrownCairn",
		{
			available: (player) => Quest.IsCompleted(player, 'MAMAZON_CHAMP'),
			text: "Enter Village",
			note: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

			<span class='npcText'>"Halt! We won't allow any allies of the <span class="npc'>Mamazons</span> \
			to enter our peaceful village! Begone thot!"</span>

			It seems like you've burned your bridges with the @@.npc;Mamazons@@.`
		},
		{
			available: (player) => !Quest.IsCompleted(player, 'MAMAZON_CHAMP') && player.GetStat('BODY', 'Ass') < 40,
			text: "Enter Village",
			note: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

			<span class='npcText'>"Halt! Begone from <span class="npc">Glutezon</span> lands you pancake arsed whore, \
			before we kill you for being a <span class="npc">Mamazon</span> spy!"</span>

			It seems like you've encountered the fabled @@.npc;Glutezons@@, a tribe of jungle women with immense \
			asses. Perhaps if you were equally as endowed they might allow you to enter their village…`
		},
		{
			available: (player) => !Quest.IsCompleted(player, 'MAMAZON_CHAMP') && player.GetStat('BODY', 'Ass') >= 40,
			text: "Enter Village",
			destination: "AB_GlutezonVillage"
		}
		],
		AB_MamazonGate: ["AB_ForestClearing",
			{
				available: (player) => Quest.IsCompleted(player, 'GLUTEZON_CHAMP'),
				text: "Enter Village",
				note: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npcText'>"Halt! We won't allow any allies of the <span class="npc'>Glutezons</span> \
				to enter our peaceful village! Begone thot!"</span>

				It seems like you've burned your bridges with the @@.npc;Mamazons@@.`
			},
			{
				available: (player) => !Quest.IsCompleted(player, 'GLUTEZON_CHAMP') && player.GetStat('BODY', 'Bust') < 40,
				text: "Enter Village",
				note: `You try to approach the gate but are stopped by an arrow plunging into the ground in front of you!

				<span class='npcText'>"Halt! Begone from <span class="npc">Mamazon</span> lands you flat chested harlot, \
				before we kill you for being a <span class="npc">Glutezon</span> spy!"</span>

				It seems like you've encountered the fabled @@.npc;Mamazons@@, a tribe of jungle women with immense \
				breasts. Perhaps if you were equally as endowed they might allow you to enter their village…`
			},
			{
				available: (player) => !Quest.IsCompleted(player, 'GLUTEZON_CHAMP') && player.GetStat('BODY', 'Bust') >= 40,
				text: "Enter Village",
				destination: "AB_MamazonVillage"
			}
		],
		AB_GlutezonVillage: ["AB_GlutezonGate", "AB_GlutezonQueen", "AB_GlutezonShop"],
		AB_GlutezonQueen: ["AB_GlutezonVillage"],
		AB_GlutezonShop: ["AB_GlutezonVillage"],
		AB_MamazonVillage: ["AB_MamazonGate", "AB_MamazonQueen", "AB_MamazonShop", "AB_MamazonCroplands"],
		AB_MamazonQueen: ["AB_MamazonVillage"],
		AB_MamazonShop: ["AB_MamazonVillage"],
		AB_MamazonCroplands: ["AB_MamazonVillage"],
		CombatAbamondGenericWin: [{text: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericLose: [{text: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericLoseNonFatal: [{text: "Continue", destination: "CaveRogueLikeGUI"}],
		CombatAbamondGenericFlee: [{text: "Continue", destination: "CaveRogueLikeGUI"}]
	});
}
