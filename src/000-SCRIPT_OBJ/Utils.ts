namespace App.Utils {
	/**
	 * replaces special HTML characters with their '&xxx' forms
	 */
	export function escapeHtml(text: string) {
		const map = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;'
		};
		return text.replace(/[&<>"']/g, m => map[m]);
	}

	export function gaussianPair(mu: number, sigma: number): [number, number] {
		let u = Math.random();
		let v = Math.random();
		let s = u * u + v * v;
		while (s === 0 || s > 1) {
			u = Math.random();
			v = Math.random();
			s = u * u + v * v;
		}

		const f = Math.sqrt(-2 * Math.log(s) / s) * sigma;
		return [u * s + mu, v * f + mu];
	}
}

function hasOwnProperty<X extends object, Y extends PropertyKey> (obj: X, prop: Y): obj is X & Record<Y, unknown> {
  return obj.hasOwnProperty(prop)
}

namespace App {
	export function isDefined<T>(v: T): v is NonNullable <T> {
		return !_.isNil(v);
	}

	export function assertIsDefined<T>(val: T): asserts val is NonNullable<T> {
		if (!isDefined(val)) {
			throw `Expected 'val' to be defined, but received ${val}`;
		}
	}

	export function valueOrDefault<T>(v: T, def: NonNullable<T>): NonNullable<T> {
		return isDefined(v) ? v : def;
	}

	export function registerTravelDestination(destinations: Record<string, Data.Travel.Destinations>) {
		return Object.assign(App.Data.Travel.destinations, destinations);
	}

	export function registerPassageDisplayNames(names: Record<string, Data.Travel.DynamicText>) {
		return Object.assign(App.Data.Travel.passageDisplayNames, names);
	}

	interface PassageLink {
		link: string;
		text?: string;
		setter?: string;
	}

	const sc2LinkSimple = /\[\[([^\|^(?:\->)]+)(?:\||(?:\->))?(?:([^\]]+))?\](?:\[(.+)\])?\]/;

	export function parseSCPassageLink(s: string): PassageLink {
		const m = sc2LinkSimple.exec(s);
		if (!m) {
			throw `Link parsing error: '${s} does not look like a passage link to me`;
		}
		return {link: m[1], text: m[2], setter: m[3]};
	}
}

Object.defineProperty(Array.prototype, 'randomElement', {
	configurable : true,
	writable     : true,

	value() {
		const length = this.length >>> 0;

		if (length === 0) {
			throw "randomElement: Array is empty";
		}

		return this.random();
	}
});

Object.defineProperty(Math, 'mean', {
	configurable : true,
	writable     : true,

	value(v0: number, ...vn: number[]): number {
		let sum = v0;
		for (const v of vn) {
			sum += v;
		}
		return sum / (vn.length + 1);
	}
});

interface Math {
	/** Returns average value of the arguments */
	mean(v0: number, ...vn: number[]): number;
}

interface Array<T> {
	randomElement(): T;
}
