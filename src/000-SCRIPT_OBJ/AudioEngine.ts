namespace App {
	/**
	 * Simple class for managing the playing of music files and other sound effects in the game.
	 * @class AudioEngine
	 */
	export class Audio {
		#bgmTracks: TwineSugarCube.AudioList | null = null;
		#init = false;

		Init():void {
			if (this.#init == true) return;

			this.#bgmTracks = SimpleAudio.lists.get("bgm");
			assertIsDefined(this.#bgmTracks);
			this.#bgmTracks.loop(true);
			this.#init = true;
		}

		get BGMOn():boolean {return this.#bgmTracks?.isPlaying() ?? false;}
		get BGMOff(): boolean {return !this.BGMOn;}

		/**
		 * Stop or pause the current playing bgm (also any other ones)
		 */
		private _StopAllBGM(pauseBGM = false) {
			if (pauseBGM) {
				this.#bgmTracks?.pause();
			} else {
				this.#bgmTracks?.stop();
			}
		}

		isPlaying(): boolean {
			return this.#bgmTracks?.isPlaying() ?? false;
		}

		/**
		 * Turn the BGM system on.
		 */
		StartBGM(resumeBGM: boolean): void {
			if (this.isPlaying() == false && this.#init == true) {
				console.log("AudioEngine: BGM turned on.");
				if (!resumeBGM) {
					this.#bgmTracks?.stop();
				}
				this.#bgmTracks?.play();
			}
		}

		/**
		 * Turn the BGM system off.
		 */
		StopBGM(pauseBGM: boolean): void {
			console.log("AudioEngine: BGM turned off.");
			this._StopAllBGM(pauseBGM);
		}

		/**
		 * Called from Settings when game loads.
		 */
		VolumeInit(): void {
			try {
				this.VolumeAdjust();
			} catch (ex) {
				console.log("Waiting for audio system to be initialized…");
				setTimeout(this.VolumeInit.bind(this), 1000);
			}
		}

		/**
		 * Called from Settings when the user changes the volume slider
		 */
		VolumeAdjust(): void {
			const vol = settings.bgmVolume / 10; // turn into range between 0 and 1
			if (vol == 0 && this.BGMOn) {
				this.StopBGM(true);
			} else if (vol != 0 && this.BGMOff) {
				this.StartBGM(true);
			}

			this.#bgmTracks?.volume(vol);
		}

		Transition(_Passage: string): void {
			if (settings.bgmVolume / 10 != 0 && this.BGMOff) {
				this.StartBGM(true); // resume playing any bgm files on transition.
			}
		}
	}
}
