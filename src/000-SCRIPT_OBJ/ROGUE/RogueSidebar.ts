namespace App.Rogue {
	interface SideBarOptions {
		display: ROT.Display;
		position: XY;
		size: XY;
	}
	export class Sidebar {
		#data: string[] = [];
		#options: SideBarOptions = {
			display: null,
			position: new XY(),
			size: new XY()
		};

		constructor(options: SideBarOptions) {
			Object.assign(this.#options, options);
		}

		clear() {
			this.#data = [];
		}

		write(text: string) {
			this.#data.push(text);
		};

		Title(text: string) {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			d.drawText(pos.x, pos.y, text, size.x);
		}

		Level(text: string) {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			d.drawText(pos.x, pos.y + 1, text, size.x);
		}

		Torches(text: string) {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			d.drawText(pos.x, pos.y + 3, text, size.x);
		}

		Shovels(text: string) {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			d.drawText(pos.x, pos.y + 4, text, size.x);
		}

		Help() {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			d.drawText(pos.x, pos.y + 6, "COMMANDS", size.x);

			if (settings.alternateControlForRogue == true) {
				d.drawText(pos.x, pos.y + 7, "Move: WASD", size.x);
				d.drawText(pos.x, pos.y + 8, "(diagonal): QEZX", size.x);
				d.drawText(pos.x, pos.y + 9, "Torch = T", size.x);
				d.drawText(pos.x, pos.y + 10, "Dig = R", size.x);
				d.drawText(pos.x, pos.y + 11, "Up/Down = R", size.x);
			} else {
				d.drawText(pos.x, pos.y + 7, "Move = NUMPAD", size.x);
				d.drawText(pos.x, pos.y + 8, "Torch = /", size.x);
				d.drawText(pos.x, pos.y + 9, "Dig = NUMPAD5", size.x);
				d.drawText(pos.x, pos.y + 10, "Up/Down = NUMPAD5", size.x);
			}
		}

		flush() {
			var o = this.#options;
			var d = o.display;
			var pos = o.position;
			var size = o.size;

			/* clear */
			for (var i = 0; i < size.x; i++) {
				for (var j = 0; j < size.y; j++) {
					d.draw(pos.x + i, pos.y + j, null, null, null);
				}
			}

			var text = this.#data.join(" ");
			d.drawText(pos.x, pos.y, text, size.x);
		};

	}
}
