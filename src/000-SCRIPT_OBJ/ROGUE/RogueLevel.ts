
namespace App.Rogue {
	export class Level {
		#depth: number;

		/* FIXME data structure for storing entities */
		#beings: Map<number, Entity> = new Map<number, Being>();

		/* FIXME map data */
		#size = new App.Rogue.XY(80, 40);
		#map:InstanceType<typeof ROT.Map.Cellular>;
		#walls: Map<number, Entity> = new Map<number, Entity>();
		#freeCells: Map<number, Entity> = new Map<number, Entity>();
		#borders: Map<number, Entity> = new Map<number, Entity>();
		#entrance: number; // encoded XY
		#exit: number; // encoded XY
		#treasure:Map<number, XY> = new Map<number, XY>(); // FIXME get rid of this
		#empty = new Rogue.Entity({ch: null, fg: null, bg: null});
		#wall = new Rogue.Entity({ch: null, fg: null, bg: "#888"});

		constructor(depth: number) {
			this.#depth = depth;
			this.#beings = new Map<number, Being>();
		}

		getSize(): XY {
			return this.#size;
		}

		get freeCells() {
			return this.#freeCells;
		}

		get depth(): number {
			return this.#depth;
		}

		get beings() {
			return this.#beings;
		}

		/**
		 * Create a Cave map.
		 * 0 = free space, 1 = wall;
		 * @param w width (x axis)
		 * @param h height (y axis)
		 */

		generateMap(w: number, h: number) {
			this.#map = new ROT.Map.Cellular(w, h);
			this.#map.randomize(0.6 - this.#depth / 1000);

			/* make a few generations */
			for (let i = 0; i < 5; i++) {
				this.#map.create();
			}

			this.#map.connect((x, y, c) => {this._createCB(x, y, c);}, 1, (from, to) => {this._connectCB(from, to);});
		}

		private _createCB(x: number, y: number, content: number) {
			const xy = encodeXy(x, y);
			if (content === 1) {
				this.#freeCells.set(xy, new App.Rogue.Entity({ch: null, fg: null, bg: null}));
				//this._freeCells[xy].setPosition(xy, this);
			} else {
				this.#walls.set(xy, this.#wall);
			}
		}

		private _connectCB(_from:[number, number], _to: [number, number]) {
			//console.log("from="+from+", to="+to);
			//this._cells[from] = new App.Rogue.Entity( { ch:"X", fg:"#777", bg:null });
			//this._cells[to] = new App.Rogue.Entity( { ch:"O", fg:"#777", bg:null });
		}

		getWalls() {return this.#walls;}
		getFreeCells() {return this.#freeCells;}
		getBorders() {return this.#borders;}

		/**
		 * Fill in the borders of the map.
		 * Generate an entrance and save it.
		 * @param w width, x axis
		 * @param h height, y axis
		 */
		fillBorders(w: number, h: number) {
			for (var i = 0; i < w; i++) {
				// Top of map.
				let xyNum = encodeXy(i, 0);
				this.#freeCells.delete(xyNum);
				this.#walls.delete(xyNum);
				this.#borders.set(xyNum, this.#wall);

				// Bottom of map.
				xyNum = encodeXy(i, h - 1);
				this.#freeCells.delete(xyNum);
				this.#walls.delete(xyNum);
				this.#borders.set(xyNum, this.#wall);
			}

			for (i = 0; i < h; i++) {
				// Left of map.
				let xyNum = encodeXy(0, i);
				this.#freeCells.delete(xyNum);
				this.#walls.delete(xyNum);
				this.#borders.set(xyNum, this.#wall);

				// Right of map.
				xyNum = encodeXy(w - 1, i);
				this.#freeCells.delete(xyNum);
				this.#walls.delete(xyNum);
				this.#borders.set(xyNum, this.#wall);
			}
		}

		cellsAtRadius(x: number, y: number, r: number, free: boolean): XY[] {
			var cells: XY[] = [];

			const startY = Math.max(0, (y - r));
			const endY = Math.min(this.#size.y - 1, (y + r));
			const startX = Math.max(0, x - r);
			const endX = Math.min(this.#size.x - 1, x + r);

			// console.log("startY="+startY+",endY="+endY);

			for (var row = startY; row <= endY; row++) {
				const dy = row - y;
				var rangeX = r - Math.abs(row - y);
				//console.log("rangeX"+rangeX+",startX="+startX+",endX="+endX);

				for (var col = startX; col <= endX; col++) {
					const dx = col - x;
					if (Math.sqrt(dx * dx + dy * dy) > r) continue;
					var xy = new XY(col, row);
					if (free && this.#freeCells.has(xyToNumber(xy))) {
						cells.push(xy);
					} else if (!free) {
						cells.push(xy);
					}
				}
			}

			return cells;
		}

		cellsOutsideRadius(x: number, y: number, r: number, free: boolean) {
			// Get inclusion cells
			const inside = this.cellsAtRadius(x, y, r, free).map(xy => xyToNumber(xy));

			var cells = [...this.#freeCells.keys()].filter(key => !inside.includes(key));
			if (!free) cells = cells.concat([...this.#walls.keys()].filter(key => !inside.includes(key)));

			return cells;
		}

		/**
		 * Each  level can have up to 10 treasure spots on it. The chance is basically 1% per depth for each spot.
		 */
		genTreasure() {
			var count = 0;
			for (var i = 0; i < 10; i++) {
				if ((Math.round(Math.random() * 100) + 1) <= 100) count++;
			}

			if (count < 1) return; // noop
			var cells = [...this.#freeCells.keys()];
			//Remove stairs from array
			cells = cells.filter(c => c !== this.#exit && c !== this.#entrance);
			for (i = 0; i < count; i++) {
				var treasure = cells.randomElement();
				this.#treasure.set(treasure, numberToXY(treasure));
				const trEntity = new App.Rogue.Entity({ch: '*', fg: '#A52A2A', bg: null});
				trEntity.Type = 'dig_spot';
				this.#freeCells.set(treasure, trEntity);
			}
		}

		genMonsters() {
			console.log("genMonsters() called");
			// Select generic monster encounters.
			var tLevel = this.#depth;
			console.log("tLevel=" + tLevel);
			var Mobs = App.Data.Abamond.Mobs['COMMON'].filter(o =>
				o.level >= (tLevel - 10) &&
				o.level <= tLevel);

			console.log(Mobs);
			var MobPool = ((tLevel * 5) * Math.random())

			while (MobPool > 0 && Mobs.length >= 1) {
				let m = Mobs[Math.floor(Math.random() * Mobs.length)];
				MobPool -= m.level;
				this._placeMonster(m);
			}

		};

		// Place the monster we just generated
		private _placeMonster(Mob) {
			var entity = new Being({ch: Mob.symbol, fg: Mob.color, bg: null});
			entity.Name = Mob.name;
			entity.Encounter = Mob.encounter;
			var cells = [...this.#freeCells.keys()].filter(c => {
				return c !== this.#exit && c !== this.#entrance && !this.#beings.has(c);
			});
			var place = cells.randomElement();
			var XY = numberToXY(place);
			console.log("Placing mob:" + Mob.name + " at " + XY.toString());
			this.setEntity(entity, XY);

		}

		isTreasure(xy: XY) {
			return this.#treasure.has(xyToNumber(xy));
		}

		/**
		 * Real gross hack for now, need to fix with future refactor
		 */
		digAt(xy: XY) {
			const xyNum = xyToNumber(xy);
			var nothing = (50 - this.#depth);
			var loot: Data.LootItem;

			this.#treasure.delete(xyNum);
			this.#freeCells.delete(xyNum);

			this.#freeCells.set(xyNum, new Entity({ch: null, fg: null, bg: null}));

			// no loot
			if (nothing > 0 && (nothing > Math.random() * 100)) {
				Engine.instance.textBuffer.write("You find nothing!");
				return;
			}

			// Hack for Quest(s) {
			if (setup.player.QuestFlags.hasOwnProperty("FINDING_YOUR_BALLS_2")
				&& setup.player.QuestFlags["FINDING_YOUR_BALLS_2"] == "ACTIVE"
				&& (typeof setup.player.GetItemByName("rare ore") === 'undefined')
				&& (Math.random() * 100 < this.#depth)) {
				setup.player.AddItem("QUEST", "rare ore", 1);
				Engine.instance.textBuffer.write("You find: a rare ore!");
				return;
			}

			var coins = Math.ceil(1 + (Math.random() * (this.#depth * 5)));
			if (Math.random() * 100 > 40) {
				// Money
				App.Entity.Player.instance.earnMoney(coins, App.Entity.Income.Loot);
				Engine.instance.textBuffer.write("You find " + coins + " coins!");
				App.PR.RefreshTwineMoney();
			} else {

				if (coins > 400) { // legendary
					loot = App.PR.GetRandomListItem(App.Data.Loot["DUNGEON_LEGENDARY"]);
				} else if (coins > 300) { // rare
					loot = App.PR.GetRandomListItem(App.Data.Loot["DUNGEON_RARE"]);
				} else if (coins > 150) { // uncommon
					loot = App.PR.GetRandomListItem(App.Data.Loot["DUNGEON_UNCOMMON"]);
				} else { // common
					loot = App.PR.GetRandomListItem(App.Data.Loot["DUNGEON_COMMON"]);
				}

				var count = (Math.max(loot.min, (Math.round(Math.random() * loot.max))));
				setup.player.AddItem(loot.category, loot.tag, count);
				var name = setup.player.GetItemByName(loot.tag);
				if (typeof name !== 'undefined' && name != null) {
					Engine.instance.textBuffer.write("You find: " + name.Name + "!");
				} else {
					Engine.instance.textBuffer.write("You find: " + loot.tag + "(bug)!");
				}

			}

		}

		/**
		 * Pick a random free cell and plop down the staircase up/out
		 */
		genEntrance() {
			const entrance = [...this.#freeCells.keys()].randomElement();
			this.#entrance = entrance;
			const entranceObj = new App.Rogue.Entity({ch: 'O', fg: '#3f3', bg: null});
			entranceObj.Type = 'stairs_up';
			this.#freeCells.set(entrance, entranceObj);
		}

		/**
		 * Pick a random free cell minimum distance of 30 spaces away
		 */
		genExit() {
			if (this.#depth >= 100) return;

			if (typeof this.#entrance === 'undefined') this.genEntrance();
			const entranceXY = numberToXY(this.#entrance);
			var cells = this.cellsOutsideRadius(entranceXY.x, entranceXY.y, 30, true);
			var exit = cells.randomElement();
			this.#exit = clone(exit);
			const exitObj = new App.Rogue.Entity(({ch: 'X', fg: '#1ABC9C', bg: null}));
			exitObj.Type = 'stairs_down';
			this.#freeCells.set(exit, exitObj);
		}

		getEntrance() {return this.#entrance;}
		get entranceXY() {return numberToXY(this.#entrance);}
		getExit() {return this.#exit};
		get exitXY() {return numberToXY(this.#exit);}

		removeBeing(entity: Entity) {
			if (entity.Level == this) {
				var key = entity.XY;
				this.#beings.delete(xyToNumber(key));
				if (Engine.instance.level == this) {
					Engine.instance.draw(key);
				}
			}
		}

		setEntity(entity: Entity, xy: XY) {
			/* FIXME remove from old position, draw */
			if (entity.Level == this) {
				var oldXY = entity.XY;
				this.#beings.delete(xyToNumber(oldXY));
				if (Engine.instance.level == this) {Engine.instance.draw(oldXY);}
			}

			entity.setPosition(xy, this); /* propagate position data to the entity itself */

			/* FIXME set new position, draw */
			this.#beings.set(xyToNumber(xy), entity);
			if (Engine.instance.level == this) {
				Engine.instance.draw(xy);
				//Engine.instance.textBuffer.write(`An entity moves to ${xy.x},${xy.y}.`);
			}
		}

		getEntityAt(xy: XY) {
			const xyNum = xyToNumber(xy);
			return this.#beings.get(xyNum) ?? this.#walls.get(xyNum) ?? this.#freeCells.get(xyNum) ?? this.#borders.get(xyNum);
		}

		getBeings() {
			/* FIXME list of all beings */
			return this.#beings;
		}
	}
}
