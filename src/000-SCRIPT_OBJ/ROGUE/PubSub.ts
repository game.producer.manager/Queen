;(function() {
    var _subscribers = {};

    globalThis.publish = function(message, publisher, data) {
        var subscribers = _subscribers[message] || [];
        subscribers.forEach(function(subscriber) {
            subscriber.handleMessage(message, publisher, data);
        });
    }

    globalThis.subscribe = function(message, subscriber) {
        if (!(message in _subscribers)) {
            _subscribers[message] = [];
        }
        _subscribers[message].push(subscriber);
    },

	globalThis.unsubscribe = function(message, subscriber) {
            var index = _subscribers[message].indexOf(subscriber);
            _subscribers[message].splice(index, 1);
        }
})();
