namespace App.Rogue {
	export interface EntityVisual {
		ch: string | null;
		fg: string | null;
		bg: string | null;
	}

// Base Entity Class - dig sites, traps, stairs, etc.
	export class Entity {
		#xy: XY = new XY(0, 0);
		#level: Level | null;
		#type: string | null = null;
		#name: string | null = null;
		#encounter: string| null = null;
		#visual: EntityVisual;

		constructor(visual:EntityVisual) {
			this.#visual = visual;
			this.#level = null;
		}

		get Type() {return this.#type;}
		set Type(t) {this.#type = t;}
		get Visual() {return this.#visual;}
		set Visual(v) {this.#visual = v;}
		get XY() {return this.#xy;}
		set XY(xy) {this.#xy = xy;}
		get Level() {return this.#level;}
		set Level(l) {this.#level = l;}
		get Name() {return this.#name;}
		set Name(n) {this.#name = n;}
		get Encounter() {return this.#encounter;}
		set Encounter(e) {this.#encounter = e;}

		setPosition(xy: XY, level: Level) {
			this.#xy = xy;
			this.#level = level;
			return this;
		}
	}

	// Living creatures
	export class Being extends Entity implements ROT.SpeedActor {
		#speed: number;
		#hp: number;
		#awake: boolean;

		constructor(visual: EntityVisual) {
			super(visual);
			this.#speed = 100;
			this.#hp = 10;
			this.#awake = false;

			Engine.instance.scheduler.add(this, true); // Add newly created creatures to scheduler

		}

		get Speed() {return this.#speed;}
		set Speed(s) {this.#speed = s;}
		get Awake() {return this.#awake;}
		set Awake(s) {
			if (s == true && !this.#awake) {
				Engine.instance.textBuffer.write(this.Name + " notices you!");
			}
			this.#awake = s;
		}

		// Used by ROT engine.
		getSpeed() {
			return this.#speed;
		}

		damage(d: number) {
			this.#hp -= d;
			if (this.#hp <= 0) {
				this.die();
			}
		}

		die() {
			Engine.instance.scheduler.remove(this);
			this.Level.removeBeing(this);
		}

		act() {
			console.log("RogueBeing: act() called");
			//Check if I need to wakeup.
			if (this.XY.dist(App.Rogue.Engine.instance.player.XY) <= 10) {
				this.Awake = true;
			} else {
				this.Awake = false; // go to sleep if the player is too far away
			}

			if (this.Awake == true) { // Chase the player.
				var x = Engine.instance.player.XY.x;
				var y = Engine.instance.player.XY.y;
				var that = this;
				var passableCallback = (x: number, y: number) => {
					const k = encodeXy(x, y);
					return (Engine.instance.level.getBeings().get(k) == that)
						|| (Engine.instance.level.freeCells.has(k))
						&& ((Engine.instance.level.getBeings().get(k) instanceof App.Rogue.Being) == false); // obstacle free
				}
				var astar = new ROT.Path.AStar(x, y, passableCallback, {topology: 4});

				var path: number[][] = [];
				var pathCallback = (x: number, y: number) => {
					path.push([x, y]);
				}

				astar.compute(this.XY.x, this.XY.y, pathCallback);
				// Now move.
				path.shift(); // remove mobs position
				if (path.length == 1) {
					Engine.instance.scheduler.remove(this);
					this.Level.removeBeing(this);
					Engine.instance.engine.lock();

					setup.Combat.InitializeScene({flee: 30, fleePassage: "CombatAbamondGenericFlee"});
					setup.Combat.LoadEncounter(this.Encounter as string);
					SugarCube.Engine.play("Combat");
					// Switch to combat mode here.
				} else if (path.length > 0) {
					x = path[0][0];
					y = path[0][1];
					var newXY = new App.Rogue.XY(x, y);
					this.Level.setEntity(this, newXY);
				}

			}
		}
	}

	// The livingist creature - the player
	export class Player extends Being {
		#lightLevel: number;
		#lightDuration: number;
		#keys: Record<number, number>;
		#akeys: Record<number, number>;

		constructor(visual: EntityVisual) {
			super(visual);

			this.#lightLevel = 1;
			this.#lightDuration = 0;

			this.#keys = {};
			this.#akeys = {};
			//Directions

			//North
			this.#keys[ROT.KEYS.VK_NUMPAD8] = 0;
			this.#keys[ROT.KEYS.VK_UP] = 0; // Arrow Key
			this.#akeys[ROT.KEYS.VK_W] = 0;
			//North East
			this.#keys[ROT.KEYS.VK_NUMPAD9] = 1;
			this.#akeys[ROT.KEYS.VK_E] = 1;
			// East
			this.#keys[ROT.KEYS.VK_NUMPAD6] = 2;
			this.#keys[ROT.KEYS.VK_RIGHT] = 2; // Arrow Key
			this.#akeys[ROT.KEYS.VK_D] = 2;
			// South East
			this.#keys[ROT.KEYS.VK_NUMPAD3] = 3;
			this.#akeys[ROT.KEYS.VK_X] = 3;
			// South
			this.#keys[ROT.KEYS.VK_NUMPAD2] = 4;
			this.#keys[ROT.KEYS.VK_DOWN] = 4; // Arrow Key
			this.#akeys[ROT.KEYS.VK_S] = 4;
			// South West
			this.#keys[ROT.KEYS.VK_NUMPAD1] = 5;
			this.#akeys[ROT.KEYS.VK_Z] = 5;
			// West
			this.#keys[ROT.KEYS.VK_NUMPAD4] = 6;
			this.#keys[ROT.KEYS.VK_LEFT] = 6; // Arrow Key
			this.#akeys[ROT.KEYS.VK_A] = 6;
			// North West
			this.#keys[ROT.KEYS.VK_NUMPAD7] = 7;
			this.#akeys[ROT.KEYS.VK_Q] = 7;

			// Reserved
			this.#keys[ROT.KEYS.VK_PERIOD] = -1;
			this.#keys[ROT.KEYS.VK_CLEAR] = -1;

			// Ascend / Descend / Dig
			this.#keys[ROT.KEYS.VK_NUMPAD5] = -1;
			this.#akeys[ROT.KEYS.VK_R] = -1;

			// Use Torch
			this.#keys[ROT.KEYS.VK_DIVIDE] = -1;
			this.#keys[ROT.KEYS.VK_SLASH] = -1;
			this.#akeys[ROT.KEYS.VK_T] = -1;
		}

		get LightLevel() {return this.#lightLevel;}
		set LightLevel(l) {this.#lightLevel = l;}
		get LightDuration() {return this.#lightDuration;}
		set LightDuration(d) {this.#lightDuration = d;}

		get Torches() {
			var torch = setup.player.GetItemByName("torch");
			return typeof torch === 'undefined' ? 0 : torch.Charges();
		}

		get Shovels() {
			var shovel = setup.player.GetItemByName("shovel");
			return typeof shovel === 'undefined' ? 0 : shovel.Charges();
		}

		act() {
			console.log("Player:act() called");
			this._printEntityAtLocationMessage();
			Engine.instance.textBuffer.flush();
			Engine.instance.engine.lock();
			$(document).on("keydown", this.handleEvent.bind(this));
		}

		die() {
			Engine.instance.scheduler.remove(this);
			Engine.instance.over();
		}

		private _printEntityAtLocationMessage() {
			if (this.Level.freeCells.hasOwnProperty(this.XY.toString()) != true) return;
			var entity = this.Level.freeCells.get(xyToNumber(this.XY));
			if (typeof entity === 'undefined') return;

			var keyCmd = SugarCube.settings.alternateControlForRogue == true ? "'r'" : 'NUMPAD5';

			switch (entity.Type) {
				case null:
					break;
				case 'stairs_up':
					if (this.Level.depth > 1) {
						Engine.instance.textBuffer.write("Press " + keyCmd + " to ascend a level.");
					} else {
						Engine.instance.textBuffer.write("Press " + keyCmd + " to exit.");
					}
					break;
				case 'stairs_down':
					Engine.instance.textBuffer.write("Press " + keyCmd + " to descend a level.");
					break;
				case 'dig_spot':
					Engine.instance.textBuffer.write("Press " + keyCmd + " to dig here");
					break;
			}
		}

		handleEvent(e: JQuery.KeyDownEvent) {

			var keyHandled = this._handleKey(e.keyCode);

			if (keyHandled) {
				$(document).off("keydown");
				Engine.instance.engine.unlock();
			}
		}

		_handleKey(code: number) {

			console.debug("code=" + code);

			var keys = SugarCube.settings.alternateControlForRogue == true ? this.#akeys : this.#keys;

			if (code in keys) {
				//Engine.instance.textBuffer.clear();

				// Traverse staircase
				if ((code == ROT.KEYS.VK_NUMPAD5) || (code == ROT.KEYS.VK_R)) {

					// Handle Up / Down
					if (this.XY.is(this.Level.entranceXY)) {
						if (setup.player.GetStat("STAT", "Energy") < 1) {
							Engine.instance.textBuffer.write("You are too tired to climb up.");
							return 1;
						}
						setup.player.AdjustStat("Energy", -1);
						PR.RefreshTwineMeter(CoreStat.Energy);
						Engine.instance.Ascend();
						return true;
					} else if (this.XY.is(this.Level.exitXY)) {
						if (setup.player.GetStat("STAT", "Energy") < 1) {
							Engine.instance.textBuffer.write("You are too tired to climb down.");
							return 1;
						}
						setup.player.AdjustStat("Energy", -1);
						PR.RefreshTwineMeter(CoreStat.Energy);
						Engine.instance.Descend();
						return true;
					} else if (this.Level.isTreasure(this.XY)) {

						if (this.Shovels > 0) {
							Engine.instance.textBuffer.write("You start digging…");
							this.Level.digAt(this.XY);

							// Randomly draw down charge.
							if (this.Level.depth > (200 * Math.random())) {
								var shovel = setup.player.GetItemByName("shovel");
								setup.player.UseItem(shovel.Id);
								Engine.instance.textBuffer.write("Your shovel breaks!");
							}
							Engine.instance.RefreshStatus();
							return true;
						} else {
							Engine.instance.textBuffer.write("Out of shovels!!");
							return true;
						}
					}
					return true;
				}

				// Use a light
				if (code == ROT.KEYS.VK_SLASH || code == ROT.KEYS.VK_DIVIDE || code == ROT.KEYS.VK_T) {
					if (this.Torches > 0) {
						var torch = setup.player.GetItemByName("torch");
						setup.player.UseItem(torch.Id); // draw down a charge
						Engine.instance.textBuffer.write("You light a torch.");
						this.LightLevel = 10;
						this.LightDuration = 100;
						Engine.instance.drawWithLight(this.XY);
						Engine.instance.RefreshStatus();
						return true;
					} else {
						Engine.instance.textBuffer.write("You don't have any torches!");
					}
				}

				var direction = keys[code];
				if (direction == -1) { /* noop */
					/* FIXME show something? */
					return true;
				}

				var dir = ROT.DIRS[8][direction];
				var xy = this.XY.plus(new App.Rogue.XY(dir[0], dir[1]));

				const xyNum = xyToNumber(xy);
				// Collision detection…
				if (!this.Level.getFreeCells().has(xyNum)) {
					return true;
				}

				const being = this.Level.beings.get(xyNum); // get monster ob
				// Check for monster at location
				if (being) {
					if (being.Encounter != null) {
						Engine.instance.scheduler.remove(being);
						this.Level.removeBeing(being);
						Engine.instance.engine.lock();

						setup.Combat.InitializeScene({flee: 30, fleePassage: "CombatAbamondGenericFlee"});
						setup.Combat.LoadEncounter(being.Encounter);
						this.Level.setEntity(this, xy);
						Engine.instance.redraw(xy);
						SugarCube.Engine.play("Combat");
						return true;
					}
				}

				if (this.LightDuration < 20 && this.LightDuration > 1 && Math.floor((Math.random() * 3)) == 0) Engine.instance.textBuffer.write("Your torch is sputtering.");
				if (this.LightDuration == 1) Engine.instance.textBuffer.write("Your torch goes out!");
				if (this.LightDuration > 0) this.#lightDuration--;
				if (this.LightDuration < 1) {
					if (SugarCube.settings.alternateControlForRogue == true) {
						Engine.instance.textBuffer.write("It is dark. Press 't' to light a torch.");
					} else {
						Engine.instance.textBuffer.write("It is dark. Press '/' to light a torch.");
					}
					this.LightLevel = 1;
				}

				if (this.LightDuration > 0 && Math.random() < 0.10) {
					// get approximate direction to exit
					const exit: XY = this.Level.exitXY;
					const dirToExit = Math.atan2(xy.y - exit.y, exit.x - xy.x); // y axis it from top to bottom

					function dirString(angle: number) {
						const sector = angle / (Math.PI / 4);
						if (angle < -3 || sector > 3) return "west";
						if (sector > 1) return "north";
						if (sector < -1) return "south";
						return "east";
					}
					Engine.instance.textBuffer.write(`A light breeze from the ${dirString(dirToExit)} causes your torch to flicker.`);
				}

				this.Level.setEntity(this, xy);
				Engine.instance.redraw(xy);
				return true;
			}

			return false; /* unknown key */
		}
	}
}
