
namespace App.Rogue {
	export class Engine {

		//Internal Variables
		#scheduler: InstanceType<typeof ROT.Scheduler.Speed>;
		#engine: ROT.Engine;
		#player: Player;
		#level: Level;
		#display: ROT.Display;
		#textBuffer: TextBuffer;
		#sideBar: Sidebar;
		#elementSelector: string;
		#passage: string;
		#depth = 1;
		#maxDepth = 100;
		#lastDrawnCells: number[] = [];
		#currentDrawnCells: number[] = [];
		#title = "Dungeon";

		private static _instance: Engine;

		static get instance(): Engine {
			if (Engine._instance === undefined) {
				Engine._instance = new Engine();
			}
			return Engine._instance;
		}

		LoadScene(elementSelector: string, ExitPassage: string, Depth: number) {
			this.#elementSelector = elementSelector;

			this.#title = "Abamond Caves";
			this.#passage = ExitPassage;
			this.#display = new ROT.Display({width: 100, height: 40, fontSize: 12});
			this.#scheduler = new ROT.Scheduler.Speed();
			this.#engine = new ROT.Engine(this.#scheduler);
			this.#depth = Depth;
			this.#maxDepth = 100;
			this.#lastDrawnCells = [];

			this.#player = new Rogue.Player({ch: "@", fg: "hotpink", bg: null});

			this.#level = this._genLevel(this.#depth);
			const bufferSize = 5;
			const size = this.#level.getSize();
			// side info panel
			this.#sideBar = new Rogue.Sidebar({
				display: this.#display,
				position: new Rogue.XY(size.x + 1, 0),
				size: new Rogue.XY(20, size.y + bufferSize)
			});
			//Bottom chat window
			this.#textBuffer = new Rogue.TextBuffer({
				display: this.#display,
				position: new Rogue.XY(0, size.y),
				size: new Rogue.XY(size.x, bufferSize),
				lines: bufferSize
			});

			this._switchLevel();
		}

		GetDepth() {return this.#depth;}

		Descend() {
			this.#depth += 1;
			if (setup.player.JobFlags.hasOwnProperty("ABAMOND_CAVE_LEVEL")) {
				if (setup.player.JobFlags["ABAMOND_CAVE_LEVEL"] as number < this.#depth) setup.player.JobFlags["ABAMOND_CAVE_LEVEL"] = this.#depth;
			} else {
				setup.player.JobFlags["ABAMOND_CAVE_LEVEL"] = this.#depth;
			}
			this.#lastDrawnCells = []; // Clear buffer
			this.#level = this._genLevel(this.#depth);
			this._switchLevel();
		}

		Ascend() {
			console.log("Current depth:" + this.#depth);
			this.#depth -= 1;
			console.log("Moving to depth:" + this.#depth);
			this.#lastDrawnCells = [];
			if (this.#depth <= 0) {
				console.debug("Exiting dungeon…");
				setup.player.Phase = 3;
				SugarCube.Engine.play(this.#passage);
			} else {
				this.#level = this._genLevel(this.#depth);
				this._switchLevel(true);
			}
		}

		DrawUI() {
			$(document).one(":passageend", () => this._DrawUICB());
		}

		draw(xy: XY) {
			const entity = this.#level.getEntityAt(xy);
			if (entity) {
				var visual = entity.Visual;
				this.#display.draw(xy.x, xy.y, visual.ch, visual.fg, visual.bg);
			}
		}

		redraw(xy: XY) {
			this.#lastDrawnCells = this.#currentDrawnCells;
			this.drawWithLight(xy);
		}

		over() {
			this.#engine.lock();
			/* FIXME show something */
		}

		private _genLevel(depth: number) {
			var level = new App.Rogue.Level(depth);
			level.generateMap(80, 40);
			level.fillBorders(80, 40);
			level.genEntrance();
			level.genExit();
			level.genTreasure();
			level.genMonsters();
			return level;
		}

		RefreshStatus() {
			this.#sideBar.clear();
			this.#sideBar.flush();
			// Draw GUI.
			this.#sideBar.Title(this.#title);
			this.#sideBar.Level("Level: " + this.#depth + "/" + this.#maxDepth);
			this.#sideBar.Torches("Torches: " + this.#player.Torches);
			this.#sideBar.Shovels("Shovels: " + this.#player.Shovels);
			this.#sideBar.Help();
		}

		get scheduler() {
			return this.#scheduler;
		}

		get textBuffer() {
			return this.#textBuffer;
		}

		get engine() {
			return this.#engine;
		}

		get level() {
			return this.#level;
		}

		get player() {
			return this.#player;
		}

		private _switchLevel(opt = false) {
			this.#scheduler.clear();
			this.#scheduler.add(this.#player, true);

			if (opt) {
				this.#level.setEntity(this.#player, this.#level.exitXY);
			} else {
				this.#level.setEntity(this.#player, this.#level.entranceXY);
			}
			var size = this.#level.getSize();

			const bufferSize = 5;
			this.#display.setOptions({width: size.x + 20, height: size.y + bufferSize});

			this.#textBuffer.clear();

			this.#display.clear();
			this.drawWithLight(this.#player.XY);

			this.RefreshStatus();

			/* add new beings to the scheduler */
			const beings = this.#level.getBeings();
			for (const b of beings.values()) {
				if (b instanceof Being) {
					this.#scheduler.add(b, true);
				}
			}
		}

		drawWithLight(pxy: XY) {
			/** light level */
			const LL = this.#player.LightLevel;
			this.#currentDrawnCells = this.#level.cellsAtRadius(pxy.x, pxy.y, LL, false).map(c => xyToNumber(c));

			let walls = new Array<Array<number>>(LL * 2 + 1);
			let visibilityMap = new Array<Array<boolean>>(LL * 2 + 1);
			for (let i = 0; i < walls.length; ++i) {
				walls[i] = new Array<number>(LL * 2 + 1);
				walls[i].fill(0); // starting with no walls
				visibilityMap[i] = new Array(LL * 2 + 1);
				visibilityMap[i].fill(false); // everything is invisible
			}

			const x0 = pxy.x - LL;
			const y0 = pxy.y - LL;

			const wallXY = this.#level.getWalls();
			for (let xy of this.#currentDrawnCells) {
				if (wallXY.get(xy)) {
					const xyVal = numberToXY(xy);
					walls[xyVal.x - x0][xyVal.y - y0] = 1; // put a wall
				}
			}

			// ray tracing to detect visible walls
			function traceLine(i: number, j: number) { // i and j are orts
				for (let l = 1; l <= LL; ++l) {
					const x = Math.round(LL + l * i);
					const y = Math.round(LL + l * j);
					visibilityMap[x][y] = true;
					if (walls[x][y] === 1) { // wall found
						break;
					}
				}
			}

			// player's position is always visible
			visibilityMap[LL][LL] = true;

			traceLine(1, 0);
			traceLine(-1, 0);
			traceLine(0, 1);
			traceLine(0, -1);
			for (let t = 1; t <= LL; ++t) {
				let vX = LL;
				let vY = t;
				const vL = Math.sqrt(vX * vX + vY * vY);
				const iX = vX / vL;
				const iY = vY / vL;

				traceLine(iX, iY);
				traceLine(-iX, iY);
				traceLine(iX, -iY);
				traceLine(-iX, -iY);

				traceLine(iY, iX);
				traceLine(-iY, iX);
				traceLine(iY, -iX);
				traceLine(-iY, -iX);
			}

			for (const xyNum of this.#currentDrawnCells) {
				const xy = numberToXY(xyNum);
				if (visibilityMap[xy.x - x0][xy.y - y0]) {
					this.draw(xy);
				} else {
					this.#display.draw(xy.x, xy.y, null, null, null);
				}
			}

			if (this.#lastDrawnCells.length > 0) {
				var mapped = this._mapCells(this.#currentDrawnCells, this.#lastDrawnCells);
				this._drawBlank(mapped);
			}
		}

		private _mapCells(dest: number[], source: number[]) {
			return source.filter(function (key) {return dest.includes(key) == false;});
		}

		private _drawBlank(cells: number[]) {
			for (const c of cells) {
				const xy = numberToXY(c);
				this.#display.draw(xy.x, xy.y, null, null, null);
			}
		}
		/** CALL BACKS **/

		private _DrawUICB() {
			$(this.#elementSelector).append(this.#display.getContainer() as HTMLElement);
			this.#engine.start();
		}

	}
}
