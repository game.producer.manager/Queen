
namespace App.Rogue {

	interface TextBufferOptions {
		display: ROT.Display;
		position: XY;
		size: XY;
		lines: number;
	}
	export class TextBuffer {
		#data: string[] = [];
		#options: TextBufferOptions = {
			display: null,
			position: new App.Rogue.XY(),
			size: new App.Rogue.XY(),
			lines: 5,
		};

		constructor(options: TextBufferOptions) {
			Object.assign(this.#options, options);
		}

		configure(options: TextBufferOptions): void {
			Object.assign(this.#options, options);
		}

		clear(): void {
			this.#data = [];
		}

		private _chunkString(str: string, length: number) {
			return str.match(new RegExp('.{1,' + length + '}', 'g'));
		}

		write(text: string): void {
			// Chunk long messages into buffer sized segments.
			const s = this._chunkString(text, this.#options.size.x);
			console.log(s);
			this.#data = this.#data.concat(s?.toString() ?? "");
			// Only keep 'lines' length
			if (this.#data.length >= this.#options.lines) {
				this.#data.splice(0, this.#data.length - this.#options.lines);
			}
			//this._data.push(text);
		}

		flush(): void {
			const o = this.#options;
			const d = o.display;
			const pos = o.position;
			const size = o.size;

			/* clear */
			for (let i = 0; i < size.x; i++) {
				for (let j = 0; j < size.y; j++) {
					d.draw(pos.x + i, pos.y + j, null, null, null);
				}
			}

			var text = this.#data.join("\n");
			d.drawText(pos.x, pos.y, text, size.x);
		}
	}
}
