namespace App {

	export namespace Items {

		/**
		 * Fetch the default charges from an items data record.
		 */
		export function GetCharges(type: Data.ItemTypeStr, tag: string): number;
		export function GetCharges(rec: Data.ConsumableItemDesc): number;
		export function GetCharges(typeOrRec: Data.ItemTypeStr | Data.ConsumableItemDesc, tag?: any): number {
			return ((typeof typeOrRec === "string") ? FetchData(typeOrRec, tag) : typeOrRec)['Charges'] ?? 1;
		}

		/**
		 * Return the appropriate data dictionary for the item in question. If item type is invalid, returns null.
		 */
		export function TryGetItemsDictionary<T extends keyof Data.ItemTypeDescMap>(Type: T): {[x: string]: Data.ItemTypeDescMap[T]} {
			type R = {[x: string]: Data.ItemTypeDescMap[T]};
			switch (Type) {
				case "DRUGS": return Data.Drugs as R;
				case "FOOD": return Data.Food as R;
				case "COSMETICS": return Data.Cosmetics as R;
				case "MISC_CONSUMABLE": return Data.Misc as R;
				case 'MISC_LOOT': return Data.MiscLoot as R;
				case "CLOTHES":
				case "WEAPON": return Data.Clothes as R;
				case "QUEST": return Data.QuestItems as R;
				case "LOOT_BOX": return Data.LootBoxes as R;
				case 'REEL': return Data.Slots as R;
			}
			throw "WTF, TS?!";
		}

		/**
		 * Return the appropriate data dictionary item for the item in question. If item type is invalid or
		 * item is not found, shows error message and throws exception
		 */
		export function FetchData<T extends keyof Data.ItemTypeDescMap>(Type: T, Name: string): Data.ItemTypeDescMap[T] {
			var itemsDictionary = TryGetItemsDictionary(Type);
			if (itemsDictionary == null) {
				const errorMessage = "Invalid item type: " + Type;
				alert(errorMessage);
				throw new Error(errorMessage);
			}

			var itemData = itemsDictionary[Name];
			if (itemData === null || itemData === undefined) {
				const errorMessage = `Item with name "${Name}" of type "${Type}" not found`;
				alert(errorMessage);
				throw new Error(errorMessage);
			}
			return itemData;
		}


		/**
		 * Automatically calculate the price of an item based on it's attributes.
		 * @param Category the dictionary name
		 * @param Tag the key of the item
		 * @returns the price in gold coins.
		 */
		export function CalculateBasePrice(category: Data.ItemTypeStr, tag: string): number {
			try {
				const d = FetchData(category, tag);
				let price = 0;

				switch (category) {
					case 'REEL':
						price = d.Value ?? 500;
						break;
					case 'DRUGS':
					case 'FOOD':
						// Drugs and food have a price which is the sum of their effect values
						price += typeof d["Effects"] !== 'undefined' ? CalculateEffectPrice(d["Effects"]) : 0;
						break;
					case 'COSMETICS':
					case 'MISC_LOOT':
					case 'MISC_CONSUMABLE':
						price = d.Value ?? 500;
						break;
					case 'CLOTHES':
					case 'WEAPON':
						// Base price set on style rank.
						if (typeof d["Style"] !== 'undefined') {

							switch (d["Style"]) {
								case 'COMMON': price = 50; break;
								case 'UNCOMMON': price = 100; break;
								case 'RARE': price = 250; break;
								case 'LEGENDARY': price = 600; break;
								default: price = 50; break;
							}

						} else {
							price = 50;
						}

						//modify based on type
						if (typeof d["Type"] !== 'undefined') {

							switch (d["Type"]) {
								case 'ACCESSORY': price *= 1.4; break;
								case 'CLOTHING': price *= 1.1; break;
								case 'ONE PIECE': price *= 2.5; break;
								case 'WEAPON': price *= 3; break;
							}
						}

						// Wigs have a special stat on them.
						if (typeof d["HairBonus"] !== 'undefined') price = price + (d["HairBonus"] * 2);

						// Check for Categories. Extra categories other than 1 increase price by 10% per category
						if (typeof d["Category"] !== 'undefined')
							price = (d["Category"].length > 1) ? price + ((price * 0.1) * (d["Category"].length - 1)) : price;

						// Add values from worn effects
						price += typeof d["WearEffect"] !== 'undefined' ? CalculateEffectPrice(d["WearEffect"]) : 0;

						// Add values from active effects
						price += typeof d["ActiveEffect"] !== 'undefined' ? CalculateActiveEffectPrice(d["ActiveEffect"]) : 0;

						// Round up
						price = Math.ceil(price);

						break;

				}

				if (typeof (d.PriceCoefficient) === "number") {
					price *= d.PriceCoefficient;
				}

				return (price == 0) ? 100 : price;
			} catch (e) {
				// Whatever, I guess?
				return 100;
			}
		}

		function CalculateEffectPrice(Effects: string[]): number { // TODO Clothing effects
			if (Effects.length == 0) return 0;
			var price = 0;
			if (Effects.length > 1) {
				price += Effects.reduce((accumulator, effect) => accumulator + App.Data.EffectLib[effect].VALUE, 0);
			} else {
				price += App.Data.EffectLib[Effects[0]].VALUE;
			}

			return price;
		}

		function CalculateActiveEffectPrice(Effects: string[]): number { // TODO Clothing effects
			if (Effects.length == 0) return 0;
			var price = 0;
			if (Effects.length > 1) {
				price += Effects.reduce((accumulator, effect) => accumulator + App.Data.EffectLibClothing[effect].VALUE, 0);
			} else {
				price += App.Data.EffectLibClothing[Effects[0]].VALUE;
			}

			return price;
		}
		/**
		 * Search and find clothing items by a variety of categories.
		 */
		function ListAllClothes(Category: string, Rank: string, Slot: App.Data.ClothingSlot) {
			type R = {tag: string, rank: string, slot: string, cat: string[]};
			var Clothes: R[] = [];

			for (var k in App.Data.Clothes) {
				if (!App.Data.Clothes.hasOwnProperty(k)) continue;
				var cat = App.Data.Clothes[k]['Category'];
				if (typeof cat !== 'undefined' && cat.length >= 1) {
					var item = {tag: k, rank: App.Data.Clothes[k]['Style'], slot: App.Data.Clothes[k]['Slot'], cat: cat};
					if (Category != null && $.inArray(Category, cat) != -1) {
						Clothes.push(item);
					} else if (Category == null) {
						Clothes.push(item);
					}
				}
			}

			if (Rank != null) Clothes = Clothes.filter(function (ob) {return ob.rank == Rank;});
			if (Slot != null) Clothes = Clothes.filter(function (ob) {return ob.slot == Slot;});

			return Clothes;

		}

		export type ItemListingRecord = {
			cat: Data.ItemTypeStr,
			tag: string, name: string,
			desc: string,
			price: number,
			style: string | null,
			category: string[] | null,
			useEffects: string[],
			wearEffects: string[],
			activeEffects: string[],
			meta: string[];
			inMarket: boolean;

		};
		/**
		 * Just a debug function
		 */
		export function ListAllPrices(category: Data.ItemTypeStr, filter = 0) {
			const out: ItemListingRecord[] = [];
			const d = TryGetItemsDictionary(category);

			for (const k of Object.keys(d)) {
				const item = d[k];
				const res = {
					cat: category,
					tag: k,
					name: item.Name,
					desc: item['ShortDesc'] ?? "",
					price: CalculateBasePrice(category, k),
					style: item['Style'] ?? null,
					category: item['Category'] ?? null,
					useEffects: item['Effects'] ?? [],
					wearEffects: item['WearEffect'] ?? [],
					activeEffects: item['ActiveEffect'] ?? [],
					meta: item['Meta'] ?? null,
					inMarket: item.InMarket ?? true,
				};

				out.push(res);
			}

			out.sort(function (a, b) {return a.price - b.price;});
			return (filter) ? out.filter(function (a) {return a.price < filter;}) : out;
		}

		/**
		 * Fetch a random item based on price / category.
		 * @param {App.Data.ItemTypeStr|App.Data.ItemTypeStr[]} Category
		 * @param {*} filterOb
		 * @returns {null|*}
		 */
		export function PickItem(Category, filterOb) {
			var items: ItemListingRecord[] = [];
			console.debug(filterOb);
			if (Array.isArray(Category)) {
				while (Category.length > 0) {
					var tmpCategory = App.PR.GetRandomListItem(Category);
					Category.splice(Category.indexOf(tmpCategory), 1); // Pop the item off.
					items = items.concat(ListAllPrices(tmpCategory, filterOb.price));
				}
			} else {
				items = ListAllPrices(Category, filterOb.price);
			}

			// Ignore items that we don't want showing up randomly.
			if (items.length > 0) items = items.filter(o => o.inMarket);
			// Filter on clothing category
			if (items.length > 0 && (filterOb.hasOwnProperty("category")))
				items = items.filter(function (o) {return o.category != null && o.category.contains(filterOb.category);});
			// Filter on meta data
			if (items.length > 0 && (filterOb.hasOwnProperty("meta_key")))
				items = items.filter(function (o) {return o.meta != null && o.meta.contains(filterOb.meta_key);});
			console.debug(items);
			return items.length > 0 ? App.PR.GetRandomListItem(items) : null;
		}

		function makeItem<T extends keyof ItemTypeMap>(type: T, tag: string, inventory?: App.Entity.ItemTypeInventoryTypeMap[T]): ItemTypeMap[T];
		function makeItem<T extends keyof ItemTypeMap>(type: T, tag: string, inventory?: any): ItemTypeMap[T] {
			type R = ItemTypeMap[T];
			const d: any = FetchData(type, tag);
			switch (type) {
				case "CLOTHES":
				case "WEAPON":// might change this in the future, for now weapons are "clothing"
					return new Clothing(tag, d, inventory) as R;
				case "COSMETICS":
				case "DRUGS":
				case "FOOD":
				case "MISC_CONSUMABLE":
				case "MISC_LOOT":
				case "LOOT_BOX":
					return new Consumable(type as Data.ConsumableItemTypeStr, tag, d, inventory) as R;
				case "QUEST":
					return new QuestItem(tag, d, inventory) as R;
				case "REEL":
					return new Reel(tag, d, inventory) as R;
				default:
					throw ""
			}
		}

		export function Factory<T extends keyof ItemTypeMap>(Type: T, Tag: string, Inventory?: Entity.ItemTypeInventoryTypeMap[T], Count?: number): ItemTypeMap[T] {
			var o = makeItem(Type, Tag, Inventory);
			// HACK: when an item is created in an "disconnected" state, without an inventory manager,
			// this probably mean we want to get a description from the item. Thus we discard charges
			// from the data record in that case.
			if (o instanceof Consumable && Inventory !== undefined && Count == undefined) {
				Count = GetCharges(o.Data);
				// @ts-ignore
				if (Count > 1) o.AddCharges(Count - 1);
			}

			return o;
		}

		/**
		 * Generate an item id
		 */
		export function MakeId<T extends Data.ItemTypeStr>(Category: T, Tag: string): Data.ItemNameTemplate<T> {
			return `${Category}/${Tag}` as Data.ItemNameTemplate<T>;
		}

		export function SplitId(Id: string): {Category: Data.ItemTypeStr, Tag: string} {
			var _t = Id.split('/');
			return {Category: _t[0] as Data.ItemTypeStr, Tag: _t[1]};
		}

		//#region Item classes

		export class BaseItem {
			#tag: string;
			#itemClass: Data.ItemTypeStr;
			#desc: Data.ItemDesc;

			constructor(itemClass: Data.ItemTypeStr, tag: string, desc: Data.ItemDesc) {
				this.#itemClass = itemClass;
				this.#tag = tag;
				this.#desc = $.extend(true, {}, desc);
			}

			get Id(): string {
				return Items.MakeId(this.#itemClass, this.#tag);
			}

			get Tag(): string {
				return this.#tag;
			}

			get ItemClass(): Data.ItemTypeStr {
				return this.#itemClass;
			}

			get Name(): string {
				return this.#desc.Name;
			}

			get Data(): Data.ItemDesc {
				return this.#desc;
			}
		}
		export class Item<TypeStr extends keyof App.Entity.ItemTypeInventoryTypeMap> extends BaseItem {
			private _inventory: App.Entity.ItemTypeInventoryTypeMap[TypeStr];
			/**
			 *
			 * @param {ItemClass} itemClass
			 * @param {string} Tag
			 * @param {object} d
			 * @param {Inventory} Inventory
			 * @constructor
			 */
			constructor(itemClass: TypeStr, Tag: string, d: App.Data.ItemTypeDescMap[TypeStr], Inventory: App.Entity.ItemTypeInventoryTypeMap[TypeStr]) {
				super(itemClass, Tag, d);
				this._inventory = Inventory;
			}

			get Inventory(): App.Entity.ItemTypeInventoryTypeMap[TypeStr] {
				return this._inventory;
			}

			get Data(): App.Data.ItemTypeDescMap[TypeStr] {
				return super.Data as App.Data.ItemTypeDescMap[TypeStr];
			}

			get Description(): string {
				return "";
			}
		}

		export class Clothing extends Item<"CLOTHES" | "WEAPON"> {
			#knowledge: string[] = [];
			constructor(Tag: string, d: Data.ClothingItemDesc, InventoryObj: Entity.ClothingManager) {
				super('CLOTHES', Tag, d, InventoryObj);

				for (var i = 0; i < this.WearEffect.length; i++)
					this.#knowledge = this.#knowledge.concat(Data.EffectLib[this.WearEffect[i]]["KNOWLEDGE"]);
				for (i = 0; i < this.ActiveEffect.length; i++)
					this.#knowledge = this.#knowledge.concat(Data.EffectLibClothing[this.ActiveEffect[i]]["KNOWLEDGE"]);
			}

			/**
			 * Short description of item
			 */
			get Description() {
				var result = this.Data.ShortDesc;
				// if (result instanceof String) result = String(result);
				if (typeof (result) !== "string" || result === "") return this.Name;
				result = result.replace("{COLOR}", String(this.Data.Color));

				if (this.IsLocked()) result += " <span style='color:red'>(Locked)</span>";

				return result;
			}

			/**
			 * Examine an item, relate detailed description and any knowledge.
			 */
			Examine(Player: App.Entity.Player, OmitDescription = false) {
				if (OmitDescription == true) {
					Output = this.Category.join(", ") + " " + this.RankDesc + "\n";
					var Usages = Player.GetHistory("CLOTHING_EFFECTS_KNOWN", this.Tag);
					var max = Math.min(Usages, this.GetKnowledge().length);

					for (var i = 0; i < max; i++)
						Output += App.PR.pEffectMeter(this.GetKnowledge()[i], this) + " ";
					return Output;
				}

				var Output = this.Data["LongDesc"];
				var Usages = Player.GetHistory("CLOTHING_EFFECTS_KNOWN", this.Tag);

				Output += "\n";

				Output += "@@.state-neutral;Style Categories  @@ ";
				Output += this.Category.join(", ");
				Output += "\n";
				Output += "@@.state-neutral;Rank @@ " + this.RankDesc + "\n";
				var max = Math.min(Usages, this.GetKnowledge().length);

				for (var i = 0; i < max; i++)
					Output += App.PR.pEffectMeter(this.GetKnowledge()[i], this) + " ";
				return Output;
			}

			PrintEffectsOnly(Player: App.Entity.Player) {
				var Output = "";
				var Usages = Player.GetHistory("CLOTHING_EFFECTS_KNOWN", this.Tag);
				var max = Math.min(Usages, this.GetKnowledge().length);

				for (var i = 0; i < max; i++)
					Output += App.PR.pEffectMeter(this.GetKnowledge()[i], this) + " ";
				return Output;
			}

			/**
			 * Apply (worn) affects overnight like skill gains / body mods.
			 * Apply all effects of this clothing item, usually overnight.
			 */
			ApplyEffects(Player: App.Entity.Player) {
				for (const e of this.WearEffect) {
					if (Player.debugMode == true) {
						console.group("Applying effect: ", e);
					}
					App.Data.EffectLib[e].FUN(Player, this);
					if (Player.debugMode == true) {
						console.groupEnd();
					}
				}
			}

			/**
			 * Learn Knowledge sleeping…
			 */
			LearnKnowledge(Player: App.Entity.Player) {
				var flag = (Player.GetHistory("CLOTHING_KNOWLEDGE", this.Tag) > 0);
				var know = Player.GetHistory("CLOTHING_EFFECTS_KNOWN", this.Tag);
				if (flag && know < this.GetKnowledge().length) {
					var output = "@@.state-neutral;You learn something… your " + this.Name + " has an effect!@@ " + App.PR.pEffectMeter(this.GetKnowledge()[know], this);
					Player.AddHistory("CLOTHING_EFFECTS_KNOWN", this.Tag, 1);
					Player.RemoveHistory("CLOTHING_KNOWLEDGE", this.Tag);
					return output;
				}
			}

			/**
			 * List all categories this piece of clothing belongs to.
			 */
			get Category(): Data.Clothes.CategoryStr[] {
				return this.Data.Category ?? ["Ordinary"];
			}

			/**
			 * Return the style of the item if it fits within the category supplied.
			 */
			CategoryBonus(Cat: Data.Clothes.CategoryStr) {
				return this.Category.includes(Cat) ? this.Style : 0;
			}

			/**
			 * What slot this is worn in.
			 */
			get Slot() {
				return this.Data.Slot;
			}

			/**
			 * Slots to disable when this is worn.
			 */
			get Restrict() {
				return this.Data.Restrict;
			}

			/**
			 * Effects that happen when the item is worn, overnight.
			 */
			get WearEffect(): string[] {
				return this.Data.WearEffect ?? [];
			}

			/**
			 * Effects that can be applied to active skill rolls.
			 */
			get ActiveEffect(): string[] {
				return this.Data.ActiveEffect ?? [];
			}

			/**
			 * Color of item.
			 */
			get Color(): string {
				return this.Data.Color;
			}

			// STYLE TABLE
			// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
			// ACCESSORY    3       6           9       12
			// CLOTHING     5       10          15      20
			// ONE PIECE    10      20          30      40
			/**
			 * Style bonus related to quality of clothing item.
			 */
			get Style(): number {
				const bonus = {
					"ACCESSORY": {"COMMON": 3, "UNCOMMON": 6, "RARE": 9, "LEGENDARY": 12},
					"WEAPON": {"COMMON": 3, "UNCOMMON": 6, "RARE": 9, "LEGENDARY": 12},
					"CLOTHING": {"COMMON": 5, "UNCOMMON": 10, "RARE": 15, "LEGENDARY": 20},
					"ONE PIECE": {"COMMON": 10, "UNCOMMON": 20, "RARE": 30, "LEGENDARY": 40}
				};

				if (this.Data.Type == "WEAPON") return 0;
				return bonus[this.Data.Type][this.Data.Style];
			}
			/**
			 * Show stars relating to rank on clothing.
			 */
			get RankDesc(): string {
				switch (this.Data.Style) {
					case "COMMON": return "<span style='color:gold'>&#9733;</span>";
					case "UNCOMMON": return "<span style='color:gold'>&#9733;&#9733;</span>";
					case "RARE": return "<span style='color:gold'>&#9733;&#9733;&#9733;</span>";
					case "LEGENDARY": return "<span style='color:gold'>&#9733;&#9733;&#9733;&#9733;</span>";
				}
			}

			get Rank(): number {
				switch (this.Data.Style) {
					case "COMMON": return 1;
					case "UNCOMMON": return 2;
					case "RARE": return 3;
					case "LEGENDARY": return 4;
				}
			}

			HairColor(): Data.HairColor {
				return this.Data.Color as Data.HairColor ?? "black";
			}

			HairLength(): number {
				return this.Data.HairLength ?? 0;
			}

			HairStyle(): string {
				return this.Data.HairStyle ?? "";
			}

			HairBonus(): number {
				return this.Data.HairBonus ?? 0;
			}

			/**
			 * Locked items cannot be removed unless unlocked.
			 */
			IsLocked(): boolean {
				if (this.Inventory !== undefined && this.Inventory.IsWorn(this.Id, this.Slot)) return this.Inventory.IsLocked(this.Slot);
				return false;
			}

			/**
			 * Query the effects (if present) on this piece of clothing and return the bonus
			 */
			GetBonus(skillName: Data.WearSkill): number {
				return this.ActiveEffect.reduce((ac, ef) => ac + Data.EffectLibClothing[ef].FUN(skillName, this), 0);
			}

			/**
			 * Checks whether this item can be sold in shops
			 */
			InMarket(): boolean {
				var inMarket = this.Data.InMarket;
				return typeof (inMarket) === "boolean" ? inMarket : true; // Default to yes.
			}

			GetKnowledge() {return this.#knowledge.sort();}
		}

		export class BaseInventoryItem<TypeStr extends Data.InventoryItemTypeStr> extends Item<TypeStr>{

			get ItemClass(): Data.InventoryItemTypeStr {
				return super.ItemClass as Data.InventoryItemTypeStr;
			}
		}

		export class Consumable extends BaseInventoryItem<Data.ConsumableItemTypeStr> {
			private _messageBuffer: string[] = [];
			#knowledge: string[] = [];

			constructor(Category: Data.ConsumableItemTypeStr, Tag: string, d: App.Data.ConsumableItemDesc, InventoryObj: App.Entity.InventoryManager) {
				super(Category, Tag, d, InventoryObj);

				this.#knowledge = this.UseEffect.map(e => App.Data.EffectLib[e].KNOWLEDGE).flat().sort();
			}

			get Type(): string {
				return this.Data.Type;
			}

			/**
			 * Short description of item
			 */
			get Description() {
				return this.Data.ShortDesc;
			}

			/**
			 * Shows long description of item and any knowledge known about it.
			 */
			Examine(Player: App.Entity.Player, OmitDescription = false): string {
				var Output = OmitDescription ? "" : this.Data["LongDesc"];
				var Usages = Player.GetHistory("ITEMS", this.Tag);

				if (Usages == 0) return Output;

				if (!OmitDescription) Output += "\n\n";
				var max = Math.min(Usages, this.GetKnowledge().length);

				for (var i = 0; i < max; i++)
					Output += App.PR.pEffectMeter(this.GetKnowledge()[i], this) + "  ";

				return Output;
			}

			PrintEffectsOnly(Player: Entity.Player) {
				var Output = "";
				var Usages = Player.GetHistory("ITEMS", this.Tag);
				var max = Math.min(Usages, this.GetKnowledge().length);

				for (var i = 0; i < max; i++)
					Output += App.PR.pEffectMeter(this.GetKnowledge()[i], this) + " ";
				return Output;
			}

			get UseEffect() {
				return this.Data.Effects ?? [];
			}

			/**
			 * Apply all effects of this consumable item.
			 */
			ApplyEffects(player: App.Entity.Player) {
				const effects = this.Data.Effects;
				if (effects) {
					for (const e of effects) {
						if (player.debugMode == true) {console.group("Applying effect: ", e);}
						const tmp = App.Data.EffectLib[e].FUN(player, this);
						if ((typeof tmp !== 'undefined') && (tmp != "")) this._messageBuffer.push(tmp);
						if (player.debugMode == true) {console.groupEnd();}
					}
				}

				// Knowledge.
				var Usages = player.GetHistory("ITEMS", this.Tag);

				if (Usages <= this.GetKnowledge().length)
					this._messageBuffer.push("\n\n@@.state-neutral;You learn something… this item has an effect!@@ " + App.PR.pEffectMeter(this.GetKnowledge()[(Usages - 1)], this));

			}

			/**
			 * This message is printed when a player uses an item
			 */
			Message(Player: App.Entity.Player): string {
				var Output = this.Data.Message;
				if (this._messageBuffer.length > 0) Output += this._messageBuffer.join("\n");
				this._messageBuffer = [];
				return Output === undefined ? "" : App.PR.TokenizeString(Player, undefined, Output);
			}

			// /**
			//  * Price of the item
			//  * @todo unused
			//  */
			// get Price(): number {
			// 	return this.Data.Price;
			// }

			/**
			 * No. of uses
			 */
			Charges() : number{
				return this.Inventory.Charges(this.ItemClass, this.Tag);
			}

			SetCharges(n: number) {
				return this.Inventory.SetCharges(this.ItemClass, this.Tag, n);
			}

			AddCharges(n: number): number {
				return this.Inventory.AddCharges(this.ItemClass, this.Tag, n);
			}

			RemoveCharges(n: number) {
				return this.Inventory.AddCharges(this.ItemClass, this.Tag, -n);
			}

			IsFull() {
				return this.Charges() == App.Entity.InventoryManager.MAX_ITEM_CHARGES;
			}

			// TODO: HasBonus / GetBonus should probably be factored out of the game. Not sure if this was ever used.
			/**
			 * Determine if an item has a bonus of a specific type.
			 */
			HasBonus(b: string): boolean {
				return this.Data.hasOwnProperty("SkillBonus")? this.Data["SkillBonus"].hasOwnProperty(b): false;
			}

			/**
			 * Get the bonus for an item by key
			 * @param b - bonus type to find
			 */
			GetBonus(b: string): number[] {
				if (this.Data.hasOwnProperty("SkillBonus") == false) return [0, 0, 0];
				if (this.Data["SkillBonus"].hasOwnProperty(b) == false) return [0, 0, 0];
				return this.Data["SkillBonus"][b];
			}

			GetKnowledge() {return this.#knowledge;}

			IsFavorite() {
				return this.Inventory.IsFavorite(this.Id);
			}

			SetFavorite(Value: boolean = true) {
				if (Value == true) {
					this.Inventory.AddFavorite(this.Id);
				} else {
					this.Inventory.DeleteFavorite(this.Id);
				}
				return Value;
			}

			ToggleFavorite() {
				return this.SetFavorite(!this.IsFavorite());
			}
		}


		export class QuestItem extends BaseInventoryItem<"QUEST"> {
			constructor(Tag: string, d: App.Data.InventoryItemDesc, InventoryObj: App.Entity.InventoryManager) {
				super("QUEST", Tag, d, InventoryObj);
			}

			/**
			 * Short description
			 */
			get Description() {
				return this.Data.ShortDesc;
			}

			/**
			 * Sub category of quest item. If applicable.
			 */
			get Type() {
				return this.Data.Type;
			}

			/**
			 * Long description of item. No knowledge on quest items.
			 */
			Examine(_player: App.Entity.Player, OmitDescription = false) {
				return OmitDescription ? "" : this.Data.LongDesc;
			}

			/**
			 * Doesn't work. You can't have more than 1 of any quest item.
			 */
			AddCharges() {return 1;};

			RemoveCharges() {
				return this.Inventory.AddCharges(this.ItemClass, this.Tag, -1);
			}

			/** Fake: This makes it so that quest items are unique.
			 */
			Charges() {return 1;}

			IsFavorite() {return false;}
		};

		export class Reel extends BaseInventoryItem<"REEL"> {

			private static styleMap: Record<App.Data.ReelRankStr, string> = {
				'COMMON': "item-common",
				'UNCOMMON': "item-uncommon",
				'RARE': "item-rare",
				'LEGENDARY': "item-legendary"
			};

			constructor(id: string, d: App.Data.ReelDesc, InventoryObj: App.Entity.InventoryManager) {
				super("REEL", id, d, InventoryObj);
			}

			/**
			 * The name of the reel.
			 */
			get Name() {
				return this.Data.Name;
			}

			get Description() {
				return `@@.${Reel.styleMap[this.Data.RANK]};(${this.Data.RANK}) ${this.Data.Name}@@`;
			}

			Examine(_player: App.Entity.Player, omitDescription = false): string {
				var attrs: App.Data.ReelAction[] = ['ASS', 'BJ', 'HAND', 'TITS', 'FEM', 'PERV', 'BEAUTY'];
				var text = ['Ass Fucking', 'Blowjobs', 'Handjobs', 'Tit Fucking', 'Femininity', 'Perversion', 'Beauty'];
				var output = omitDescription ? "" : "A slot reel used for whoring. It has the following attributes:\n";
				for (var x = 0; x < attrs.length; x++) {
					var percent = this.CalcPercent(attrs[x]);
					if (percent <= 0) continue;
					output += text[x] + " - " + percent + "% ";
				}
				return output;
			}

			CalcPercent(key: App.Data.ReelAction): number {
				var filterArr = this.Data.DATA.filter(function (o) {return o == key;});
				return Math.round((filterArr.length / this.Data.DATA.length) * 100);
			}

			get Rank() {return this.Data.RANK;}

			get Reels() {
				return this.Data.DATA;
			}

			get Css() {
				return this.Data.CSS;
			}

			/**
			 * Find a particular symbol at an array index
			 */
			Symbol(index: number) {
				return this.Data.DATA[index];
			}

			get Type() {return this.ItemClass;}

			/**
			 * No. of uses
			 */
			Charges(): number {
				return this.Inventory.Charges(this.ItemClass, this.Tag);
			}
		};

		export type InventoryItem = Consumable | QuestItem | Reel;
		export type AnyItem = Clothing | InventoryItem;
	}
}
