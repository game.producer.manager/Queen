namespace App.DebugUtils {
	export function checkTravelDestinations() {

		const allStartPoints = Object.keys(App.Data.Travel.destinations);
		const checkDestinationExists = (d: string, from: string) => {
			if (!allStartPoints.includes(d)) {
				console.error(`No routes from ${d}, enter from ${from}`);
			}
		}

		const allDestinations: Set<string> = new Set();

		for (const k of allStartPoints) {
			for (const d of App.Data.Travel.destinations[k]) {
				if (typeof d === "string") {
					checkDestinationExists(d, k);
					allDestinations.add(d);
				} else {
					const dest = d['destination'];
					if (dest) {
						const destStr = typeof dest === "function" ? dest(setup.player) : dest;
						checkDestinationExists(destStr, k);
						allDestinations.add(destStr);
					}
				}
			}
		}

		Story.lookupWith(p => !p.tags.includesAny('custom-menu', 'Twine.audio', 'Twine.image'))
			.filter(p => !allDestinations.has(p.title))
			.forEach(p => {console.log(`No route to ${p.title}`);})
	}
}
