// Gambling classes.
// Real rough, probably clean up some a bit in the future, but maybe not.
// 'Pirates Dice' (poker like game with dice) - TBD
// 'Shut the Coffin' simple point like dice game
// 'Blackjack' We all know and love. Right? - TBD
// 'Grand Hazard' Sorta like craps. Old West style game. - TBD

namespace App.Gambling {

	type Offer = "Item" | "Coins";
	type WantChoice = "Hand Job" | "Blow Job" | "Tit Fuck" | "Ass Fuck";
	const enum BetStatus {
		Nothing = 0,
		Lose = 1,
		Win = 2
	}
	interface Gambler {
		/** name */
		n: string;

		bet1Offer: Offer;
		bet1Want:  "Coins" | number;
		bet1Value: number;
		bet1Status: BetStatus;

		bet2Offer: Offer;
		bet2Want: "Coins" | number;
		bet2Value: number;
		bet2Status: BetStatus;

		lust: number;
	}

	interface Sex {
		name: string;
		val: number;
		sex: SkillStr;
	}

	export class Coffin {
		private myScore: number[] = [0, 0, 0, 0, 0, 0, 0, 0];
		private opponentScore = [0, 0, 0, 0, 0, 0, 0, 0];
		private returnPassage = "Deck"; // default
		private casinoFlag = false;
		private wants: WantChoice[] = ["Hand Job", "Blow Job", "Tit Fuck", "Ass Fuck"];
		private nouns = ["a slutty handjob", "a sloppy cock sucking", "a titty wank", "your sissy asshole"];
		private skills: SkillStr[] = ["HandJobs", "BlowJobs", "TitFucking", "AssFucking"];
		private icons = ["handBetIcon", "bjBetIcon", "titBetIcon", "assBetIcon"];

		private gamblers: Gambler[] = [];
		private gamblerPosition = 0;
		private selectedBet = null;
		private element = "#GamblingGUI";
		private roundNum = 1;
		private maxRounds = 6;
		private gamesPlayed = 1;
		private maxGames = 5;
		private diceRolling = false;
		private playerTurn = true;
		private suddenDeath = false;
		private screen = "NEW"; // NEW -> BET -> GAME -> OVER (BACK TO BET OR END PASSAGE)
		private chatLog: string[] | null = null;
		private interval: any;

		get ReturnPassage() {return this.returnPassage;}
		set ReturnPassage(v: string) {this.returnPassage = v;}


		SetupGame(returnPassage: string, casino = false) {
			console.log("Setting up game of Coffin…");
			this.ReturnPassage = returnPassage;
			this.casinoFlag = casino;
			$(document).one(":passageend", this._SetupGame.bind(this));
		}

		PrintResults() {
			var buffer = "";
			// Tally money
			var cash = this._TotalMoney();
			var items = this._TotalItems();
			var sex = this._TotalSex();

			if (cash == 0 && items.length == 0 && sex.length == 0) {
				return "You walk away from the game, none the richer, but none the worse for wear. Maybe you'll play next time?";
			}

			buffer += "The game is over and it's time to settle accounts…\n\n";

			buffer += this._PrintSex(sex);
			buffer += this._PrintItems(items);

			if (cash > 0) buffer += "You @@.state-positive;collect@@ @@.item-money;" + cash + " coins@@ in winnings.";
			if (cash < 0) buffer += "You @@.state-negative;pay@@ @@.item-money;" + Math.abs(cash) + " coins@@ to cover your losses.";

			this._AwardSex(sex);
			this._AwardItems(items);

			return buffer;
		}

		private _PrintItems(items) {
			var buffer = "";
			for (var i = 0; i < items.length; i++) {
				buffer += "@@color:cyan;" + items[i].name + "@@ gives you " + items[i].data.desc + ".\n\n";
			}

			return buffer;
		}

		// Fetch a random sex scene, then format it.
		private _PrintSex(sex: Sex[]) {
			var buffer = "";
			var lastName: string, lastSex = "";
			for (var i = 0; i < sex.length; i++) {
				// Don't repeat sex scenes on pirates if they are the same.
				if (lastName == sex[i].name && lastSex == sex[i].sex) continue;
				lastName = sex[i].name;
				lastSex = sex[i].sex;
				// Fetch an appropriate random passage by skill.
				var passage = App.PR.GetRandomListItem(Scenes[sex[i].sex]);
				// Make a fake npc to pass to tokenizer.
				var npc = Entity.Player.instance.GetNPC("Dummy"); //Fake NPC
				npc.Name = sex[i].name;
				passage = App.PR.TokenizeString(Entity.Player.instance, npc, passage); // Tokenize passage
				// Fetch picture
				var num = Math.max(1, Math.min(Math.round(Math.random() * 10), 10));
				var pic;
				switch (sex[i].sex) {
					case "AssFucking": pic = "ss_anal_" + num; break;
					case "HandJobs": pic = "ss_hand_" + num; break;
					case "BlowJobs": pic = "ss_bj_" + num; break;
					case "TitFucking": pic = "ss_tit_" + num; break;
					default: pic = "ss_anal_" + num; break;
				}

				buffer += "\
            <div style='width:800px'><div style='float:right;width:180px;height:122px' class='"+ pic + "'></div>" + passage + "\n\n</div>";
			}

			return buffer;
		}

		// Sex that needs to be paid
		private _TotalSex() {
			var sex: Sex[] = [];

			for (var i = 0; i < this.gamblers.length; i++) {
				var o = this.gamblers[i];
				var s: Sex; ;
				if (o.bet1Want != "Coins" && o.bet1Status == BetStatus.Lose) {
					sex.push({name: o.n, val: o.bet1Value, sex: this.skills[o.bet1Want]});
				}

				if (o.bet2Want != "Coins" && o.bet2Status == BetStatus.Lose) {
					sex.push({name: o.n, val: o.bet2Value, sex: this.skills[o.bet2Want]});
				}
			}

			if (sex.length > 0) {
				var cashValue = sex.length > 1 ? sex.reduce((acc, obj) => acc + obj.val, 0) : sex[0].val;

				this._TrackStat('SexPaid', cashValue);
			}
			return sex;
		}

		private _AwardSex(sex: Sex[]) {

			for (var i = 0; i < sex.length; i++) {
				// Call skill check to award xp, then fetch the modifier back to use
				// as an indicator of how much to adjust the NPC's feelings.
				var result = setup.player.SkillRoll(sex[i].sex, sex[i].val, 1, true);
				setup.player.GetNPC("Crew").AdjustStat("Mood", Math.ceil((5 * result)));
				setup.player.GetNPC("Crew").AdjustStat("Lust", Math.ceil((-5 * result)));
			}
		}

		private _AwardItems(items) {
			for (var i = 0; i < items.length; i++)
			setup.player.AddItem(items[i].data.cat, items[i].data.tag, 0);
		}

		private _AdjustMoney(status) {
			var o = this.gamblers[this.gamblerPosition];
			var offer = this.selectedBet == 1 ? o.bet1Offer : o.bet2Offer;
			var want = this.selectedBet == 1 ? o.bet1Want : o.bet2Want;
			var val = this.selectedBet == 1 ? o.bet1Value : o.bet2Value;

			if (status == 1 && want == "Coins") {   //Player lost
				Entity.Player.instance.spendMoney(val, Entity.Spending.Gambling);
				App.PR.RefreshTwineMoney();

			} else if (status == 2 && offer == "Coins") { //Player won
				Entity.Player.instance.earnMoney(val, Entity.Income.Gambling);
				App.PR.RefreshTwineMoney();
			}

		}

		// Items won.
		private _TotalItems() {
			var items: {data: Items.ItemListingRecord, name: string}[] = [];
			var scale = function (n) {
				var out = n + Math.ceil(n * Math.random()) + Math.ceil((n / 2) * Math.random());
				console.log('Item adjusted from value (' + n + ') to (' + out + ')');
				return out;
			};

			for (var i = 0; i < this.gamblers.length; i++) {
				var o = this.gamblers[i];
				var d: Items.ItemListingRecord | null = null;

				if (o.bet1Offer == "Item" && o.bet1Status == BetStatus.Win)
					d = App.Items.PickItem(['FOOD', 'DRUGS', 'COSMETICS'], {price: scale(o.bet1Value)});

				if (o.bet2Offer == "Item" && o.bet2Status == BetStatus.Win)
					d = App.Items.PickItem(['FOOD', 'DRUGS', 'COSMETICS'], {price: scale(o.bet2Value)});

				if (d != null) {
					items.push({data: d, name: o.n});
				}
			}

			if (items.length > 0) {
				this._TrackStat('ItemsWon', items.length);
				var cashValue = items.length > 1 ? items.reduce(function (acc, obj) { //Reduce objects if needed.
					return acc + obj.data.price;
				}, 0) : items[0].data.price;

				this._TrackStat('ItemsWonValue', cashValue);
			}
			return items;
		}

		// Money won or lost.
		private _TotalMoney() {
			var cash = 0;
			for (var i = 0; i < this.gamblers.length; i++) {
				var o = this.gamblers[i];
				if (o.bet1Offer == "Coins" && o.bet1Status == 2) cash += o.bet1Value;
				if (o.bet1Want == "Coins" && o.bet1Status == 1) cash -= o.bet1Value;
				if (o.bet2Offer == "Coins" && o.bet2Status == 2) cash += o.bet2Value;
				if (o.bet2Want == "Coins" && o.bet2Status == 1) cash -= o.bet2Value;
			}

			if (cash > 0) this._TrackStat('CoinsWon', cash);
			if (cash < 0) this._TrackStat('CoinsLost', Math.abs(cash));
			return cash;
		}

		// Setup functions
		private _SetupGame() {

			if (this.screen == "NEW") {
				this._CreateGamblers(this.casinoFlag);
				this._SetupPoints();
				this._ButtonSetup();
				this._DisplayGambler(0);
			} else if (this.screen == "BET") {
				this._SetupPoints();
				this._ButtonSetup();
				this._cbStartGame();
				this._DisplayGambler(this.gamblerPosition);
				this._HighlightBet();
			} else if (this.screen == "GAME" || this.screen == "OVER") {
				this._ReDrawPoints();
				this._ButtonSetup();
				this._PrintRound();
				$('#CoffinBetContainer').css('display', 'none');
				$('#CoffinDiceContainer').css('display', 'block');
				$('#betButtons').css('display', 'none');

				if (this.playerTurn == true && this.screen == "GAME") {
					this._DialogBox("YOUR TURN", "gold");
					$('#playButtons').css('display', 'block');
				} else {
					$('#playButtons').css('display', 'none');
					$('#endButtons').css('display', 'block');
					if (this.gamesPlayed > this.maxGames) $('#cmdPlayAgain').css('display', 'none');
				}
			}

			this._RefreshChatLog();
		}

		private _CreateGamblers(flag = false) {
			this.gamblers = [];
			this.gamblerPosition = 0;
			this.selectedBet = null;
			this.roundNum = 1;
			this.gamesPlayed = 1;
			this.chatLog = null;

			for (var i = 0; i < 6; i++) {
				let n = App.PR.GetRandomListItem(App.Data.Names["Male"]);
				if (!flag) {
					n = "Pirate " + n;
					const bet1Offer = (100 * Math.random()) > 90 ? "Item" : "Coins";
					const bet1Want = (100 * Math.random() > 40) ? "Coins" : Math.round((this.wants.length - 1) * Math.random());
					const bet1Value = this._CalculateBetValue(bet1Offer, bet1Want);
					const bet1Status = 0; // 0 nothing, 1 lose, 2 win

					const bet2Offer = (100 * Math.random()) > 90 ? "Item" : "Coins";
					const bet2Want = (100 * Math.random() > 40) ? "Coins" : Math.round((this.wants.length - 1) * Math.random());
					const bet2Value = this._CalculateBetValue(bet2Offer, bet2Want);
					const bet2Status = 0;

					// Add lust
					var npcLust = Entity.Player.instance.GetNPC("Crew").Lust;
					// 1-10 + random 0-40
					const lust = (Math.floor(npcLust / 10)) + Math.floor(((40 * npcLust / 100) * Math.random()));
					this.gamblers.push({n, bet1Offer, bet1Want, bet1Value, bet1Status, bet2Offer, bet2Want, bet2Value, bet2Status, lust});
				} else {
					const bet1Offer = "Coins";
					const bet1Want = "Coins";
					const bet1Value = 50 + Math.ceil(50 * Math.random());
					const bet1Status = 0;

					const bet2Offer = "Coins";
					const bet2Want = "Coins";
					const bet2Value = 100 + Math.ceil(100 * Math.random());
					const bet2Status = 0;

					const lust = 50;
					this.gamblers.push({n, bet1Offer, bet1Want, bet1Value, bet1Status, bet2Offer, bet2Want, bet2Value, bet2Status, lust});
				}
			}
		}

		private _CalculateBetValue(Offer, Want) {
			if ((Offer == "Item" || Offer == "Coins") && Want == "Coins") return 25 + Math.ceil(25 * Math.random());
			return Math.max(Math.ceil(25 * Math.random()) + Math.ceil(75 * (Entity.Player.instance.Skills[this.skills[Want]] / 100)), 5);
		}

		private _DisplayGambler(pos = 0) {
			const g = this.gamblers[pos];
			console.log(g);
			$("#CoffinNPCName").text(g.n);

			//Setup first bet.
			var icon = g.bet1Offer == "Coins" ? "coinBetIcon" : "itemBetIcon";
			var value = g.bet1Offer == "Coins" ? g.bet1Value : "";
			$('#betIcon1a').removeClass().addClass(icon).text(value);

			icon = g.bet1Want == "Coins" ? "coinBetIcon" : this.icons[g.bet1Want];
			value = g.bet1Want == "Coins" ? g.bet1Value : "";
			$('#betIcon1b').removeClass().addClass(icon).text(value);

			this._DecorateBet(g, 1);

			//Setup second bet.
			icon = g.bet2Offer == "Coins" ? "coinBetIcon" : "itemBetIcon";
			value = g.bet2Offer == "Coins" ? g.bet2Value : "";
			$('#betIcon2a').removeClass().addClass(icon).text(value);

			icon = g.bet2Want == "Coins" ? "coinBetIcon" : this.icons[g.bet2Want];
			value = g.bet2Want == "Coins" ? g.bet2Value : "";
			$('#betIcon2b').removeClass().addClass(icon).text(value);
			this._DecorateBet(g, 2);
		}

		private _DecorateBet(g, n) {
			var value = n == 1 ? g.bet1Value : g.bet2Value;
			var want = n == 1 ? g.bet1Want : g.bet2Want;
			var status = n == 1 ? g.bet1Status : g.bet2Status;
			var elementID = n == 1 ? "#CoffinBet1Blocker" : "#CoffinBet2Blocker";
			var opts = n == 1 ? {on: 1, off: 2} : {on: 2, off: 1};

			var e = $(elementID).empty().removeClass(); // Clear
			var d = $('<div>');

			if (want == "Coins" && Entity.Player.instance.Money < value) { // Not enough cash.
				e.addClass("disabledGamePanel");
				d.html('<span style="color:red">Not enough money.</span>');
				e.append(d);
			} else if (value == 0) { // Player sex skill too low for pirate lust
				e.addClass("disabledGamePanel");
				d.html("<span style='color:red'>Not skillfull enough.</span>");
				e.append(d);
			} else if (status != 0) {
				e.addClass("disabledGamePanel");
				var s = status == 1 ? "<span style='color:red'>Bet already lost.</span>" : "<span style='color:lime'>Bet already won.</span>";
				d.html(s);
				e.append(s);
			} else {
				e.addClass("activeGamePanel").on("click", opts, this._cbSelectBet.bind(this));
			}
		}

		private _SetupPoints() {
			this.myScore = [0, 0, 0, 0, 0, 0, 0, 0];
			this.opponentScore = [0, 0, 0, 0, 0, 0, 0, 0];
			this.suddenDeath = false;

			for (var i = 0; i < this.myScore.length; i++) {
				$('#pcbox' + i).removeClass().addClass('coffinNumBox CoffinpointEmpty');
				$('#npcbox' + i).removeClass().addClass('coffinNumBox CoffinpointEmpty');
			}
		}

		private _ReDrawPoints() {
			for (var i = 0; i < this.myScore.length; i++) {
				if (this.myScore[i] == 1) {
					$('#pcbox' + i).removeClass().addClass('coffinNumBox CoffinpointHit');
					$('#npcbox' + i).removeClass().addClass('coffinNumBox CoffinpointMissed');
				} else if (this.opponentScore[i] == 1) {
					$('#pcbox' + i).removeClass().addClass('coffinNumBox CoffinpointMissed');
					$('#npcbox' + i).removeClass().addClass('coffinNumBox CoffinpointHit');
				} else {
					$('#pcbox' + i).removeClass().addClass('coffinNumBox CoffinpointEmpty');
					$('#npcbox' + i).removeClass().addClass('coffinNumBox CoffinpointEmpty');
				}

			}
		}

		private _ButtonSetup() {
			// Initial buttons
			$('#cmdQuit').on("click", this._cbQuitGame.bind(this));
			$('#cmdStart').on("click", this._cbStartGame.bind(this));

			//Betting Phase buttons;
			$('#cmdNextBet').on("click", this._cbNextBet.bind(this));
			$('#cmdPrevBet').on("click", this._cbPrevBet.bind(this));
			$('#cmdTakeBet').on("click", this._cbTakeBet.bind(this));
			$("#cmdQuitBet").on("click", this._cbQuitGame.bind(this));

			//Game playing buttons
			$('#cmdRollDice').on("click", this._cbRollDice.bind(this));
			$('#cmdPlayAgain').on("click", this._cbPlayAgain.bind(this));
			$("#cmdQuitPlay").on("click", this._cbQuitGame.bind(this));
		}

		private _HighlightBet(data?: {off: string}) {
			if (this.selectedBet != null) {
				$('#CoffinBet' + this.selectedBet + 'Blocker').addClass("selectedBet");
				if (typeof data !== 'undefined')
					$('#CoffinBet' + data.off + 'Blocker').removeClass("selectedBet");
			}
		}

		private _PrintRound() {
			var str = this.playerTurn == true ? "<span style='color:green'>YOUR TURN</span>" : "<span style='color:red'>OPPONENTS TURN</span>";
			str = this.suddenDeath == true ? str + " <span style='color:orange'>SUDDEN DEATH</span>" : str;
			$('#RoundContainer').css('display', 'block').html("<span>Round " + this.roundNum + "/" + this.maxRounds + "</span> " + str);
		}

		private _PrintGames() {
			$('#GamesPlayedContainer').css("display", 'block').html('<span>Games played ' + this.gamesPlayed + '/' + this.maxGames + '</span>');
		}

		private _WriteChat(msg: string) {
			var o = this.gamblers[this.gamblerPosition];
			var npc = Entity.Player.instance.GetNPC("Dummy"); // Fake NPC
			npc.Name = o.n; // Swapsie name
			msg = App.PR.TokenizeString(Entity.Player.instance, npc, msg);
			$('#cofUItray').append("<P>" + msg + "</P>");
			$('#cofUItray').animate({scrollTop: $('#cofUItray').prop("scrollHeight")}, 1000);
			if (this.chatLog == null) {
				this.chatLog = [];
			}
			this.chatLog.push(msg);

		}

		private _WriteStartChat() {
			var npcwager = this._GetOfferBetString();
			var pcwager = this._GetWantBetString();
			var msg = "\
        You start to play dice with NPC_NAME.\n\
        The wager is his "+ npcwager + " for " + pcwager + ".\n";
			this._WriteChat(msg);
		}

		private _RefreshChatLog() {
			if (this.chatLog != null) {
				for (var i = 0; i < this.chatLog.length; i++) {
					$('#cofUItray').append("<P>" + this.chatLog[i] + "</P>");
				}
				$('#cofUItray').scrollTop($('#cofUItray').prop("scrollHeight"));
			}
		}

		private _GetOfferBetString() {
			var o = this.gamblers[this.gamblerPosition];
			var bet = this.selectedBet == 1 ? o.bet1Offer : o.bet2Offer;
			var val = this.selectedBet == 1 ? o.bet1Value : o.bet2Value;
			if (bet == "Coins") return val + " coins";
			if (bet == "Item") return "a mystery item";
		}

		private _GetWantBetString() {
			var o = this.gamblers[this.gamblerPosition];
			var bet = this.selectedBet == 1 ? o.bet1Want : o.bet2Want;
			var val = this.selectedBet == 1 ? o.bet1Value : o.bet2Value;
			if (bet == "Coins") return val + " coins";
			return this.nouns[bet];
		}

		// Callbacks
		private _cbQuitGame() {
			console.log("_cbQuitGame called");
			Engine.play("CoffinGameEnd");
			this.screen = "NEW"; // force the game to reset the next time it's loaded.
			this.playerTurn = true;
		}

		private _cbStartGame() {
			console.log("_cbStartGame called");
			$('#CoffinBetContainer').css("display", "block");
			$('#startButtons').css("display", "none");
			$('#betButtons').css("display", "block");
			if (this.selectedBet == null) this._DialogBox("PLACE BET", "gold");
			this.screen = "BET";
		}

		private _cbPlayAgain() {
			console.log("_cbPlayAgain called");
			this._SetupPoints();
			$('#CoffinBetContainer').css("display", "block");
			$('#CoffinDiceContainer').css('display', 'none');
			$('#endButtons').css("display", "none");
			$('#betButtons').css("display", "block");
			$('#RoundContainer').css('display', 'none');
			if (this.selectedBet == null) this._DialogBox("PLACE BET", "gold");
			this._DisplayGambler(this.gamblerPosition);
			this.playerTurn = true;
			this.suddenDeath = false;
			this.roundNum = 1;
			this.screen = "BET";
		}

		private _cbNextBet() {
			var pos = this.gamblerPosition + 1 >= this.gamblers.length ? 0 : this.gamblerPosition + 1;
			this._DisplayGambler(pos);
			this.gamblerPosition = pos;
			this.selectedBet = null;
			$('#CoffinBet1Blocker').removeClass("selectedBet");
			$('#CoffinBet2Blocker').removeClass("selectedBet");
		}

		private _cbPrevBet() {
			var pos = this.gamblerPosition - 1 < 0 ? this.gamblers.length - 1 : this.gamblerPosition - 1;
			this._DisplayGambler(pos);
			this.gamblerPosition = pos;
			this.selectedBet = null;
		}

		// This is where we place the bet and start the game playing
		private _cbTakeBet() {
			if (this.selectedBet == null) {
				this._DialogBox("PLACE BET", "gold");
				return;
			}
			$('#CoffinBetContainer').css('display', 'none');
			$('#CoffinDiceContainer').css('display', 'block');
			$('#betButtons').css('display', 'none');
			$('#playButtons').css('display', 'block');
			this.screen = "GAME";

			if ((this.gamesPlayed % 2) == 0) {
				this._DialogBox("YOUR TURN", "gold");
			} else {
				this._DialogBox("OPPONENTS TURN", "gold");
				$("#cmdRollDice").css('display', 'none');
				this.playerTurn = false;
				this.interval = setInterval(this._NPCRollDice.bind(this), this._NPCDiceDelay());
				this._DisableMenuLinks();
			}

			this._PrintRound();
			this._PrintGames();
			this._WriteStartChat();
			this._TrackStat("Played", 1);
		}

		private _cbSelectBet(e) {
			this.selectedBet = e.data.on;
			this._HighlightBet(e.data);
		}

		private _cbRollDice() {
			if (this.diceRolling == true || this.playerTurn == false) return; //noop
			rollADie({element: document.getElementById('CoffinDiceContainer') as HTMLElement, numberOfDice: 2, callback: this._cbDiceRolled.bind(this), delay: 3000});
		}

		private _cbDiceRolled(e) {
			var point = this._CheckPoint(e);
			if (point != -1) this._AssignPoint(point);
			var win = this._CheckWin(e);

			//Scored hit.
			if (point != -1 || (this.suddenDeath == true && win != 0)) {

				if (win != 0) { //Something happened and we need to end the game.

					this.screen = "OVER";

					if (this.playerTurn == false) {
						clearInterval(this.interval); // Stop the npc if he's rolling
						this._EnableMenuLinks();
						$("#cmdRollDice").css('display', 'inline-block'); // turn button back on
					}

					this.gamesPlayed += 1;

					// switch button trays
					$('#playButtons').css("display", "none");
					$('#endButtons').css("display", "block");
					if (this.gamesPlayed > this.maxGames) $('#cmdPlayAgain').css('display', 'none');
					if (win == 1 || win == 2) {
						if (this.selectedBet == 1) {
							this.gamblers[this.gamblerPosition].bet1Status = win;
						} else {
							this.gamblers[this.gamblerPosition].bet2Status = win;
						}

					}
				}

			} else {
				// Code for handling misses

				if (this.playerTurn) {
					this._WriteChat("<span style='color:red'>You miss the roll!</span>");
				} else {
					this._WriteChat("NPC_NAME <span style='color:red'>misses the roll!</span>");
				}

				var that = this;
				if ((this.gamesPlayed % 2) == 1) { // THE NPC IS PLAYER 1

					if (this.playerTurn == true) { // THE PC WAS THE ONE ROLLING
						this.roundNum++; // END THIS ROUND
						this._PrintRound();

						if (this.roundNum >= this.maxRounds) { //Check to see who wins or DRAW
							if (this._TotalPoints(this.myScore) > this._TotalPoints(this.opponentScore)) {
								this._DialogBox("YOU WIN!", "gold");
								this._WriteChat("You won the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat("Won", 1);
								this.gamesPlayed += 1;
								this._AdjustMoney(2);
								if (this.selectedBet == 1) {
									this.gamblers[this.gamblerPosition].bet1Status = 2; // win
								} else {
									this.gamblers[this.gamblerPosition].bet2Status = 2;
								}
							} else if (this._TotalPoints(this.myScore) < this._TotalPoints(this.opponentScore)) {
								this._DialogBox("YOU LOSE!", "red");
								this._WriteChat("You lost the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat("Lost", 1);
								this.gamesPlayed += 1;
								this._AdjustMoney(1);
								if (this.selectedBet == 1) {
									this.gamblers[this.gamblerPosition].bet1Status = 1; // lose
								} else {
									this.gamblers[this.gamblerPosition].bet2Status = 1;
								}
							} else {
								this._DialogBox("DRAW", "gold");
								this.gamesPlayed += 1;
								this._WriteChat("The game ends in a draw.");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat('Draw', 1);
							}
						} else {
							// HAND OVER CONTROL TO NPC
							$("#cmdRollDice").css('display', 'none'); // HIDE ROLL BUTTON
							setTimeout(function () {that._DialogBox("OPPONENTS TURN", "gold")}, 500);
							this.playerTurn = false;
							this.interval = setInterval(this._NPCRollDice.bind(this), this._NPCDiceDelay());
							this._DisableMenuLinks();
							this._PrintRound();
						}

					} else { // THE NPC WAS ROLLING

						clearInterval(this.interval); // STOP NPC ROLLING
						this._EnableMenuLinks();
						$("#cmdRollDice").css('display', 'inline-block'); // SHOW ROLL BUTTON
						// HAND OVER CONTROL TO PC
						this.playerTurn = true;
						setTimeout(function () {that._DialogBox("YOUR TURN", "gold")}, 500);

					}
					// TURN OFF PLAY AGAIN BUTTON AT MAX GAMES
					if (this.gamesPlayed > this.maxGames) $('#cmdPlayAgain').css('display', 'none');

				} else { // THE PC IS PLAYER 1
					if (this.playerTurn == true) { // THE PC WAS THE ONE ROLLING

						// HAND OVER CONTROL TO NPC
						$("#cmdRollDice").css('display', 'none'); // HIDE ROLL BUTTON
						setTimeout(function () {that._DialogBox("OPPONENTS TURN", "gold")}, 500);
						this.playerTurn = false;
						this.interval = setInterval(this._NPCRollDice.bind(this), this._NPCDiceDelay());
						this._DisableMenuLinks();

					} else { // THE NPC WAS ROLLING
						this.roundNum++; //END THIS ROUND
						this._PrintRound();

						if (this.roundNum >= this.maxRounds) { //DRAW THE GAME.
							if (this._TotalPoints(this.myScore) > this._TotalPoints(this.opponentScore)) {
								this._DialogBox("YOU WIN!", "gold");
								this._WriteChat("You won the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat("Won", 1);
								this.gamesPlayed += 1;
								this._AdjustMoney(2);
								if (this.selectedBet == 1) {
									this.gamblers[this.gamblerPosition].bet1Status = 2; // win
								} else {
									this.gamblers[this.gamblerPosition].bet2Status = 2;
								}
							} else if (this._TotalPoints(this.myScore) < this._TotalPoints(this.opponentScore)) {
								this._DialogBox("YOU LOSE!", "red");
								this._WriteChat("You lost the match!");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat("Lost", 1);
								this.gamesPlayed += 1;
								this._AdjustMoney(1);
								if (this.selectedBet == 1) {
									this.gamblers[this.gamblerPosition].bet1Status = 1; // lose
								} else {
									this.gamblers[this.gamblerPosition].bet2Status = 1;
								}
							} else {
								this._DialogBox("DRAW", "gold");
								this.gamesPlayed += 1;
								this._WriteChat("The game ends in a draw.");
								$('#playButtons').css("display", "none");
								$('#endButtons').css("display", "block");
								this._TrackStat('Draw', 1);
							}

						}

						this.playerTurn = true;
						clearInterval(this.interval); // STOP NPC ROLLING
						this._EnableMenuLinks();
						setTimeout(function () {that._DialogBox("YOUR TURN", "gold")}, 500);
						$("#cmdRollDice").css('display', 'inline-block'); // SHOW ROLL BUTTON
					}
					// TURN OFF PLAY AGAIN BUTTON AT MAX GAMES
					if (this.gamesPlayed > this.maxGames) $('#cmdPlayAgain').css('display', 'none');
				}

			}

		}

		private _DisableMenuLinks() {
			var container = $('#ui-bar');
			container.append($('<div>').attr('id', 'CoffinMenuBlockerTop'));
			container.append($('<div>').attr('id', 'CoffinMenuBlockerBottom'));

		}

		private _EnableMenuLinks() {
			$('#CoffinMenuBlockerTop').remove();
			$('#CoffinMenuBlockerBottom').remove();
		}

		private _NPCRollDice() {
			rollADie({element: document.getElementById('CoffinDiceContainer') as HTMLElement, numberOfDice: 2, callback: this._cbDiceRolled.bind(this), delay: 3000});
		}

		private _NPCDiceDelay() {
			return (SugarCube.settings.fastAnimations == true) ? 1000 : 2500;
		}

		private _CheckWin(e) {
			var me = this._TotalPoints(this.myScore);
			var him = this._TotalPoints(this.opponentScore);

			//Doubles are an automatic win.
			if (this.suddenDeath == true && (e[0] == e[1])) {
				this._RisingDialog("DOUBLES", "orange");
				if (this.playerTurn == true) {
					this._DialogBox("YOU WIN!", "gold");
					this._WriteChat("You won the match!");
					this._TrackStat("Won", 1);
					this._AdjustMoney(2);
					return 2;
				} else {
					this._DialogBox("YOU LOSE!", "red");
					this._WriteChat("You lost the match!");
					this._TrackStat("Lost", 1);
					this._AdjustMoney(1);
					return 1;
				}
			}

			if (me == 4 && him == 4) {
				this._DialogBox("DRAW", "gold");
				this._WriteChat("The game ends in a draw.");
				this._TrackStat("Draw", 1);
				return -1;
			} else if (me >= 5) {
				this._DialogBox("YOU WIN!", "gold");
				this._WriteChat("You won the match!")
				this._TrackStat("Won", 1);
				this._AdjustMoney(2);
				return 2;
			} else if (him >= 5) {
				this._DialogBox("YOU LOSE!", "red");
				this._WriteChat("You lost the match!");
				this._TrackStat("Lost", 1);
				this._AdjustMoney(1);
				return 1;
			} else if (me + him == 7) {
				if (this.suddenDeath == false) {
					this._RisingDialog("SUDDEN DEATH", "orange");
					this._WriteChat("The game enters <span style='color:orange'>SUDDEN DEATH!</span>")
				}
				this.suddenDeath = true;
				return 0;
			}

			return 0;
		}

		private _CheckPoint(dice: number[]) {
			var point = dice.reduce(function (a, b) {return a + b;});
			if (point > 9) return -1; // Out of range of any possible points.
			var index = point - 2; // shift 2-9 to 0-7
			if (this.myScore[index] == 1 || this.opponentScore[index] == 1) return -1; // Point already assigned.
			return index;
		}

		private _AssignPoint(index: number) {
			if (this.playerTurn == true) {
				this.myScore[index] = 1;
				$('#pcbox' + index).addClass("CoffinpointHit");
				$('#npcbox' + index).addClass("CoffinpointMissed");
				this._WriteChat("<span style='color:lime'>You hit the " + (index + 2) + "!</span>");
			} else {
				this.opponentScore[index] = 1;
				$('#npcbox' + index).addClass("CoffinpointHit");
				$('#pcbox' + index).addClass("CoffinpointMissed");
				this._WriteChat("NPC_NAME <span style='color:lime'>hits the " + (index + 2) + "!</span>");
			}
		}

		private _TotalPoints(Points: number[]) {
			return Points.reduce(function (a, b) {return a + b;});
		}

		private _DialogBox(message: string, color: string) {
			var props = {top: "90px", right: "0px", width: "800px", color: "DeepPink"};
			if (typeof color !== 'undefined') props.color = color;
			App.PR.DialogBox(this.element, message, props);
		}

		private _RisingDialog(message: string, color: string) {
			App.PR.RisingDialog(this.element, message, color);
		}

		private _TrackStat(stat: string, value: number) {
			if (Entity.Player.instance.GameStats.COFFIN.hasOwnProperty(stat))
				Entity.Player.instance.GameStats.COFFIN[stat] += value;
		}
	}

	// Sex scenes
	var Scenes: Record<string, string[]> = {};

	Scenes["HandJobs"] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced cock stroker. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. You'd love to be able to suck on it, or even \
    better, have him hammer away at your sissy asshole, but the bet was only for a quick handy, and it'd be \
    unprofessional of you to put your pleasure ahead of business.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    You marvel at it's size and girth and feel a faint flutter tingle up your backside. Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the twitching in your arsehole and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You take NPC_NAME's cock firmly in hand and start to gently tug him to erection. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You rub his cock back and forth across your \
    your face, looking him in the eyes while you slather it with your own drool for lubrication. Satisified that he's \
    now wet enough, you begin to stroke his meat in ernest. Within minutes he's panting and ready to blow. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    With practiced care, you angle his errection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and you \
    eagerly lick it up, earning you a smile from NPC_NAME.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    With practiced care, you angle his errection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and it \
    takes a surprisingly amount of restraint for you to resist simply licking it up.\n\n\
    <<else>>\
    You try your best to avoid the stream of jizz coming right at you, but unfortunately most of it lands on your face. \
    Apparently, NPC_NAME doesn't notice your look of disgust, but how could he since you're practically covered in a thick \
    layer of his baby batter.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a good wank)\
    "
		];

	Scenes["BlowJobs"] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced cock sucker. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. Being able to suck on such a beautiful \
    dick hardly seems like a penalty for losing a bet, but alas here you are, about to get a mouth full of \
    delicious dong.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    You marvel at it's size and girth and involuntarily lick your lips, a slight trail of drool \
    escapes them and pools on your chest… Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the desire welling up in you and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You take NPC_NAME's cock firmly in hand and start to gently tug him to errection. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You rub his cock back and forth across your \
    your face, looking him in the eyes while you run it back and forth across your lips. Slowly you engulf the \
    head, rolling your tongue across it with long circular motions. NPC_NAME's dick instantly becomes as hard \
    as a rock and your start to suck on it in earnest. It doesn't take long after that before he's holding the \
    side of your head and jackhammering his dick down your throat. Without warning he smashes his pelvis into \
    your face and seizes up, a hot torrent of cum bubbling forth from his balls. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    Your eyes widen as his hot jizz starts to pour down your throat, you quickly move your head back so that the \
    remaining ejaculate pools your mouth. Moaning, you play with it on your tongue and savor it's flavor.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    Your eyes widen as his hot jizz starts to pump down your throat. Unbidden, a strong desire to taste this \
    man's cum springs forth in your mind - a shocking relevation to say the least. Fortunately, or perhaps \
    unfortunately, he's holding you too tightly and all you can do is whimper while his dick twitches in \
    your mouth.\n\n\
    <<else>>\
    You try your best to pull away, but NPC_NAME is holding you firmly and your forced to try to desperately \
    breathe through your nose as a seemingly endless amount of sperm is pumped into your stomach.\n\n\
    <</if>>\
    <</if>>\
    Eventually NPC_NAME is spent and he pulls out of your mouth with a load 'pop', a trail of your saliva mixed \
    with his cum spurts from your lips and lands on your chest.\n\n\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a blow job)\
    "
		];

	Scenes["TitFucking"] =
		[
			"You kneel in front of NPC_NAME and assume the familiar position of an experienced whore. He quickly fetches his \
    impressive schlong out of his pants and waves it lightly in your face. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    You marvel at it's size and girth and involuntarily lick your lips. You'd love to be able to suck on it, or even \
    better, have him hammer away at your sissy asshole, but the bet was only for a titty fuck, and it'd be \
    unprofessional of you to put your pleasure ahead of business.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    You marvel at it's size and girth and involuntarily lick your lips, a slight trail of drool \
    escapes them and pools on your chest… Wait? What?! When did you \
    start not only finding the cocks of other men impressive, but… sexually desirable? You concentrate and try to \
    suppress the desire welling up in you and instead get down to the task at hand.\n\n\
    <<else>>\
    It's bigger than average and you feel a slight tinge of envy, coupled with the shame of having to give this \
    bastard some sexual release, but a bet is a bet and a whore is a whore and unfortunately for you, you're a \
    whore who lost a bet. Time to get to work.\n\n\
    <</if>>\
    <</if>>\
    You engulf NPC_NAME's cock with your nBUST and start to gently massage it. You feel his member stiffen \
    under your ministrations and eventually he lets loose a small gasp. You gently lick the tip of his \
    dick, letting large streams of saliva drool out of your mouth to coat his member and your tits. Once \
    sufficiently lubricated, you begin to earnestly jack him off with your fuck bags. It doesn't take long \
    after that before you feel his hands grasp your shoulders and his shoulders stiffen. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    With practiced care, you angle his errection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your forehead, but some of it drips down to your lips and you \
    eagerly lick it up, earning you a smile from NPC_NAME.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    With practiced care, you angle his errection act your face and with a couple of final pumps, he's spurting his \
    load all over you. The majority of it lands on your tits, but some of it spatters your lips and it \
    takes a surprisingly amount of restraint for you to resist simply licking it up.\n\n\
    <<else>>\
    You try your best to avoid the stream of jizz coming right at you, but unfortunately most of it lands on your face and tits. \
    Apparently, NPC_NAME doesn't notice your look of disgust, but how could he since you're practically covered in a thick \
    layer of his baby batter.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a tit fuck)\
    "
		];

	Scenes["AssFucking"] =
		[
			"You bend over in front of NPC_NAME, your bare ass and sissy slut hole exposed to the air. \
    You feel something wet and warm being rubbed between your arsecheeks, then a finger starts to gently \
    probe your anus. After a couple of minutes of these ministrations, the finger is replaced with the head \
    of a meaty cock. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    You shudder in anticipation. You weren't always such an anal whore, but now that you have a taste for it \
    you can't imagine going back. Just the thought of what's about to happen causes your nPENIS to stiffen and \
    your sissy asshole to quiver in excitement.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    You catch yourself in act of starting to drive back your sissy asshole to engulf his cock. A deep feeling of \
    horror wells up in your breast as you realize that you've begun to not only enjoy, but to greet the prospects \
    of such perverse acts with anticipation.\n\n\
    <<else>>\
    Your stomach does a small flip and you begin to feel ill at the prospect of another man ravaging your \
    slutty asshole. You do your best to ignore the feeling and swear to yourself that no matter what, you'll \
    never succumb to the life of a whore… even if sometimes it does feel a little good.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME works his tool up your well lubricated backside. His hands firmly grip onto your nHIPS and \
    he uses them for leverage as he starts to pounds your pASS arse. \
    <<if setup.player.GetStat('STAT', 'Perversion') > 80>>\
    You've long become accustomed to cocks pounding your sissy hole and the feeling it provides is one of \
    intense pleasure. Within moments you already feel your own orgasm building up as NPC_NAME continues to \
    mercilessly rail you. Eventually, he cums hard - impaling himself in you up to the hilt. The deep action \
    stirs something inside you and you feel your pBALLS start to well with your own cum. You scream out \
    sp(Yes! Fuck me! Fuck my ass!) and have an intense orgasm as NPC_NAME empties himself deep within your \
    slutty ass.\n\n\
    <<else>>\
    <<if setup.player.GetStat('STAT', 'Perversion') > 50>>\
    Even though you've become accustomed to cocks pounding your sissy hole, the feeling at first is quite \
    painful. You grit your teeth and endure while the sound of NPC_NAME slapping against your nASS \
    echoes in air. Slowly, you notice that the feeling of pain starts to turn into one of pleasure. You panick \
    at first, but the almost hypnotic sound of your ass being fucked combined with these new sensations \
    start to cause your breath to hitch and your mind to go blank. You almost don't even notice it when \
    NPC_NAME finally cums, impaling himself deep within your bowels. You let out a cry of shock, but one \
    tinged wth an undercurrent of desire.\n\n\
    <<else>>\
    The feeling of NPC_NAME's cock entering you is an almost indescribable mix of pain with an undercurrent \
    of something that your mind refuses to recognize as pleasure. The slapping sounds that fill the air from \
    NPC_NAME pillaging your arse form a strange cadence and you find yourself disassociating with your body \
    while you endure the act. Somewhere deep in the back of your mind a faint seed of perversion is sprouting, \
    a thought that you find entirely disturbing. You almost don't notice when NPC_NAME finally cums, filling \
    your bowels with his hot sticky seed.\n\n\
    <</if>>\
    <</if>>\
    NPC_NAME says s(Thanks PLAYER_NAME, that was one hell of a arse fuck)\
    "
		];

}
