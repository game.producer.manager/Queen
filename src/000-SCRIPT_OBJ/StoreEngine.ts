namespace App {
	export class StoreEngine {

		/**
		 * Creates a store object and returns it to the Twine engine.
		 */
		static OpenStore(Player: Entity.Player, npc: Entity.NPC) {
			const storeName = npc.StoreName();
			if (storeName !== undefined) {
				return new Store(Player, npc, App.Data.Stores[storeName]);
			}
			throw `NPC "${npc.Name}" has no store`;
		}

		/**
		 * Checks to see if an NPC has a store inventory or not attached to them.
		 */
		static HasStore(npc: Entity.NPC) {
			const storeName = npc.StoreName();
			if (storeName === undefined || !App.Data.Stores.hasOwnProperty(storeName)) {
				return false;
			}

			const tmp = App.Data.Stores[storeName];
			const unlockFlag = tmp.UNLOCK_FLAG;
			if (unlockFlag !== undefined && (Entity.Player.instance.QuestFlags.hasOwnProperty(unlockFlag) == false ||
					tmp.UNLOCK_FLAG_VALUE != Entity.Player.instance.QuestFlags[unlockFlag])) return false;

			return true;
		}

		/**
		 * Is the NPCs store open.
		 */
		static IsOpen(Player: Entity.Player, npc: Entity.NPC) {
			const storeName = npc.StoreName();
			if (storeName === undefined) {
				return false;
			}

			return App.Data.Stores[storeName]?.OPEN.includes(Player.Phase);
		}

		/**
		 * Lock/unlock a store item.
		 */
		static ToggleStoreItem(Player: Entity.Player, StoreKey: string, ItemTag: string, Locked: 0 | 1) {
			var Match = Player.StoreInventory[StoreKey].INVENTORY.filter(Item => Item.TAG == ItemTag);

			for (var i = 0; i < Match.length; i++)
				Match[i].LOCK = Locked;
		}

	}

	interface ItemOverview {
		TYPE: Data.ItemTypeStr;
		TAG: string;
		PRICE?: number;
	}

	interface ItemOverviewForBuying extends ItemOverview {
		QTY: number;
	}
	/**
	 * Store Container Object
	 */
	export class Store {

		#Player: App.Entity.Player;
		#NPC: App.Entity.NPC;
		#Data: App.Data.StoreDesc;
		#Id: string;

		constructor(Player: App.Entity.Player, NPC: App.Entity.NPC, StoreData: App.Data.StoreDesc) {
			this.#Player = Player;
			this.#NPC = NPC;
			this.#Data = StoreData;
			this.#Id = this.#Data.ID;
			//Hack to add store to player state if the store doesn't exist already.
			if (Player.StoreInventory.hasOwnProperty(this.#Id) == false) {
				Player.StoreInventory[this.#Id] = {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []};
			}

			this.StockInventory();
		}

		GetName() {return this.#Data.NAME;};

		GetInventory(Category: string) {
			var Mood = this.#NPC.Mood;
			var Player = this.#Player;

			var Inventory = $.grep(this.#Player.StoreInventory[this.#Id].INVENTORY, function (Item) {
				return ((Item.LOCK != 1) && (Item.CATEGORY == Category) && (Mood >= Item.MOOD));
			});

			//return Inventory; // Show item, but no buy.
			return Inventory.filter(function (Item) {return Player.OwnsWardrobeItem(Item) == false;});
		};

		GetCommonInventory() {
			return this.GetInventory("COMMON");
		}

		GetRareInventory() {
			var Mood = this.#NPC.Mood;
			return $.grep(this.#Player.StoreInventory[this.#Data.ID].RARE,
				function (Item) {
					return ((Item.LOCK != 1) && (Item.CATEGORY == "RARE") && (Mood >= Item.MOOD));
				});
		}

		/**
		 * Can the player afford this onion? Er.. item.
		 * @param Item The object that represents the stores inventory for the item
		 */
		PlayerCanAfford(Item: ItemOverview): boolean {
			return (this.#Player.Money >= this.GetPrice(Item));
		}

		/**
		 * Fetch the price from the Item calculator. Applies store bonus and discount for good npc mood.
		 * @param Item The object that represents the stores inventory for the item
		 */
		GetPrice(Item: ItemOverview) {
			var price = App.Items.CalculateBasePrice(Item.TYPE, Item.TAG);
			if (typeof Item.PRICE !== 'undefined') price = Math.ceil(price * Item.PRICE);

			// Up to 30% discount with maximum NPC mood. Mood must be above 50
			var discount = Math.floor(price * 0.3);
			return (this.#NPC.Mood > 50) ? Math.ceil(price - (discount * ((this.#NPC.Mood - 50) / 50))) : price;

		}

		BuyItems(Item: ItemOverviewForBuying, Count?: number) {
			if (Count === undefined) Count = Item.QTY;
			var itemPrice = this.GetPrice(Item);
			// looping because some items contain more than 1 charge
			// and we can't fetch that here
			for (var i = 0; i < Count; ++i) {
				if (this.#Player.Money < itemPrice || Item.QTY === 0 || this.#Player.MaxItemCapacity(Item)) break;
				Item.QTY = Math.max(0, (Item.QTY - 1));
				this.#Player.AddItem(Item.TYPE, Item.TAG, 0);
				this.#Player.spendMoney(itemPrice, Entity.Spending.Shopping, Item.TYPE);
			}
		}

		GenerateMarket() {
			if (this.#Data.RESTOCK == 0) {
				// Clear all the data…
				this.#Player.StoreInventory[this.#Id].INVENTORY = [];
				this.#Player.StoreInventory[this.#Id].RARE = [];

				var items = 3 + (Math.floor(Math.random() * 4)); // 3 - 6 items.

				for (var i = 0; i < items; i++) {
					var roll = (1 + (Math.floor(Math.random() * 100)));
					var entry = "";
					var qty = 0;

					if (roll < 20) {  // cosmetics
						const keys = $.grep(Object.keys(App.Data.Cosmetics), c => App.Data.Cosmetics[c].InMarket ?? true);
						entry = App.PR.GetRandomListItem(keys);
						this.#Player.StoreInventory[this.#Id].INVENTORY.push(
							{"CATEGORY": "COMMON", "TYPE": "COSMETICS", "QTY": 12, "MAX": 12, "PRICE": 1.3, "MOOD": 0, "LOCK": 0, "TAG": entry});
					}

					if (roll >= 20 && roll < 60) { // food
						const keys = $.grep(Object.keys(App.Data.Food), c => App.Data.Food[c].InMarket ?? true);
						entry = App.PR.GetRandomListItem(keys);
						qty = (1 + (Math.floor(Math.random() * 4)));
						this.#Player.StoreInventory[this.#Id].INVENTORY.push(
							{"CATEGORY": "COMMON", "TYPE": "FOOD", "QTY": qty, "MAX": qty, "PRICE": 1.3, "MOOD": 0, "LOCK": 0, "TAG": entry});
					}

					if (roll >= 60 && roll < 80) { // drugs
						const keys = $.grep(Object.keys(App.Data.Drugs), c => App.Data.Drugs[c].InMarket ?? true);
						entry = App.PR.GetRandomListItem(keys);
						qty = (1 + (Math.floor(Math.random() * 4)));
						this.#Player.StoreInventory[this.#Id].INVENTORY.push(
							{"CATEGORY": "COMMON", "TYPE": "DRUGS", "QTY": qty, "MAX": qty, "PRICE": 1.3, "MOOD": 0, "LOCK": 0, "TAG": entry});
					}

					if (roll >= 80 && roll <= 95) { // clothes
						let keys:string[] = [];
						// if roll == 95 then include legendary items
						if (roll == 95) {
							keys = $.grep(Object.keys(App.Data.Clothes), c => App.Data.Clothes[c].InMarket ?? true);
						} else {
							keys = $.grep(Object.keys(App.Data.Clothes), c => (App.Data.Clothes[c].InMarket ?? true) && (App.Data.Clothes[c].Style != "LEGENDARY"));
						}

						if (keys && keys.length > 0) {
							entry = keys.randomElement();
							if (App.Data.Clothes[entry].Style == "LEGENDARY") {
								this.#Player.StoreInventory[this.#Id].RARE.push(
									{
										"CATEGORY": "RARE",
										"TYPE": "CLOTHES",
										"QTY": 1,
										"MAX": 1,
										"PRICE": 1.3,
										"MOOD": 0,
										"LOCK": 0,
										"TAG": entry
									});
							} else {
								this.#Player.StoreInventory[this.#Id].INVENTORY.push(
									{
										"CATEGORY": "COMMON",
										"TYPE": "CLOTHES",
										"QTY": 1,
										"MAX": 1,
										"PRICE": 1.3,
										"MOOD": 0,
										"LOCK": 0,
										"TAG": entry
									});
							}
						}
					}

					if (roll > 95) { // slot reels.
						entry = Object.keys(App.Data.Slots)[(Math.floor(Math.random() * Object.keys(App.Data.Slots).length))];
						this.#Player.StoreInventory[this.#Id].INVENTORY.push(
							{"CATEGORY": "COMMON", "TYPE": "REEL", "QTY": 1, "MAX": 1, "PRICE": 1.3, "MOOD": 0, "LOCK": 0, "TAG": entry});
					}
				}
			}
		}

		/**
		 * Returns days until the next restocking
		 */
		DaysUntilRestocking() {
			// Don't stock stuff in markets
			if (this.#Data.RESTOCK == 0) return 0;
			return this.#Data.RESTOCK - (this.#Player.Day
				- this.#Player.StoreInventory[this.#Id].LAST_STOCKED);
		}

		/**
		 * Owner's mood
		 */
		OwnerMood() {
			return this.#NPC.Mood;
		}

		/* FIXME: Let's make this trigger for the SHIP whenever you land at a port. But not otherwise. */
		/* FIXME: This entire system is a load of complicated garbage. Rewrite and simplify it in the future */
		StockInventory() {
			// Don't stock stuff in markets
			if (this.#Data.RESTOCK == 0) return;

			if ((this.#Player.StoreInventory[this.#Id].LAST_STOCKED == 0)
				|| (this.#Player.StoreInventory[this.#Id].LAST_STOCKED + this.#Data.RESTOCK <= this.#Player.Day)) {

				// Something is bugged, prepare the inventory array.
				if (this.#Player.StoreInventory[this.#Id].INVENTORY.length == 0)
					this.#Player.StoreInventory[this.#Id].INVENTORY = this.#Data.INVENTORY;

				// Add any records in Data that do not exist in the player state.Typically we added something to a shop
				// and the player already has a shop record in their state object.
				var toAdd = this.#Data.INVENTORY.filter(f1 => this.#Player.StoreInventory[this.#Id].INVENTORY.find(f2 => f1.TAG == f2.TAG) === undefined);
				toAdd.forEach(a => this.#Player.StoreInventory[this.#Id].INVENTORY.push(a));

				//Restock qty on items.
				for (var i = 0; i < this.#Player.StoreInventory[this.#Id].INVENTORY.length; i++)
					this.#Player.StoreInventory[this.#Id].INVENTORY[i].QTY = this.#Player.StoreInventory[this.#Id].INVENTORY[i].MAX;

				this.#Player.StoreInventory[this.#Id].RARE = []; //Clear array.

				var Mood = this.#NPC.Mood;
				var MaxRares = this.#Data.MAX_RARES ?? 1;
				// List of all possible rares from inventory listing.
				var Rares = $.grep(this.#Player.StoreInventory[this.#Id].INVENTORY, function (Item) {
					return ((Item.CATEGORY == "RARE") && (Mood >= Item.MOOD) && (Item.LOCK != 1));
				});

				// Add multiple rare items to the store inventory.
				if (Rares.length > 0) {
					for (i = 0; i < MaxRares; i++) {
						// Filter out Rares that already exist in the rares entry.
						Rares = Rares.filter((o) => {
							var current = this.#Player.StoreInventory[this.#Id].RARE;
							for (var i = 0; i < current.length; i++)
								if (current[i].TYPE == o.TYPE && current[i].TAG == o.TAG) return false;
							return true;
						});

						if (Rares.length > 0) {
							// Copy data record object into new variable or we get some bad reference juju.
							this.#Player.StoreInventory[this.#Id].RARE.push(clone(Rares.randomElement()));
						}
					}
				}

				this.#Player.StoreInventory[this.#Id].LAST_STOCKED = this.#Player.Day;
			}
		}

		PrintItem(item: ItemOverview) {
			var oItem = App.Items.Factory(item.TYPE, item.TAG);
			var res = "<span class='inventoryItem'>" + oItem.Description;
			if (this.#Player.Inventory.IsFavorite(oItem.Id)) {
				res += "&nbsp;" + App.PR.GetItemFavoriteIcon(true);
			}

			if (SugarCube.settings.inlineItemDetails) {
				res += '<span class="tooltip">' + oItem.Examine(this.#Player, false) + '</span></span>';
				res += "<br><div class='inventoryItemDetails'>" + oItem.Examine(this.#Player, true) + '</div>';
			} else {
				res += '</span>';
			}
			return res;
		}

		/**
		 * Used for examining items through shop interface
		 */
		PrintItemLong(Item: ItemOverview): string {
			var oItem = App.Items.Factory(Item.TYPE, Item.TAG);
			var Player = this.#Player;
			return "@@.action-general;You take a look at the @@" + oItem.Description + ".\n" + oItem.Examine(Player);
		}
	}
}
