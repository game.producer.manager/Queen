namespace App.Entity {
	type CoreStatsObject = Record<CoreStat, number>;
	type BodyStatsObject = Record<BodyStat, number>;
	type SkillStatsObject = Record<Skill, number>;

	type SkillStats = Partial<Record<Skill, {Success: number; Failure: number}>>;

	interface CoffinDiceStats {
		Played: number;
		Won: number;
		Lost: number;
		Draw: number;
		CoinsWon: number;
		CoinsLost: number;
		ItemsWon: number;
		ItemsWonValue: number;
		SexPaid: number;
	}

	export const enum Income {
		Betting = 'Betting',
		Gambling = 'Gambling',
		Jobs = 'Jobs',
		SexualJobs = 'SexualJobs',
		Loot = 'Loot',
		Whoring = 'Whoring',
		Unknown = 'Unknown'
	}

	export const enum Spending {
		Betting = 'Betting',
		Gambling = 'Gambling',
		Quests = 'Quests',
		Jobs = 'Jobs',
		Shopping = 'Shopping'
	}

	interface SpendingTargetMap {
		[Spending.Betting]: number;
		[Spending.Gambling]: number;
		[Spending.Jobs]: number;
		[Spending.Quests]: number;
		[Spending.Shopping]: Record<Data.ItemType, number>;
	}

	interface GameStats {
		MoneyEarned: Record<Income, number>;
		MoneySpendings: {[P in Spending]: SpendingTargetMap[P]};
		TokensEarned: number;
		Skills: SkillStats;
		COFFIN: CoffinDiceStats;
	}

	interface StatTypeObjectMap {
		[StatType.STAT]: CoreStatsObject;
		[StatType.SKILL]: SkillStatsObject;
		[StatType.BODY]: BodyStatsObject;
	}

	export const enum FlagType {
		Job = 'Job',
		Quest = 'Quest'
	}

	/**
	 * The basic player state object.
	 * This will be serialized by the SugarCube and thus may not contain any functions
	 */
	export class PlayerState {
		// Player Basic Variables
		OriginalName = "Joseph";
		SlaveName = "Josie";
		GirlfriendName = "Annette";
		NickName = "";
		HairColor: Data.HairColor = "black";
		HairStyle = "boy cut";
		HairBonus = 0;
		MakeupStyle = "plain faced";
		MakeupBonus = 0;
		EyeColor = "brown";
		Money = 0;
		Tokens = 0;
		SailDays = 1;
		LastUsedMakeup = "minimal blush and lipstick";
		LastUsedHair = "a spunky boy cut";
		LastQuickWardrobe = "Best Clothes";
		debugMode = false;
		difficultySetting = 1;
		WhoreTutorial = false;

		JobFlags: Record<string, TaskFlag> = {};

		VoodooEffects: Record<string, string | number> = {};
		QuestFlags: Record<string, TaskFlag> = {"Q001": "ACTIVE"}; // Default Quest.

		History: Record<string, Record<string, number>> = {
			"ITEMS": {},
			"CLOTHING_KNOWLEDGE": {}, // Flag to gain knowledge
			"CLOTHING_EFFECTS_KNOWN": {},
			"DAYS_WORN": {},
			"SEX": {},
			"CUSTOMERS": {},
			"MONEY": {}
		};

		// Game/Environment Variables
		Day = 1;
		Phase: DayPhase = 0; // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night

		// Player Statistic Variables
		CoreStats: CoreStatsObject = {
			"Health": 0,
			"Nutrition": 0,
			"WillPower": 0,
			"Perversion": 0,
			"Femininity": 0,
			"Fitness": 0,
			"Toxicity": 0,
			"Hormones": 0,
			"Energy": 0,
			"Futa": 0
		};

		CoreStatsXP: CoreStatsObject = {
			"Health": 0,
			"Nutrition": 0,
			"WillPower": 0,
			"Perversion": 0,
			"Femininity": 0,
			"Fitness": 0,
			"Toxicity": 0,
			"Hormones": 0,
			"Energy": 0,
			"Futa": 0,
		};

		BodyStats: BodyStatsObject = {
			"Face": 0,
			"Lips": 0,
			"Bust": 0,
			"BustFirmness": 0,
			"Ass": 0,
			"Waist": 0,
			"Hips": 0,
			"Penis": 0,
			"Hair": 0,
			"Height": 0,
			"Balls": 0,
			"Lactation": 0
		};

		BodyXP: BodyStatsObject = {
			"Face": 0,
			"Lips": 0,
			"Bust": 0,
			"BustFirmness": 0,
			"Ass": 0,
			"Waist": 0,
			"Hips": 0,
			"Penis": 0,
			"Hair": 0,
			"Height": 0,
			"Balls": 0,
			"Lactation": 0
		};

		Skills: SkillStatsObject = {
			"HandJobs": 0,
			"TitFucking": 0,
			"BlowJobs": 0,
			"AssFucking": 0,
			"Dancing": 0,
			"Singing": 0,
			"Seduction": 0,
			"Courtesan": 0,
			"Cleaning": 0,
			"Cooking": 0,
			"Serving": 0,
			"Swashbuckling": 0,
			"Sailing": 0,
			"Navigating": 0,
			"Styling": 0,
			"BoobJitsu": 0,
			"AssFu": 0,
		};

		SkillsXP: SkillStatsObject = {
			"HandJobs": 0,
			"TitFucking": 0,
			"BlowJobs": 0,
			"AssFucking": 0,
			"Dancing": 0,
			"Singing": 0,
			"Seduction": 0,
			"Courtesan": 0,
			"Cleaning": 0,
			"Cooking": 0,
			"Serving": 0,
			"Swashbuckling": 0,
			"Sailing": 0,
			"Navigating": 0,
			"Styling": 0,
			"BoobJitsu": 0,
			"AssFu": 0,
		};


		FaceData: any = {}

		/**
		 * Clothing items the Player owns.
		 */
		Wardrobe: string[] = [];
		Inventory: {[x: string]: {[x: string]: number}} = {};
		InventoryFavorites: Set<string> = new Set();
		Equipment: Partial<Record<Data.ClothingSlot, {ID: string; Locked: boolean} | null>> = {};
		StoreInventory: Record<string, StoreInventory> = {};
		NPCS: Record<string, NPCState> = {};

		Slots: {[x: number]: string | null} = {
			0: null, 1: null, 2: null, 3: null, 4: null, 5: null, 6: null, 7: null, 8: null
		};

		CurrentSlots = 3; // Starting allocation of whoring

		BodyEffects: string[] = []; // lists effect names

		GameStats: GameStats = {
			MoneyEarned: {
				Betting: 0,
				Gambling: 0,
				Jobs: 0,
				Loot: 0,
				SexualJobs: 0,
				Whoring: 0,
				Unknown: 0
			},
			MoneySpendings: {
				Betting: 0,
				Gambling: 0,
				Quests: 0,
				Jobs: 0,
				Shopping: {
					CLOTHES: 0,
					COSMETICS: 0,
					DRUGS: 0,
					FOOD: 0,
					LOOT_BOX: 0,
					MISC_CONSUMABLE: 0,
					MISC_LOOT: 0,
					QUEST: 0,
					REEL: 0,
					WEAPON: 0
				}
			},
			TokensEarned: 0,
			Skills: {},
			COFFIN: {
				Played: 0,
				Won: 0,
				Lost: 0,
				Draw: 0,
				CoinsWon: 0,
				CoinsLost: 0,
				ItemsWon: 0,
				ItemsWonValue: 0,
				SexPaid: 0
			}
		};

		constructor() {
			$.extend(true, this.FaceData, Data.DAD.FacePresets['Default 1']);

			for (const skill of Object.keys(this.Skills)) {
				this.GameStats.Skills[skill as Skill] = {"Failure": 0, "Success": 0};
			}
		}
	}


	export type ItemCallback<ThisType> = (this: ThisType, charges: number, name: string, itemClass: Data.InventoryItemTypeStr) => void;
	export type ItemPredicate = (charges: number, name: string, itemClass: Data.InventoryItemTypeStr) => boolean;
	/**
	 * Provides convenient methods for inspecting an inventory objects (@see App.Entity.PlayerState.Inventory
	 * and below the Player class).
	 *
	 * Might be used by NPCs in a future version, thus is not part of the Player class.
	 *
	 * This object is not meant to be serialized by SugarCube
	 *
	 */
	export class InventoryManager {
		static MAX_ITEM_CHARGES = 100;

		private _stateObjName: string;
		private _items: {[x: string]: Items.InventoryItem};
		private _reelInSlots: {[x: number]: Items.Reel | null};

		get _state(): Entity.PlayerState {
			return State.variables[this._stateObjName];
		}

		constructor(stateObjName: string) {
			this._stateObjName = stateObjName;

			// create item objects for each record
			this._items = {};
			this.ForEachItemRecord(undefined, undefined, (charges, tag, itemClass) => {
				this._ensureWrapObjectExists(itemClass, tag, charges);
			});

			this._reelInSlots = {};
			for (var slot in this._state.Slots) {
				if (!this._state.Slots.hasOwnProperty(slot)) continue;
				const reelTag = this._state.Slots[slot];
				this._reelInSlots[slot] = reelTag == null ? null : Items.Factory("REEL", reelTag, this)
			}
			if (this._state.InventoryFavorites == undefined) this._state.InventoryFavorites = new Set();
		}

		/**
		 * Looks through all classes for item with the given tag and returns the item class
		 * @param Tag
		 * @param FindAll return all matching items (the first one otherwise)
		 * @private
		 * TODO refactor the loop to exit early if FindAll = false
		 */
		private _FindItemClass(Tag: string): Data.InventoryItemTypeStr;
		private _FindItemClass(Tag: string, FindAll: false): Data.InventoryItemTypeStr;
		private _FindItemClass(Tag: string, FindAll: true): Data.InventoryItemTypeStr[];
		private _FindItemClass(Tag: string, FindAll = false): Data.InventoryItemTypeStr | Data.InventoryItemTypeStr[] | null {
			const res: Data.InventoryItemTypeStr[] = [];
			Object.keys(this._state.Inventory).forEach((type) => {
				if (this._state.Inventory[type].hasOwnProperty(Tag)) {
					res.push(type as Data.InventoryItemTypeStr);
				}
			});
			if (FindAll) {
				return res;
			}
			if (res.length == 0) return null;
			return res[0];
		}


		ForEachItemRecord<HandlerThisType>(Tag: string | undefined, ItemClass: App.Data.InventoryItemTypeStr | undefined,
			func: ItemCallback<HandlerThisType>, thisObj?: HandlerThisType): void {
			const types = ItemClass == undefined ? Object.keys(this._state.Inventory) : [ItemClass];
			types.forEach(function (this: InventoryManager, type) {
				if (!this._state.Inventory.hasOwnProperty(type)) return;
				if (Tag != undefined) {
					if (this._state.Inventory[type].hasOwnProperty(Tag)) {
						func.call(thisObj, this._state.Inventory[type][Tag], Tag, type);
					}
				} else {
					for (const n in this._state.Inventory[type]) {
						if (!this._state.Inventory[type].hasOwnProperty(n)) continue;
						func.call(thisObj, this._state.Inventory[type][n], n, type);
					}
				}
			}, this);
		}

		EveryItemRecord(Tag: string | undefined, ItemClass: App.Data.InventoryItemTypeStr | undefined, func: ItemPredicate, thisObj?: any): void {
			const types = ItemClass == undefined ? Object.keys(this._state.Inventory) : [ItemClass];
			types.every(function (this: InventoryManager, type) {
				if (!this._state.Inventory.hasOwnProperty(type)) return;
				if (Tag != undefined) {
					if (this._state.Inventory[type].hasOwnProperty(Tag)) {
						return func.call(thisObj, this._state.Inventory[type][Tag], Tag, type);
					}
				} else {
					for (const n in this._state.Inventory[type]) {
						if (!this._state.Inventory[type].hasOwnProperty(n)) continue;
						if (!func.call(thisObj, this._state.Inventory[type][n], n, type)) return false;
					}
					return true;
				}
			}, this);
		}

		private _ensureWrapObjectExists<T extends  keyof App.InventoryItemTypeMap>(ItemClass: T, Tag: string, _charges: number, Id?: string): App.Items.InventoryItem {
			if (Id == undefined) Id = App.Items.MakeId(ItemClass, Tag);
			if (!this._items.hasOwnProperty(Id)) {
				this._items[Id] = App.Items.Factory(ItemClass, Tag, this, 0); // charges is stored already, hence '0' to prevent stack overflowing
			}
			return this._items[Id];
		}

		filter(func: (item: App.Items.InventoryItem) => boolean, thisObj?: any): App.Items.InventoryItem[] {
			var res: App.Items.InventoryItem[] = [];
			this.ForEachItemRecord(undefined, undefined, function (this: InventoryManager, ch, nm, cl) {
				var itemId = App.Items.MakeId(cl, nm);
				var itemWrapObj = this._ensureWrapObjectExists(cl, nm, ch, itemId);
				if (func.call(thisObj, itemWrapObj) == true) res.push(itemWrapObj);
			}, this);
			return res;
		}

		Charges(ItemClass: App.Data.InventoryItemTypeStr | undefined, Tag: string): number {
			var res = 0;
			this.ForEachItemRecord(Tag, ItemClass, function (n) {res += n;});
			return res;
		}

		SetCharges(ItemClass: App.Data.InventoryItemTypeStr, Tag: string, Count: number): number {
			var cl = (ItemClass == undefined) ? this._FindItemClass(Tag, true)[0] : ItemClass;
			var clamped = Math.clamp(Math.floor(Count), 0, InventoryManager.MAX_ITEM_CHARGES);
			if (clamped == 0) {
				if (this._state.Inventory.hasOwnProperty(cl) && this._state.Inventory[cl].hasOwnProperty(Tag)) {
					delete this._state.Inventory[cl][Tag];
					delete this._items[App.Items.MakeId(cl, Tag)];
				}
			} else {
				if (!this._state.Inventory.hasOwnProperty(cl)) this._state.Inventory[cl] = {};
				this._state.Inventory[cl][Tag] = clamped;
				this._ensureWrapObjectExists(ItemClass, Tag, clamped);
			}
			return clamped;
		}

		/**
		 * Adds (or removes) charges to a given item. If item is not in the inventory and Amount > 0, item record
		 * is created.
		 * If Amount < 0, resulting number of charges does not go below zero.
		  * @returns new number of charges
		 */
		AddCharges(ItemClass: App.Data.InventoryItemTypeStr, Tag: string, Amount: number): number {
			var cl = (ItemClass == undefined) ? this._FindItemClass(Tag, true)[0] : ItemClass;
			if (cl == undefined) throw Error("No item tagged '" + Tag + "'");
			// slight hack to not allow items that don't exist to be added to the inventory.
			var data = App.Items.TryGetItemsDictionary(cl);
			if (data == null) throw Error("No item class '" + cl + "' exists.");
			if (!data.hasOwnProperty(Tag)) throw Error("No item tagged '" + Tag + "' exists in class '" + cl + "'");
			if (!this._state.Inventory.hasOwnProperty(cl)) this._state.Inventory[cl] = {};
			if (!this._state.Inventory[cl].hasOwnProperty(Tag)) {
				this._state.Inventory[cl][Tag] = 0;
			}

			// Items with Charges in their data grant that many charges when added to the inventory.
			// All usage however only subtracts 1 charge.
			if (Amount > 0) {
				Amount = Amount * App.Items.GetCharges(cl, Tag);
			} else {
				Amount = Amount == 0 ? App.Items.GetCharges(cl, Tag) : Amount; // add charges if no charge specified?
			}

			return this.SetCharges(cl, Tag, this._state.Inventory[cl][Tag] + Amount);
		}

		AddItem<T extends keyof App.InventoryItemTypeMap>(ItemClass: T, Tag: string, Charges: number): App.InventoryItemTypeMap[T] {
			type R = App.InventoryItemTypeMap[T];
			this.AddCharges(ItemClass, Tag, Charges == undefined ? 1 : Charges);
			return this._items[App.Items.MakeId(ItemClass, Tag)] as R;
		}

		RemoveItem(Id: string) {
			var n = App.Items.SplitId(Id);
			this.SetCharges(n.Category as App.Data.InventoryItemTypeStr, n.Tag, 0);
		}

		/**
		* Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		* equipped in that slot and place it back in the inventory.
		*/
		EquipReel(toEquipID: string, reelSlot: number): void {
			var nm = App.Items.SplitId(toEquipID);
			this.AddCharges("REEL", nm.Tag, -1);

			const reelTag = this._state.Slots[reelSlot];
			if (reelTag != null) {
				this.AddCharges("REEL", reelTag, 1);
			}

			this._state.Slots[reelSlot] = nm.Tag;
			this._reelInSlots[reelSlot] = App.Items.Factory("REEL", nm.Tag, this);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		RemoveReel(slotID: number) {
			const reelTag = this._state.Slots[slotID];
			if ((typeof reelTag !== 'undefined') && (reelTag != null)) {
				this.AddCharges("REEL", reelTag, 1);
				this._state.Slots[slotID] = null;
				this._reelInSlots[slotID] = null;
			}
		}

		/**
		* Turn the equipped reels into an array to iterate/read.
		*/
		EquippedReelItems(): App.Items.Reel[] {
			var arr = Object.values(this._reelInSlots).filter(function (o) {return (typeof o !== 'undefined') && (o != null);});
			return (typeof arr === 'undefined') ? [] : arr as App.Items.Reel[];
		}

		ReelSlots() {
			return this._reelInSlots;
		}

		/**
		 * Adds item to the set of favorites
		 */
		AddFavorite(Id: string) {
			this._state.InventoryFavorites.add(Id);
		}

		/**
		 * Removes item from the favorites set
		 */
		DeleteFavorite(Id: string) {
			this._state.InventoryFavorites.delete(Id);
		}

		/**
		 * Tests whether the item is in the favorites set
		 */
		IsFavorite(Id: string) {
			return this._state.InventoryFavorites.has(Id);
		}

	};

	export class ClothingManager {
		private _stateObjName: string;
		private _wardrobeItems: Items.Clothing[] = [];
		private _equippedItems: Partial<Record<Data.ClothingSlot, Items.Clothing | null>> = {};

		private get _state(): Entity.PlayerState {
			return State.variables[this._stateObjName];
		}

		private get _wardrobe() {
			return this._state.Wardrobe;
		}

		private get _equipment() {
			return this._state.Equipment;
		}

		/**
		 * Creates object for tracking equipment state
		 */
		static EquipmentRecord(Id: string, isLocked: boolean = false) {
			return {ID: Id, Locked: isLocked === undefined ? false : isLocked};
		}

		constructor(stateObjName: string) {
			this._stateObjName = stateObjName;

			for (const id of this._wardrobe) {
				// wardrobe lists item ids
				var t = App.Items.SplitId(id);
				this._ensureWrapObjectExists(t.Tag, id);
			}

			for (var prop in this._equipment) {
				if (!this._equipment.hasOwnProperty(prop)) continue;
				if (this._equipment[prop] == null) {this._equippedItems[prop] = null; continue;}
				var n = App.Items.SplitId(this._equipment[prop].ID);
				this._equippedItems[prop] = App.Items.Factory(n.Category, n.Tag, this);
			}
		}

		private _ensureWrapObjectExists(Tag: string, Id?: string): App.Items.Clothing {
			if (Id == undefined) Id = App.Items.MakeId("CLOTHES", Tag);
			for (var i = 0; i < this._wardrobeItems.length; ++i) {
				if (this._wardrobeItems[i].Id == Id) return this._wardrobeItems[i];
			}
			this._wardrobeItems.push(App.Items.Factory("CLOTHES", Tag, this));
			return this._wardrobeItems[this._wardrobeItems.length - 1];
		}

		/**
		 * Finds slot in which item with the given Id is worn
		 */
		private _findWornSlot(Id: string): App.Data.ClothingSlot | null {
			for (var prop in this._equippedItems) {
				if (!this._equippedItems.hasOwnProperty(prop)) continue;
				if (this._equippedItems[prop]?.Id == Id) return prop as App.Data.ClothingSlot;
			}
			return null;
		}

		/**
		 * Returns true an item with the given Id is worn in the given slot. If slot param is omitted,
		 * every slot is checked.
		 */
		IsWorn(Id:string, Slot?:App.Data.ClothingSlot): boolean {
			if (Slot == undefined) return this._findWornSlot(Id) != null;
			const eq = this._equipment[Slot];
			return (this._equipment.hasOwnProperty(Slot) && eq?.ID == Id);
		}

		/**
		 * Is item in the named slot is locked?
		 * @returns True is there is an item in the slot and it's locked, false otherwise
		 */
		IsLocked(Slot: App.Data.ClothingSlot): boolean {
			return this._equipment[Slot]?.Locked == true;
		}

		/**
		 * Useful helper method.
		 */
		SetLock(Slot: App.Data.ClothingSlot, Lock: boolean) {
			const eq = this._equipment[Slot];
			if (eq) {
				eq.Locked = Lock;
			}
		}

		/**
		 * Wear item and optionally set its 'locked' state.
		 * @param Id
		 * @param Lock lock or unlock item. Leaves locked state as is if the parameter is omitted.
		 */
		Wear(Id: string, Lock?: boolean): void {
			var slot = this._findWornSlot(Id);
			if (slot === null) { // currently the item is not worn
				for (var i = 0; i < this._wardrobeItems.length; ++i) {
					if (this._wardrobeItems[i].Id != Id) continue;
					var itm = this._wardrobeItems[i];
					slot = itm.Slot;

					var slotsToUndress: App.Data.ClothingSlot[] = itm.Restrict ?? [];
					slotsToUndress.push(slot);
					// handle restriction by removing items from the restricted slots
					for (const sl of slotsToUndress) {
						const ei = this._equippedItems[sl] ?? null;
						if (ei) { // move worn item into the wardrobe
							this._wardrobeItems.push(ei);
							this._wardrobe.push(ei.Id);
							delete this._equippedItems[sl];
							delete this._equipment[sl];
						}
					}
					this._equippedItems[slot] = itm;
					this._wardrobeItems.splice(i, 1);
					this._equipment[slot] = ClothingManager.EquipmentRecord(itm.Id, itm.IsLocked());
					this._wardrobe.splice(i, 1);
					break;
				}
			}
			if (slot != null && Lock !== undefined) {
				this.SetLock(slot, Lock);
			}
		}

		TakeOffSlot(slot: Data.ClothingSlot): void {
			const itm = this._equippedItems[slot];
			if (itm) {
				this._equippedItems[slot] = null;
				this._equipment[slot] = null;
				this._wardrobeItems.push(itm);
				this._wardrobe.push(itm.Id); // push(Id) ?
			}
		}

		TakeOff(Id: string) {
			let slot: Data.ClothingSlot;
			for (slot in this._equippedItems) {
				const itm = this._equippedItems[slot];
				if (!itm || itm.Id != Id) {continue};

				this.TakeOffSlot(slot);
				break;
			}
		}

		AddItem(Name: string, Wear = false) {
			var item = App.Items.Factory("CLOTHES", Name, this);
			this._wardrobe.push(item.Id);
			this._wardrobeItems.push(item);
			if (Wear == true) {
				this.Wear(item.Id);
			}
			return item;
		}

		get Wardrobe() {
			return this._wardrobeItems;
		}

		get Equipment() {
			return this._equippedItems;
		}
	};

	export class Player {
		#clothing: App.Entity.ClothingManager| null = null;
		#inventory: App.Entity.InventoryManager | null = null;

		private get _state(): App.Entity.PlayerState {
			return State.variables.PlayerState;
		}

		Init() {
			this._state.OriginalName = App.Data.Names.Male.randomElement();
			this._state.SlaveName = App.Data.Names.Sissy.randomElement();
			let GfName = App.Data.Names.Female.randomElement();

			while (GfName === this._state.SlaveName) {
				GfName = App.Data.Names.Female.randomElement();
			}

			this._state.GirlfriendName = GfName;

			this.SetStartingStats(App.StatType.STAT);
			this.SetStartingStats(App.StatType.BODY);
			this.SetStartingStats(App.StatType.SKILL);

			this._state.Inventory = {
				"COSMETICS": {
					"hair accessories": 10,
					"hair products": 10,
					"basic makeup": 10
				},
				"REEL": {
					"COMMON_WHORE": 2,
					"COMMON_WILDCARD": 1
				}
			};

			// TODO replace with App.Data.Clothes lookup
			function makeEquipRecord(Name: string) {
				var tmp = App.Data.Clothes[Name];
				return App.Entity.ClothingManager.EquipmentRecord(
					App.Items.MakeId("CLOTHES", Name), tmp.hasOwnProperty("Locked") ? tmp.Locked : false);
			}

			this._state.Equipment = {
				"Wig": makeEquipRecord("cheap wig"),
				"Neck": makeEquipRecord("collar"),
				"Bra": makeEquipRecord("chemise"),
				"Panty": makeEquipRecord("cotton bloomers"),
				"Stockings": makeEquipRecord("cotton stockings"),
				"Dress": makeEquipRecord("cotton dress"),
				"Shoes": makeEquipRecord("worn boots"),
				"Penis": makeEquipRecord("chastity cage"),
			};

			this._state.NPCS = {};

			this._state.StoreInventory = {
				"GALLEY": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"CARGO": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"ISLATAVERN": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"ISLASTORE": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"SMUGGLERS": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"PEACOCK": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"GOLDEN_GOODS": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"LUSTY_LASS": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"SAUCY_SLATTERN": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"YVONNE_STORE": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"MARKET_STORE": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"BAZAAR_STORE": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"Emily": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"Bradshaw": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"BarnabyLong": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"MeghanLong": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"Isabella": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"Fineas": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []},
				"Bella": {"LAST_STOCKED": 0, "INVENTORY": [], "RARE": []}
			};
		}

		/**
		 * Previously we stored a dictionary of NPC objects. Now we just store some vital information and make new ones as needed.
		 */
		GetNPC(npcTag: string): Entity.NPC {
			const data = App.Data.NPCS[npcTag];
			if (!this._state.NPCS.hasOwnProperty(npcTag)) {
				this._state.NPCS[npcTag] = {QuestFlags: {}, Mood: data["Mood"], Lust: data["Lust"]};
				return new Entity.NPC(data, this._state.NPCS[npcTag]);
			}
			return new Entity.NPC(data, this._state.NPCS[npcTag]);
		}

		/**
		 * Helper function. Sets the starting statistics for a player from the config objects.
		 */
		private SetStartingStats(Type: StatType): void {
			const ConfigOb = this.GetStatConfig(Type);

			let prop: keyof typeof ConfigOb;
			for (prop in ConfigOb) {
				const configVal = ConfigOb[prop];
				if (!configVal.hasOwnProperty("START")) continue;
				if (Type == StatType.BODY) this.AdjustBody(prop as BodyStatStr, configVal.START);
				if (Type == StatType.SKILL) this.AdjustSkill(prop as SkillStr, configVal.START);
				if (Type == StatType.STAT) this.AdjustStat(prop as CoreStatStr, configVal.START);
			}
		}

		/**
		 * Helping function to get the starting value of a statistic.
		 */
		GetStartStat<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T]): number;
		GetStartStat<T extends keyof StatTypeStrMap>(type: T, stat: StatTypeStrMap[T]): number;
		GetStartStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, stat: S): number;
		GetStartStat<T extends keyof StatTypeMap>(type: T, stat: any): number {
			return this.GetStatConfig(type)[stat]["START"];
		}

		/**
		 * With Icon = true then return string time with Icon.
		 * With Icon = false return numerical time of day.
		 */
		GetPhase(addIcon: boolean = false): string {
			const phase = this._state.Phase;
			return addIcon ? `${App.PR.phaseName(phase)} ${Player.GetPhaseIcon(phase)}` : App.PR.phaseName(phase);
		}

		/**
		 * Returns the phase icon for the current phase.
		 */
		static GetPhaseIcon(Arg: number): string {
			if (Arg == 0) return "@@color:yellow;&#9788;@@";
			if (Arg == 1) return "@@color:orange;&#9728;@@";
			if (Arg == 2) return "@@color:azure;&#9734;@@";
			if (Arg == 3) return "@@color:cornflowerblue;&#9789;@@";
			if (Arg == 4) return "@@color:DeepPink;&#9789;@@";
			return "@@color:DeepPink;&#9789;@@";
		}

		phaseIconStrip(): string {
			let res = '';
			const phase = this._state.Phase;
			for (let i = 0; i < 5; ++i) {
				res += i === phase ? `[${Player.GetPhaseIcon(i)}]` : Player.GetPhaseIcon(i);
			}
			return res;
		}

		/**
		 * Moved this here to use it in a couple of different place.
		 */
		private static _ModFormula(Roll: number, Target: number): number { // TODO free function?
			return Math.clamp(Roll / Target, 0.1, 2.0);
		}

		/**
		 * Moved this here to use it in a couple of different places.
		 */
		private static _TargetFormula(Val: number, Difficulty: number): number { // TODO free function?
			return (100 - Math.clamp(50 + Val - Difficulty, 5, 95));
		}

		/**
		 * Sort of a generic skill roll that just uses the base formulas and doesn't inherently
		 * grant xp. Used for arbitrary checks that are derived from meta statistics, etc.
		 */
		GenericRoll(SkillVal: number, Difficulty: number, Amount: number, Scaling: boolean = false): number {
			Scaling = Scaling || false;
			var Target = Player._TargetFormula(SkillVal, Difficulty);
			var DiceRoll = Math.floor(Math.random() * 100);
			var Mod = Player._ModFormula(DiceRoll, Target);
			if (this._state.debugMode)
				console.log("GenericRoll(" + SkillVal + "," + Difficulty + "," + Amount + "," + Scaling + "):  Target=" +
					Target + ", DiceRoll=" + DiceRoll + ", Mod=" + Mod + "\n");

			if (Scaling == true) return (Amount * Mod);
			return DiceRoll >= Target ? 0 : 1;
		}

		AcknowledgeSkillUse(skill: Skill, XpEarned: number) {
			// Corrupt player for performing sex skill checks.
			switch (skill) {
				case App.Skill.HandJobs:
					this.CorruptWillPower(XpEarned, 40);
					break;
				case App.Skill.TitFucking:
					this.CorruptWillPower(XpEarned, 50);
					break;
				case App.Skill.BlowJobs:
					this.CorruptWillPower(XpEarned, 60);
					break;
				case App.Skill.AssFucking:
					this.CorruptWillPower(XpEarned, 70);
					break;
			}
		}

		/**
		 * Performs a skill roll.
		 * @param SkillName - Skill to check.
		 * @param Difficulty - Test difficulty.
		 * @param Scaling - Return value is always the XpMod and never 0.
		 * @returns result of check.
		 */

		//TODO: Is this called from anywhere other than SkillRoll? Should I just simplify and move to that method?
		private _SkillRoll(SkillName: App.Skill, Difficulty: number, Scaling: boolean = false): number {
			var Target = this.CalculateSkillTarget(SkillName, Difficulty);
			var DiceRoll = Math.floor(Math.random() * 100);
			var Synergy = this.GetSynergyBonus(SkillName);

			DiceRoll += Math.clamp(Synergy, 0, 100); // Cap 100
			DiceRoll += this.RollBonus(StatType.SKILL, SkillName);

			var BaseXp = Math.clamp(Difficulty - this.GetStat(StatType.SKILL, SkillName), 10, 50);
			var XpMod = Player._ModFormula(DiceRoll, Target);
			var XpEarned = Math.ceil(BaseXp * XpMod);

			this.AdjustSkillXP(SkillName, XpEarned);

			this.AcknowledgeSkillUse(SkillName, XpEarned);

			if (this._state.debugMode) {
				console.debug("SkillRoll(%s,%d): Target = %d, DiceRoll = %d XPMod = %f\n", SkillName, Difficulty, Target, DiceRoll, XpMod);
			}

			if (this._state.GameStats.Skills[SkillName] === undefined) {
				this._state.GameStats.Skills[SkillName] = {Success: 0, Failure: 0};
			}

			if (DiceRoll >= Target) {
				(this._state.GameStats.Skills as Required<SkillStats>)[SkillName].Success += 1;
			} else {
				(this._state.GameStats.Skills as Required<SkillStats>)[SkillName].Failure += 1;
			}

			if (Scaling == true) return XpMod;
			if (DiceRoll >= Target) return 1;
			return 0;
		}

		CorruptWillPower(XP: number, Difficulty: number = 50) {
			XP = Math.abs(XP); // make sure negatives become positives.

			console.debug("CorruptWillPower(%d, %d) called", XP, Difficulty);

			var Perv = this.GetStat(App.StatType.STAT, App.CoreStat.Perversion);
			var mod = 1 - Math.max(0, Math.min(Perv / Difficulty, 1)); // 0 - 1
			var toWillPowerXP = Math.ceil(XP * mod) * -1.0;
			var toPerversionXP = Math.ceil((XP * mod) / 2);

			if (this._state.difficultySetting == 1) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.8) * -1.0;
			} else if (this._state.difficultySetting == 2) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.7) * -1.0;
			}

			this.AdjustXP(App.StatType.STAT, App.CoreStat.WillPower, toWillPowerXP, 0, true);
			this.AdjustXP(App.StatType.STAT, App.CoreStat.Perversion, toPerversionXP, 0, true);
		}

		/**
		 *
		 * @param Type SKILL or STAT
		 * @param Name Attribute to check
		 */
		RollBonus<T extends keyof StatTypeStrMap>(Type: T, Name: StatTypeStrMap[T]): number {
			var bonus = 0;

			if (this._state.VoodooEffects.hasOwnProperty("PIRATES_PROWESS") && Type == StatType.SKILL) {
				bonus += this._state.VoodooEffects["PIRATES_PROWESS"] as number;
			}
			if (this._state.difficultySetting == 1) bonus += 5;
			if (this._state.difficultySetting == 2) bonus += 10;
			bonus += this.GetWornSkillBonus(Name);

			return bonus;
		}

		/**
		 * Performs a skill roll with a value amount which is modified when returned. Only works on skills
		 * and will auto generate and apply XP to characters.
		 */
		SkillRoll(SkillName: Skill, Difficulty: number, Amount: number, Scaling?: boolean): number;
		SkillRoll(SkillName: SkillStr, Difficulty: number, Amount: number, Scaling?: boolean): number;
		SkillRoll(SkillName: Skill, Difficulty: number, Amount: number, Scaling?: boolean): number {
			var Mod = this._SkillRoll(SkillName, Difficulty, Scaling);
			var ret = Math.ceil(Amount * Mod);
			if (this._state.debugMode) console.log("SkillRoll: Mod=" + Mod + ",Amount=" + Amount + ",Scaling=" + Scaling + ",Ret=" + ret + "\n");
			return Math.ceil(Amount * Mod);
		}

		/**
		 * Like a skill roll, but doesn't grant xp. Can roll against other stats as well.
		 */
		StatRoll<T extends keyof StatTypeMap>(Type: T, Name: StatTypeMap[T], Difficulty: number, Amount: number, Scaling?: boolean): number;
		StatRoll<T extends keyof StatTypeStrMap>(Type: T, Name: StatTypeStrMap[T], Difficulty: number, Amount: number, Scaling?: boolean): number;
		StatRoll<T extends keyof StatTypeMap>(Type: T, Name: StatTypeMap[T], Difficulty: number, Amount: number, Scaling = false): number {
			var Target = this.CalculateSkillTarget(Name, Difficulty, Type);
			var DiceRoll = (Math.floor(Math.random() * 100) + 1);

			if (Type === StatType.SKILL) {
				DiceRoll += Math.clamp(this.GetSynergyBonus(Name as Skill), 0, 100); // Cap 100
			}
			DiceRoll += this.RollBonus(Type, Name);

			const Mod = Player._ModFormula(DiceRoll, Target);

			if (this._state.debugMode) console.debug(`StatRoll(${Name},${Difficulty}):  Target = ${Target}, DiceRoll = ${DiceRoll} Mod=${Mod}\n`);

			// Kludge because some "skills" are called from here due to not granting XP, such as slot machine skills.

			if (Scaling) {
				if (this._state.GameStats.Skills.hasOwnProperty(Name))
					(this._state.GameStats.Skills as Required<SkillStats>)[Name as Skill].Success += 1;

				return (Amount * Mod);
			}

			if (DiceRoll >= Target) {
				if (this._state.GameStats.Skills.hasOwnProperty(Name))
					(this._state.GameStats.Skills as Required<SkillStats>)[Name as Skill].Success += 1;
				return 1;
			} else {
				if (this._state.GameStats.Skills.hasOwnProperty(Name))
					(this._state.GameStats.Skills as Required<SkillStats>)[Name as Skill].Failure += 1;
				return 0;
			}
		}

		/**
		 * Calculate the target of the 1-100 dice roll for a skill check. Always a 5% chance of success/failure.
		 */
		CalculateSkillTarget<T extends keyof StatTypeMap>(skillName: StatTypeMap[T], difficulty: number, alternate: T): number;
		CalculateSkillTarget(skillName: Skill, difficulty: number): number;
		CalculateSkillTarget<T extends keyof StatTypeMap>(skillName: StatTypeMap[T], difficulty: number, alternate: T | 'SKILL' = 'SKILL'): number {
			let skillVal = this.GetStat(alternate as T, skillName);
			return Player._TargetFormula(skillVal, difficulty);
		}

		/**
		 * Checks the SkillSynergy dictionary and adds any skill bonuses to dice rolls on
		 * skill checks. For example, TitFucking gets a bonus for the size of the Players Bust score.
		 */
		GetSynergyBonus(SkillName: App.Skill | App.CoreStat) : number{
			if (!App.Data.Lists.SkillSynergy.hasOwnProperty(SkillName)) return 0;
			var Bonus = 0;
			const synergy = App.Data.Lists.SkillSynergy[SkillName] ?? [];
			for (const s of synergy) {
				Bonus += Math.ceil(this.GetStatPercent(s.TYPE, s.NAME) * s.BONUS);
			}
			return Bonus;
		}

		/**
		 * Restyle hair.
		 */
		ReStyle() {
			if (this.CanReStyle() == false) return;
			var lm = this._state.LastUsedMakeup;
			var Makeup = App.Data.Lists.MakeupStyles.filter(function (Item) {return Item.SHORT == lm;})[0].NAME;
			var lh = this._state.LastUsedHair;
			var Hair = App.Data.Lists.HairStyles.filter(function (Item) {return Item.SHORT == lh;})[0].NAME;

			const wig = this.GetEquipmentInSlot("Wig");
			this.DoStyling(wig?.Id ?? Hair, Makeup);
			this.AdjustStat("Energy", -1);
		}

		/**
		 * Simple routine to check if the player can reapply their style.
		 * @returns {boolean}
		 */
		CanReStyle(): boolean {
			if (this._state.CoreStats["Energy"] < 1) return false;
			if (this._state.LastUsedMakeup == this._state.MakeupStyle) return false;
			var m1 = this.Inventory.Charges('COSMETICS', 'basic makeup');
			var m2 = this.Inventory.Charges('COSMETICS', 'expensive makeup');
			var h1 = this.Inventory.Charges('COSMETICS', 'hair accessories');
			var h2 = this.Inventory.Charges('COSMETICS', 'hair products');
			var lm = this._state.LastUsedMakeup;
			var Makeup = App.Data.Lists.MakeupStyles.filter(function (Item) {return Item.SHORT == lm;})[0];
			if ((m1 < Makeup.RESOURCE1) || (m2 < Makeup.RESOURCE2)) return false;

			if (this.GetEquipmentInSlot("Wig") !== null) return true;
			var lh = this._state.LastUsedHair;
			var Hair = App.Data.Lists.HairStyles.filter(function (Item) {return Item.SHORT == lh;})[0];
			return ((h1 >= Hair.RESOURCE1) && (h2 >= Hair.RESOURCE2));
		}

		/** TODO: THIS ENTIRE AREA IS GARBAGE. REFACTOR IT AND REDO MAKEUP AND HAIRSTYLE STUFF **/

		/**
		 * Style hair and makeup.
		 * @param HairID
		 * @param MakeupID
		 */
		DoStyling(HairID: string, MakeupID: string) {
			var obj = this.GetItemById(HairID);
			const wig = this.GetEquipmentInSlot("Wig");
			if (typeof obj !== 'undefined') { // We passed an Item Id and found an item.
				if ((wig === null) || (wig.Id != HairID))
					this.Wear(this.WardrobeItem(HairID));
			} else {
				if (wig != null) this.Remove(wig);

				var Hair = App.Data.Lists["HairStyles"].filter(function (Item) {return Item["NAME"] == HairID;})[0];

				if (this.GetItemCharges("hair tool") >= Hair["RESOURCE1"] && this.GetItemCharges("hair treatment") >= Hair["RESOURCE2"]) {
					this._state.HairStyle = Hair["SHORT"];
					this._state.HairBonus = this.SkillRoll(Skill.Styling, Hair["DIFFICULTY"], Hair["STYLE"], true);
					this._state.LastUsedHair = Hair["SHORT"];
					this.UseItemCharges("hair tool", Hair["RESOURCE1"]);
					this.UseItemCharges("hair treatment", Hair["RESOURCE2"]);
				}
			}

			var Makeup = App.Data.Lists["MakeupStyles"].filter(function (Item) {return Item["NAME"] == MakeupID;})[0];

			if (this.GetItemCharges("basic makeup") >= Makeup["RESOURCE1"] && this.GetItemCharges("expensive makeup") >= Makeup["RESOURCE2"]) {
				this._state.MakeupStyle = Makeup["SHORT"];
				this._state.MakeupBonus = this.SkillRoll(Skill.Styling, Makeup["DIFFICULTY"], Makeup["STYLE"], true);
				this._state.LastUsedMakeup = Makeup["SHORT"];
				this.UseItemCharges("basic makeup", Makeup["RESOURCE1"]);
				this.UseItemCharges("expensive makeup", Makeup["RESOURCE2"]);
			}

			App.Avatar._DrawPortrait();
		}

		/**
		 * Calculates the Players "Beauty" based on other statistics.
		 */
		Beauty(): number {
			var cBeauty = Math.round((this.GetStat("BODY", "Face") * 0.4) + (this.Figure() * 0.4) + (this.GetStat("STAT", "Fitness") * 0.3));
			return Math.clamp(cBeauty, 0, 100);
		}

		/**
		 * Fetish rating is derived from the enlarged (or minimized) size of the Players body parts.
		 * Generally however, bigger (what) means more points. Used to calculate whoring pay and some
		 * other activities.
		 */
		Fetish(): number {
			var score = 0;
			// 5 - 15 for boobs and ass each   (30 pts)
			if (this.GetStatPercent("BODY", "Bust") >= 30) score += Math.round((5 + (this.GetStatPercent("BODY", "Bust") / 10)));
			if (this.GetStatPercent("BODY", "Ass") >= 30) score += Math.round((5 + (this.GetStatPercent("BODY", "Ass") / 10)));

			// 10 pts for extra firm or extra sagging boobs
			if (this.GetStatPercent("BODY", "BustFirmness") > 90) score += this.GetStatPercent("BODY", "BustFirmness") - 90;
			if (this.GetStatPercent("BODY", "BustFirmness") < 10) score += 10 - this.GetStatPercent("BODY", "BustFirmness");

			// up to 10 each for lips and waist and hips (35 pts)
			if (this.GetStatPercent("BODY", "Lips") >= 80) score += Math.round((this.GetStatPercent("BODY", "Lips") / 10));
			if (this.GetStatPercent("BODY", "Hips") >= 80) score += Math.round((this.GetStatPercent("BODY", "Hips") / 10));
			if (this.GetStatPercent("BODY", "Waist") <= 30) score += 5;
			if (this.GetStatPercent("BODY", "Waist") <= 15) score += 5;
			if (this.GetStatPercent("BODY", "Waist") <= 1) score += 5;

			// Penis and Balls, 10 each. (10 pts)
			if ((this.GetStatPercent("BODY", "Penis") <= 10) && (this.GetStatPercent("BODY", "Balls") <= 10)) score += 10; // tiny genitals!
			if ((this.GetStatPercent("BODY", "Penis") >= 90) && (this.GetStatPercent("BODY", "Balls") >= 90)) score += 10; // big genitals

			return Math.clamp(Math.round((score / 75) * 100), 0, 100);
		}

		/**
		 * Derived statistic that reports "style" made up out of hair, makeup and clothing.
		 */
		Style(): number {
			var cStyle = Math.round((this.HairRating() * 0.25) + (this.MakeupRating() * 0.25) + (this.ClothesRating() * 0.5));
			return Math.clamp(cStyle, 0, 100);
		}

		/**
		 * shim function that returns the "hair rating" of the player, but checks first if they are wearing
		 * a wig and then reports that number instead.
		 */
		HairRating() {
			return Math.clamp(this.GetEquipmentInSlot("Wig")?.HairBonus() ?? this._state.HairBonus, 0, 100);
		}

		GetHairStyle() {
			return this.GetEquipmentInSlot("Wig")?.HairStyle() ?? this._state.HairStyle;
		}

		GetHairColor() {
			return this.GetEquipmentInSlot("Wig")?.HairColor() ?? this._state.HairColor;
		}

		/**
		 * The makeup rating of the player.
		 */
		MakeupRating() {
			return Math.clamp(this._state.MakeupBonus, 0, 100);
		}

		/**
		 * Derived statistic (face + makeup). Bonus payout for hand jobs if you have a good face.
		 */
		FaceRating(): number {
			return Math.ceil(Math.max(0, Math.min(((this.MakeupRating() + this.GetStat("BODY", "Face")) / 2), 100)));
		}

		/**
		 * Iterates through players worn items and sums .Style property.
		 */
		ClothesRating(): number {
			var cStyle = 0;
			let prop: Data.ClothingSlot;
			for (prop in this.Clothing.Equipment) {
				if (!this.Clothing.Equipment.hasOwnProperty(prop)) continue;
				var eqItem = this.Clothing.Equipment[prop];
				if (!(eqItem instanceof App.Items.Clothing)) continue;
				cStyle += eqItem.Style;
			}
			return Math.max(1, Math.min(Math.round(((cStyle / 100) * 100)), 100)); // 1 - 100 rating
		}

		GetStyleSpecRating(Spec: Data.Clothes.CategoryStr): number {
			var Rating = 0;

			let prop: Data.ClothingSlot;
			for (prop in this.Clothing.Equipment) {
				if (!this.Clothing.Equipment.hasOwnProperty(prop)) continue;
				var eqItem = this.Clothing.Equipment[prop];
				if (!(eqItem instanceof App.Items.Clothing)) continue;

				Rating += eqItem.CategoryBonus(Spec);
			}
			return Rating;
		}

		/**
		 * Derived statistic, lends itself to Beauty. WaistRating, BustRating, HipsRating and AssRating contribute.
		 */
		Figure(): number {
			var tFig = Math.round((this.WaistRating() + this.BustRating() + this.HipsRating() + this.AssRating()) / 4);
			return Math.clamp(tFig, 1, 100); // Normalize between 1 - 100
		}

		/**
		 * Calculates "golden ratio" for waist @ Player's height and then returns a score relative their current waist.
		 */
		WaistRating(): number {
			var GoldenWaist = Math.round((App.PR.StatToCM(this, "Height") * 0.375)); // 54cm to 78cm
			return Math.round(((GoldenWaist / App.PR.WaistInCM(this)) / 1.8) * 100);
		}

		/**
		 * Calculates "golden ratio" for bust @ Player's height and then returns a score relative their current bust.
		 */
		BustRating(): number {
			var GoldenBust = (Math.round((App.PR.StatToCM(this, "Height") * 0.375)) * 1.5);
			return Math.round(((App.PR.BustCCtoCM(this) / GoldenBust) / 1.6) * 100);
		}

		/**
		 * Calculates "golden ratio" for hips @ Player's height and then returns a score relative their current bust.
		 */
		HipsRating(): number {
			var GoldenHips = (Math.round((App.PR.StatToCM(this, "Height") * 0.375)) * 1.5);
			return Math.round(((App.PR.HipsInCM(this) / GoldenHips) / 1.6) * 100);
		}

		/**
		 * Combination of Ass + Hips.
		 */
		AssRating(): number {
			return Math.round((this.GetStatPercent("BODY", "Ass") + this.GetStatPercent("BODY", "Hips")) / 2);
		}

		/**
		 * For now just the percentage of the lips 1-100.
		 */
		LipsRating(): number {
			return this.GetStatPercent(StatType.BODY, BodyStat.Lips);
		}

		/**
		 * Returns the config settings for a statistic type.
		 */
		GetStatConfig<T extends keyof Data.StatConfigMap>(Type: T): Record<StatTypeMap[T], Data.StatConfigMap[T]>;
		GetStatConfig<T extends keyof Data.StatConfigStrMap>(Type: T): Record<StatTypeStrMap[T], Data.StatConfigStrMap[T]>;
		GetStatConfig(Type: StatType): any {
			return App.PR.GetStatConfig(Type);
		}

		/**
		 * Returns object that holds stat values
		 */
		GetStatObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			if (type == StatType.STAT) return this._state.CoreStats as StatTypeObjectMap[T];
			if (type == StatType.SKILL) return this._state.Skills as StatTypeObjectMap[T];
			if (type == StatType.BODY) return this._state.BodyStats as StatTypeObjectMap[T];
			throw "WTF TS?!"
		}

		/**
		 * Returns object that holds stat XP values
		 */
		GetStatXPObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			if (type == StatType.STAT) return this._state.CoreStatsXP as StatTypeObjectMap[T];
			if (type == StatType.SKILL) return this._state.SkillsXP as StatTypeObjectMap[T];
			if (type == StatType.BODY) return this._state.BodyXP as StatTypeObjectMap[T];
			throw "WTF TS?!"
		}

		/**
		 * Return a statistic value (raw)
		 */
		GetStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number;
		GetStat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T]): number;
		GetStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T]): number;
		GetStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] {
			const statObj = this.GetStatObject(type);
			let res = statObj[statName];
			if (res !== undefined) {
				return res;
			}
			const st = this.GetStartStat(type, statName);
			if (st != undefined) {
				statObj[statName as any] = st;
			}
			return statObj[statName];
		}

		/**
		 * Returns the current XP of a statistic.
		 */
		GetStatXP<T extends keyof StatTypeMap, S extends StatTypeMap[T]>(type: T, statName: S): number;
		GetStatXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): number;
		GetStatXP<T extends keyof StatTypeStrMap, S extends StatTypeStrMap[T]>(type: T, statName: S): number;
		GetStatXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] {
			const xpObj = this.GetStatXPObject(type);
			const res = xpObj[statName];
			if (!res) {
				// @ts-ignore
				return 0;
			}
			return xpObj[statName];
		}

		GetMaxStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return this.GetStatConfig(type)[statName]["MAX"];
		}
		GetMinStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return this.GetStatConfig(type)[statName]["MIN"];
		}

		GetStatPercent<T extends StatTypeStr>(type: T, statName: StatTypeStrMap[T], statValue?: number): number;
		GetStatPercent<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number {
			var minStatValue = this.GetMinStat(type, statName);
			var value = (statValue === undefined ? this.GetStat(type, statName) : statValue);
			return Math.floor(((value - minStatValue) / (this.GetMaxStat(type, statName) - minStatValue)) * 100);
		}

		/**
		 * Set raw statistic value
		 */
		setStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T], value: number): void;
		setStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S, value: number): void {
			this.GetStatObject(type)[statName as any] = value;
		}

		/**
		 * Returns XP cost for increasing stat level by one to @see TargetScore
		 * @param Type Stat name (e.g. "BODY")
		 * @param StatName Stat name (e.g. "Lips")
		 * @param TargetScore Target level (e.g. 13)
		 * @returns XP cost
		 */
		GetLeveling<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T], TargetScore: number): {COST: number, STEP: number};
		GetLeveling<T extends keyof Data.StatConfigMap>(Type: T, StatName: StatTypeMap[T], TargetScore: number): {COST: number, STEP: number} {
			var statCfg = this.GetStatConfig(Type)[StatName];
			var Levels = statCfg.LEVELING_COST ?? statCfg.LEVELING;
			//var Percent = Math.round(( (( TargetScore - this.GetMinStat(Type, StatName)) / ( this.GetMaxStat(Type, StatName) - this.GetMinStat(Type,StatName))) * 100));
			var Percent = this.GetStatPercent(Type, StatName, this.GetStat(Type, StatName) + TargetScore);
			if (Levels instanceof Function) {
				return {"COST": Levels(Percent) - Levels(Percent - 1), "STEP": 1};
			}
			var Level = {"COST": 100, "STEP": 1};

			for (const [prop, val] of Object.entries(Levels)) {
				if ((val === 'FIXED') || (val === 'NONE') || (Percent <= parseInt(prop))) {
					Level = {
						"COST": val["COST"],
						"STEP": ((this.GetMaxStat(Type, StatName) / 100) * val["STEP"])
					};
					break;
				}
			}

			return Level;
		}

		GetCapStat<T extends keyof App.StatTypeMap>(Type: T, StatName: App.StatTypeMap[T], Amount: number): number {
			return Math.round((Math.max(this.GetMinStat(Type, StatName), Math.min(Amount, this.GetMaxStat(Type, StatName)))) * 100) / 100;
		}

		AdjustStat(StatName: CoreStat, Amount: number): void;
		AdjustStat(StatName: CoreStatStr, Amount: number): void;
		AdjustStat(StatName: CoreStat, Amount: number): void {
			if (this._state.debugMode) console.log("AdjustStat: Name=" + StatName + ", Amount=" + Amount);
			this._state.CoreStats[StatName] = this.GetCapStat(StatType.STAT, StatName, (this.GetStat("STAT", StatName) + Amount));
		}

		AdjustBody(StatName: BodyStat, Amount: number): void;
		AdjustBody(StatName: BodyStatStr, Amount: number): void;
		AdjustBody(StatName: BodyStat, Amount: number): void {
			if (this._state.debugMode) console.log("AdjustBody: Name=" + StatName + ", Amount=" + Amount);
			this._state.BodyStats[StatName] = this.GetCapStat(StatType.BODY, StatName, (this.GetStat('BODY', StatName) + Amount));
		}

		AdjustSkill(StatName: Skill, Amount: number): void;
		AdjustSkill(StatName: SkillStr, Amount: number): void;
		AdjustSkill(StatName: Skill, Amount: number): void {
			if (this._state.debugMode) console.log("AdjustSkill: Name=" + StatName + ", Amount=" + Amount);
			this._state.Skills[StatName] = this.GetCapStat(StatType.SKILL, StatName, (this.GetStat('SKILL', StatName) + Amount));
		}

		//Used to directly set XP to a specific amount.
		SetXP<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T], Amount: number): void;
		SetXP<T extends keyof StatTypeStrMap>(Type: T, StatName: StatTypeStrMap[T], Amount: number): void;
		SetXP<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T], Amount: number): void {
			switch (Type) {
				case StatType.STAT:
					this._state.CoreStatsXP[StatName as CoreStat] = Amount;
					break;
				case StatType.SKILL:
					this._state.SkillsXP[StatName as Skill] = Amount;
					break;
				case StatType.BODY:
					this._state.BodyXP[StatName as BodyStat] = Amount;
					break;
			}
			if (this._state.debugMode)
				console.debug("SetXP: set to %d", Amount);
		}

		AdjustXP<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T], Amount: number, Limiter?: number, Nocap?: boolean): void;
		AdjustXP<T extends keyof StatTypeStrMap>(Type: T, StatName: StatTypeStrMap[T], Amount: number, Limiter?: number, Nocap?: boolean): void;
		AdjustXP<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T], Amount: number, Limiter: number = 0, Nocap: boolean = false): void {
			Amount = Math.ceil(Amount); // No floats.
			if (this._state.debugMode)
				console.debug(`AdjustXP: Type=${Type},Stat=${StatName},Amount=${Amount},Limit=${Limiter},NoCap=${Nocap}`);

			if ((Amount > 0) && (this.GetStat(Type, StatName) >= Limiter) && (Limiter != 0)) return;
			if ((Amount < 0) && (this.GetStat(Type, StatName) <= Limiter) && (Limiter != 0)) return;
			if ((Amount > 0) && (this.GetStat(Type, StatName) >= this.GetMaxStat(Type, StatName))) return;
			if ((Amount < 0) && (this.GetStat(Type, StatName) <= this.GetMinStat(Type, StatName))) return;

			if (Nocap == false && Math.abs(this.GetStatXP(Type, StatName)) >= 1000) {
				Amount = Math.ceil(Amount / 10);
			} else if (Nocap == false && Math.abs(this.GetStatXP(Type, StatName)) >= 500) {
				Amount = Math.ceil(Amount / 4);
			} else if (Nocap == false && Math.abs(this.GetStatXP(Type, StatName)) >= 250) {
				Amount = Math.ceil(Amount / 2);
			}

			if (Type === "STAT") {this._state.CoreStatsXP[StatName as CoreStat] += Amount;}
			else if (Type === "SKILL") {this._state.SkillsXP[StatName as Skill] += Amount;}
			else if (Type === "BODY") {this._state.BodyXP[StatName as BodyStat] += Amount;}
			if (this._state.debugMode)
				console.debug("AdjustXP: Adjusted by " + Amount);
		}

		AdjustStatXP(StatName: App.CoreStatStr, Amount: number, Limiter: number = 0): void {
			return this.AdjustXP("STAT", StatName, Amount, Limiter);
		}

		AdjustBodyXP(StatName: BodyStatStr, Amount: number, Limiter: number = 0): void {
			return this.AdjustXP("BODY", StatName, Amount, Limiter);
		}

		AdjustSkillXP(StatName: SkillStr, Amount: number, Limiter = 0): void {
			return this.AdjustXP("SKILL", StatName, Amount, Limiter);
		}

		LevelStat<T extends keyof StatTypeMap>(Type: T, StatName: StatTypeMap[T]): void {
			var TargetScore = this.GetStatXP(Type, StatName) < 0 ? -1 : 1;
			var Leveling = this.GetLeveling(Type, StatName, TargetScore);
			if ((Math.abs(this.GetStatXP(Type, StatName))) < Leveling.COST) return;

			if (TargetScore < 0 && this.GetStat(Type, StatName) == this.GetMinStat(Type, StatName)) return;
			if (TargetScore > 0 && this.GetStat(Type, StatName) == this.GetMaxStat(Type, StatName)) return;

			console.log("Leveling Stat:" + StatName + "(" + this.GetStat(Type, StatName) + ")");

			var Cost = (TargetScore < 0) ? Leveling.COST : (Leveling.COST * -1.0);
			var Step = (TargetScore < 0) ? (Leveling.STEP * -1.0) : Leveling.STEP;

			switch (Type) {
				case StatType.SKILL:
					var statTitle = Data.Lists.SkillConfig[StatName as Skill].ALTNAME ?? StatName;
					this.AdjustSkill(StatName as Skill, Step);
					var adjective = Step < 0 ? " skill decreases! How did this happen!?" : " skill improves!";
					setup.Notifications.AddMessage('KNOWLEDGE', this.Day + 1,
						"Your " + App.PR.ColorizeString(this.GetStatPercent('SKILL', StatName as Skill), statTitle) + adjective);
					break;

				case StatType.STAT:
					this.AdjustStat(StatName as CoreStat, Step);
					if (App.Data.Lists.BodyChanges.hasOwnProperty(StatName))
						setup.Notifications.AddMessage('STAT_CHANGE', this.Day + 1, this.pBodyChange(StatName, (Step < 0 ? "Shrink" : "Grow")));
					break;

				case StatType.BODY:
					this.AdjustBody(StatName as BodyStat, Step);
					if (App.Data.Lists.BodyChanges.hasOwnProperty(StatName))
						setup.Notifications.AddMessage('STAT_CHANGE', this.Day + 1, this.pBodyChange(StatName, (Step < 0 ? "Shrink" : "Grow")));
					break;

			}

			this.AdjustXP(Type, StatName, Cost, 0, true);

		}

		LevelStatGroup(Type: StatType) {
			var Keys: string[] = [];
			if (Type == StatType.STAT) Keys = Object.keys(this._state.CoreStats);
			if (Type == StatType.SKILL) Keys = Object.keys(this._state.Skills);
			if (Type == StatType.BODY) Keys = Object.keys(this._state.BodyStats);

			for (const k of Keys) this.LevelStat(Type, k as StatTypeMap[typeof Type]);
		}

		earnMoney(m: number, cat: Income) {
			var mi = Math.ceil(m);
			this._state.GameStats.MoneyEarned[cat] += mi;
			this._state.Money = Math.max(0, (this._state.Money + mi));
		}

		spendMoney(m: number, cat: Exclude<Spending, Spending.Shopping>): void;
		spendMoney(m: number, cat: Spending.Shopping, subcat: Data.ItemType): void;
		spendMoney(m: number, cat: Spending.Shopping, subcat: Data.ItemTypeStr): void;
		spendMoney(m: number, cat: Spending, subCat?: any) {
			const mi = Math.ceil(m);
			if (cat === Spending.Shopping) {
				this._state.GameStats.MoneySpendings.Shopping[subCat as Data.ItemType] += mi;
			} else {
				this._state.GameStats.MoneySpendings[cat] += mi;
			}
			this._state.Money = Math.max(0, (this._state.Money - mi));
		}

		AdjustTokens(m: number) {
			var mi = Math.ceil(m);
			console.debug("AdjustTokens: " + mi);
			if (mi > 0) {this._state.GameStats.TokensEarned += mi;}
			console.debug("state.Tokens=" + this._state.Tokens);
			this._state.Tokens = Math.max(0, (this._state.Tokens + mi));
			console.debug("state.Tokens=" + this._state.Tokens);
		}

		RandomAdjustBodyXP(Amount: number) {
			this.AdjustBodyXP(Object.keys(this._state.BodyStats).random() as BodyStatStr, Amount, 0);
		}

		BodyEffects(): string[] {
			if (this._state.BodyEffects !== undefined) return App.Data.NaturalBodyEffects.concat(this._state.BodyEffects);
			return App.Data.NaturalBodyEffects;
		}

		/**
		 * Add named effect to the list of active body effects
		 */
		AddBodyEffect(EffectName: string) {
			if (this._state.BodyEffects.indexOf(EffectName) === -1) {
				this._state.BodyEffects.push(EffectName);
			}
		}

		/**
		 * Removes named effect from the list of active body effects
		 */
		RemoveBodyEffect(EffectName: string) {
			if (this._state.BodyEffects.indexOf(EffectName) !== -1) {
				this._state.BodyEffects = this._state.BodyEffects.filter(function (s) {return s !== EffectName;});
			}
		}

		// Resting and Sleeping functions.
		NextDay() {
			// Gain 'Knowledge' about worn clothes, log days worn.
			// Apply passive effects on worn items.
			let prop: Data.ClothingSlot;
			for (prop in this.Clothing.Equipment) {
				if (!this.Clothing.Equipment.hasOwnProperty(prop)) continue;
				const eqItem = this.Clothing.Equipment[prop];
				if (!(eqItem instanceof App.Items.Clothing)) continue;

				if (Math.random() > 0.8)
					this.AddHistory('CLOTHING_KNOWLEDGE', eqItem.Name, 1); // tracking effect knowledge
				this.AddHistory("DAYS_WORN", eqItem.Name, 1); // tracking just days worn
				eqItem.ApplyEffects(this);
				const logMsg = eqItem.LearnKnowledge(this);
				if ((typeof logMsg != 'undefined') && logMsg != "") setup.Notifications.AddMessage('KNOWLEDGE', this.Day + 1, logMsg);
			}

			// Tarot card effects and dream.
			if (this.JobFlags.hasOwnProperty('TAROT_CARD')) {
				const tarot = App.Data.Tarot[this.JobFlags["TAROT_CARD"] as string];
				this.ApplyEffects(tarot.Effects);
				setup.Notifications.AddMessage('DREAMS', this.Day + 1, tarot.Msg);
				delete this.JobFlags["TAROT_CARD"];
			}

			this.ApplyEffects(this.BodyEffects());

			this.LevelStatGroup(StatType.STAT);
			this.LevelStatGroup(StatType.BODY);
			this.LevelStatGroup(StatType.SKILL);

			this._state.HairBonus = 0;
			this._state.MakeupBonus = 0;
			this._state.HairStyle = "bed head";
			this._state.MakeupStyle = "plain faced";

			// Decrease voodoo effects
			this.EndHexDuration();

			this._state.Day++;
			// What day are we on our current voyage.
			this._state.SailDays = ((this._state.SailDays + 1) >= App.Data.Lists["ShipRoute"].length) ? 0 : (this._state.SailDays + 1);
			this._state.Phase = 0;
			this.NPCNextDay();
			App.Quest.NextDay(this);
			App.Avatar.DrawPortrait();
		} // NextDay

		/**
		 * Move time counter to next phase of day.
		 * @param phases - Number of phases to increment.
		 */
		NextPhase(phases: number = 1) {
			if (this._state.Phase == 4) return; // Can't advance to next day, only do that when sleeping.

			for (var i = 0; i < phases; i++) {
				this._state.Phase++;
				this.AdjustStat(CoreStat.Nutrition, -5);
				this.LevelStat(StatType.STAT, CoreStat.Nutrition);
				if (this._state.Phase == 4) break;
			}
		}

		Rest() {
			this.NextPhase(1);
			this.ApplyEffects(["NATURAL_RESTING"]);
			this.LevelStatGroup(StatType.SKILL);
		}

		NPCNextDay() {
			for (var prop in this.NPCS) { // NPC mood/desire/quest flags.
				const npcState = this.NPCS[prop];
				if (!npcState) continue;
				const npc = this.GetNPC(prop);
				npc.AdjustFeelings();
				npc.ResetFlags();
			}
		}

		GetShipLocation() {
			const Routes = App.Data.Lists.ShipRoute;
			if (this._state.SailDays >= Routes.length) this._state.SailDays = 0; // Shouldn't happen, but fix it if it does.

			const dict = {
				"X": Routes[this._state.SailDays].left,
				"Y": Routes[this._state.SailDays].top,
				"Passage": "",
				"Title": ""
			};

			const titles: {[x: string]: {passage: string, title: string}} = {
				"IslaHarbor": {passage: "IslaHarbor", title: "Isla Harbor"},
				"GoldenIsle": {passage: "GoldenIsle", title: "Golden Isle"},
				"Abamond": {passage: "Abamond", title: "Abamond"},
				"PortRoyale": {passage: "PortRoyale", title: "Port Royale"},
			};

			const pos = Routes[this._state.SailDays]["P"];
			if (titles.hasOwnProperty(pos)) {
				dict["Passage"] = titles[pos].passage;
				dict["Title"] = titles[pos].title;
			}

			return dict;
		}

		/**
		 * @param n Number of days to look ahead.
		 * @returns
		 */
		IsInPort(n: number = 0): boolean {
			var Routes = App.Data.Lists["ShipRoute"];
			var days = (this._state.SailDays + n >= Routes.length ? 0 : this._state.SailDays + n);
			return (Routes[days]["P"] != "AtSea");
		}

		/**
		 *
		 * @param n Days to advance
		 */
		AdvanceSailDays(n: number = 1): boolean {
			if (this.IsInPort(0) == true || this.IsInPort(n) == true) return false;
			var Routes = App.Data.Lists["ShipRoute"];
			this._state.SailDays = (this._state.SailDays >= Routes.length ? n : this._state.SailDays + n);
			return true;
		}

		// Equipment and Inventory Related Functions

		/**
		 * Does the character own the item in question
		 * @param ItemDictOrType The dictionary entry from the Store
		 */
		OwnsWardrobeItem(ItemDictOrType: string | {TYPE: Data.ItemTypeStr, TAG: string}, Name?: string): boolean {
			var Type;
			if (typeof (ItemDictOrType) === "object" && !(ItemDictOrType instanceof String) && typeof (Name) === "undefined") {
				Type = ItemDictOrType.TYPE;
				Name = ItemDictOrType.TAG;
			} else {
				if (ItemDictOrType instanceof String) ItemDictOrType = String(ItemDictOrType);
				if (typeof (ItemDictOrType) != "string" || typeof (Name) != "string") throw new Error("Invalid arguments");
				Type = ItemDictOrType;
			}

			if (Type != "CLOTHES" && Type != "WEAPON") return false;
			if (this.Clothing.Wardrobe.filter(function (o) {return o.Name == Name;}).length > 0) return true;
			var Slot = App.Data.Clothes[Name].Slot;
			var EquipmentInSlot = this.Clothing.Equipment[Slot];
			if (EquipmentInSlot == null || !(EquipmentInSlot instanceof App.Items.Clothing)) return false;
			return EquipmentInSlot.Name == Name;
		}

		MaxItemCapacity(ItemDict: {TAG: string}) {
			var o = this.GetItemByName(ItemDict.TAG);

			if (o != undefined) {
				if (o.Charges() >= 100) return true;
			}
			return false;
		}

		WardrobeItem(id: string): Items.Clothing {
			return this.Clothing.Wardrobe.filter(function (o) {
				return o.Id == id;
			})[0];
		}

		WardrobeItemsBySlot(Slot: Data.ClothingSlot): Items.Clothing[] {
			var res = this.Clothing.Wardrobe.filter(function (Item) {return Item.Slot == Slot;});
			res.sort(function (a, b) {return a.Name.localeCompare(b.Name);});
			return res;
		}

		PrintEquipment(Slot: Data.ClothingSlot): string {
			const eq = this.Clothing.Equipment[Slot];
			if (!eq) return "@@.state-disabled;Nothing@@";
			return eq.Description;
		}

		GetEquipmentInSlot(Slot: Data.ClothingSlot): Items.Clothing | null {
			const eq = this.Clothing.Equipment[Slot];
			return eq ? eq : null;
		}

		/**
		 * Search equipped items
		 * @param Name
		 * @param SlotFlag Search by slot instead of item name.
		 */
		IsEquipped(slot: Data.ClothingSlot | Data.ClothingSlot[], slotFlag: true): boolean;
		IsEquipped(name: string| string[], slotFlag?: false): boolean;
		IsEquipped(nameOrSlot: string | string[], SlotFlag = false): boolean {
			function _IsEquipped(Name: string, Flag: boolean, _this: Entity.Player): boolean {
				if (Flag) {
					return _this.Clothing.Equipment[nameOrSlot as Data.ClothingSlot] != null;
				}

				let prop: Data.ClothingSlot;
				for (prop in _this.Clothing.Equipment) {
					const itm = _this.Clothing.Equipment[prop];
					if (itm && itm.Name === Name) return true;
				}
				return false;
			}

			if (Array.isArray(nameOrSlot)) {
				return nameOrSlot.some(n => _IsEquipped(n, SlotFlag, this));
			}

			return _IsEquipped(nameOrSlot, SlotFlag, this);

		}

		Wear(item: App.Items.Clothing | string, lock = false) {
			if (typeof item === "string") {
				this.Clothing.Wear(item, lock);
			} else {
				this.Clothing.Wear(item.Id, lock);
			}
			App.Avatar._DrawPortrait();
		}

		SetLock(Slot: Data.ClothingSlot, Lock: boolean) {
			this.Clothing.SetLock(Slot, Lock);
		}

		AutoWearCategory(Category: Data.Clothes.CategoryStr) {
			let slot: Data.ClothingSlot;
			for (slot in this.Clothing.Equipment) {
				if (!this.Clothing.Equipment.hasOwnProperty(slot)) continue;
				var currentlyWorn = this.Clothing.Equipment[slot];

				if (currentlyWorn?.IsLocked()) continue;

				// Get all matching items by Category and Slot.
				var matchingItems = $.grep(this.Clothing.Wardrobe, function (clothing) {
					return clothing.Slot == slot && $.inArray(Category, clothing.Category) >= 0;
				});

				if (matchingItems.length < 1) continue; // Nothing matching

				// Sorting by style descending
				matchingItems.sort(function (a, b) {return b.Style - a.Style;});

				var wear = currentlyWorn == null // Nothing being worn, so wear it.
					|| $.inArray(Category, currentlyWorn.Category) < 0 // Item in slot is not of the right category, so swap them.
					|| currentlyWorn.Style < matchingItems[0].Style; // Item in slot has less style, so swap them.

				//if (wear) this.Wear(matchingItems[0]);

				if (wear) this.Clothing.Wear(matchingItems[0].Id);
			}
			App.Avatar._DrawPortrait();
		}

		Strip() {
			let prop: Data.ClothingSlot;
			for (prop in this.Clothing.Equipment) {
				const item = this.Clothing.Equipment[prop];
				if (item) {
					this.Clothing.TakeOff(item.Id);
				}
			}
			App.Avatar._DrawPortrait();
		}

		/**
		 * Tests is any of the named clothes worn in the given slot
		 * @returns True is any of the items is worn, false otherwise
		 */
		IsAnyClothingWorn(Tags: string[], Slot: Data.ClothingSlot): boolean {
			return Tags.some(t => this.Clothing.IsWorn(App.Items.MakeId("CLOTHES", t), Slot));
		}

		Remove(item: App.Items.Clothing) {
			if (!item) return;
			this.Clothing.TakeOff(item.Id);
			App.Avatar._DrawPortrait();
		}

		GetItemByName(Name: string) : App.Items.InventoryItem {
			return this.Inventory.filter(o => o.Tag == Name)[0];
		}

		/**
		 *
		 * @param Id String of "CATEGORY/NAME" template
		 */
		GetItemById(Id: string): Items.AnyItem | undefined {
			let result: Items.AnyItem | undefined | null = undefined;

			var ItemList: Items.AnyItem[] = this.Inventory.filter(function (o) {return o.Id == Id;}); // Look in items first.

			if (ItemList.length < 1) { // Now check wardrobe
				ItemList = this.Clothing.Wardrobe.filter(function (o) {return o.Id == Id;});
			}

			if (ItemList.length < 1) { // Check worn stuff.
				let k: Data.ClothingSlot;
				for (k in this.Clothing.Equipment) {
					const itm = this.Clothing.Equipment[k];
					if (itm && itm.Id == Id) {
						result = this.Clothing.Equipment[k];
						break;
					}
				}
			}

			if (!result) result = undefined;
			if (!result && ItemList.length > 0) result = ItemList[0];
			return result;
		}

		GetItemByTypes(Types: string[], Sort: boolean = false): Items.InventoryItem[] {
			var res = this.Inventory.filter(o => Types.contains(o.Type));

			if (!Sort) return res;

			res.sort((a, b) => {
				var af = this.Inventory.IsFavorite(a.Id);
				if (af != this.Inventory.IsFavorite(b.Id)) {return af ? -1 : 1;}
				return a.Name.localeCompare(b.Name);
			});
			return res;
		}

		/**
		 * Returns the total number of charges across all items belonging to a certain type.
		 */
		GetItemCharges(Type: string): number {
			var Items = this.Inventory.filter(function (o) {return o.Type == Type;});
			var Count = 0;
			for (var i = 0; i < Items.length; i++) Count += Items[i].Charges();
			return Count;
		}

		/**
		 * Returns number of charges for item with the given name and type (if provided) or among all types otherwise.
		 * @param Name Item name
		 * @param Item type (category) name
		 * @returns Number of charges available or zero.
		 */
		ItemCharges(Name: string, Type?: Data.InventoryItemTypeStr) {
			return this.Inventory.Charges(Type, Name);
		}

		/**
		 * Attempt to iterate through all items of same type and consume charges from them until Amount has been
		 * satisfied. It will delete items if it consumes all their charges.
		 */
		UseItemCharges(Type: string, Amount: number) {
			if (Amount <= 0) return;
			var Items = this.Inventory.filter(function (o) {return o.Type == Type;}) as Items.Consumable[];
			var Count = 0;

			while (Amount > 0 && Count < Items.length) {
				var Item = Items[Count];
				if (Item.Charges() <= 0) {
					Count++;
				} else {
					Item.RemoveCharges(1);
					Amount -= 1;
				}
			}
		}

		DeleteItem(item: Items.InventoryItem) {
			this.Inventory.RemoveItem(item.Id);
		}

		/**
		 * Create and add an item to the player.
		 */
		AddItem(Category: Data.EquipmentItemTypeStr, Name: string, unused: any, Opt?: "WEAR"): Items.Clothing;
		AddItem<T extends keyof InventoryItemTypeMap>(Category: T, Name: string, Count: number): InventoryItemTypeMap[T];
		AddItem<T extends keyof ItemTypeMap>(Category: T, Name: string, Count: any, Opt?: "WEAR"): ItemTypeMap[T];
		AddItem(Category: App.Data.ItemTypeStr, Name: string, Count: any, Opt?: "WEAR") {
			if (Category == "CLOTHES" || Category == "WEAPON") {
				if (this.OwnsWardrobeItem(Category, Name)) return; // No duplicate equipment allowed.
				return this.Clothing.AddItem(Name, Opt == "WEAR");
			} else {
				return this.Inventory.AddItem(Category, Name, Count);
			}
		}

		GetWornSkillBonus(Skill: Data.WearSkill) {
			var bonus = 0;
			for (const eq of Object.values(this.Clothing.Equipment)) {
				if (!eq) {continue;}
				var tBonus = eq.GetBonus(Skill);
				if (tBonus > 0) {
					bonus += tBonus;
					if (this._state.debugMode == true) console.log("Found skill bonus : " + Skill + " on" + eq.Name);
				}
			}
			return bonus;
		}

		/**
		 * Find item and reduce charges. Delete from inventory if out of charges.
		 * @param ItemId
		 * @param Charges to consume
		 * @returns The item object
		 */
		TakeItem(ItemId: string, Charges = 1): Items.AnyItem | undefined {
			var o = this.GetItemById(ItemId);
			if (o instanceof Items.Clothing || o instanceof Items.Reel) {
				return o;
			}
			if (o) {
				o.RemoveCharges(Charges); // will remove the item from inventory if charges reaches 0
			}
			return o;
		}

		/**
		 * Use an item. Apply effects. Delete from inventory if out of charges.
		 */
		UseItem(ItemId: string): string {
			var o = this.TakeItem(ItemId);
			if (o instanceof Items.Consumable) {
				this.AddHistory("ITEMS", o.Tag, 1);
				o.ApplyEffects(this);
				var msg = o.Message(this);
				return msg;
			}
			return "";
		}

		/**
		 * Applies named effects on the player
		 */
		ApplyEffects(EffectNames: string[]): void {
			for (const eName of EffectNames) {
				console.group(`Effect ${eName}`);
				App.Data.EffectLib[eName].FUN(this, null);
				console.groupEnd();
			}
		}

		pBodyChange(BodyPart: string, Direction: "Grow" | "Shrink"): string {
			return App.Data.Lists["BodyChanges"][BodyPart][Direction];
		}

		GetHistory(Type: string, Flag: string): number {
			if ((typeof this._state.History[Type] === 'undefined')) return 0;
			if ((typeof this._state.History[Type][Flag] === 'undefined')) return 0;

			return this._state.History[Type][Flag];
		}

		AddHistory(Type: string, Flag: string, Amount: number): void {
			if ((typeof this._state.History[Type] === 'undefined')) this._state.History[Type] = {};
			if ((typeof this._state.History[Type][Flag] === 'undefined')) this._state.History[Type][Flag] = 0;

			const t = this.GetHistory(Type, Flag);
			this._state.History[Type][Flag] = (t + Amount);
		}

		RemoveHistory(Type: string, Flag: string) {
			if ((typeof this._state.History[Type][Flag] !== 'undefined')) delete this._state.History[Type][Flag];
		}

		// Voodoo
		HasHex(Hex: string): boolean {
			return this._state.VoodooEffects.hasOwnProperty(Hex);
		}

		SetHex(Hex: string, Value: string | number): void {
			this._state.VoodooEffects[Hex] = Value;
		}

		RemoveHex(Hex: string): void {
			delete this._state.VoodooEffects[Hex];
		}

		EndHexDuration(): void {
			for (var prop in this._state.VoodooEffects) {
				if (!this._state.VoodooEffects.hasOwnProperty(prop)) continue;
				switch (prop) {
					case "PIRATES_PROWESS_DURATION":
						(<number>this._state.VoodooEffects[prop])--;
						if (this._state.VoodooEffects[prop] <= 0) {
							delete this._state.VoodooEffects["PIRATES_PROWESS"];
							delete this._state.VoodooEffects["PIRATES_PROWESS_DURATION"];
							setup.Notifications.AddMessage('STATUS_CHANGE', this.Day + 1, "You feel the effects of your pirates skill leave you…");
						}
				}
			}
		}

		// Acquire everything for debug purposes
		AcquireAllItems(): void {
			console.group("AcquireAllItems");
			for (var prop in App.Data.Clothes) {
				if (App.Data.Clothes.hasOwnProperty(prop)) {
					if (!this.OwnsWardrobeItem("CLOTHES", prop)) {
						console.log("Adding \"" + prop + "\" (clothes)");
						this.AddItem("CLOTHES", prop, 1);
					} else {
						console.log("\"" + prop + "\" (clothes) already owned");
					}
				}
			}
			console.groupEnd();
		}

		/**
		 * Returns number that represents how high-class the PC looks. 0 is obvious commoner and 100 is a rich noble. Can be higher or lower.
		 */
		HighClassPresentability(): number {
			// For now just consider "Slutty Lady" the proper attire, while other clothes contribute only part of their style.
			// Do not count parts that are not visible in a "proper" situation.
			const getBonus = (slot: Data.ClothingSlot, equipmentItem: Items.Clothing) => {
				// The proper attire
				var bonus = equipmentItem.CategoryBonus("Slutty Lady");
				if (bonus > 0) {
					console.log("Slot: " + slot + ", equipment name: \"" + equipmentItem.Name + "\", bonus: " + bonus);
					return bonus;
				}

				// Not so proper, but closer than others
				bonus = equipmentItem.CategoryBonus("High Class Whore") / 2;
				if (bonus > 0) {
					console.log("Slot: " + slot + ", equipment name: \"" + equipmentItem.Name + "\", bonus: " + bonus);
					return bonus;
				}

				// Oh well
				bonus = equipmentItem.Style / 4;
				console.log("Slot: " + slot + ", equipment name: \"" + equipmentItem.Name + "\", bonus: " + bonus);
				return bonus;
			};

			console.group("HighClassPresentability");
			var result = 0;

			let slot: Data.ClothingSlot;
			for (slot in this.Clothing.Equipment) {
				if (slot == "Nipples" || slot == "Bra" || slot == "Panty" || slot == "Butt" || slot == "Penis") continue;
				if (!this.Clothing.Equipment.hasOwnProperty(slot)) continue;

				var equipmentItem = this.Clothing.Equipment[slot];
				if (equipmentItem == null) continue;

				result += getBonus(slot, equipmentItem);
			}

			console.log("Total for clothes: " + result);

			var forHair = this.HairRating() * 0.25;
			var forMakeup = this.MakeupRating() * 0.25;
			console.log("For hair: " + forHair);
			console.log("For makeup: " + forMakeup);

			result += forHair + forMakeup;
			console.log("Total: " + result);
			console.groupEnd();
			return result;
		}

		// TODO: include other factors and maybe calibrate.
		/**
		 * Returns number that represents how obvious it is that the PC is male. 100 means completely impassable and 0 means completely passable. Can be higher or lower.
		 */
		ObviousTrappiness(): number {
			console.group("ObviousTrappiness");
			console.log(this._state.BodyStats);
			console.log(this._state.CoreStats);

			// Huge penis makes trappiness very, very obvious
			var penisOversizing = Math.max(this._state.BodyStats.Penis - 75, 0);
			var penisContribution = penisOversizing * penisOversizing;
			console.log("penisContribution: " + penisContribution);

			// Let's say DD breasts give -50, and we have diminishing returns
			var bustContribution = - Math.sqrt(this._state.BodyStats.Bust * 50 * 50 / 11);
			console.log("bustContribution: " + bustContribution);

			// Style gives a small contribution, but synergizes with femininity
			var styleContribution = - this.ClothesRating() * .1;
			console.log("styleContribution: " + styleContribution);

			// Femininity is important
			var femininityContribution = - this._state.CoreStats.Femininity * .3;
			console.log("femininityContribution: " + femininityContribution);

			// Full femininity and full style give -50 together
			var femininityStyleSynergy = - this._state.CoreStats.Femininity * this.ClothesRating() * .005;
			console.log("femininityStyleSynergy: " + femininityStyleSynergy);

			// Base value is full
			var result = 100 + penisContribution + bustContribution + styleContribution + femininityContribution + femininityStyleSynergy;
			// Let's make sure result is not too far from soft borders
			if (result > 100) result = 100 + Math.sqrt(result - 100);
			if (result < 0) result = - Math.sqrt(-result);

			console.log("result: " + result);
			console.groupEnd();

			return result;
		}

		// region SLOT wheel stuff

		/**
		 * Unlock a slot.
		 */
		UnlockSlot(): boolean {
			if (this._state.CurrentSlots + 1 > this.MaxSlots) return false;
			this._state.CurrentSlots++;
			return true;
		}

		/**
		 * Fetch all reels in the players inventory.
		 */
		GetReelsInInventory(): Items.Reel[] {
			return this.Inventory.filter(function (o) {return (o.Type == 'REEL');}) as Items.Reel[];
		}

		/**
		 * Fetch a single reel by ID.
		 */
		GetReelByID(filterID: string): Items.Reel | null {
			var arr = this.GetReelsInInventory();
			arr = arr.filter(function (o) {return (o.Id == filterID);});
			return (arr.length > 0) ? arr[0] : null; // huh? At least grab the first one.
		}

		/**
		 * Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		 * equipped in that slot and place it back in the inventory.
		 */
		EquipReel(toEquipID: string, reelSlot: number): void {
			this.Inventory.EquipReel(toEquipID, reelSlot);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		RemoveReel(slotID: number): void {
			this.Inventory.RemoveReel(slotID);
		}

		/**
		 * Turn the equipped reels into an array to iterate/read.
		 */
		GetReels(): Items.Reel[] {
			return this.Inventory.EquippedReelItems();
		}

		get IsFuta(): boolean {
			return this._state.CoreStats[CoreStat.Futa] > 0;
		}

		// endregion

		get InventoryManager(): InventoryManager {
			if (!this.#inventory) {
				this.#inventory = new App.Entity.InventoryManager("PlayerState");
			}
			return this.#inventory;
		}

		get Clothing(): App.Entity.ClothingManager {
			if (!this.#clothing) {
				this.#clothing = new App.Entity.ClothingManager("PlayerState");
			}
			return this.#clothing;
		}

		/**
		 * Returns total number of item charges in inventory
		 */
		InventoryItemsCount(): number {
			var res = 0;
			this.InventoryManager.ForEachItemRecord(undefined, undefined, function (n /*, tag, itemClass*/) {res += n;});
			return res;
		}

		/**
		 * Returns total number of different item types in inventory
		 */
		InventoryItemsTypes(): number {
			var res = 0;
			this.InventoryManager.ForEachItemRecord(undefined, undefined, function (/*n, tag, itemClass*/) {res += 1;});
			return res;
		}
		// redirections for the state properties

		get OriginalName() {return this._state.OriginalName;}
		set OriginalName(n) {this._state.OriginalName = n;}
		get SlaveName() {return this._state.SlaveName;}
		set SlaveName(n) {this._state.SlaveName = n;};
		get GirlfriendName() {return this._state.GirlfriendName;}
		set GirlfriendName(n) {this._state.GirlfriendName = n;}
		get NickName() {return this._state.NickName;}
		get HairColor() {return this._state.HairColor;}
		set HairColor(c) {this._state.HairColor = c;}
		get HairStyle() {return this._state.HairStyle;}
		set HairStyle(s) {this._state.HairStyle = s;}
		get HairBonus() {return this._state.HairBonus;}
		set HairBonus(v) {this._state.HairBonus = v;}
		get MakeupStyle() {return this._state.MakeupStyle;}
		set MakeupStyle(s) {this._state.MakeupStyle = s;}
		get MakeupBonus() {return this._state.MakeupBonus;}
		set MakeupBonus(v) {this._state.MakeupBonus = v;}
		get EyeColor() {return this._state.EyeColor;}
		get Money() {return this._state.Money;}
		set Money(c) {this._state.Money = c;}
		get Tokens() {return this._state.Tokens;}
		set Tokens(c) {this._state.Tokens = c;}
		get SailDays() {return this._state.SailDays;}
		get LastUsedMakeup() {return this._state.LastUsedMakeup;}
		get LastUsedHair() {return this._state.LastUsedHair;}
		get LastQuickWardrobe() {return this._state.LastQuickWardrobe;}
		get debugMode() {return this._state.debugMode;}
		set debugMode(v) {this._state.debugMode = v;}
		get difficultySetting() {return this._state.difficultySetting;}
		get WhoreTutorial() {return this._state.WhoreTutorial;}
		set WhoreTutorial(v) {this._state.WhoreTutorial = v;}

		get JobFlags() {return this._state.JobFlags;}
		get VoodooEffects() {return this._state.VoodooEffects;}
		get QuestFlags() {return this._state.QuestFlags;} // Default Quest.

		increaseFlagValue(type: FlagType, name: string, value: number): number {
			const flagsObj = type === FlagType.Job ? this._state.JobFlags : this._state.QuestFlags;
			const newVal = (flagsObj[name] ?? 0) as number + value;
			flagsObj[name] = newVal;
			return newVal;
		}

		deleteFlag(type: FlagType, name: string): void {
			const flagsObj = type === FlagType.Job ? this._state.JobFlags : this._state.QuestFlags;
			delete flagsObj[name];
		}


		get History() {return this._state.History;}

		// Game/Environment Variables
		get Day() {return this._state.Day;}
		set Day(n) {this._state.Day = n;};
		get Phase() {return this._state.Phase;} // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night
		set Phase(n) {this._state.Phase = n;}

		// Player Statistic Variables
		get CoreStats() {return this._state.CoreStats;}

		get CoreStatsXP() {return this._state.CoreStatsXP;}

		get BodyStats() {return this._state.BodyStats;}

		get BodyXP() {return this._state.BodyXP;}

		get Skills() {return this._state.Skills;}

		get SkillsXP() {return this._state.SkillsXP;}

		get Wardrobe() {return this.Clothing.Wardrobe;}

		get Inventory() {return this.InventoryManager;}
		get Equipment() {return this.Clothing.Equipment;}

		get StoreInventory() {return this._state.StoreInventory;}
		get NPCS() {return this._state.NPCS;}

		get Slots() {return this.Inventory.ReelSlots();}
		get CurrentSlots() {return this._state.CurrentSlots;} // Starting allocation of whoring

		get MaxSlots() {return 9;} // YOU SHALL NOT PASS

		SaveLoaded(): void {
			this.#inventory = null;
			this.#clothing = null;
		}

		get GameStats() {return this._state.GameStats;}

		get FaceData() {return this._state.FaceData;}

		SetFace(PresetID: string) {
			this._state.FaceData = App.Data.DAD.FacePresets[PresetID];
		}

		static get instance(): Player {
			return setup.player;
		}
	};

	function isNumber(val: any): asserts val is number {
		if (typeof val !== 'number') {
			throw `Unexpected value of type ${typeof val} when a number was expected.`
		}
	}
}
