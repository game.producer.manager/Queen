namespace App.UI {

	export abstract class DOMPassage {
		constructor(nameOrNames: string | string[]) {
			if (Array.isArray(nameOrNames)) {
				for (const n of nameOrNames) {
					PassageRenderManager.instance.register(n, this);
				}
			} else {
				PassageRenderManager.instance.register(nameOrNames, this);
			}
		}
		public abstract render(passageName: string, content: HTMLElement): HTMLElement | DocumentFragment | null;
	}

	type PassageRenderHandler = (passageName: string, content: HTMLElement) => HTMLElement | DocumentFragment | null;

	export class PassageRenderManager {

		private static instance_: PassageRenderManager | null = null;
		static get instance() {
			if (PassageRenderManager.instance_ === null) {
				PassageRenderManager.instance_ = new PassageRenderManager();
			}
			return PassageRenderManager.instance_;
		}

		constructor() {
			$(document).on(":passagerender", ev => {
				const renderer = this.passages_.get(ev.passage.title);
				if (renderer) {
					const newContent = renderer.render(ev.passage.title, ev.content);
					if (newContent) {
						ev.content.append(newContent);
					}
				}

				for (const h of this.handlers_) {
					const newContent = h(ev.passage.title, ev.content);
					if (newContent) {
						ev.content.append(newContent);
					}
				}
			});

			$(document).on(":passagedisplay", () => {
				const pn = "StoryCaption";
				const renderer = this.passages_.get(pn);
				if (renderer && !variables()['InIntro']) {
					const elem = document.querySelector("#my-story-caption") as HTMLElement;
					const newContent = renderer.render(pn, elem);
					if (newContent) {
						App.UI.replace(elem, newContent);
					}
				}
			});
		}

		register(passageName: string, passage: DOMPassage) {
			this.passages_.set(passageName, passage);
		}

		registerHandler(handler: PassageRenderHandler) {
			this.handlers_.push(handler);
		}

		unregister(passageName: string) {
			this.passages_.delete(passageName);
		}

		private passages_: Map<string, DOMPassage> = new Map();
		private handlers_: PassageRenderHandler[] = [];
	}
}
