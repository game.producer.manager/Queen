namespace App.Notifications {
	// Class for handling player notifications. Currently only affects the overnight sleeping, but could naturally
	// be extended to handle messages EOF messages of jobs (scenes)

	type MessageCategory = "DREAMS" | "STAT_CHANGE" | "STATUS_CHANGE" | "KNOWLEDGE";

	export class Engine {
		/** Array of message objects. */
		private _messages: Message[] = [];

		private getMessages(category: MessageCategory, day: number) {
			return this._messages.filter( o => o.Category == category && o.Day == day);
		}

		/**
		 * @returns Array of messages
		 */
		get Messages(): Message[] {
			return this._messages;
		}

		/**
		 *
		 * @param category Type of message. DREAMS | STAT_CHANGE | STATUS_CHANGE | KNOWLEDGE
		 * @param day Typically you want the player.Day + 1 to display on the sleep screen.
		 * @param message Message to display. Can have tokens added to it.
		 */
		AddMessage(category: MessageCategory, day: number, message: string): void {
			if (category == 'STAT_CHANGE') {
				this._messages.push(new StatMessage(category, day, message));
			} else {
				this._messages.push(new Message(category, day, message));
			}
		}
		/**
		 * Print a plain string list of messages separated by optional character.
		 */
		StrPrint(category: MessageCategory, Day: number, header: string, color: string, bgColor: string, character = '<br>') {
			if (setup.player.debugMode) console.debug("Notification:StrPrint(" + category + "," + Day + "," + character);
			var output = this.getMessages(category, Day).map(o => o.Print).join(character);
			if (output != "") {
				output = this.Header(header, color, bgColor) + output;
			}

			return output;
		}

		/**
		 * Format a header for a message group
		 * @param Header Text to display
		 * @param Color Text color
		 * @param BgColor Background color
		 */
		Header(header: string, color: string, bgColor: string) {
			var str = "<div style='width:100%;margin-bottom:10px;padding-left:20px;font-weight:bold;color:" +
				color + ";background-color:" + bgColor + "'>" + header + "</div>";

			return str;
		}
	}


	class Message {
		private _Category: MessageCategory;
		private _Day: number;
		private _Message: string;
		/**
		 *
		 * @param Category Category or Section head of message.
		 * @param Day Day message shall render on.
		 * @param Message The text of the message.
		 */
		constructor(Category: MessageCategory, Day: number, Message: string) {
			this._Category = Category;
			this._Day = Day;
			this._Message = Message;
		};

		get Category() {return this._Category;}
		get Day() {return this._Day;}
		get Message() {return this._Message;}

		get Print() {
			return App.PR.TokenizeString(setup.player, undefined, this._Message);
		}

	};

	/**
	 * Reserved for future use.
	 */
	class StatMessage extends Message {
		constructor(Category: MessageCategory, Day: number, Message: string) {
			super(Category, Day, Message);
		}

		get Print() {
			return super.Print;
		}

	}
}
