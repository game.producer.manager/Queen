namespace App {
	export let Avatar: Entity.AvatarEngine;

	export namespace Combat {
		export const EnemyData: Record<string, Data.Combat.Enemy> = {};
		export const Moves: Record<string, Data.Combat.Style> = {};
		export const EncounterData: Record<string, Data.Combat.EncounterDesc> = {};
		export const ClubData: Record<string, Data.Combat.ClubEncounter[]> = {};
		export const ClubBetData: Record<string, string[]> = {};
	}

	export namespace Data {
		//namespace AvatarMaps {}
		export namespace Abamond {
			export const Mobs: Record<string, Data.ModDesc[]> = {};
		}

		export const Drugs: Record<string, DrugItemDesc> = {};
		export const Food: Record<string, FoodItemDesc> = {};
		export const Quests: Record<string, Tasks.Quest> = {};
		export const JobData: Record<string, Data.Tasks.Job> = {};
		export let Cosmetics: Record<string, CosmeticsItemDesc> = {};
		export const Misc: Record<string, ConsumableItemDesc> = {};
		export const MiscLoot: Record<string, ConsumableItemDesc> = {};
		export const Clothes: Record<string, ClothingItemDesc> ={};
		export const Stores: Record<string, StoreDesc> = {};
		export const NPCS: Record<string, NPCDesc> = {};
		export const NPCGroups: Record<string, string[]> = {};
		export const QuestItems: Record<string, InventoryItemDesc> = {};
		export const LootBoxes: Record<string, ConsumableItemDesc> = {};
		export const Slots: Record<string, ReelDesc> = {};

		export const EffectLib: Record<string, EffectDesc> = {};
		export const EffectLibClothing: Record<string, ClothingWearEffectDesc> = {};

		export const Events: Record<string, EventDesc[]> = {};
		export const Whoring: Record<string, Whoring.WhoringLocationDesc> = {};

		export const Loot: Record<string, Data.LootItem[]> = {};
		export const LootTables: Record<string, LootTableItem[]> = {};

		export const Tarot: Record<string, TarotCardDesc> = {};

		export const DADNPC: Record<string, any> = {};
		export namespace DAD {
			export let ExtraParts: any = {};
			export let FaceStruct: any = {};
			export const FacePresets: any = {};
		}

		export namespace Travel {
			export var destinations: Record<string, Destinations> = {};
			export var passageDisplayNames: Record<string, Data.Travel.DynamicText> = {};
		}
	}

	export namespace Entity { };
	export namespace UI {
		export namespace Widgets { };
	};
}

/*


App.Data.JobData = {};
App.Data.Loot = {};
App.Data.LootTables = {};
App.Data.Tarot = {};
App.Data.Whoring = {};

App.Gambling = {};
App.Gambling.Coffin = {};

App.Combat = { };
App.Combat.ClubData = [ ];
App.Combat.ClubBetData = [ ];
App.Combat.EncounterData = { };
App.Combat.EnemyData = { };
App.Combat.Engines = { };
App.Combat.Moves = {};

App.Rogue = {};

App.Notifications = {};
*/

// export the App object from the SugarCube closured eval()
/** @namespace */
globalThis.App = App;
