namespace App {
	/**
	 * This class manages the "job" system. Basically short term repeatable quests.
	 * They are not tracked by the journal and focus mostly on earning cheap rewards and flavor text.
	 */
	export class JobEngine {
		#jobs: App.Job[] = [];
		#DanceInfo = { // Hack for dancing jobs.
			DANCE: "Sexy Dancer" as Data.Clothes.CategoryStr,
			DAY: 1,
			PHASE: 0
		};
		constructor() {
		}

		/**
		 * The Job engine singleton
		 */
		static get instance() : JobEngine {
			return setup.jobEngine;
		}

		/**
		 * Hack for reporting the current 'desired dance' for dance halls.
		 */
		GetDance() : Data.Clothes.CategoryStr {
			if (this.#DanceInfo["DAY"] < setup.player.Day || this.#DanceInfo["PHASE"] < setup.player.Phase) {
				this.#DanceInfo["DANCE"] = App.PR.GetRandomListItem(App.Data.Fashion["STYLES"]);
				this.#DanceInfo["DAY"] = setup.player.Day;
				this.#DanceInfo["PHASE"] = setup.player.Phase;
			}
			return this.#DanceInfo["DANCE"];
		}

		/**
		 * Loads the job data into an array of Job objects. Can be called again if necessary.
		 */
		LoadJobs() {
			if (this.#jobs.length < 1) {
				this.#jobs = Object.values(App.Data.JobData).map(jd => new App.Job(jd));
			}
		}

		/**
		 * Lists all jobs at person/location
		 */
		GetJobs(_player: Entity.Player, giver: string) {
			this.LoadJobs();
			return this.#jobs.filter(j => j.Giver == giver).sort();
		}

		/**
		 * Return a Job by it's id property.
		 */
		GetJob(JobID: string) {
			this.LoadJobs();
			return this.#jobs.find(j => j.ID() == JobID);
		}

		JobAvailable(Player: App.Entity.Player, NPC: App.Entity.NPC, JobID: string) {
			this.LoadJobs();
			return this.#jobs.find(j => j.ID() == JobID && j.Available(Player, NPC));
		}
		/**
		 * Lists all AVAILABLE jobs at a person
		 */
		GetAvailableJobs(player: App.Entity.Player, giver: string) {
			const npc = player.GetNPC(giver);
			return this.GetJobs(player, giver).filter(j => j.Available(player, npc));
		}

		/**
		 * Lists available jobs at a location
		 */
		GetAvailableLocationJobs(Player:App.Entity.Player, Giver: string) {
			return this.GetJobs(Player, Giver).filter(j => j.Requirements(Player, Giver));
		}

		/**
		 * Lists all UNAVAILABLE jobs on person
		 */
		GetUnavailableJobs(Player:App.Entity.Player, Giver: string) {
			return this.GetJobs(Player, Giver).filter(j => !j.Available(Player, Player.GetNPC(Giver)) && !j.Hidden()).sort();
		}

		/**
		 * Lists unavailable jobs at a location
		 */
		GetUnavailableLocationJobs(Player:App.Entity.Player, Giver: string) {
			return this.GetJobs(Player, Giver).filter(j => !j.Requirements(Player, Giver));
		}

		/**
		 * True if there are AVAILABLE jobs at this person/location
		 */
		JobsAvailable(Player:App.Entity.Player, Giver: string) {
			return this.GetAvailableJobs(Player, Giver).length != 0;
		}

		/**
		 * True if there are ANY jobs at this person/location
		 */
		HasJobs(Player:App.Entity.Player, Giver: string) {
			return (this.GetAvailableJobs(Player, Giver).length != 0) || (this.GetUnavailableJobs(Player, Giver).length != 0);
		}

		HasJobFlag(Player: App.Entity.Player, Flag: string) {
			return Player.JobFlags.hasOwnProperty(Flag);
		}

		SetJobFlag(Player: App.Entity.Player, Flag: string, Value: string|number) {
			Player.JobFlags[Flag] = Value;
		}

		//this.Init(App.Data.JobData);
	}
}
