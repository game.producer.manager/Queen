namespace App {
	type BonusFunction = (player: Entity.Player, arg: any) => number;
	type SexualitySource = Data.Whoring.BonusCategory;
	type BonusSource = Data.ReelAction | Data.Clothes.CategoryStr | SexualitySource | Data.Whoring.SexualBodyPart;

	interface Payout {
		slot: number;
		payout: number;
		xp: number;
		mood: number;
		lust: number;
		sat: number;
		type: string;
		want: number;
	}
	interface MoneyEarning {
		money: number;
		want: Data.ReelAction;
		type: string;
	}

	/**
	 * Main engine for slot machine mini game.
	 */
	export class SlotEngine {
		#Customers: Customer[] = [];
		#Rares: string[] = [];
		#MaxSlots = 9;
		#Element = "#WhoreUI";
		#InventoryElement = "#SlotInventoryUI";
		#DataKey = "";
		#Spins = 5;
		#Spinning = false;
		#SelectedCustomer: number = -1;
		#Player: App.Entity.Player | undefined;
		#ActiveReels = [];
		#MoneyEarned: MoneyEarning[] = [];
		#XPEarned: Partial<Record<Data.ReelAction, number>> = {};
		#Misses = 0;
		#DesperationSpins = 0;
		#ReturnPassage = "Deck";
		#EndStatus: string[] = [];
		#Title = "A BACK ALLEY";
		#Marquee: string | null = null;
		#SelectedSlot: number = -1;
		#bonusMods: Record<BonusSource, BonusFunction>;
		#Reels: EZSlots | undefined;

		private static _instance: SlotEngine;
		/**
		 * Dictionary of string tokens to css class names.
		 */
		#SlotTypes:Record<Data.ReelAction, string> = {
			"ASS": "AssSlotReel",
			"TITS": "TitsSlotReel",
			"BJ": "BJSlotReel",
			"HAND": "HandSlotReel",
			"FEM": "FemSlotReel",
			"PERV": "PervSlotReel",
			"BEAUTY": "BeautySlotReel"
		};

		#SlotTypesToNames: Record<Data.ReelAction, string> = {
			"ASS": "Ass Fucking",
			"TITS": "Tit Fucking",
			"BJ": "BlowJobs",
			"HAND": "HandJobs",
			"FEM": "Femininity",
			"PERV": "Perversion",
			"BEAUTY": "Beauty"
		};

		#SlotTypesToSkillNames: Record<Data.ReelAction, Skill | SexualitySource> = {
			"ASS": Skill.AssFucking,
			"TITS": Skill.TitFucking,
			"BJ": Skill.BlowJobs,
			"HAND": Skill.HandJobs,
			"FEM": "Femininity",
			"PERV": "Perversion",
			"BEAUTY": "Beauty"
		};

		#WildCards: Data.ReelWildcard[] = [
			"FEM", "PERV", "BEAUTY"
		];

		constructor() {
			this.#bonusMods = {
				'PERV': this._PervBonus,
				'Perversion': this._StatBonus,
				'FEM': this._FemBonus,
				'Femininity': this._StatBonus,
				'BEAUTY': this._StyleBonus,
				'Beauty': this._StyleBonus,

				'Bust': this._BodyBonus,
				'Ass': this._BodyBonus,
				'Face': this._BodyBonus,
				'Lips': this._BodyBonus,
				'Penis': this._BodyBonus,

				'ASS': function (player, d) {return player.StatRoll("SKILL", "AssFucking", d, 1, true);},
				'HAND': function (player, d) {return player.StatRoll("SKILL", "HandJobs", d, 1, true);},
				'TITS': function (player, d) {return player.StatRoll("SKILL", "TitFucking", d, 1, true);},
				'BJ': function (player, d) {return player.StatRoll("SKILL", "BlowJobs", d, 1, true);},

				"Pirate Slut": this._FashionBonus,
				"Bimbo": this._FashionBonus,
				"Sissy Lolita": this._FashionBonus,
				"Gothic Lolita": this._FashionBonus,
				"BDSM": this._FashionBonus,
				"Daddy's Girl": this._FashionBonus,
				"Naughty Nun": this._FashionBonus,
				"Pet Girl": this._FashionBonus,
				"High Class Whore": this._FashionBonus,
				"Slutty Lady": this._FashionBonus,
				"Sexy Dancer": this._FashionBonus,
				"Ordinary": this._FashionBonus
			};
		}

		// region PUBLIC FUNCTIONS

		static get instance(): SlotEngine {
			if (!SlotEngine._instance) {
				SlotEngine._instance = new SlotEngine();
			}
			return SlotEngine._instance;
		}

		LoadScene(dataKey: string, returnPassage: string, Player: App.Entity.Player, elementID?: string) {
			if (typeof elementID !== 'undefined') this.#Element = elementID;
			this.#Player = Player;
			this.#DataKey = dataKey;
			this.#ReturnPassage = returnPassage;

			// Load the data dictionary for this scene.
			var dict = App.Data.Whoring[dataKey];

			// You can have up to 6 customers. This is largely dictated by the overall lust of the NPC container object.
			var npc = this.#Player.GetNPC(dict.NPC_TAG);
			//var maxCustomers = Math.round(Math.max(1, Math.min( (6 * (npc.Lust()/100)), 6)));
			this.#Title = dict.DESC;
			this.#Customers = []; // Clear data.
			this.#Rares = [];
			this.#MoneyEarned = []; //
			this.#XPEarned = {};
			this.#Spins = 5;
			this.#DesperationSpins = 0;
			this.#Misses = 0;
			this.#EndStatus = [];
			this.#SelectedCustomer = -1;
			this.#Marquee = dict.MARQUEE ?? null;

			while (this.#Customers.length < 6) {
				// We could generate a rare customer.
				if (dict.RARES.length > 0) {
					if (Math.floor(Math.random() * 100) >= 90) {
						const customerObject = new Customer(App.PR.GetRandomListItem(dict.RARES));
						// Check to see if we already drew this rare.
						if (!this.#Rares.includes(customerObject.Name)) {
							this.#Customers.push(customerObject);
							this.#Rares.push(customerObject.Name);
							continue;
						}
					}
				}

				this.#Customers.push(new Customer(this.#DataKey));
			}
		}

		get ReturnPassage(): string {
			return this.#ReturnPassage;
		}

		/**
		 * Called from the Twine passage. Set the player object in the engine and attach an event to listen
		 * for when the passage finishes rendering to fire off jquery.
		 */
		DrawUI(Player: Entity.Player) {
			if (typeof Player !== 'undefined') this.#Player = Player;
			$(document).one(":passageend", this._DrawUICB.bind(this));
		}

		/**
		 * Fetch the equipped slots on the player.
		 */
		GetSlots() {
			if (this.#Player === undefined) return [];
			return this.Player.GetReels();
		}

		/**
		 * Called from the skills panel to draw the interface for slot management.
		 */
		DrawSlotInventory(Player?: Entity.Player) {
			if (typeof Player !== 'undefined') this.#Player = Player;
			$(document).one(":passageend", this._DrawSlotInventoryCB.bind(this));
		}


		private _AddXXX(ob: Customer, root: JQuery) {
			var num = random(1, 10);
			var pic: string;
			switch (ob.MostSatisfied().want) {
				case "ASS": pic = "ss_anal_" + num; break;
				case "HAND": pic = "ss_hand_" + num; break;
				case "BJ": pic = "ss_bj_" + num; break;
				case "TITS": pic = "ss_tit_" + num; break;
				default: pic = "ss_anal_" + num; break;
			}

			root.addClass(pic);
		}

		/**
		 * Called from the 'WhoreEnd' passage to give out loot/rewards and to tell the player about it.
		 */
		PrintResults(Player: Entity.Player): string {
			if (this.#EndStatus.length > 0) return this.#EndStatus.join('\n');

			this.#Player = Player;
			this.#EndStatus = [];

			this.#Player.NextPhase();

			var tmp = "";
			var bonus = 0;
			var i = 0;
			var oSatisfied = this.#Customers.filter(function (o) {return o.Satisfaction >= 100;});

			Player.AddHistory("CUSTOMERS", this.#DataKey, oSatisfied.length); // Keep track of satisfied customers.

			switch (oSatisfied.length) {
				case 0: tmp = "You satisfied no customers, this is going to catch up with you later."; bonus = 1.0; break;
				case 1:
				case 2: tmp = "You satisfied " + oSatisfied.length + " customers, earning you a slight boost in overall mood."; bonus = 1.1; break;
				case 3:
				case 4:
				case 5: tmp = "You satisfied " + oSatisfied.length + " customers, earning you a healthy boost in overall mood."; bonus = 1.2; break;
				case 6: tmp = "You satisfied " + oSatisfied.length + " customers, earning you a large boost in overall mood."; bonus = 1.5; break;
			}

			this.#EndStatus.push(tmp); // Add mood chat.

			var npcTags = this.#Customers.map(function (o) {return o.Tag;}).filter(function (a, b, s) {return s.indexOf(a) == b;}); // All available tags.

			var oMood = this.#Customers.filter(function (o) {return (o.Mood != o.oMood);}); // Get all affected customers.
			var mood = Math.floor(Math.floor(Math.floor((oMood.reduce(function (a, o) {return (a + (o.Mood - o.oMood))}, 0) / 6)) * bonus) * 0.8); // Set mood adjustment
			var affTags = oMood.map(function (o) {return o.Tag;}).filter(function (a, b, s) {return s.indexOf(a) == b;}); // Tags we touched their mood.

			for (i = 0; i < npcTags.length; i++) // Iterate through and adjust mood.
				if (affTags.contains(npcTags[i]))
					this.#Player.GetNPC(npcTags[i]).AdjustStat("Mood", mood);

			// Do the same for lust.

			var oLust = this.#Customers.filter(function (o) {return (o.Lust != o.oLust);}); // Get all affected customers.
			var lust = (Math.floor(Math.floor((oLust.reduce(function (a, o) {return (a + (o.oLust - o.Lust))}, 0) / 6)) * 0.5) * -1.0); // Set mood adjustment
			affTags = oLust.map(function (o) {return o.Tag;}).filter(function (a, b, s) {return s.indexOf(a) == b;}); // Tags we touched their mood.

			for (i = 0; i < npcTags.length; i++) // Iterate through and adjust mood.
				if (affTags.contains(npcTags[i]))
					this.#Player.GetNPC(npcTags[i]).AdjustStat("Lust", lust);

			// Show XP gain.
			// Sort gain by XP points via a temporary array
			var xpArray = Object.keys(this.#XPEarned).map((key) => {
				return {"key": key as Data.ReelAction, "value": this.#XPEarned[key as Data.ReelAction]}
			});
			// Sort the array based on the "value" element property
			xpArray.sort(function (first, second) {return second.value - first.value;});

			for (var i = 0; i < xpArray.length; ++i) {
				var key = xpArray[i].key;
				var statName = this.#SlotTypesToSkillNames[key];
				if (statName == 'Beauty') continue;

				var name = this.#SlotTypesToNames[key];

				var xp = xpArray[i].value;
				if (statName === 'Perversion' || statName === 'Femininity') {
					this.#Player.AdjustStatXP(statName, xp);
				} else {
					this.#Player.AcknowledgeSkillUse(statName, xp);
					this.#Player.AdjustSkillXP(statName, xp);
				}

				this.#EndStatus.push("You gained @@.item-xp;" + xp + "@@ " + name + " XP.");
			}

			// Show Money Gain.
			var money = this.#Customers.reduce(function (a, o) {return a + o.Spent}, 0);
			this.#Player.earnMoney(money, Entity.Income.Whoring);
			this.#EndStatus.push("You gained @@.item-money;" + money + "@@ coins in payment for services rendered.");

			// Check for rare loot drops.
			for (i = 0; i < oSatisfied.length; i++) {
				if ((1 + Math.floor(Math.random() * 100)) <= (oSatisfied[i].Mood - 60)) {
					var tipTotal = oSatisfied[i].Spent;
					var item = App.Items.PickItem(['FOOD', 'DRUGS', 'COSMETICS'], {price: tipTotal});
					if (item != null) {
						this.#Player.AddItem(item.cat as Data.InventoryItemTypeStr, item.tag, 0);
						this.#EndStatus.push(oSatisfied[i].Name + " gave you an extra tip: " + item.desc);
					}
				}
			}

			var desperation = this.#DesperationSpins * 20;
			if (desperation > 0) {
				this.#Player.AdjustStatXP("WillPower", (desperation * -1.0));
				this.#EndStatus.push("You lost @@.state-negative;" + desperation + " willpower XP@@. This is a dangerous situation.");
			}
			return this.#EndStatus.join("\n");
		}

		private get Player() {
			if (!this.#Player) {throw "Player is undefined";}
			return this.#Player;
		}

		private get Reels() {
			if (!this.#Reels) {throw "Reels are undefined"};
			return this.#Reels;
		}

		// UI SPECIFIC FUNCTIONS

		private _DrawCustomers() {
			for (let i = 0; i < 6; i++)
				$(`#WhoreCustomer${i}`).remove();

			for (let i = 0; i < this.#Customers.length; i++)
				$(this.#Element).append(this._GetCustomerUI(i));
		}

		private _RedrawCustomerUI(index: number) {
			var mood = $('#WhoreMood' + index);
			mood.width(this.#Customers[index].Mood);
			var lust = $('#WhoreLust' + index);
			lust.width(this.#Customers[index].Lust);
			var sat = $('#WhoreSatisfaction' + index);
			sat.width(this.#Customers[index].Satisfaction);
			var root = $('#WhoreCustomer' + index);
			if (this.#SelectedCustomer == index) {
				root.css('border-color', 'lime');
			} else {
				root.css('border-color', 'saddlebrown');
			}
		}

		private _GetCustomerUI(index: number) {
			var ob = this.#Customers[index];

			var root = $('<div>').addClass('WhoreCustomerGUI').attr('id', "WhoreCustomer" + index);
			// Assign select customer callback
			if (ob.Satisfaction < 100) {
				root.on("click", {customer: index}, this._CustomerSelectCB.bind(this));
			} else {
				root.empty();
				root.addClass('WhoreCustomer');
				root.css('opacity', '0.4');
				root.css('cursor', 'not-allowed');
				this._AddXXX(ob, root);
				var completed = $('<div>').addClass('CustomerComplete').text('SATISFIED');
				completed.css('opacity', '0');
				completed.animate({opacity: 1.0}, 2000);
				root.append(completed);
				return root;
			}

			if (this.#SelectedCustomer == index) {
				root.css('border-color', 'lime');
			} else {
				root.css('border-color', 'saddlebrown');
			}

			// Customer Name and Rank
			var name = $('<span>').addClass('WhoreLabel').text(ob.Name);
			root.append(name);

			var rank = $('<span>').addClass('WhoreRank').addClass('Pay' + ob.Payout).text('$');
			root.append(rank);

			// Mood, Lust and Satisfaction labels and bars.
			var mood = $('<span>').addClass('WhoreMood').text('Mood');
			root.append(mood);
			var moodBar = $('<div>').addClass('WhoreMood').attr('id', "WhoreMood" + index).width(ob.Mood);
			root.append(moodBar);

			var lust = $('<span>').addClass('WhoreLust').text('Lust ');
			root.append(lust);
			var lustBar = $('<div>').addClass('WhoreLust').attr('id', "WhoreLust" + index).width(ob.Lust);
			root.append(lustBar);

			var satisfaction = $('<span>').addClass('WhoreSatisfaction').text('Sat.');
			root.append(satisfaction);
			var satisfactionBar = $('<div>').addClass('WhoreSatisfaction').attr('id', "WhoreSatisfaction" + index).width(ob.Satisfaction);

			root.append(satisfactionBar);

			// Wants and bonus
			var wantLabel = $('<span>').addClass('WhoreWant').text('Wants');
			root.append(wantLabel);

			var want: JQuery, want_class: string;
			for (var i = 0; i < ob.Wants.length; i++) {
				want_class = "WhoreWant" + (i + 1) + " WhoreWantColor" + (i + 1);
				want = $('<span>').addClass(want_class).text(ob.Wants[i]);
				root.append(want);
			}

			var bonusLabel = $('<span>').addClass('WhoreBonus').text('Bonus');
			root.append(bonusLabel);

			var bonusSlot_class = "WhoreBonusSlot " + "BonusCat" + ob.BonusCat;
			var bonusSlot = $('<span>').addClass(bonusSlot_class).text(ob.Bonus);
			root.append(bonusSlot);

			return root;
		}

		private _DrawSlots() {

			$('#SlotContainer').remove();
			// Make slot container div.
			var root = $('<div>').attr('id', "SlotContainer");
			$(this.#Element).append(root);

			// Calculate locked slots.
			var lockedSlots = this.#MaxSlots - this.GetSlots().length;
			var before: number, after: number;

			switch (lockedSlots) {
				case 1: before = 0; after = 1; break;
				case 2: before = 1; after = 1; break;
				case 3: before = 1; after = 2; break;
				case 4: before = 2; after = 2; break;
				case 5: before = 3; after = 2; break;
				case 6: before = 3; after = 3; break;
				case 7: before = 3; after = 4; break;
				case 8: before = 4; after = 4; break;
				default: before = 4; after = 5;
			}

			// Draw 'before' locked slots.
			var lockedSlot: JQuery;
			for (let i = 0; i < before; i++) {
				lockedSlot = $('<div class="LockedSlot"></div>');
				root.append(lockedSlot);
			}

			// Get slot reels.
			var slots: Data.ReelAction[][] = [];
			var starting:number[] = [];
			var winning:number[] = [];

			for (const slot of this.GetSlots()) {
				slots.push(slot.Reels);
				starting.push(0);
				winning.push(0);
			}
			if (slots.length >= 3) {
				this.#Reels = new EZSlots(this, "SlotContainer",
					{
						"reelCount": slots.length, "startingSet": starting, "winningSet": winning, time: 2.0,
						"symbols": slots, "height": 90, "width": 60, "callback": this._SlotCB.bind(this)
					});

			}

			// Draw 'after' locked slots.
			for (let i = 0; i < after; i++) {
				lockedSlot = $('<div style="background-image:[img[locked_slot_icon]]" class="LockedSlot"></div>');
				root.append(lockedSlot);
			}
			// Add Spin button, hook up click event.
			var spinButton = $('<button>').addClass("WhoreSpinButton").text("SPIN AHOY!");
			spinButton.on("click", this._SpinEH.bind(this));
			var statusPanel = $('#WhoreStatusPanel');
			statusPanel.append(spinButton);

			// Add cash out button, hook up click event.
			var cashOutButton = $('<button>').addClass("WhoreCashOutButton").text("CASH OUT!");
			cashOutButton.on("click", this._CashOutEH.bind(this));
			statusPanel.append(cashOutButton);
		}

		/**
		 * Draw the slot inventory management screen.
		 */
		private _DrawSlotInventory() {
			// Get the root panel in the screen.
			var root = $(this.#InventoryElement);
			// Find the container div and empty it
			root.empty();

			// Calculate locked slots.
			var lockedSlots = this.#Player.MaxSlots - this.#Player.CurrentSlots;
			var before: number, after: number;
			switch (lockedSlots) {
				case 1: before = 0; after = 9; break;
				case 2: before = 1; after = 9; break;
				case 3: before = 1; after = 8; break;
				case 4: before = 2; after = 8; break;
				case 5: before = 3; after = 8; break;
				case 6: before = 3; after = 7; break;
				case 7: before = 3; after = 6; break;
				case 8: before = 4; after = 6; break;
				default: before = -1; after = 10;
			}

			// Draw active.
			var i = 0, slot: JQuery;
			for (const key of Object.keys(this.#Player.Slots)) {
				i++;
				const reel = this.#Player.Slots[key];
				// Check to see if this is a locked slot.
				if ((this.#Player.Slots[key] == null || typeof this.#Player.Slots[key] === 'undefined') && i <= before) {
					// Empty slot that is not unlocked. Add a place holder.
					slot = $('<div class="LockedSlot2"></div>');
				} else if ((this.#Player.Slots[key] == null || typeof this.#Player.Slots[key] === 'undefined') && i >= after) {
					// Empty slot that is not unlocked. Add a place holder.
					slot = $('<div class="LockedSlot2"></div>');
				} else if ((this.#Player.Slots[key] == null || typeof this.#Player.Slots[key] === 'undefined') && i >= before && i <= after) {
					// Slot is empty AND unlocked.
					slot = $('<div>').attr('id', 'SlotInventory_' + key).addClass('OpenSlot');
					slot.on("click", {slot: key}, this._SelectSlotCB.bind(this));
					if (this.#SelectedSlot == null) this.#SelectedSlot = parseInt(key);
				} else {
					// Slot is not empty and it's unlocked.
					slot = $('<div>').attr('id', 'SlotInventory_' + key).addClass('SlottedSlot');
					slot.addClass(reel.Css);
					slot.on("click", {slot: key}, this._SelectSlotCB.bind(this));
					if (this.#SelectedSlot == null) this.#SelectedSlot = parseInt(key);
				}

				if (this.#SelectedSlot != null && key == this.#SelectedSlot.toString()) slot.css('border', 'solid 1px lime');

				root.append(slot);
			}

			this._DrawSlotInventoryList();
			this._DrawInventoryCurrent();
		}

		private _DrawSlotInventoryList() {
			var root = $(this.#InventoryElement);
			// Draw slot inventory
			var inventory = $('<div>').addClass('SlotInventory');
			root.append(inventory);

			var reels = this.#Player.GetReelsInInventory();

			for (var i = 0; i < reels.length; i++) {
				var item = $('<div>').addClass('SlotInventoryItem');
				var head = $('<span>').addClass('SlotHeader').html(reels[i].Charges() + " x (" + reels[i].Rank + ") " + reels[i].Name);

				switch (reels[i].Rank) {
					case 'COMMON': head.css('color', 'grey'); break;
					case 'UNCOMMON': head.css('color', 'lime'); break;
					case 'RARE': head.css('color', 'cyan'); break;
					case 'LEGENDARY': head.css('color', 'orange'); break;
				}

				item.append(head);
				item.append($('<br>'));
				for (const action of Object.keys(this.#SlotTypesToNames)) {
					var percent = reels[i].CalcPercent(action as Data.ReelAction);
					if (percent <= 0) continue;
					var span = $('<span>').addClass('SlotAttribute').text(this.#SlotTypesToNames[action] + " " + percent + "%");
					item.append(span);
				}

				item.append($('<br>'));
				var button = $('<button>').addClass('EquipSlotButton').text('EQUIP');
				button.on('click', {'id': reels[i].Id}, this._EquipSlotCB.bind(this));

				item.append(button);
				inventory.append(item);
			}
		}

		private _DrawInventoryCurrent() {
			var root = $(this.#InventoryElement);
			// Draw current slot item info.
			var current = $('<div>').addClass('SlotCurrent');
			root.append(current);

			var reel = this.#Player.Slots[this.#SelectedSlot];

			if (!reel) {
				var tempHeader = $('<div>').addClass('SlotCurrentHeader').text("EMPTY SLOT SELECTED");
				current.append(tempHeader);
			} else {
				var header = $('<div>').addClass('SlotCurrentHeader').html("(" + reel.Rank + ") " + reel.Name);

				switch (reel.Rank) {
					case 'COMMON': header.css('color', 'grey'); break;
					case 'UNCOMMON': header.css('color', 'lime'); break;
					case 'RARE': header.css('color', 'cyan'); break;
					case 'LEGENDARY': header.css('color', 'orange'); break;
				}

				current.append(header);
				var buttonDiv = $('<div>').addClass('SlotCurrentButton');
				var button = $('<button>').addClass('RemoveSlotButton').text('REMOVE');
				button.on('click', this._RemoveSelectedSlotCB.bind(this));

				buttonDiv.append(button);
				current.append(buttonDiv);
				current.append($('<hr>').addClass('SlotCurrent'));

				for (const action of Object.keys(this.#SlotTypesToNames)) {
					var percent = reel.CalcPercent(action as Data.ReelAction);
					if (percent <= 0) continue;
					var span = $('<span>').addClass('SlotAttribute').text(this.#SlotTypesToNames[action] + " " + percent + "%");
					current.append(span);
				}

				current.append($('<hr>').addClass('SlotCurrent'));

			}
		}

		private _DrawStatus() {

			$('#WhoreStatusPanel').remove();

			var root = $('<div>').addClass('WhoreStatus').attr('id', "WhoreStatusPanel");
			if (this.#Marquee != null) {
				var marquee = $('<div>').addClass('WhoreMarquee');
				marquee.addClass(this.#Marquee);
				root.append(marquee);
			} else {
				var title = $('<div>').addClass(('WhoreStatusTitle')).text(this.#Title);
				root.append(title);
				var spinTitle = $('<span>').addClass('WhoreSpinsLeftTitle').text('SPINS LEFT');
				root.append(spinTitle);
			}

			var buyButton = $('<button>').addClass("WhoreBuySpinButton").text("BUY 5 SPINS FOR 1 ENERGY");
			buyButton.on("click", this._BuyEnergyCB.bind(this));
			root.append(buyButton);

			var wildcardTitle = $('<span>').addClass('WhoreWildcardTitle').text('WILDCARD POWER');
			root.append(wildcardTitle);

			var wildcardBox = $('<div>').addClass('WhoreWildcardBox');
			var style = $('<span>').addClass('WhoreWildcardStyle').text('Beauty');
			wildcardBox.append(style);
			var styleBar = $('<div>').addClass('WhoreWildcardStyle').width(this.#Player.Beauty());
			wildcardBox.append(styleBar);
			var perversion = $('<span>').addClass('WhoreWildcardPerversion').text('Perversion ');
			wildcardBox.append(perversion);
			var perversionBar = $('<div>').addClass('WhoreWildcardPerversion').width(this.#Player.GetStat("STAT", "Perversion"));
			wildcardBox.append(perversionBar);
			var femininity = $('<span>').addClass('WhoreWildcardFemininity').text('Femininity');
			wildcardBox.append(femininity);
			var femininityBar = $('<div>').addClass('WhoreWildcardFemininity').width(this.#Player.GetStat("STAT", "Femininity"));
			wildcardBox.append(femininityBar);
			root.append(wildcardBox);

			var lootTitle = $('<span>').addClass('WhoreLootTitle').text('CURRENT EARNINGS');
			root.append(lootTitle);

			var lootBox = $('<div>').addClass('WhoreLootBox').attr('id', 'WhoreLootBox');
			for (var i = 0; i < this.#MoneyEarned.length; i++) {
				var wantClass = $('<span>').addClass("WhoreWantColor" + this.#MoneyEarned[i]["want"]).html(this.typeAcronym(this.#MoneyEarned[i]["type"]));
				lootBox.append(wantClass);
			}

			root.append(lootBox);

			// If we reloaded page or came back from another passage, reset the bar to it's last known position.
			var width = Math.max(0, Math.min(116 * this.#Misses, 696));
			var bar = $('#DesperationFiller');
			bar.width(width);

			// Draw Desperation button

			$('#DesperationButton').remove();
			var desperationButton = $('<button>').addClass("DesperationButtonDeactivated").attr('id', 'DesperationButton').text('DESPERATION SPIN');
			desperationButton.on("click", this._DesperationButtonCB.bind(this));
			$('#DesperationContainer').append(desperationButton);

			$(this.#Element).append(root);

			this._RedrawSpins();
		}

		_DrawDesperationBar() {
			if (this.#Misses > 5) return; // Don't keep drawing past the maximum of 5 misses.
			var width = Math.max(0, Math.min(116 * this.#Misses, 696))
			var bar = $('#DesperationFiller');
			if (width == 0) {
				bar.animate({"width": width, "easing": "linear"}, 1000);
			} else {
				bar.animate({"width": width, "easing": "swing"}, 1000);
			}
		}

		private  typeAcronym(type: string) {
			return type === "BJ" ? "Bj" : type[0];
		}

		private _AddMoneyToEarningsBox(want, type) {
			var wantClass = $('<span>').addClass("WhoreWantColor" + want).html(this.typeAcronym(type) + ' ');
			$('#WhoreLootBox').append(wantClass);
		}

		private _RedrawSpins() {
			var i, spinText = "";
			var root = $('#WhoreStatusPanel');

			for (i = 0; i < this.#Spins; i++) spinText += "&#10026; "
			var spinBox = $('<div>').addClass(('WhoreSpinsLeftBox')).attr('id', "WhoreSpinsLeftBox").html(spinText);

			root.append(spinBox);
		}

		/** Make a cool looking dialog. */
		private _Dialog(message: string) {
			App.PR.DialogBox(this.#Element, message, {fgColor: "white"});
		}

		/** Rising message */
		private _RisingDialog(message: string, color: string) {
			App.PR.RisingDialog(this.#Element, message, color);
		}

		/** Draw a big circle with a bang on it.
			 <h1 class="ml8">
			 <span class="letters-container">
			 <span class="letters letters-left">Hi</span>
			 <span class="letters bang">!</span>
			 </span>
			 <span class="circle circle-white"></span>
			 <span class="circle circle-dark"></span>
			 <span class="circle circle-container"><span class="circle circle-dark-dashed"></span></span>
			 </h1>
		 */
		private _WhackPlayer() {
			this.#Player.AdjustStat(CoreStat.Health, -10);
			// Redraw Health Bar
			$('#Health').html(App.PR.pStatMeter(CoreStat.Health, this.#Player, false));
			var root = $(this.#Element);
			$('#WhoreDialogDiv').remove();

			var div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv');
			var header = $('<h1>').addClass('ml8');
			var lContainer = $('<span>').addClass('letters-container');

			header.append(lContainer);

			// Word
			var words = ['POW', 'BANG', 'SLAP', 'WHACK', 'KICK', 'SMACK', 'THUD'];
			var word = App.PR.GetRandomListItem(words);
			lContainer.append($('<span>').addClass('letters letters-left').text(word));
			lContainer.append($('<span>').addClass('letters bang').text('!'));

			// Circle
			header.append($('<span>').addClass('circle circle-white'));
			header.append($('<span>').addClass('circle circle-dark'));

			var cContainer = $('<span>').addClass('circle circle-container');
			cContainer.append($('<span>').addClass('circle circle-dark-dashed'));
			header.append(cContainer);

			// finish up.
			div.append(header);
			root.append(div);

			anime.timeline({loop: false})
				.add({
					targets: '.ml8 .circle-white',
					scale: [0, 3],
					opacity: [1, 0],
					easing: "easeInOutExpo",
					rotateZ: 360,
					duration: 1100
				}).add({
					targets: '.ml8 .circle-container',
					scale: [0, 1],
					duration: 1100,
					easing: "easeInOutExpo",
					offset: '-=1000'
				}).add({
					targets: '.ml8 .circle-dark',
					scale: [0, 1],
					duration: 1100,
					easing: "easeOutExpo",
					offset: '-=600'
				}).add({
					targets: '.ml8 .letters-left',
					scale: [0, 1],
					duration: 1200,
					offset: '-=550'
				}).add({
					targets: '.ml8 .bang',
					scale: [0, 1],
					rotateZ: [45, 15],
					duration: 1200,
					offset: '-=1000'
				}).add({
					targets: '.ml8',
					opacity: 0,
					duration: 1000,
					easing: "easeOutExpo",
					delay: 1400
				});

			anime({
				targets: '.ml8 .circle-dark-dashed',
				rotateZ: 360,
				duration: 8000,
				easing: "linear",
				loop: false
			});

			setTimeout(this._CheckIfDead.bind(this), 3000);
		}

		private _SlotWinDiv(slot, content: string) {
			var child = $("#SlotWindow_" + slot);
			var parent = child.parent();
			var pxy = parent.position();
			var cxy = child.position();
			var startTop = pxy.top - 60;
			var startLeft = cxy.left + 5;
			var endTop = pxy.top - 400;

			var newDiv = $('<div>').addClass("SlotWinner").attr('id', 'SlotWinner_' + slot).html(content);
			newDiv.css('top', startTop);
			newDiv.css('left', startLeft);
			newDiv.animate({opacity: 0.8, top: endTop}, 4000, function () {$(this).remove();});
			$(this.#Element).append(newDiv);
		}

		private _RemoveCustomer(index: number) {
			this.#SelectedCustomer = -1;
			$('#WhoreCustomer' + index).remove();
			$(this.#Element).append(this._GetCustomerUI(index));
		}

		_CheckIfDead() {
			if (this.Player.GetStat('STAT', 'Health') <= 0) Engine.play('DeathEnd');
		}

		//HELPER FUNCTIONS
		GetSlotClass(Type: Data.ReelAction) {
			return this.#SlotTypes[Type];
		}

		private _IsWild(token: Data.ReelAction): boolean;
		private _IsWild(token: number, map: Record<Data.ReelAction | 'WILD', number[]>): boolean
		private _IsWild(token: Data.ReelAction | number, map?: Record<Data.ReelAction | 'WILD', number[]>): boolean {
			if (typeof map !== 'undefined') return map['WILD'].contains(token);
			return this.#WildCards.contains(token);
		}

		private _IsPerv(num: number, map: Record<Data.ReelAction | 'WILD', number[]>) {return map['PERV'].contains(num);};
		private _IsFem(num: number, map: Record<Data.ReelAction | 'WILD', number[]>) {return map['FEM'].contains(num);};
		private _IsBeauty(num: number, map: Record<Data.ReelAction | 'WILD', number[]>) {return map['BEAUTY'].contains(num);};
		private _splitReel(a: number[]) {
			var r: number[][] = [];
			var t: number[] = [];

			for (var i = 0; i < a.length; ++i) {
				if (i == 0) {
					t.push(a[i]);
					continue;
				}

				if (a[i - 1] != (a[i] - 1)) {
					r.push(t);
					t = [];
				}

				t.push(a[i]);
			}
			r.push(t);

			return r;
		}

		// Bonus modifiers
		private _SkillBonus(player: Entity.Player, skill: SkillStr) {return player.StatRoll("SKILL", skill, 100, 1, true);};
		private _BodyBonus(player: Entity.Player, body: BodyStatStr) {return player.StatRoll("BODY", body, 100, 1, true);};
		private _StatBonus(player: Entity.Player, stat: CoreStatStr) {return player.StatRoll("STAT", stat, 100, 1, true);};
		private _FashionBonus(player: Entity.Player, fashion: Data.Clothes.CategoryStr) {return player.GetStyleSpecRating(fashion) / 100;};

		// 0.0 to 1.0 mods.
		private _StyleBonus(player: App.Entity.Player) {return (player.Beauty() / 100);};
		private _FemBonus(player: App.Entity.Player) {return player.GetStat('STAT', 'Femininity') / 100;};
		private _PervBonus(player: App.Entity.Player) {return player.GetStat('STAT', 'Perversion') / 100;};



		/**
		 * Calculate a bonus mod between 0 and 2.0
		 */
		private _CalcBonus(key: BonusSource, player: Entity.Player, arg: any): number {
			var mod = this.#bonusMods.hasOwnProperty(key) == false ? 1.0 : this.#bonusMods[key](player, arg);
			return mod;
		}

		private _CalculateJackpot(slotMap: Record<Data.ReelAction | "WILD", number[]>, key: Data.ReelAction, slots: number[]): Payout[] {
			var c = this.#Customers[this.#SelectedCustomer];
			var mood = Math.floor((c.Mood / 2));
			var lust = Math.floor((c.Lust / 2));

			var basePay = 2 + (c.Payout * 2);

			// See if the customer even WANTS this.
			var i: number;
			var wantMod = 0;
			var wantPos = 0;
			// Sex match
			if (!this._IsWild(key)) {
				for (i = 0; i < c.Wants.length; i++)
					if (c.Wants[i].toUpperCase() == key) {
						wantPos = (i + 1);
						wantMod = (i == 0) ? 1.0 : (i == 1) ? 0.75 : 0.5;
					}
			} else { // This was a wildcard match
				wantMod = this._CalcBonus(key, this.Player, key);
			}

			if (wantMod == 0) return []; // What? We didn't even WANT this. It's not a payout.

			basePay = (basePay * wantMod); // 25, 18.75, 12.5

			// This is the overall modifier for the pay.
			switch (slots.length) {
				case 9: basePay = (basePay * 10); break;
				case 8: basePay = (basePay * 5); break;
				case 7: basePay = (basePay * 4); break;
				case 6: basePay = (basePay * 3); break;
				case 5: basePay = (basePay * 2); break;
				case 4: basePay = (basePay * 1.5); break;
			}

			// Build results for the spin.
			// ========  BASE PAYOUT =========
			// Payout is equal to the Pay * the modifier returned by the skill check for sex acts
			// The difficulty is equal to the c.PayOut attribute of the customer * 20
			// For wildcard matches it's just the default pay.
			// All results for pay are multiplied by the jackpot modifier. Add result as "PAYOUT"
			// ========  BONUS PAYOUT ========
			// Take the 1/2 the payout for that slot and apply the Category as a wildcard mod, add this to "PAYOUT"
			// Check Repeat the process for the bonus mod from the customer, add this to "PAYOUT"
			// ======== SKILL XP GAIN ========
			// You can gain skill in sex and perversion and femininity. The default XP is equal to c.PayOut * 20, modified by the result mod
			// This is checked once for each matching slot (including wild cards, but not beauty). Bonus does not grant XP.
			// ======== RAISING MOOD, LUST and SATISFACTION =========
			// Satisfaction is raised by 20 * skill check mod for every reel in the hit. When it reaches max, the customer is removed (as satisfied).
			// Mood is raised by 5 * skill check mod for every reel hit. There is no maximum.
			// Lust is raised only by Perversion wildcards, it is raised by 10 flat for each one hit.
			// ======== MOOD and LUST PAYOUT ======
			// After positive adjustments the base payout is modified by 0.5 + (50/(c.Mood/2)), for a 0.5 to 1.0 modifier.
			// Bonus pay is then added equal to 1 + (50/(c.Lust/2))
			// ======== MAXIMUM SATISFACTION ======
			// When satisfaction reaches 100 or higher, the customer is removed from the pool of available customers. The
			// game ends (no more spins) when there are no customers left.

			var results: Payout[] = [];

			for (i = 0; i < slots.length; i++) {
				var result = {slot: slots[i], payout: 0, xp: 0, mood: 0, lust: 0, sat: 0, type: key, want: wantPos};
				var checkmod = 1.0;

				// Reset the type if this is a wildcard match
				if (this._IsWild(slots[i], slotMap)) {
					if (this._IsPerv(slots[i], slotMap)) result.type = 'PERV';
					if (this._IsFem(slots[i], slotMap)) result.type = 'FEM';
					if (this._IsBeauty(slots[i], slotMap)) result.type = 'BEAUTY';

					checkmod = this._CalcBonus(result.type, this.Player, null)
				} else {
					checkmod = this._CalcBonus(key, this.Player, (c.Payout * 20));
				}

				//Lets clamp the mod to 0.2 to 1.6
				checkmod = Math.max(0.2, Math.min(checkmod, 1.6));

				result.payout = basePay * checkmod;
				// Add bonus category for style/fem/perv
				result.payout += ((basePay / 3) * this._CalcBonus(c.BonusCat, this.Player, c.BonusCat));
				// Add bonus cat for body parts / styles
				result.payout += ((basePay / 3) * this._CalcBonus(c.Bonus, this.Player, c.Bonus));
				// Add XP bonus for everything but BEAUTY
				result.xp = (this._IsBeauty(slots[i], slotMap) != true) ? Math.round((c.Payout * 7.5) * wantMod) : 0;
				// Add Mood
				result.mood = Math.round(5 * checkmod);
				// Add Lust
				result.lust = (this._IsPerv(slots[i], slotMap)) ? 20 : -5;
				// Add Satisfaction
				result.sat = Math.round(15 * checkmod);
				// Modify pay by mood formula
				result.payout = (result.payout * (0.5 + (mood / 100)));
				// Add a bonus due to high lust.
				result.payout += (basePay * (lust / 100));

				result.payout = Math.round(result.payout);
				this._AddMoneyToCustomer(result.payout);
				results.push(result);
			}

			return results;
		}

		private _AddMoneyToPlayer(money: number, want: Data.ReelAction, type: string) {
			this.#MoneyEarned.push({'money': money, 'want': want, 'type': type});
			this._AddMoneyToEarningsBox(want, type);
		}

		private _AddXPToPlayer(type: Data.ReelAction, xp: number) {
			this.#XPEarned[type] = (this.#XPEarned[type] ?? 0) + xp;
		}

		private _AddMoodToCustomer(mood: number) {
			this.#Customers[this.#SelectedCustomer].Mood = Math.max(0, Math.min((this.#Customers[this.#SelectedCustomer].Mood + mood), 100));
			var div = $('#WhoreMood' + this.#SelectedCustomer);
			div.animate({width: this.#Customers[this.#SelectedCustomer].Mood, easing: 'linear'}, 500);
			//div.width(this._Customers[this._SelectedCustomer].Mood);
		}

		private _AddLustToCustomer(lust: number) {
			this.#Customers[this.#SelectedCustomer].Lust = Math.max(0, Math.min((this.#Customers[this.#SelectedCustomer].Lust + lust), 100));
			var div = $('#WhoreLust' + this.#SelectedCustomer);
			div.animate({width: this.#Customers[this.#SelectedCustomer].Lust, easing: 'linear'}, 500);
			//div.width(this._Customers[this._SelectedCustomer].Lust);
		}

		private _AddMoneyToCustomer(money: number) {
			this.#Customers[this.#SelectedCustomer].Spent += money;
		}

		private _AddSatisfactionToCustomer(satisfaction: number) {
			this.#Customers[this.#SelectedCustomer].Satisfaction = Math.max(0, Math.min((this.#Customers[this.#SelectedCustomer].Satisfaction + satisfaction), 100));
			var div = $('#WhoreSatisfaction' + this.#SelectedCustomer);
			div.animate({width: this.#Customers[this.#SelectedCustomer].Satisfaction, easing: 'linear'}, 500);
		}

		// endregion

		// region CALLBACKS AND EVENT HANDLERS

		/**
		 * Called by the Reel object when the slot animation has finished playing. Passes the results as an array of strings.
		 * @param results the slot wheel results.
		 */
		private _SlotCB(results: number[]) {
			// Convert array positions back into tokens.
			const spinResult = results.map((r, ind) => this.GetSlots()[ind].Symbol(r));
			var i: number;

			var slotMap: Record<Data.ReelAction | "WILD", number[]> = {
				"ASS": [],
				"HAND": [],
				"BJ": [],
				"TITS": [],
				"WILD": [],
				"FEM": [],
				"PERV": [],
				"BEAUTY": []
			};

			// Create mapping.

			for (const key of Object.keys(slotMap)) {
				for (i = 0; i < spinResult.length; i++) {
					if (spinResult[i] == key) slotMap[key].push(i);
					if (this._IsWild(spinResult[i]) && slotMap[key].contains(i) == false && !this._IsWild(key as Data.ReelAction)) slotMap[key].push(i); // track wilds separately
				}
			}

			// Transform map into an array of arrays representing sequences.
			var sequences: Record<string, number[][]> = {};
			for (const key of Object.keys(slotMap)) {
				// We have a minimum of 3 sequential numbers in a map category
				if (slotMap[key].length >= 3) sequences[key] = this._splitReel((slotMap[key]));
			}

			//Iterate through sequence map to calculate payout.
			var payout: Payout[] = [];
			for (const skey of Object.keys(sequences)) {
				const key = skey as Data.ReelAction;
				const seq = sequences[key];

				for (i = 0; i < seq.length; i++) {
					if (seq[i].length >= 3) {
						payout = payout.concat(this._CalculateJackpot(slotMap, key, seq[i]));
					}
				}

			}

			var timeout = 500;
			if (payout.length > 0) {
				// Let's calculate all of the satisfaction, mood and lust at one go.
				this._AddSatisfactionToCustomer(payout.reduce(function (a, o) {return (a + o.sat)}, 0));
				this._AddMoodToCustomer(payout.reduce(function (a, o) {return (a + o.mood)}, 0));
				this._AddLustToCustomer(payout.reduce(function (a, o) {return (a + o.lust)}, 0));

				// sneak peak
				var keys = Object.keys(payout[0]);
				for (i = 0; i < keys.length; i++) {
					if (keys[i] == 'slot' || keys[i] == 'type' || keys[i] == 'want') continue;
					timeout = 300 * i;
					setTimeout(this._DoSlotResults.bind(this, payout, keys[i]), timeout);
				}

				timeout += 500; // For finishing up spin.
				this.#Misses = 0;

				var jackpot = ['JACKPOT', 'JACKPOT', 'JACKPOT', 'DOUBLE JACKPOT', 'TRIPLE JACKPOT', 'QUADRUPLE JACKPOT', 'SUPER JACKPOT', 'MEGA JACKPOT', 'ULTRA JACKPOT'];
				var colors = ['lime', 'lime', 'lime', 'gold', 'gold', 'orange', 'orange', 'cyan', 'purple'];
				this._RisingDialog(jackpot[payout.length - 1], colors[payout.length - 1]);
				$('#DesperationButton').removeClass("DesperationButtonActivated").addClass("DesperationButtonDeactivated");
			} else {
				// We missed.
				this.#Misses++;
				if (this.#Misses == 5) {
					this._RisingDialog('DESPERATION MODE UNLOCKED', 'deepPink');
					$('#DesperationButton').addClass("DesperationButtonActivated").removeClass("DesperationButtonDeactivated");
				} else
					if (this.#Misses < 5) { // No desperation mode yet.
						this._RisingDialog('MISS', 'red');
					} else {
						this._WhackPlayer();
					}
			}

			setTimeout(this._DrawDesperationBar.bind(this), 100);

			setTimeout(this._UnlockSpinnerCB.bind(this), timeout);

		}

		/**
		 * Called by timeout after a spin has resolved. All animation events should be in process before this
		 * fires off. Check to see if customer is satisfied and remove him from available selection.
		 */
		private _UnlockSpinnerCB() {
			if (this.#SelectedCustomer != -1 && typeof this.#SelectedCustomer !== 'undefined' && this.#Customers.length > this.#SelectedCustomer) {
				var oCustomer = this.#Customers[this.#SelectedCustomer];
				if (typeof oCustomer !== 'undefined' && oCustomer.Satisfaction >= 100) this._RemoveCustomer(this.#SelectedCustomer);
			}
			this.#Spinning = false;
		}

		/**
		 * Print and animate the results of the slot spin. Modify the earnings and customer objects.
		 * @param {Array.<*>} results Array of payout objects
		 * @param {string} key The key we are checking on the payout object.
		 */
		private _DoSlotResults(results, key: string) {
			for (var i = 0; i < results.length; i++) {
				var html = "";
				switch (key) {
					case 'payout':
						html = '<span class="SlotPayout">$' + results[i][key] + '</span>';
						this._AddMoneyToPlayer(results[i][key], results[i].want, results[i].type);
						break;
					case 'xp':
						html = '<span class="SlotXp">' + results[i][key] + ' XP</span>';
						this._AddXPToPlayer(results[i].type, results[i][key]);
						break;
					case 'mood':
						html = '<span class="SlotMood">' + results[i][key] + ' MOOD</span>';
						break;
					case 'lust':
						html = '<span class="SlotLust">' + results[i][key] + ' LUST</span>';
						break;
					case 'sat':
						var wantType = results[i].type;
						var ob = this.#Customers[this.#SelectedCustomer];
						if (ob.SatisfiedWants.hasOwnProperty(wantType)) ob.SatisfiedWants[wantType] += 1;
						break;
				}

				if (html == "") continue;
				this._SlotWinDiv(results[i].slot, html);
			}
		}

		/**
		 * Called when you click the 'spin' button.
		 */
		private _SpinEH() {
			if (this.#Spins <= 0) return this._Dialog("NO SPINS LEFT");
			if (this.#Spinning == true) return; // No effect if spin in process. Maybe make it play a sound later?
			if (this.GetSlots().length < 3) return this._Dialog("EQUIP 3 SLOTS");
			if (this.#SelectedCustomer == -1) return this._Dialog("SELECT CUSTOMER");
			this.#Spinning = true;
			this.#Spins--;
			this._RedrawSpins();
			this.Reels.spin();
		};

		private _CashOutEH() {
			if (this.#Spinning == true) return;
			SugarCube.Engine.play("WhoreEnd");
		}

		/**
		 * Attached to the Customer's UI at creation.
		 * @param e event object
		 */
		private _CustomerSelectCB(e: {data: {customer: number}}) {

			if (this.#Spinning == true) return; // Can't switch customers in the middle of a spin.
			var oldCustomer = $('#WhoreCustomer' + this.#SelectedCustomer).css('border-color', 'saddlebrown');
			var newCustomer = $('#WhoreCustomer' + e.data.customer).css('border-color', 'lime');

			this.#SelectedCustomer = e.data.customer;
		}

		/**
		 * Attached to the 'Buy more spins' button.
		 */
		private _BuyEnergyCB() {
			if (this.GetSlots().length < 3) return this._Dialog("EQUIP 3 SLOTS");

			if (this.#Spins >= 20 || this.Player.GetStat("STAT", "Energy") < 1) return;
			this.#Spins += 5;
			this.Player.AdjustStat(CoreStat.Energy, -1);

			// Redraw Energy bar
			$('#Energy').html(App.PR.pStatMeter(CoreStat.Energy, this.Player, false));
			this._RedrawSpins();
		}

		private _DesperationButtonCB() {
			if (this.#Misses < 5 || this.#Spinning) return;
			this.#DesperationSpins++;
			this.#Spinning = true;
			this.Reels.win(this.#Customers[this.#SelectedCustomer].Wants[0]);
			$('#DesperationButton').removeClass("DesperationButtonActivated").addClass("DesperationButtonDeactivated");
		}

		private _DrawUICB() {
			this.#Spinning = false;
			this._DrawCustomers();
			this._DrawStatus();
			this._DrawSlots();

			if (this.GetSlots().length < 3) {
				this._Dialog("EQUIP 3 SLOTS");
			} else {
				if (this.#SelectedCustomer == -1) this._Dialog("SELECT CUSTOMER");
			}
		}

		private _DrawSlotInventoryCB() {
			this._DrawSlotInventory();
		};

		private _SelectSlotCB(e: {data: {slot: number}}) {
			this.#SelectedSlot = e.data.slot;
			this._DrawSlotInventory();
		};

		private _EquipSlotCB(e) {
			this.#Player.EquipReel(e.data.id, this.#SelectedSlot);
			this._DrawSlotInventory();
		};

		private _RemoveSelectedSlotCB() {
			this.#Player.RemoveReel(this.#SelectedSlot);
			this._DrawSlotInventory();
		};

		// endregion
	}

	/**
	* This is the customer object for the game. It just holds data.
	* @param {string} dataKey
	*/
	class Customer {
		/** Name of the customer. */
		Name = "Pirate Pete";
		/** Current Lust. */
		Lust = 1;
		/** Original Lust. */
		oLust = 1;
		/** Current Mood. */
		Mood = 1;
		/** Original Mood. */
		oMood = 1;
		/** Current Satisfaction. */
		Satisfaction = 1;
		/** Payout on scale of 1 to 5 */
		Payout = 5;
		/** Bonus Reward Category */
		Bonus: Data.Whoring.BonusSource = "Pirate Slut";
		/** Wildcard category on bonus */
		BonusCat: Data.Whoring.BonusCategory = "Perversion";
		Wants: Data.Whoring.SexActStr[] = [];
		/** How much we've paid out. */
		Spent = 0;
		/** NPC tag. */
		Tag = "";
		SatisfiedWants: Partial<Record<Data.ReelAction, number>> = {};

		constructor(dataKey: string) {
			if (typeof App.Data.Whoring === 'undefined' || App.Data.Whoring.hasOwnProperty(dataKey) == false) {
				alert("Bad call to new Customer() : dataKey '" + dataKey + "' is undefined or missing.");
				return;
			}

			var dict = App.Data.Whoring[dataKey];

			// Set Name.
			this.Name = App.PR.GetRandomListItem(dict.NAMES);
			if (typeof dict.TITLE !== 'undefined' && dict.TITLE != null) {this.Name = dict.TITLE + " " + this.Name;}

			// Set the Payout
			this.Payout = (dict['MIN_PAY'] >= dict['MAX_PAY']) ? dict['MIN_PAY'] : dict['MIN_PAY'] + Math.round(Math.random() * (dict['MAX_PAY'] - dict['MIN_PAY']));

			// Get the NPC from the Player.
			this.Tag = dict.NPC_TAG;
			var npc = App.Entity.Player.instance.GetNPC(this.Tag);

			if (typeof npc !== 'undefined') {
				// Adjust the mood and lust of the customer by the mood of the stored associated NPC object.
				this.oMood = Math.floor((npc.Mood / 2)) + Math.floor(Math.random() * (npc.Mood / 2));
				this.Mood = this.oMood;
				this.oLust = Math.floor((npc.Lust / 2)) + Math.floor(Math.random() * (npc.Lust / 2));
				this.Lust = this.oLust;
			}

			// Set the Wants array of the character.
			var count = 0;
			while (this.Wants.length < 3) {
				++count;
				const tmp = App.PR.GetRandomListItem(dict["WANTS"]);
				if (!this.Wants.contains(tmp)) {
					this.SatisfiedWants[tmp] = 0;
					this.Wants.push(tmp);
				}
				if (count >= 30) break;
			}

			// Set the bonus and bonus category.
			this.Bonus = App.PR.GetRandomListItem(dict.BONUS);
			this.BonusCat = App.PR.GetRandomListItem(dict.BONUS_CATS);
		}

		MostSatisfied(): {want: Data.ReelAction, val: number} {
			var sortable: {want: Data.ReelAction, val: number}[] = [];
			for (var want in this.SatisfiedWants) {
				if (!this.SatisfiedWants.hasOwnProperty(want)) continue;
				sortable.push({"want": want as Data.ReelAction, val: this.SatisfiedWants[want]});
			}

			sortable.sort(function (a, b) {
				return b.val - a.val;
			});

			return sortable[0];
		}
	}

	type EZSlotsResultCallback = (res: number[]) => void;
	interface EZSlotsOptions {
		reelCount?: number;
		symbols: Data.ReelAction[] | Data.ReelAction[][]
		startingSet?: number[];
		winningSet: number[];
		width?: number;
		height?: number;
		time?: number;
		callback?: EZSlotsResultCallback;
	}
	class EZSlots {
		private reelCount: number;
		private symbols: Data.ReelAction[] | Data.ReelAction[][];
		private sameSymbolsEachSlot = true;
		private startingSet: number[] | undefined;
		private winningSet: number[];
		private width: number;
		private height: number;
		/** time in millis for a spin to take */
		private time: number;
		private howManySymbolsToAppend: number;
		/** location for selected symbol... needs to be a few smaller than howManySymbolsToAppend */
		private endingLocation = 7;
		/** jquery object reference to main wrapper */
		private jqo: JQuery<HTMLElement>;
		/** callback function to be called once slots animation is finished */
		private callback: EZSlotsResultCallback | undefined;

		/** jquery object reference to strips sliding up and down */
		private jqoSliders: JQuery<HTMLElement>[] = [];

		#engine: SlotEngine;
		/**
		 * Gratuitously stolen from Kirk Israel and modified for this game.
		 */
		constructor(engine: SlotEngine, id: string, options: EZSlotsOptions) {
			this.#engine = engine;
			//set some variables from the options, or with defaults.
			this.reelCount = options.reelCount ?? 3; //how many reels, assume 3
			this.symbols = options.symbols;
			this.sameSymbolsEachSlot = true;
			this.startingSet = options.startingSet;
			this.winningSet = options.winningSet;
			this.width = options.width ?? 100;
			this.height = options.height ?? 100;
			this.time = options.time ? (options.time * 1000) : 6500; //
			//this.howManySymbolsToAppend = Math.round(this.time/325); //how many symbols each spin adds
			this.howManySymbolsToAppend = 30;
			this.endingLocation = 7; //
			this.jqo = $("#" + id); //
			this.callback = options.callback; //

			this.init();
		}

		spin() {
			return this.spinAll();
		}

		win(symbol: Data.ReelAction) {
			return this.spinWin(symbol);
		}
		//to initialize we construct the correct number of slot windows
		//and then populate each strip once
		private init() {
			this.jqo.addClass("ezslots"); // to get the css goodness
			// figure out if we are using the same of symbols for each window - assume if the first
			// entry of the symbols is not a string we have an array of arrays
			if (typeof this.symbols[0] != 'string') {
				this.sameSymbolsEachSlot = false;
			}
			//make each slot window
			for (var i = 0; i < this.reelCount; i++) {
				var jqoSlider = $('<div class="slider"></div>');
				var jqoWindow = $('<div class="window" id="SlotWindow_' + i + '"></div>');
				this.scaleJqo(jqoWindow).append(jqoSlider); //make window right size and put slider in it
				this.jqo.append(jqoWindow); //add window to main div
				this.jqoSliders.push(jqoSlider); //keep reference to jqo of slider
				this.addSymbolsToStrip(jqoSlider, i, false, true); //and add the initial set
			}
		}
		// convenience function since we need to apply width and height to multiple parts
		private scaleJqo(jqo: JQuery<HTMLElement>) {
			jqo.css("height", this.height + "px").css("width", this.width + "px");
			return jqo;
		}

		// add the various symbols - but take care to possibly add the "winner" as the symbol chosen
		private addSymbolsToStrip(jqoSlider :JQuery<HTMLElement>, whichReel: number, shouldWin: boolean, isInitialCall: boolean = false) {
			var symbolsToUse = this.sameSymbolsEachSlot ? this.symbols as Data.ReelAction[] : this.symbols[whichReel] as Data.ReelAction[];
			var chosen = shouldWin ? this.winningSet[whichReel] : Math.floor(Math.random() * symbolsToUse.length);
			for (var i = 0; i < this.howManySymbolsToAppend; i++) {
				var ctr = (i == this.endingLocation) ? chosen : Math.floor(Math.random() * symbolsToUse.length);
				if (i == 0 && isInitialCall && this.startingSet) {
					ctr = this.startingSet[whichReel];
				}
				// we nest "content" inside of "symbol" so we can do vertical and horizontal centering more easily
				// var jqoContent = $("<div class='content'>"+symbolsToUse[ctr]+"</div>");
				var contentDiv = "<div class='content " + this.#engine.GetSlotClass(symbolsToUse[ctr]) + "'></div>";
				var jqoContent = $(contentDiv);
				this.scaleJqo(jqoContent);
				var jqoSymbol = $("<div class='symbol'></div>");
				this.scaleJqo(jqoSymbol);
				jqoSymbol.append(jqoContent);
				jqoSlider.append(jqoSymbol);
			}
			return chosen;
		}

		//to spin, we add symbols to a strip, and then bounce it down
		private spinOne(jqoSlider :JQuery<HTMLElement>, whichReel: number, shouldWin: boolean) {
			var heightBefore = parseInt(jqoSlider.css("height"), 10);
			var chosen = this.addSymbolsToStrip(jqoSlider, whichReel, shouldWin);
			var marginTop = -(heightBefore + ((this.endingLocation) * this.height));
			if (SugarCube.settings.fastAnimations == true) {
				jqoSlider.css('margin-top', marginTop + "px");
			} else {
				jqoSlider.stop(true, true).animate(
					{"margin-top": marginTop + "px"},
					{'duration': this.time + Math.round(Math.random() * 1000), 'easing': "easeOutElastic"});
			}
			return chosen;
		}

		private spinAll(shouldWin: boolean = false) {
			var results: number[] = [];
			for (var i = 0; i < this.reelCount; i++) {
				results.push(this.spinOne(this.jqoSliders[i], i, shouldWin));
			}

			if (this.callback) {
				if (SugarCube.settings.fastAnimations == true) {
					this.callback(results);
				} else {
					setTimeout(this.callback.bind(this, results), this.time);
				}
			}

			return results;
		}

		/**
		 * Hack by me to force a specific symbol.
		 */
		private spinWin(symbol: Data.ReelAction) {
			var winningSet: number[] = [];
			for (const arr of this.symbols) { // the array of options on the reel.
				if (arr.indexOf(symbol) != -1) {
					winningSet.push(arr.indexOf(symbol));
				} else if (arr.indexOf('PERV') != -1) {
					winningSet.push(arr.indexOf('PERV'));
				} else if (arr.indexOf('FEM') != -1) {
					winningSet.push(arr.indexOf('FEM'));
				} else if (arr.indexOf('BEAUTY') != -1) {
					winningSet.push(arr.indexOf('BEAUTY'));
				} else {
					winningSet.push(0);
				}
			}

			this.winningSet = winningSet;
			this.spinAll(true);
		}
	}
}
