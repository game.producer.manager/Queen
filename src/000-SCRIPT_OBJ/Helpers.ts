namespace App.PR {
	export function PrintMemory(
		clothesElement: HTMLElement | string = '#clothes-table',
		foodElement: HTMLElement | string = '#food-table',
		drugsElement: HTMLElement | string = '#drugs-table',
	) {
		var clothes = GetMemory('CLOTHES', setup.player.History.CLOTHING_EFFECTS_KNOWN);

		var clothesTable = new Tabulator(clothesElement, {
			height: "311px",
			data: clothes,
			columns: [
				{title: "Slot", field: "Slot", width: 80},
				{title: "Category", field: "Category", width: 100},
				{
					title: "Rank", field: "Rank", width: 80, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
				//force html tags to render
				{
					title: "Name", field: "Name", width: 200, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
				//force html tags to render
				{
					title: "Effects", field: "Effects", width: 400, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
			],
			initialSort: [
				{column: "Name", dir: "desc"},
			],
		});

		var food = GetMemory('FOOD', setup.player.History.ITEMS);

		var foodTable = new Tabulator(foodElement, {
			height: "311px",
			layout: "fitColumns",
			data: food,
			columns: [
				//force html tags to render
				{
					title: "Name", field: "Name", minWidth: 200, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
				//force html tags to render
				{
					title: "Effects", field: "Effects", minWidth: 600, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
			],
			initialSort: [
				{column: "Name", dir: "desc"},
			],
		});

		var drugs = GetMemory('DRUGS', setup.player.History.ITEMS);

		var drugsTable = new Tabulator(drugsElement, {
			height: "311px",
			layout: "fitColumns",
			data: drugs,
			columns: [
				//force html tags to render
				{
					title: "Name", field: "Name", minWidth: 200, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
				//force html tags to render
				{
					title: "Effects", field: "Effects", minWidth: 600, formatter: function (cell, _formatterParams) {
						return cell.getValue();
					}
				},
			],
			initialSort: [
				{column: "Name", dir: "desc"},
			],
		});
	}

	interface MemoryItem {
		Name: string;
		Category: string;
		Rank: string;
		Slot: string;
		Effects: string;
	}
    /**
     * Helper function for journal memory.
     * Reduces the src dictionary down to valid entries in the player Dictionary
     * eg (CLOTHES, player.History.CLOTHING_EFFECTS_KNOWN)
     */
	function GetMemory(Category, Dict): MemoryItem[] {

		const Src = App.Items.TryGetItemsDictionary(Category);

		if (Src == null) return []; // empty or couldn't find

		//Reduce dictionary down to items we known about.
		const allowed = Object.keys(Dict);
		var temp = Object.keys(Src)
			.filter(key => allowed.includes(key))
			.map(f => {
				let o = App.Items.Factory(Category, f);
				return {
					Name: o.Description,
					Category: typeof o.Category !== 'undefined' ? o.Category.join(", ") : Category,
					Rank: typeof o.Rank !== 'undefined' ? o.Rank : '',
					Slot: typeof o.Slot !== 'undefined' ? o.Slot : '',
					Effects: o.PrintEffectsOnly(setup.player)
				};
			});

		return temp;
	}

    /**
     * Get a list of all effects, but print ????? for ones unlearned.
     */
	export function GetAllEffects(Type: App.Data.ShopItemTypesStr, Tag: string): string {
		let o = App.Items.Factory(Type, Tag);
		if (o instanceof Items.Reel) {
			return "";
		}
		const effects = o.GetKnowledge();
		const uses = (Type == 'CLOTHES') ? setup.player.GetHistory("CLOTHING_EFFECTS_KNOWN", Tag) :
			setup.player.GetHistory('ITEMS', Tag);

		let output:string[] = [];

		for (var i = 0; i < effects.length; i++) {
			if (i < uses) {
				output.push(pEffectMeter(effects[i], o));
			} else {
				output.push("?????");
			}
		}

		return output.join(" ");

	}

	var numericalMeters = false;

	export function setNumericalMeters(v: boolean): void {
		numericalMeters = v;
	}

	/**
     * Shortcut
     * @see App.unitSystem.lengthString
	 */
	export function lengthString(x: number, compact = false): string {
		return App.unitSystem.lengthString(x, compact);
	}

	/**
     * Shortcut
     * @see App.unitSystem.lengthValue
	 */
	export function lengthValue(x: number): number {
		return App.unitSystem.lengthValue(x);
	}

    /**
     * Returns config object which describe naming of the given type
     * @param type Config type ("BODY")
     */
	function GetNamingConfig(type: string) {
		if (type == "BODY") return App.Data.Naming.BodyConfig;
	}

    /**
     * Fetch index names required to find leveld value for a leveling property
     * @param Stat Stat type (SKILL, BODY, STAT)
     * @param Property Property name ("Bust", "Lips", etc.)
     * @param Aspect "NOUN", "ADJECTIVE"
     */
	function GetNamingIndices<T extends keyof StatTypeStrMap>(stat: T, property: StatTypeStrMap[T], aspect: string): string[] {
		const st = GetNamingConfig(stat);
		const asp = (st as any)?.[property]?.[aspect];
		if (asp === undefined) return [];
		return asp.INDEX ?? [property];
	}

    /**
     * @summary Fetch a leveled value from the given rating for given values.
     *
     * This function descends into the passed ratings object using supplied index
     * value array. The rating object is indexed as using the first element of the
     * index array. The result of this operation is passed to this function again with
     * sliced index array. This continues until index array is exhausted or indexing
     * returns a simple string or array of strings (a random element of which is returned then).
     *
     * @param Ratings object with ratings or the final string
     * @param Value array of rating index values or a single value
     */
	function GetMultiIndexLevelingProperty(Ratings: object | string, Value: number | number[]): string {
		if (typeof (Ratings) == "string") return Ratings; // value does not change

		var v;
		var nextValues: number[] = [];
		if (Array.isArray(Value)) {
			v = Value[0];
			if (Value.length > 1) nextValues = Value.slice(1);
		} else {
			v = Value;
		}
		var lastSmallerRating;
		for (var prop in Ratings) {
			if (!Ratings.hasOwnProperty(prop)) continue;
			if (parseInt(prop) > v) break;
			lastSmallerRating = prop;
		}
		if (lastSmallerRating == undefined) return "Untyped rating: " + Value;
		var res = Ratings[lastSmallerRating];
		if (nextValues.length === 0) {
			if (Array.isArray(res)) {
				return res.random(); // SugarCube adds Array.random()
			} else {
				return res;
			}
		}
		return GetMultiIndexLevelingProperty(res, nextValues);
	}

    /**
     * Fetch a rating for a statistic/value
     */
	export function GetRating(Type: string, Value: number, Colorize: boolean = false): string {
		var Ratings = App.Data.Ratings[Type];

		var lastSmallerRating;
		for (var prop in Ratings) {
			if (!Ratings.hasOwnProperty(prop)) continue;
			// Needed since many ratings start at 0.
			if (lastSmallerRating == undefined) lastSmallerRating = prop;
			if (parseInt(prop) > Value) break;
			lastSmallerRating = prop;
		}

		if (lastSmallerRating == undefined) return "Untyped rating: " + Type + "," + Value;
		if (Colorize == true) {
			return ColorizeString(Value, Ratings[lastSmallerRating]);
		} else {
			return Ratings[lastSmallerRating];
		}
	}

	/**
     * Helper function. Checks relevant statistic config and returns the leveling record if one exists.
     */
	function GetLevelingRecord(Type: StatType, Stat: string, Value: number): any {
		var Ratings = GetStatConfig(Type)[Stat].LEVELING;
		var lastSmallerProp;
		for (var prop in Ratings) {
			if (!Ratings.hasOwnProperty(prop)) continue;
			if (parseInt(prop) > Value) break;
			lastSmallerProp = prop;
		}
		if (lastSmallerProp !== undefined) return Ratings[lastSmallerProp];
		return undefined;
	}

    /**
     * Helper function. Checks relevant statistic config and returns a colorized Property value for use if one exists.
     */
	function GetLevelingProperty(Type: StatType, Stat: string, Property: string, Value: number, Colorize: boolean = false): string {
		var propValue = GetLevelingRecord(Type, Stat, Value);
		if (propValue == undefined) return "";

		const str = propValue[Property];
		if (str === "" || !Colorize) return str;

		return "<span style='color:" + App.UI.colorScale(propValue.COLOR, 16) + "'>" + str + "</span>";
	}

    /**
     * Helper function. Checks relevant statistic config and returns a NOUN (colorized) for use if one exists.
     */
	export function GetNoun<T extends keyof StatTypeMap>(Type: T, Stat: StatTypeMap[T], Value: number | number[],
		Colorize?: boolean): string;
	export function GetNoun<T extends keyof StatTypeStrMap>(Type: T, Stat: StatTypeStrMap[T], Value: number | number[],
		Colorize?: boolean): string;
	export function GetNoun(Type: StatType, Stat: string, Value: number | number[], Colorize = false): string {
		var nCfg = GetNamingConfig(Type);
		if (nCfg == undefined || !nCfg.hasOwnProperty(Stat)) return "NO_NOUN_FOR_" + Type + ":" + Stat;
		const nounRec = nCfg[Stat].NOUN;
		var str = nounRec.LEVELING === undefined ? nounRec : GetMultiIndexLevelingProperty(nounRec.LEVELING, Value);

		if (Colorize) {
			// use colour from the first index for now
			// TODO blend colors from all indices
			var color = GetLevelingRecord(Type, Stat, Array.isArray(Value) ? Value[0] : Value).COLOR;
			return "<span style='color:" + App.UI.colorScale(color, 16) + "'>" + str + "</span>";
		}
		return str;
	}

	/**
	 * Returns array of applicable adjective values for a leveling noun
	 */
	function GetNoneAdjectives<T extends App.StatTypeStr>(Type: T, Stat: App.StatTypeStrMap[T], player:Entity.Player, Colorize = false) {
		var tCfg = GetNamingConfig(Type);
		if (tCfg === undefined) {return []};
		var sCfg = tCfg[Stat as string];
		if (sCfg == undefined) return [];
		var adjCfg = sCfg["ADJECTIVE"];
		if (adjCfg == undefined) return [];

		var adjectiveRatings = adjCfg.RATING || [Type + '/' + Stat];
		var adjectiveIndices = adjCfg.INDEX || adjectiveRatings;
		var adjectiveApplicableLevels = adjCfg.APPLICABLE_LEVEL || adjectiveRatings.map(() => {return {"MIN": 0, "MAX": 100};});

		const statVal = player.GetStatPercent(Type, Stat);
		var adjectives:string[] = [];
		for (var i = 0; i < adjectiveRatings.length; ++i) {
			var s = adjectiveIndices[i].split('/');
			if (statVal >= adjectiveApplicableLevels[i].MIN && statVal <= adjectiveApplicableLevels[i].MAX) {
				var r = adjectiveRatings[i].split('/');
				adjectives.push(GetAdjective(r[0], r[1], statVal, Colorize));
			}
		}

		return adjectives;
	}

	/**
	 * Returns leveling noun, optionally with applicable adjectives prepended
	 * @param Type
	 * @param Stat
	 * @param player
	 * @param Adjectives Whether to prepend adjectives
	 * @param Colorize colorize output
	 */
	function GetplayerNoun<T extends keyof StatTypeMap>(Type: T, Stat: StatTypeMap[T], player: Entity.Player, Adjectives?: boolean, Colorize?: boolean): string;
	function GetplayerNoun<T extends keyof StatTypeStrMap>(Type: T, Stat: StatTypeStrMap[T], player: Entity.Player, Adjectives?:boolean, Colorize?: boolean): string;
	function GetplayerNoun<T extends keyof StatTypeMap>(Type: T, Stat: StatTypeMap[T], player: Entity.Player, Adjectives = false, Colorize = false): string {
		var indexNames = GetNamingIndices(Type, Stat, "NOUN");
		if (indexNames == undefined) return "NO_NOUN_FOR_" + Type + ":" + Stat;
		var indices = indexNames.map(s => s.includes('/') ? s.split('/') : [Type, s]);
		var indexValues = indices.map(x => player.GetStat(x[0] as any, x[1]));
		var str = GetNoun(Type, Stat, indexValues, Colorize);

		if (Adjectives == true) {
			str = GetNoneAdjectives(Type, Stat, player, Colorize).join(' ') + ' ' + str;
		}
		return str;
	}

    /**
     * Helper function. Get total amount of XP points needed to raise from a to b
     */
	function GetTotalXPPoints<T extends keyof App.StatTypeMap>(Type: T, Stat: App.StatTypeMap[T], ValueA: number, ValueB: number): number {
		var statCfg = GetStatConfig(Type)[Stat];
		var levelingCost = statCfg.LEVELING_COST ?? statCfg.LEVELING;

		if (levelingCost instanceof Function) return levelingCost(ValueB) - levelingCost(ValueA);
		var lastLeveledTo = ValueA;
		var res = 0;
		var prop = "";
		for (prop in levelingCost) {
			if (!levelingCost.hasOwnProperty(prop)) continue;
			const propV = parseInt(prop);
			if (propV <= ValueA) continue;
			if (propV > ValueB) break;

			res += (propV - lastLeveledTo) * levelingCost[prop].COST;
			lastLeveledTo = parseInt(prop);
		}
		res += (ValueB - lastLeveledTo) * levelingCost[prop].COST;
		return res;
	}

    /**
     * Helper function. Checks relevant statistic config and returns an ADJECTIVE (colorized) for use if one exists.
     */
	function GetAdjective<T extends keyof StatTypeMap>(Type: T, Stat: StatTypeMap[T], Value: number, Colorize?: boolean): string;
	function GetAdjective<T extends keyof StatTypeStrMap>(Type: T, Stat: StatTypeStrMap[T], Value: number, Colorize?: boolean): string;
	function GetAdjective<T extends keyof StatTypeMap>(Type: T, Stat: StatTypeMap[T], Value: number, Colorize: boolean = false): string {
		return GetLevelingProperty(Type, Stat as string, "ADJECTIVE", Value, Colorize);
	}

	function TokenizeRating<T extends keyof StatTypeMap>(player: Entity.Player, Type: T, Stat: StatTypeMap[T], str: string): string
	function TokenizeRating<T extends keyof StatTypeStrMap>(player: Entity.Player, Type: T, Stat: StatTypeStrMap[T], str: string): string
	function TokenizeRating<T extends keyof StatTypeMap>(player: Entity.Player, Type: T, Stat: StatTypeMap[T], str: string): string {
		function nounReplacer(_match: string, delim: string) {
			return GetplayerNoun(Type, Stat, player, false, true) + delim;
		}
		function adjReplace(_match: string, delim: string) {
			return GetAdjective(Type, Stat, player.GetStat(Type, Stat), true) + delim;
		}
		str = str.replace(/PLAYER_NAME/g, player.SlaveName);
		if (Type == App.StatType.BODY) {
			str = str.replace(/LENGTH_C/g, lengthString(StatToCM(player, Stat as App.BodyStatStr), true).toString());
			str = str.replace(/LENGTH/g, lengthString(StatToCM(player, Stat as App.BodyStatStr), false).toString());
		}
		str = str.replace(/NOUN([^A-Za-z_|$])/g, nounReplacer);
		str = str.replace(/ADJECTIVE([^A-Za-z_|$])/g, adjReplace);
		return TokenizeString(player, undefined, str);
	}

    /**
     * Helper function for getting stat configurations.
     */
	export function GetStatConfig<T extends keyof Data.StatConfigMap>(Type: T): Record<StatTypeMap[T], Data.StatConfigMap[T]>;
	export function GetStatConfig<T extends keyof Data.StatConfigStrMap>(Type: T): Record<StatTypeStrMap[T], Data.StatConfigStrMap[T]>;
	export function GetStatConfig(Type: StatType) {
		switch (Type) {
			case StatType.STAT: return Data.Lists.StatConfig;
			case StatType.SKILL: return Data.Lists.SkillConfig;
			case StatType.BODY: return Data.Lists.BodyConfig;
		}
	}

    /**
     * Colorizes and returns a string primitive
     */
	export function ColorizeString(value: number, str: string, maxValue?: number): string {
		return `<span style='color:${UI.colorScale(value, maxValue)}'>${str}</span>`;
	}

    /**
     * Used to colorize a string with colors corresponding to the meter scheme.
     * @param n the value to rate the color on.
     * @param s the string to colorize
     * @param h HTML safe?
     */
	function ColorizeMeter(n: number, s: string): string {
		return `<span style='color:${UI.meterColor(n, 100)}'>${s}</span>`;
	}

	export function debugColorScale(): string {
		var str = "";
		for (var i = 0; i < 16; i++)
			str += `<span style='color:${UI.colorScale(i, 16)}'>Index ${i} = ${UI.colorScale(i, 16)}</span><br/>`;
		return str;
	}

    /**
     * Prints out a 10 star meter surrounded with brackets.
     * @param Score - Current stat/score
     * @param MaxScore - Maximum stat/score
     * @param InvertMeter - reverse direction of stars relative to score so that high scores are less stars.
     * @param HtmlSafe
     */
	function pMeter(Score: number, MaxScore: number, InvertMeter: boolean = false) {
		const clampedScore = Math.clamp(Score, 0, MaxScore);
		var units = (MaxScore / 10);
		var Stars = Math.floor((clampedScore / units));
		var sMeter = "";
		var nMeter = InvertMeter ? (100 - (10 * Stars)) : (10 * Stars);
		var i = 0;

		for (i = 0; i < Stars; i++)
			sMeter += "&#9733;";
		sMeter = ColorizeMeter(nMeter, sMeter);

		if ((10 - Stars) != 0) {
			sMeter += "<span style='color:grey;'>";
			for (i = 0; i < (10 - Stars); i++)
				sMeter += "&#9733;";
			sMeter += "</span>";
		}

		if (numericalMeters) {
			return "[" + sMeter + "] " + Score;
		} else {
			return "[" + sMeter + "]";
		}
	}

    /**
     * Simple calculator turns CM into Inches.
     */
	function CMtoINCH(n: number): number {
		return Math.round(n * 0.393700);
	}

	/**
	 * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
	 * @param value
	 * @param statName - body stat with numerical value, like hair, penis, waist, etc.
	 * @param adjust - Optional arg: adjust stat by this.
	 */
	export function statValueToCM(value: number, statName: App.BodyStat, adjust?: number): number;
	export function statValueToCM(value: number, statName: App.BodyStatStr, adjust?: number): number;
	export function statValueToCM(value: number, statName: App.BodyStatStr, adjust: number = 0): number {
		const stat = GetStatConfig(App.StatType.BODY)[statName];
		const CMScale = stat.CM_MAX - stat.CM_MIN;
		const statPercent = Math.floor(((value - stat.MIN) / (stat.MAX - stat.MIN)) * 100);
		return (CMScale * ((statPercent + adjust) / 100)) + stat.CM_MIN;
	}

    /**
     * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
     * @param player
     * @param StatName - currently supported: Bust, Ass, Hips, Waist, Penis
     * @param Adjust - Optional arg: adjust stat by this and report figure.
     */
	export function StatToCM(player: Entity.Player, StatName: App.BodyStat, Adjust?: number): number;
	export function StatToCM(player: Entity.Player, StatName: App.BodyStatStr, Adjust?: number): number;
	export function StatToCM(player: Entity.Player, StatName: App.BodyStatStr, Adjust: number = 0): number {
		if (typeof player.GetStatConfig(App.StatType.BODY)[StatName] === 'undefined') return 0;

		var CMScale = player.GetStatConfig(App.StatType.BODY)[StatName].CM_MAX - player.GetStatConfig("BODY")[StatName].CM_MIN
		return (CMScale * ((player.GetStatPercent("BODY", StatName) + Adjust) / 100)) + player.GetStatConfig("BODY")[StatName].CM_MIN;
	}

    /**
     * Convert Ass stat into CM
	 * @todo maybe unused
     */
	export function AssCCtoCM(player: Entity.Player): number {return StatToCM(player, App.BodyStat.Ass);}

    /**
     * Convert Waist Stat into CM.
     */
	export function WaistInCM(player: Entity.Player): number {return StatToCM(player, App.BodyStat.Waist);}

    /**
     * Convert Hips Stat into CM.
     */
	export function HipsInCM(player: Entity.Player): number {return StatToCM(player, App.BodyStat.Hips);}

    /**
     * Convert Bust Stat into CM.
     */
	export function BustCCtoCM(player: Entity.Player): number {return StatToCM(player, App.BodyStat.Bust);}

    /**
     * Print out a 10 star colorized stat meter for a statistic.
     * @param StatName
     * @param player
     * @param Invert - reverse direction of stars relative to score so that high scores are less stars.
     */
	export function pStatMeter(StatName: App.CoreStat, player: Entity.Player, Invert: boolean = false): string {
		var StatValue = player.GetStat(App.StatType.STAT, StatName);

		if (StatName == App.CoreStat.Hormones) {
			if (StatValue > 100) // Return "Female" version of this meter.
				return pMeter((player.GetStat(App.StatType.STAT, StatName) - 100), 100, Invert);
			if (StatValue <= 100)
				return pMeter((100 - StatValue), 100, Invert);
		}

		return pMeter(player.GetStat(App.StatType.STAT, StatName), player.GetMaxStat(App.StatType.STAT, StatName), Invert);
	}

    /**
     * Return a string describing and coloring the effect
     */
	export function pEffectMeter(effect: string, item: Items.AnyItem): string {
		var output = "";
		var effectStr = effect.replace(/ /g, '&nbsp;');

		// Build color and arrow
		if (effect.indexOf('-') != -1) {
			output = "<span class='state-negative'>" + effectStr.replaceAll('-','&dArr;') + "</span>";
		} else
			if (effect.indexOf('+') != -1) {
				output = "<span class='state-positive'>" + effectStr.replaceAll('+', '&uArr;') + "</span>";
			} else
				if (effect.indexOf('?') != -1) {
					output = "<span class='state-positive'>&uArr;" + effectStr + "</span>";
					if (typeof item !== 'undefined' && typeof item.Data['Style'] !== 'undefined') {
						switch (item.Data['Style']) {
							case 'COMMON': output = output.replace(/RANK/g, "&uArr;"); break;
							case 'UNCOMMON': output = output.replace(/RANK/g, "&uArr;&uArr;"); break;
							case 'RARE': output = output.replace(/RANK/g, "&uArr;&uArr;&uArr;"); break;
							case 'LEGENDARY': output = output.replace(/RANK/g, "&uArr;&uArr;&uArr;&uArr;"); break;
						}
					}
				} else {
					output = "<span class='state-neutral'>" + effectStr + "</span>";
					output = output.replace(/RANK/g, "&uArr;");
				}

		return output;
	}
    /**
     * Print out a 10 star colorized stat meter for a skill.
     * @param StatName
     * @param player
     * @param Invert - reverse direction of stars relative to score so that high scores are less stars.
     */
	export function pSkillMeter(StatName: App.Skill, player: Entity.Player, Invert?: boolean) {
		return pMeter(player.GetStat(App.StatType.SKILL, StatName), 100, Invert);
	}

    /**
     * Print out a 10 start colorized state meter for a particular fashion style
     */
	export function pStyleMeter(Style: Data.Clothes.CategoryStr, player: Entity.Player): string {
		return pMeter(player.GetStyleSpecRating(Style), 100, false);
	}

    /**
     * Finds and prints out the NPC quest dialog as a string.
     * @param QuestID - ID of the Quest.
     * @param Stage - INTRO, MIDDLE, FINISH
     * @param player
     * @param NPC - String, ID of the NPC in player.NPCs array.
     */
	export function pQuestDialog(QuestID: string, Stage: App.QuestStage, player: Entity.Player, NPC: Entity.NPC): string {
		return TokenizeString(player, NPC, App.Data.Quests[QuestID][Stage]);
	}

    /**
     * Prints out the requirements for completion of a quest.
     */
	export function pQuestRequirements(QuestID: string, player: Entity.Player): string {
		const quest = App.Data.Quests[QuestID];
		var checks = quest.CHECKS ?? [];
		var Out: string[] = [];
		var Val: boolean | number = 0;
		var pString:string = "";
		var bMeter = false;
		let bMeterInvert = false;
		const invertConditions = ["lt", "lte"];

		for (const chk of checks) {
			let show = true;
			bMeter = false;
			bMeterInvert = false;
			pString = "";

			switch (chk.TYPE) {
				case "QUEST":
					bMeter = false;
					pString = chk.ALT_TITLE ?? chk.NAME;
					Val = cmpAny(Quest.ByTag(chk.NAME).state(player), chk.VALUE, chk.CONDITION);
					break;
				case "QUEST_FLAG":
					bMeter = false;
					Val = player.QuestFlags.hasOwnProperty(chk.NAME) == false ? false : player.QuestFlags[chk.NAME] == chk.VALUE;
					pString = chk.ALT_TITLE ?? chk.NAME;
					console.debug(chk);
					break;
				case "NPC_STAT":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = player.GetNPC(chk.OPTION ?? quest.GIVER)[chk.NAME]
					pString = player.GetNPC(chk.OPTION ?? quest.GIVER).pName();
					break;
				case "MONEY":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = player.Money;
					pString = "<span class='item-money'>Coins</span>";
					break;
				case "BODY":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = player.GetStatPercent(chk.TYPE, chk.NAME);
					pString = `${GetAdjective(chk.TYPE, chk.NAME, chk.VALUE, true)} ${GetNoun(chk.TYPE, chk.NAME, chk.VALUE, true)}`;
					break;
				case "STAT":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = player.GetStatPercent(chk.TYPE, chk.NAME);
					pString = chk.NAME;
					break;
				case "SKILL":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = player.GetStatPercent(chk.TYPE, chk.NAME);
					pString = chk.NAME;
					break;
				case "STYLE_CATEGORY":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = Math.max(0, Math.min(player.GetStyleSpecRating(chk.NAME), 100));
					pString = chk.NAME;
					break;
				case "HAIR_STYLE":
					bMeter = false;
					Val = ((player.GetHairStyle() == chk.NAME) == chk.VALUE);
					pString = "Hair style - " + chk.NAME;
					break;
				case "HAIR_COLOR":
					bMeter = false;
					Val = ((player.GetHairColor() == chk.NAME) == chk.VALUE);
					pString = "Hair color - " + chk.NAME;
					break;
				case "ITEM": {
					bMeter = false;
					const itemName = App.Items.SplitId(chk.NAME);
					pString = App.Items.Factory(itemName.Category, itemName.Tag).Description;
					var cv = chk.VALUE;
					const invItem = player.GetItemById(chk.NAME);
					if (!invItem && chk.HIDDEN) {
						show = false;
					} else {
						if (typeof cv !== 'undefined' && cv > 1 && !(invItem instanceof Items.Clothing)) {
							pString = pString + " x" + cv;
							Val = (typeof invItem !== 'undefined') ? invItem.Charges() : 0;
							bMeter = true;
						} else {
							Val = (typeof invItem !== 'undefined');
						}
					}
					break;
				}
				case "DAYS_PASSED":
					bMeter = false;
					const offset = chk.NAME ? Quest.GetFlag(player, chk.NAME) as number : 0;
					const diffDays = chk.VALUE - player.Day + offset;
					const waitConditions: Data.Tasks.Condition[] = ["eq", "gt", "gte"];
					if (waitConditions.includes(chk.CONDITION)) {
						pString = `wait ${diffDays} days`;
						Val = diffDays < 1;
					} else {
						pString = diffDays > 0 ? `${diffDays} days left` : `expired ${0 - diffDays} days ago`;
						Val = diffDays > 1;
					}
					break;
				case "IS_WEARING":
					bMeter = false;
					const isEq = chk.SLOT ? player.GetEquipmentInSlot(chk.SLOT)?.Name == chk.NAME : player.IsEquipped(chk.NAME, false);
					if (chk.VALUE === false) {
						pString = "''NOT'' ";
						Val = !isEq;
					} else {
						pString = "";
						Val = isEq;
					}
					if (chk.SLOT) {
						pString = pString + `wearing in slot '${chk.SLOT}'`;
					} else {
						pString = pString + "wearing " + chk.NAME.toLowerCase();
					}
					break;
				case "TRACK_CUSTOMERS":
					bMeter = true;
					var flag = App.Quest.GetFlag(player, "track_" + chk.NAME) as number;
					var count = player.GetHistory("CUSTOMERS", chk.NAME) as number;
					Val = (count - flag);
					pString = "Satisfy Customers";
					break;
				case "TRACK_PROGRESS":
					bMeter = true;
					bMeterInvert = invertConditions.contains(chk.CONDITION);
					Val = App.Quest.GetProgressValue(player, chk.NAME);
					pString = "Progress";
					break;
			}

			if (show) {
				if (bMeter === true) {
					Out.push(pQuestMeter(player, pString, Val as number, chk['VALUE'], bMeterInvert));
				} else {
					Out.push(pQuestCheckbox(player, pString, Val as boolean));
				}
			}

		}

		return Out.join("\n");
	}

	function pQuestCheckbox(_player: Entity.Player, pString: string, Val: boolean): string {
		var Out = pString + " ";
		Out = (Val == true ? "<span class='state-positive'> &#9745; </span>" : "<span class='state-negative'> &#9746; </span>") + Out;
		return Out;
	}

	function pQuestMeter(player: Entity.Player, Name: string, playerValue: number, GoalValue: number, Invert = false): string {
		console.debug(`pQuestMeter(${player},${Name},${playerValue},${GoalValue},${Invert})`);
		playerValue = Math.round(playerValue * 1000) / 1000;
		var c, m, p;
		if (Invert) {
			m = (100 - GoalValue);
			c = (100 - playerValue);
		} else {
			c = playerValue;
			m = GoalValue;
		}

		p = Math.floor(((c / m) * 100));
		console.debug(`pMeter(${c},${m},0) called by pQuestMeter`);

		return `<span class=\"fixed-font\">${pMeter(c, m, false)}${numericalMeters ? '/' + m : ''}</span>&nbsp; ${p} % ${Name}`;
	}

    /**
     * Get ID for a reward choices radio group
     */
	function _choiceRadioGroupId(Scene: App.Scene): string {
		return "taskRewardChoices" + Scene.Id();
	}

    /**
     * Print out task rewards
     */
	export function pTaskRewards(Task: App.Task): string[] {
		var Output: string[] = [];

		var Pay = Task.TaskData["PAY"] ?? 0;
		var Tokens = Task.TaskData["TOKENS"] ?? 0;
		var Items: string[] = [];
		var SlotUnlockCount = 0;

		for (const scene of Task.Scenes) {
			var reward = scene.RewardItems();
			if (reward.Pay > 0) {Pay += scene.RewardItems().Pay;}
			if (reward.Tokens > 0) {Tokens += scene.RewardItems().Tokens;}
			for (const ri of reward.Items) {
				if (typeof ri === 'number') continue; // we had to put number here too in order to maintain order
				var n = App.Items.SplitId(ri.Name);
				var oItem = App.Items.Factory(n.Category, n.Tag);
				Items.push(oItem.Description + " x " + ri.Value);
			}
			SlotUnlockCount += reward.SlotUnlockCount;
		}

		if (Pay > 0) {
			Output.push("<span class='item-money'>" + Pay + " coins</span>");
		}

		if (Tokens > 0) {
			Output.push(`<span class='item-courtesan-token'>${Tokens} courtesan tokens</span>`);
		}

		var i = 0;
		for (i = 0; i < SlotUnlockCount; ++i) {
			Output.push("<span style='color:cyan'>A slot reel unlock!</span>");
		}
		Output.push.apply(Output, Items);

		// we print item choices at the end, thus let's loop scenes one more time
		for (const scene of Task.Scenes) {
			var Reward = scene.RewardItems();
			if (Reward.ItemChoices.length === 0) continue;
			var SceneId = scene.Id();
			var radioName = _choiceRadioGroupId(scene);
			var choiceSelection = "<ul id=\"ItemChoice" + SceneId + "\">\n";
			for (i = 0; i < Reward.ItemChoices.length; ++i) {
				var ci = Reward.ItemChoices[i];
				n = App.Items.SplitId(ci["Name"]);
				oItem = App.Items.Factory(n.Category, n.Tag);
				choiceSelection += "<li><input type=\"radio\" id=\"" + radioName + i + "\" name=\"" + radioName + "\" value=\"" + i + "\"";
				if (i === 0) {
					choiceSelection += " checked";
				}
				choiceSelection += "><label for=\"" + radioName + i + "\">" + oItem.Description + " x " + ci["Value"] + "</label></li>";
			}
			choiceSelection += "</ul>";
			Output.push(choiceSelection);
		}
		return Output;
	}

    /**
     * Set scene item choices basing on the user selection
     */
	export function SetTaskRewardChoices(Task: App.Task) {
		for (var scene of Task.Scenes) {
			var reward = scene.RewardItems();
			if (reward.ItemChoices.length === 0) continue;
			const selector = `input[name="${_choiceRadioGroupId(scene)}"]:checked`
			const elem = document.querySelector(selector);
			if (elem) {
				reward.ChosenItem = parseInt((elem as HTMLInputElement)["value"]);
			} else {
				console.error(`Could not find reward element via selector: ${selector}`);
			}
		}
	}

    /**
     * Print the description of an item.
     */
	function pItemDesc(ItemType: App.Data.ItemTypeStr, ItemTag: string, ItemAmount: number, Opt?: boolean): string {
		var oItem = App.Items.Factory(ItemType, ItemTag);
		if (Opt && ItemAmount > 1) return oItem.Description + " x " + ItemAmount;
		return oItem.Description;
	}

    /**
     * Print out a description of the players Ass statistic.
     */
	export function pAss(player: Entity.Player, brief: boolean = false): string {
		var aPercent = player.GetStatPercent("BODY", "Ass");
		var fPercent = player.GetStatPercent("STAT", "Fitness");

		if (brief) {
			return GetAdjective("BODY", "Ass", aPercent, true) + ' ' + GetAdjective("BODY", "AssFirmness", fPercent, true);
		}

		var hPercent = player.GetStatPercent("BODY", "Hips");
		var Output = TokenizeRating(player, "BODY", "Ass", GetRating("Ass", aPercent));

		if ((aPercent > 30) || (hPercent > 30)) {
			if (aPercent < (hPercent - 15)) {
				Output += ` It is ${ColorizeString(0.2, "disproportionately small", 1.0)} for your `;
			} else if (aPercent > (hPercent + 15)) {
				Output += ` It is ${ColorizeString(0.2, "disproportionately big", 1.0)} for your `;
			} else {
				Output += ` It is ${ColorizeString(0.4, "flattered", 1)} by your `;
			}
			Output += GetAdjective("BODY", "Hips", hPercent, true) + " hips.";
		}

		return Output;
	}

    /**
     * Print out a description of the players Penis statistic.
     */
	export function pPenis(player: Entity.Player, onlyAdj: boolean = false): string {
		var pPercent = player.GetStatPercent("BODY", "Penis");
		// var iLength = CMtoINCH(player.GetStat(App.StatType.BODY, App.BodyStat.Penis));
		if (onlyAdj) return GetAdjective(App.StatType.BODY, App.BodyStat.Penis, pPercent, true);
		return TokenizeRating(player, "BODY", "Penis", GetRating("Penis", pPercent));
	}

    /**
     * Print how does the futa state matches current body state
     */
	export function pFutaStatus(player: Entity.Player): string {
		const pFuta = player.GetStatPercent("STAT", "Futa");
		const hormones = player.GetStat("STAT", "Hormones");
		if ((hormones > 100)) {
			const pPenis = player.GetStatPercent("BODY", "Penis");
			const dp = pFuta - pPenis;
			if (dp > 90) return "You crave for a bigger penis."
			if (dp > 60) return "You feel an urge to grow a bigger penis."
			if (dp > 30) return "You are pretty sure a bigger penis would be a good thing to get."
			if (dp > 5) return "You feel your penis could be a bit bigger."
			return "You consider your penis size to be about right for you."
		} else {
			const pBust = player.GetStatPercent("BODY", "Bust");
			const db = pFuta - pBust;
			if (db > 90) return "You crave for bigger tits."
			if (db > 60) return "You feel an urge to grow your boobs."
			if (db > 30) return "You are pretty sure bigger tits would be a good thing to get."
			if (db > 5) return "You feel your bust could be a bit bigger."
			return "You consider your bust size to be about right for you."
		}
	}

    /**
     * Prints out a description of the player's height statistic.
     */
	export function pHeight(player: Entity.Player, brief = false): string {
		var pHeight = StatToCM(player, "Height");
		return lengthString(pHeight, true) + (brief ? "" : " tall");
	}

    /**
     * Prints out a description of the player's fetish statistic.
     */
	export function pFetish(player: Entity.Player): string {
		return ColorizeString(player.Fetish(), GetRating("Fetish", player.Fetish()));
	}

    /**
     * Prints out a description of the player's height statistic.
     */
	export function pBeauty(player: Entity.Player): string {
		return ColorizeString(player.Beauty(), GetRating("Beauty", player.Beauty()));
	}

    /**
     * Print out a description of the player's Balls statistic.
     */
	export function pBalls(player: Entity.Player, brief = false) {
		var bPercent = player.GetStatPercent("BODY", "Balls");
		if (brief) return GetAdjective("BODY", "Balls", bPercent, true);
		return TokenizeRating(player, "BODY", "Balls", GetRating("Balls", bPercent));
	}

    /**
     * Print out a description of the player's Waist statistic.
     */
	export function pWaist(player: Entity.Player, brief = false) {
		var wPercent = player.GetStatPercent("BODY", "Waist");
		// var iLength = CMtoINCH(WaistInCM(player));
		if (brief) return GetAdjective("BODY", "Waist", wPercent, true);
		return TokenizeRating(player, "BODY", "Waist", GetRating("Waist", wPercent));
	}

    /**
     * Print out a description of the player's Bust statistic.
     */
	export function pBust(player: Entity.Player, brief = false) {
		var bPercent = player.GetStatPercent("BODY", "Bust");
		if (brief) {
			var fPercent = player.GetStatPercent("BODY", "BustFirmness");
			return GetAdjective("BODY", "Bust", bPercent, true) + ' ' + GetAdjective("BODY", "BustFirmness", fPercent, true);
		}
		return TokenizeRating(player, "BODY", "Bust", GetRating("Bust", bPercent));
	};

    /**
     * Print out a description of the player's bust firmness statistic.
     */
	export function pBustFirmness(player: Entity.Player, brief = false) {
		var fPercent = player.GetStatPercent("BODY", "BustFirmness");
		if (brief) {
			return GetAdjective("BODY", "BustFirmness", fPercent, true);
		}
		return TokenizeRating(player, "BODY", "BustFirmness", GetRating("BustFirmness", fPercent));
	}

    /**
     * Print out a description of the player's bust lactation statistic.
     */
	export function pLactation(player: Entity.Player, brief = false) {
		var fPercent = player.GetStatPercent("BODY", "Lactation");
		if (brief) {
			return GetAdjective("BODY", "Lactation", fPercent, true);
		}
		return TokenizeRating(player, "BODY", "Lactation", GetRating("Lactation", fPercent));
	}

    /**
     * Print out a description of the player's Bust (CUP) statistic.
     */
	export function pCup(player: Entity.Player) {
		var bustStatVal = player.GetStat(StatType.BODY, BodyStat.Bust);
		var cc = (GetTotalXPPoints(StatType.BODY, BodyStat.Bust, 0, bustStatVal) +
			player.GetStatXP(StatType.BODY, BodyStat.Bust)) / 3.23;
		return App.unitSystem.cupString(bustStatVal) + " cup (" + App.unitSystem.massString(cc, true, 1000) + " each)";
	}

    /**
     * Print out a description of the player's Lips statistic.
     */
	export function pLips(player: Entity.Player, brief = false) {
		var lPercent = player.GetStatPercent("BODY", "Lips");
		if (brief) return GetAdjective("BODY", "Lips", lPercent, true);
		return TokenizeRating(player, "BODY", "Lips", GetRating("Lips", lPercent));
	}
    /**
     * Print out a description of the player's Hips statistic.
     */
	export function pHips(player: Entity.Player, brief = false) {
		var hPercent = player.GetStatPercent("BODY", "Hips");
		if (brief) return GetAdjective("BODY", "Hips", hPercent, true);
		return TokenizeRating(player, "BODY", "Hips", GetRating("Hips", hPercent));
	}

    /**
     * Print out a description of the player's Eyes.
     */
	export function pEyes(player: Entity.Player) {
		var lashes = player.GetStatPercent("STAT", "Hormones") >= 75 ? "long" : "average length";
		return "You have " + lashes + " eyelashes and " + player.EyeColor + " colored eyes.";
	}
    /**
     * Print out a description of the player's Style.
     */
	export function pStyle(player: Entity.Player) {
		return ColorizeString(player.Style(), GetRating("Style", player.Style()));
	}

    /**
     * Print out a description of the player's Clothing.
     */
	export function pClothing(player: Entity.Player) {
		return ColorizeString(player.ClothesRating(), GetRating("Clothing", player.ClothesRating()));
	}

    /**
     * Print out a description of the player's Makeup.
     */
	export function pMakeup(player: Entity.Player) {
		if (player.MakeupStyle == "plain faced") return "You are plain faced and not wearing any makeup";
		return "You are wearing " + ColorizeString(player.MakeupRating(), player.MakeupStyle) + " makeup";
	}

    /**
     * Print out a description of the player's Face.
     */
	export function pFace(player: Entity.Player, brief = false) {
		var fPercent = player.GetStatPercent("BODY", "Face");
		if (brief) return GetAdjective("BODY", "Face", fPercent, true);
		var Output = TokenizeRating(player, "BODY", "Face", GetRating("Face", fPercent));
		if (player.MakeupRating() == 0) {
			Output += " it is bare and devoid of cosmetics.";
		} else if (player.MakeupRating() < 40) {
			Output += " it is poorly done up in " + ColorizeString(player.MakeupRating(), player.MakeupStyle) + " makeup.";
		} else if (player.MakeupRating() < 60) {
			Output += " it is moderately well done up in " + ColorizeString(player.MakeupRating(), player.MakeupStyle) + " makeup, somewhat enhancing your appeal.";
		} else if (player.MakeupRating() < 80) {
			Output += " it is expertly done up in " + ColorizeString(player.MakeupRating(), player.MakeupStyle) + " makeup, enhancing your appeal.";
		} else {
			Output += " it is flawlessly painted in " + ColorizeString(player.MakeupRating(), player.MakeupStyle) + " makeup, greatly enhancing your appeal.";
		}
		return Output;
	}

    /**
     * Print out a description of the player's Fitness.
     */
	export function pFitness(player: Entity.Player, brief = false) {
		var fPercent = player.GetStatPercent("STAT", "Fitness");
		if (brief) return GetAdjective("STAT", "Fitness", fPercent, true);
		return TokenizeRating(player, "STAT", "Fitness", GetRating("Fitness", fPercent));
	}

    /**
     * Print out a description of the player's Hormones.
     */
	export function pHormones(player: Entity.Player): string {
		return GetAdjective(StatType.STAT, CoreStat.Hormones, player.GetStat(StatType.STAT, CoreStat.Hormones), true);
	}

    /**
     * Print out a description of the player's Hair.
     */
	export function pHair(player: Entity.Player, brief = false): string {
		var Wig = player.GetEquipmentInSlot("Wig");
		if (Wig) {
			if (brief) {
				return `${Wig.HairLength()} ${Wig.HairColor()} wig`;
			}
			return "You are wearing a wig to hide your natural hair. It is " + Wig.HairColor() + " and " +
				lengthString(Wig.HairLength()) + " long, styled in " +
				ColorizeString(Wig.HairBonus(), Wig.HairStyle()) + ".";
		}
		if (brief) {
			return lengthString(StatToCM(player, "Hair"), false) + " " + player.HairColor;
		}
		return "Your hair is " + player.HairColor + " and " + lengthString(StatToCM(player, "Hair"), false) +
			" long, styled in " + ColorizeString(player.HairRating(), player.HairStyle) + ".";
	}

    /**
     * Print out a description of the player's Figure.
     */
	export function pFigure(player: Entity.Player): string {
		var pBust = StatToCM(player, "Bust");
		var pWaist = StatToCM(player, "Waist");
		var pHips = StatToCM(player, "Hips");

		var statsStr = `${lengthValue(pBust)}-${lengthValue(pWaist)}-${lengthValue(pHips)} figure`;

		var rBustHips = pBust / pHips;

		// Cases for being fat (too much waist). Go on a diet.
		if (pWaist >= 120) return `a ${ColorizeString(0, "morbidly obese", 1)} ${statsStr}`;
		if (pWaist >= 100) return `an ${ColorizeString(0.05, "obese", 1)} ${statsStr}`;
		if (pWaist >= 90) return `a ${ColorizeString(0.1, "fat", 1)} ${statsStr}`;

		//Boobs and hips are in proportion
		if (rBustHips <= 1.1 && rBustHips >= 0.9) {
			if (pBust >= 95 && pWaist <= 75) return `an ${ColorizeString(1, "extreme hourglass", 1)} ${statsStr}r`;
			if (pBust >= 95) return `an ${ColorizeString(0.9, "hourglass", 1)} ${statsStr}`;
			if (pBust >= 90) return `a ${ColorizeString(0.8, "very curvy", 1)} ${statsStr}`;
			if (pBust >= 85) return `a ${ColorizeString(0.7, "curvy", 1)}  ${statsStr}`;
			if (pBust >= 82) return `a ${ColorizeString(0.6, "slightly curvy", 1)} ${statsStr}`;
			if (pWaist <= 55) return `a ${ColorizeString(0.5, "petite", 1)} ${statsStr}`;
			if (pWaist <= 70) return `a ${ColorizeString(0.4, "slender", 1)} ${statsStr}`;
			if (pWaist < 80) return `a ${ColorizeString(0.3, "thin", 1)} ${statsStr}`;
			return `an ${ColorizeString(0.2, "average", 1)} ${statsStr}`;
		}

		// Boobs are bigger than hips
		if (rBustHips >= 1.25 && pBust >= 90) return `a ${ColorizeString(0.6, "top heavy", 1)} ${statsStr}`;

		if (rBustHips <= 0.75 && pHips >= 90) return `a ${ColorizeString(0.6, "bottom heavy", 1)} ${statsStr}`;

		if (pWaist <= 55) return `a ${ColorizeString(0.4, "petite", 1)} ${statsStr}`;
		if (pWaist <= 70) return `a ${ColorizeString(0.4, "slender", 1)} ${statsStr}`;
		if (pWaist < 80) return `a ${ColorizeString(0.4, "thin", 1)} ${statsStr}`;

		return `an ${ColorizeString(0.2, "average", 1)} ${statsStr}`;
	}


    /**
     * Replace tokens in string with calculated/derived literals and return it.
     */
	export function TokenizeString(player: Entity.Player, NPC: Entity.NPC | undefined, str: string): string {
		if (typeof NPC !== 'undefined') {
			str = str.replace(/NPC_NAME's/g, "<span class='npc'>" + NPC.Name + "'s</span>");
			str = str.replace(/NPC_NAME/g, "<span class='npc'>" + NPC.Name + "</span>");
		}

		function adjReplacer(_match: string, stat:BodyStatStr) {
			return GetAdjective(StatType.BODY, stat, player.GetStat(App.StatType.BODY, stat), true);
		}
		function nounReplacer(_match: string, stat:BodyStatStr) {
			return GetplayerNoun("BODY", stat, player, false, true);
		}
		function pReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> StatName
			var statName = stat[0] + stat.slice(1).toLowerCase().replace(/_([a-z])/g, (_m, c) => c.toUpperCase());
			var statFuncName = 'p' + statName;
			if (PR.hasOwnProperty(statFuncName))
				// @ts-ignore
				return PR[statFuncName](player, true) + delim;
			return prefix + stat + delim;
		}
		function nReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> StatName
			var statName = stat[0] + stat.slice(1).toLowerCase().replace(/_([a-z])/g, (_m, c) => c.toUpperCase()) as BodyStatStr;
			if (App.Data.Naming.BodyConfig.hasOwnProperty(statName))
				return GetplayerNoun("BODY", statName, player, true, true) + delim;
			return prefix + stat + delim;
		}
		function vReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> StatName
			var statName = stat[0] + stat.slice(1).toLowerCase().replace(/_([a-z])/g, (_m, c) => c.toUpperCase());
			if (player.GetStatObject(App.StatType.BODY).hasOwnProperty(statName))
				return player.GetStat(App.StatType.BODY, statName as BodyStat) + delim;
			return prefix + stat + delim;
		}

		// Usage: p(Slot1|Slot2|...|$optional default string)
		// Use with player.IsEquipped(string|array,bool) for most cases.
		function equipReplacer(_match: string, part: string) {
			var slots = part.split("|");
			for (var i = 0; i < slots.length; i++) {
				if (slots[i][0] == '$') return slots[i].slice(1); // default string
				const equip = player.GetEquipmentInSlot(slots[i] as Data.ClothingSlot);
				if (equip != null) return equip.Description;
			}
			return "<span style='color:red'>bug!</span>";
		}

		// Like pReplacer, but pass an argument instead of using the characters statistic.
		function pReplacer2(_match: string, _prefix: string, stat: string, num: string, delim: string) {
			var statName = stat[0] + stat.slice(1).toLowerCase().replace(/_([a-z])/g, (_m, c) => c.toUpperCase());
			var statType: StatType = player.CoreStats.hasOwnProperty(statName) ? StatType.STAT :
				player.Skills.hasOwnProperty(statName) ? StatType.SKILL : StatType.BODY;

			return GetAdjective(statType, statName as any, Number(num), true) + delim;
		}

		str = str.replace(/PLAYER_NAME/g, "<span style='color:DeepPink'>" + player.SlaveName + "</span>");
		str = str.replace(/GF_NAME/g, "<span style='color:pink'>" + player.GirlfriendName + "</span>");
		str = str.replace(/pCUP/g, pCup(player)); // needs special handling because it has only a single parameter
		str = str.replace(/NOUN_([A-Za-z_]+)/g, nounReplacer);
		str = str.replace(/ADJECTIVE_([A-Za-z_]+)/g, adjReplacer);
		str = str.replace(/pBLOWJOBS/g, GetAdjective("SKILL", "BlowJobs", player.GetStat("SKILL", "BlowJobs"), true));
		str = str.replace(/pPHASE/g, player.GetPhase(false));
		str = str.replace(/pEQUIP\(([^\)]*)\)/g, equipReplacer);
		str = str.replace(/(p)([A-Z]+)_([0-9]+)([^0-9]|$)/g, pReplacer2);
		str = str.replace(/(q)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replace(/(p)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replace(/(n)([A-Z_]+)([^A-Za-z]|$)/g, nReplacer);
		str = str.replace(/(v)([A-Z_]+)([^A-Za-z]|$)/g, vReplacer);
		// Hack for highlighting NPC speech
		str = str.replace(/s\(([^\)]+)\)/g, function (_m, p) {return `<span class='npcText'>"${p}"</span>`;});
		// Important! highlight NPC speech
		str = str.replace(/s\!\(([^\)]+)\)/g, function (_m, p) {return `<span class='impText'>"${p}"</span>`;});
		// Highlighting PC speech
		str = str.replace(/sp\(([^\)]+)\)/g, function (_m, p) {return `<span class='pcText'>"${p}"</span>`;});
		// Highlighting PC thoughts
		str = str.replace(/tp\(([^\)]+)\)/g, function (_m, p) {return `<span class='pcThought'>"${p}"</span>`;});

		return str;
	}

	function pSkillName(skill: SkillStr): string {
		return App.Data.Lists.SkillConfig[skill].ALTNAME ?? skill;
	}

	export function pShipMapIcon(index: number): void {
		if (index >= App.Data.Lists.ShipRoute.length) index = 0; // Force reset.
		var top = App.Data.Lists.ShipRoute[index].top;
		var left = App.Data.Lists.ShipRoute[index].left;
		console.log("Placing map icon at top=" + top + ", left=" + left);
		$(document).one(":passageend", function () {$("#mapIcon").css({"top": top, "left": left});});
	}

	export function phaseName(phase: number): string {
		return phase >= 0 && phase <= 4 ? ["morning", "afternoon", "evening", "night", "late night"][phase] : phase.toString();
	}

    /**
	 * Pass it an array, returns a random element of that array.
     */
	export function GetRandomListItem<T>(list: T[]): T {
		return list.randomElement();
	}

    /**
     * Get icon for marking favorite item in inventory or shop lists
     */
	export function GetItemFavoriteIcon(isFavorite: boolean): string {
		return isFavorite ? "<span class='action-general'>&#9733;</span>" : "<span>&#9734;</span>";
	}

    /**
     * Get random item from inventory
     */
	export function GetRandomItemId(player: Entity.Player): string {
		const itemTypeCount = player.InventoryItemsCount();
		let randomIndex = Math.floor(Math.random() * itemTypeCount);
		let id = "";
		player.InventoryManager.EveryItemRecord(undefined, undefined,
			function (n, tag, itemClass) {
				if (randomIndex <= 0) {
					id = Items.MakeId(itemClass, tag);
					return false;
				}
				randomIndex -= n;
				return true;
			}
		);
		return id;
	}

	export function RefreshTwineMoney(): void {
		try {
			$("#Money").text(setup.player.Money.toString());
		} catch (err) {

		}
	}

	function RefreshTwineTokens(): void {
		try {
			$("#Tokens").text(setup.player.Tokens.toString());
		} catch (err) {

		}
	}

	export function pHormoneSymbol(): string {
		var val = setup.player.CoreStats.Hormones;

		if (val < 78) {
			return "<span id='HormoneSymbol' class='state-masculinity'>♂</span>";
		} else if (val >= 144) {
			return "<span id='HormoneSymbol' class='state-feminity'>♀</span>";
		} else {
			return "<span id='HormoneSymbol' class='state-neutral'>⚥</span>";
		}
	}

	export function RefreshTwineMeter(m: App.CoreStat): void {
		var invert = (m === 'Toxicity') ? true : false;
		var str = "";
		try {
			$("#" + m).html(pStatMeter(m, setup.player, invert));

			if (m === 'Hormones') {
				$("#HormoneSymbol").html(pHormoneSymbol());
			}

		} catch (err) {

		}
	}

	function RefreshSlaveName() {
		try {
			$("#scoreSlaveName").text('"' + setup.player.SlaveName + '"');
		} catch (err) {

		}
	}

	export function RefreshTwineScore() {
		// Redraw Energy Bars
		try {
			RefreshTwineMeter(App.CoreStat.Health);
			RefreshTwineMeter(App.CoreStat.Energy);
			RefreshTwineMeter(App.CoreStat.WillPower);
			RefreshTwineMeter(App.CoreStat.Perversion);
			RefreshTwineMeter(App.CoreStat.Nutrition);
			RefreshTwineMeter(App.CoreStat.Femininity);
			RefreshTwineMeter(App.CoreStat.Toxicity);
			RefreshTwineMeter(App.CoreStat.Hormones);
		} catch (err) {
			//no-op
		}
	}

    /**
     * Prints item description for the inventory list
     */
	export function PrintItem(item: App.Items.Clothing | App.Items.Consumable | App.Items.QuestItem, player: Entity.Player): string {
		var res = "<span class='inventoryItem'>" + item.Description;
		if (settings.inlineItemDetails) {
			res += '<span class="tooltip">' + item.Examine(player, false) + '</span></span>';
			res += "<br><div class='inventoryItemDetails'>" + item.Examine(player, true) + '</div>';
		} else {
			res += '</span>';
		}
		return res;
	}

    /**
     * Highlight active button in a tabbar
     *
     * Finds the active element and appends " active" to its style, removing " active" from all
     * other children of the tabbar.
     * @param tabbarId  Id of the tab bar element
     * @param activeButtonId Id of the button for the active tab
     * @param activeTabText Text to set for .activeTabCaption children of the tab bar
     *
     * @example <div id="tabbar">
     *  <span class="activeTabCaption">placeholder text</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton">Button2</button></span>
     * </div>
     *
     * Then calling HighlightActiveTabButton("tabbar", "btn1", "Sample text")
     *
     * will replace "placeholder text" with "Sample text" and append " active" to btn1 class:
     * <span class="tablink" id="btn1"><button class="mybutton active">Button1</button></span>
     *
     * The next call HighlightActiveTabButton("tabbar", "btn2", "Sample text2") will result in:
     * <div id="tabbar">
     *  <span class="activeTabCaption">Sample text2</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton active">Button2</button></span>
     * </div>
     */
	export function HighlightActiveTabButton(tabbarId: string, activeButtonId: string, activeTabText: string): void {
		const tabBar = document.getElementById(tabbarId);
		if (!tabBar) return;
		const tabs = tabBar.getElementsByClassName("tablink");
		for (const e of tabs) {
			if (e.firstChild instanceof HTMLElement) {
				e.firstChild.className = e.firstChild.className.replace(" active", "");
			}
		}
		const linkElem = document.getElementById(activeButtonId);
		if (linkElem && linkElem.firstChild instanceof HTMLElement) linkElem.firstChild.className += " active";
		const activeTab = tabBar.getElementsByClassName("activeTabCaption");
		for (const e of activeTab) {
			if (e instanceof HTMLElement) {
				e.innerText = activeTabText;
			}
		}
	}

	export function RisingDialog(element: string | HTMLElement, message: string, color?: string): void;
	export function RisingDialog(element: any, message: string, color: string = "white"): void {
		var root = $(element);
		$('#WhoreDialogDiv2').remove();

		var div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv2');
		var header = $('<h1>').addClass('ml13').html(message);
		header.css('color', color);
		div.append(header);
		root.append(div)

		// Wrap every letter in a span
		$('.ml13').each(function () {
			$(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml13 .letter',
				translateY: [100, 0],
				translateZ: 0,
				opacity: [0, 1],
				easing: "easeOutExpo",
				duration: 1000,
				delay: function (_el, i) {
					return 300 + 30 * i;
				}
			}).add({
				targets: '.ml13 .letter',
				translateY: [0, -100],
				opacity: [1, 0],
				easing: "easeInExpo",
				duration: 1000,
				delay: function (_el, i) {
					return 100 + 30 * i;
				}
			});
	}

	export function DialogBox(element: HTMLElement, message: string, props?: Record<string, string | number>): void;
	export function DialogBox(element: string, message: string, props?: Record<string, string | number>): void;
	export function DialogBox(element: any, message: string, props: Record<string, string | number> = {}) {
		const bgColor = props.color;
		const fgColor = props.fgColor;
		const lineProps = bgColor !== undefined ? {"background-color": bgColor} : {};
		const textProps = fgColor !== undefined ? {"color": fgColor} : {};
		var root = $(element);
		$('#WhoreDialogDiv').remove();

		var div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv').css(props);
		var header = $('<h1>').addClass('ml1');
		var inner = $('<span>').addClass('text-wrapper');

		inner.append($('<span>').addClass('line line1').css(lineProps));
		inner.append($('<span>').addClass('letters').html(message).css(textProps));
		inner.append($('<span>').addClass('line line2').css(lineProps));

		header.append(inner);
		div.append(header);
		root.append(div);

		// Javascript animations.
		$('.ml1 .letters').each(function () {
			$(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml1 .letter',
				scale: [0.3, 1],
				opacity: [0, 1],
				translateZ: 0,
				easing: "easeOutExpo",
				duration: 600,
				delay: function (_el, i) {
					return 70 * (i + 1)
				}
			}).add({
				targets: '.ml1 .line',
				scaleX: [0, 1],
				opacity: [0.5, 1],
				easing: "easeOutExpo",
				duration: 700,
				offset: '-=875',
				delay: function (_el, i, l) {
					return 80 * (l - i);
				}
			}).add({
				targets: '.ml1',
				opacity: 0,
				duration: 1000,
				easing: "easeOutExpo",
				// delay: 1000
				//delay: 500
			});
	}

	//Stuff to support fight club

	function FightClubFlag(club: string): string {
		return "FIGHTCLUB_TRACK_" + club.replace(/ /g, "_");
	}

	export function AddFightClubResult(player: Entity.Player, club: string, victory: boolean): number {
		var key = victory ? FightClubFlag(club) + "_WINS" : FightClubFlag(club) + "_LOSSES";
		if (player.QuestFlags.hasOwnProperty(key)) {
			player.QuestFlags[key] = 1 + <number>player.QuestFlags[key];
		} else {
			player.QuestFlags[key] = 1;
		}

		return <number>player.QuestFlags[key];
	}

	export function FightClubMenu(player: Entity.Player, club: string): string {
		var clubFlag = FightClubFlag(club);
		var winFlag = clubFlag + "_WINS";

		var wins = player.QuestFlags.hasOwnProperty(winFlag) ? player.QuestFlags[winFlag] as number : 0;

		var rows = App.Combat.ClubData[club];

		var str = "";

		for (var i = 0; i < rows.length; i++) {
			var r = rows[i];
			str += "<tr>";
			str += "<td>" + ColorizeString((i + 1), r.Title, rows.length) + "</td>";

			//Fight
			if (wins < r.WinsRequired) {
				str += "<td><span style='color:grey'>Need " + r.WinsRequired + " wins</span></td>";
			} else if (wins > r.MaxWins && r.MaxWins != 0) {
				str += "<td><span style='color:orange'>Too Experienced</span></td>";
			} else if (player.Phase > 3) {
				str += "<td><span style='color:red'>CLOSED</span></td>"
			} else {
				str += "<td> \
                <<link 'Fight!' 'Combat'>> \
                <<run setup.Combat.InitializeScene({noWeapons:true});>>\
                <<run setup.Combat.LoadEncounter('"+ r.Encounter + "');>>\
                <</link>>\
                </td>";
			}
			str += "</tr>";
		}

		return str;
	}

	export function playerStatChooser<T extends keyof StatTypeStrMap>(type: T, stat: StatTypeStrMap[T], steps: number| number[]) {
		const res:string[] = [];

		let values: number[] = [];
		if (typeof steps === "number") {
			const cfg = GetStatConfig(type)[stat];
			const step = (cfg.MAX - cfg.MIN) / steps;
			for (let v = cfg.MIN; v <= cfg.MAX; v += step) {
				values.push(v);
			}
		} else {
			values = steps;
		}

		function setStat<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], value: number) {
			setup.player.setStat(type, stat, value);
		}

		for (const v of values) {
			const adj = GetAdjective(type, stat, v, true);
			const strVal = adj !== "" ? adj : lengthString(statValueToCM(v, stat as BodyStatStr))
			res.push(App.UI.link(strVal, setStat, [type, stat, v]));
		}
		return res.join(" | ");
	}

	/**
 * Handler for the meters 'print numbers' setting
 */
	 export function handleMetersNumberValueSettingChanged() {
		if (settings.displayMeterNumber) {
			// to accommodate longer meters
			$('#ui-bar').css("width","350px"); // seems to work better on most browsers
			App.PR.setNumericalMeters(true);
		} else {
			App.PR.setNumericalMeters(false);
			$('#ui-bar').css("width","330px");
		}

		App.PR.RefreshTwineScore();
	}

	export function handleDisplayBodyScoreChanged() {
		if (!settings.displayBodyScore) {
			$('#bodyScoreContainer').empty();
		}
	}

	export function handleAutosaveChanged() {
		const safeLocationTag = "hideout";
		if (settings.autosaveAtSafePlaces) {
			if (Array.isArray(Config.saves.autosave)) {
				if (!Config.saves.autosave.contains(safeLocationTag)) {
					Config.saves.autosave.push(safeLocationTag);
				}
			} else {
				Config.saves.autosave = [safeLocationTag];
			}
		} else {
			if (Array.isArray(Config.saves.autosave)) {
				Config.saves.autosave = Config.saves.autosave.delete(safeLocationTag);
			} else {
				Config.saves.autosave = [];
			}
		}
	}
}
