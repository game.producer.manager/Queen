namespace App {
	/**
	 * For generating random treasure scenarios
	 */
	export class Loot {
		#prices : {[x: string]: number} = {};
		constructor() {
			//Init vars
			// Populate
			for (const prop in Data.Clothes) {
				this.#prices["CLOTHES/" + prop] = Items.CalculateBasePrice('CLOTHES', prop);
			}

			for (const prop in Data.Drugs) {
				this.#prices["DRUGS/" + prop] = Items.CalculateBasePrice('DRUGS', prop);
			}

			for (const prop in Data.Food) {
				this.#prices["FOOD/" + prop] = Items.CalculateBasePrice('FOOD', prop);
			}

			for (const prop in Data.Cosmetics) {
				this.#prices["COSMETICS/" + prop] = Items.CalculateBasePrice('COSMETICS', prop);
			}

			for (const prop in Data.Misc) {
				this.#prices["MISC_CONSUMABLE/" + prop] = Items.CalculateBasePrice('MISC_CONSUMABLE', prop);
			}

			for (const prop in Data.Slots) {
				this.#prices["REEL/" + prop] = Items.CalculateBasePrice('REEL', prop);
			}

		}

		get Prices() {return this.#prices;}


		/**
		 *
		 * @param Lists ['CLOTHES', 'DRUGS', 'FOOD', 'COSMETICS', 'MISC_CONSUMABLE', 'REEL' ]
		 * @param PoolMin Minimum loot pool
		 * @param PoolMax Maximum loot pool
		 * @param Filter Function to apply to find item
		 */
		GenerateStash(Lists: Data.ItemTypeStr[], PoolMin: number, PoolMax: number, Filter) {
			var pool = Math.max(PoolMin, Math.ceil(PoolMax * Math.random()));
			var loot: Record<string, number> = {};

			var i = 0;
			for (let item = this._FindItem(App.PR.GetRandomListItem(Lists), pool, Filter); item !== null && i < 100;
			item = this._FindItem(App.PR.GetRandomListItem(Lists), pool, Filter), ++i) { // safety
				if (item !== null) {
					//Add item to loot dictionary, or increment.
					if (loot.hasOwnProperty(item.record)) {
						loot[item.record] += 1;
					} else {
						loot[item.record] = 1;
					}
					pool -= item.price;
				}
				i++;
			}

			return loot;
		}
		/**
		 *
		 * @param Table array of objects defining loot drop chances
		 * @param PoolMin minimum amount of loot
		 * @param PoolMax maximum amount of loot
		 */
		GenerateLootBox(Table: Data.LootTableItem[], PoolMin: number, PoolMax: number): string {
			var buffer = "";
			const player = Entity.Player.instance;

			var pool = Math.ceil(PoolMin + (PoolMax - PoolMin) * Math.random());

			// Multiply loot value.
			if (player.HasHex("TREASURE_FINDER")) {
				pool = Math.ceil(pool * (player.VoodooEffects["TREASURE_FINDER"] as number));
				player.RemoveHex("TREASURE_FINDER");
			}

			var counter:number[] = [];
			var loot: Record<string, PoolItem> = {};
			var coins = 0; // cash converts

			var i = 0;
			var breakcount = 0;

			while (pool > 0 && breakcount < 100) { // Break out after 100 rolls no matter what.

				const obj = Table[i];

				if (Math.ceil(100 * Math.random()) <= obj.Chance) {
					//Handle money.
					if (obj.Type == 'COINS') {
						var tmpCoins = Math.max(obj.Min as number, Math.min(Math.ceil(obj.Max as number * Math.random()), pool));
						coins += tmpCoins;
						pool -= tmpCoins;
						if (pool <= 0) {
							break;
						} else {
							breakcount++;
							i++;
							i = i >= Table.length ? 0 : i;
							continue;
						}
					}

					if (counter[i] >= (obj.MaxCount ?? Number.MAX_SAFE_INTEGER)) {
						breakcount++;
						i++;
						i = i >= Table.length ? 0 : i;
						continue;
					}

					//Fetch an item instead.
					var tmpPool = (obj.hasOwnProperty("Max") ?
						Math.max(obj.Min as number, Math.min(Math.ceil(obj.Max as number * Math.random()), pool)) : pool);
					const item = this._FindItem(obj.Type, tmpPool, obj.Filter);

					if (item != null) {
						//Add item to loot dictionary, or increment.
						if (loot.hasOwnProperty(item.record)) {
							loot[item.record].Count += 1;
							counter[i]++;
						} else {
							loot[item.record] = {...item, Count: 1};
							loot[item.record].Count = 1;
							counter[i] = 1;
						}

						pool -= (obj.hasOwnProperty('Free') && obj.Free == true) ? 0 : item.price;
					}

					//increment counters
					breakcount++;
					i++;
					i = i >= Table.length ? 0 : i;
				}
			}

			var k = Object.keys(loot).sort((a, b) => loot[a].title.localeCompare(loot[b].title));
			if (coins == 0 && k.length < 1) return "<span style='color:red'>Poof! It's empty?!?!?</span>";

			for (i = 0; i < k.length; i++) {
				var obj = loot[k[i]];
				player.AddItem(obj.category as Data.ItemTypeStr, obj.tag, obj.Count);
			}

			if (coins) buffer += "You found <span class='item-money'>" + coins + " coins</span>!\n";
			var out = k.map(o => loot[o].title + (loot[o].Count > 1 ?
				"<span class='item-money'> x" + loot[o].Count + "</span>" : ""));
			buffer += out.join("\n");

			if (coins) player.earnMoney(coins, Entity.Income.Loot);

			return buffer;
		}

		/**
		 *
		 * @param Category Category to search.
		 * @param Pool amount of funds available
		 * @param Filter function to apply to result
		 */
		_FindItem(Category: Data.ItemTypeStr, Pool: number, Filter): FoundItem | null {
			var ds = App.Items.TryGetItemsDictionary(Category); // Create a data source.
			if (typeof Filter === 'function') ds = Filter(ds, Category, Pool); //optional filter.
			var that = this;
			var tags = Object.keys(ds).filter(k =>
				((typeof ds[k].InMarket !== 'undefined' && ds[k].InMarket != false) ||
					typeof ds[k].InMarket === 'undefined') &&
				setup.player.OwnsWardrobeItem(Category, k) != true && // no duplicate clothing
				that.Prices[Category + "/" + k] < Pool);

			if (tags.length < 1) return null;
			var t = App.PR.GetRandomListItem(tags);
			var record = Category + "/" + t;

			const desc = Category === 'REEL' ? ds[t].Name : ds[t]['ShortDesc'].replace(/{COLOR}/g, ds[t]['Color']); // legacy
			var item = {
				record: record, // CLOTHES/cheap wig
				price: this.Prices[record], // number
				tag: t, //cheap wig
				category: Category, // CLOTHES
				title: desc // blah blah a cheap wig
			};

			return item;

		}

		static get instance(): Loot {
			return setup.Loot;
		}

		/**
		 * Filter creation shortcut
		 * @param Test the test to apply to the object to
		 */
		static GetFilter(Test: (obj: object)=>boolean) {
			var f = function (a: object, _b: Data.ItemTypeStr, _c: number) {
				var o = {};
				for (var p in a)
					if (Test(a[p]))
						o[p] = a[p];
				return o;
			};

			return f;
		}

		/**
		 * Simple comparison on a property.
		 * @param A Object property to test
		 * @param b value to test == or includes for arrays
		 */
		static cmp(A: string, b: string | number |boolean) {
			var t = function (o) {return (o[A] == b) || (Array.isArray(o[A]) && o[A].includes(b));}
			return this.GetFilter(t);
		}
		/**
		 *
		 * @param A Object property to test
		 * @param b array of values to check
		 */
		static any(A: string, values: string[]) {
			var t = function (o: object) {
				const ov = o[A];
				return Array.isArray(ov) ? ov.includesAny(...values) : values.includes(ov);
			};

			return this.GetFilter(t);
		}

		TestLoot() {
			var Table = [
				{
					Type: "FOOD" as Data.ItemTypeStr,
					Chance: 50,
					Min: 200,
					Max: 500,
					Filter: App.Loot.any("Effects",
						["BUST_XP_COMMON", "BUST_XP_UNCOMMON", "BUST_XP_RARE", "BUST_XP_LEGENDARY"])
				}
			];


			return this.GenerateLootBox(Table, 100, 1000);
		}
	}

	interface FoundItem {
		record: string; // CLOTHES/cheap wig
		price: number; // number
		tag: string; //cheap wig
		category: string; // CLOTHES
		title: string; // blah blah a cheap wig
	}

	interface PoolItem extends FoundItem {
		Count: number;
	}
}
