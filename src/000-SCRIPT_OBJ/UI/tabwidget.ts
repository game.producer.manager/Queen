namespace App.UI.Widgets {
	export abstract class Tab {
		#id: string;
		#shortName: string;
		#longName: string;
		#buttonStyle?: string;

		constructor(id: string, shortName: string, longName: string, buttonStyle?: string) {
			this.#id = id;
			this.#shortName = shortName;
			this.#longName = longName;
			this.#buttonStyle = buttonStyle;
		}

		get id() {
			return this.#id;
		}

		get shortName() {
			return this.#shortName;
		}

		get longName() {
			return this.#longName;
		}

		get buttonStyle() {
			return this.#buttonStyle;
		}

		abstract render(): Node;
	}

	interface TabData {
		tab: Tab;
		tabButton: HTMLSpanElement;
		content: HTMLDivElement;
	}

	export class TabWidget {
		#tabs: TabData[] = [];
		#element: HTMLDivElement;
		#content: HTMLDivElement;
		#tabbar: HTMLDivElement;
		#activeTabCaption: HTMLSpanElement;
		#tabButtons: HTMLDivElement;
		#selectedTabId: string = "";

		constructor() {
			this.#element = document.createElement('div');
			this.#tabbar = appendNewElement('div', this.#element, undefined, ['tabbar']);
			this.#content = appendNewElement('div', this.#element);
			this.#activeTabCaption = appendNewElement('span', this.#tabbar, undefined, ['activeTabCaption']);
			this.#tabButtons = appendNewElement('div', this.#tabbar);
			this.#tabButtons.style.float = "right";
		}

		addTab(tab: Tab) {
			//this.#tabs.push(tab);
			const tabButton = appendNewElement('button', this.#tabButtons, tab.shortName, ['tablink']);
			if (tab.buttonStyle) {
				tabButton.classList.add(tab.buttonStyle);
			}
			tabButton.onclick = () => {
				this.selectTab(tab);
			}
			const content = document.createElement('div');
			content.append(tab.render());
			this.#tabs.push({tab: tab, tabButton: tabButton, content: content});
		}

		selectTab(tab: Tab) {
			const newTabIndex = this.#tabs.findIndex(t => t.tab.id == tab.id);
			if (newTabIndex < 0) {
				throw `Can't find tab with id '${tab.id}'`;
			}

			this.selectTabByIndex(newTabIndex);
		}

		selectTabByIndex(index: number) {
			const newTab = this.#tabs[index];

			const currentTab = this.#tabs.find((t) => t.tab.id == this.#selectedTabId);
			if (currentTab) {
				currentTab.tabButton.classList.remove('active');
			}

			this.#selectedTabId = newTab.tab.id;
			App.UI.replace(this.#content, newTab.content);
			App.UI.replace(this.#activeTabCaption, newTab.tab.longName);
			newTab.tabButton.classList.add('active');
		}

		refreshTab(tab: Tab) {
			const currentTab = this.#tabs.find((t) => t.tab.id == tab.id);
			App.UI.replace(currentTab.content, currentTab.tab.render());
		}

		get element() {
			return this.#element;
		}
	}
}
