namespace App.UI {
	export function pGameStats(player: Entity.Player): string {
		let res: string = '';
		const GS = player.GameStats;
		const totalMoneyEarned = Object.values(GS.MoneyEarned).reduce((a, c) => a + c, 0);
		if (totalMoneyEarned > 0) {
			res += `You managed to earn ${totalMoneyEarned} coins. Those include:<ul>`;
			const nameMapping: Record<Entity.Income, string> = {
				Betting: "betting",
				Gambling: "gambling",
				Jobs: "regular jobs",
				Loot: "looting",
				SexualJobs: "sexual jobs",
				Whoring: "whoring yourself",
				Unknown: "other incomes"
			};
			let incomes: {lbl: string, amount: number}[] = [];
			let k: Entity.Income;
			for (k in GS.MoneyEarned) {
				const amount = GS.MoneyEarned[k];
				if (amount > 0) {
					incomes.push({lbl: nameMapping[k], amount: amount});
				}
			}

			incomes.sort((a, b) => b.amount - a.amount);
			res += incomes.map(i => `<li>${i.amount} coins from ${i.lbl}.</li>`).join('');
			res += "</ul>";

			if (GS.TokensEarned > 0) {
				res += `You also earned ${GS.TokensEarned} courtesan tokens.<br>`;
			}

			res += "<hr>";

			// spendings
			const ms = GS.MoneySpendings;
			const totalShoppingSpend = Object.values(ms.Shopping).reduce((a, c) => a + c, 0);
			const totalOtherSpend = ms.Betting + ms.Gambling + ms.Jobs + ms.Quests;

			if (totalShoppingSpend + totalOtherSpend > 0) {
				res += `Your spendings so far are ${totalShoppingSpend + totalOtherSpend} coins. Those include:<ul>`;
				const spendingNameMapping: Record<Entity.Spending, string> = {
					Betting: "paid for bets",
					Gambling: "lost gambling",
					Jobs: "paid for services",
					Quests: "spent on mission",
					Shopping: "laid out at shops"
				};
				for (const sp of [Entity.Spending.Betting, Entity.Spending.Gambling, Entity.Spending.Jobs, Entity.Spending.Quests]) {
					if (ms[sp] > 0) {
						res += `<li>${ms[sp]} coins were ${spendingNameMapping[sp]}.</li>`;
					}
				}
				if (totalShoppingSpend > 0) {
					res += `<li>${totalShoppingSpend} were ${spendingNameMapping[Entity.Spending.Shopping]}.</li>`;
				}
				res += "</ul>";

				if (totalShoppingSpend > 0) { // shopping details
					const shoppingCategories: Record<Data.ItemType, string> = {
						CLOTHES: "clothing",
						COSMETICS: "cosmetics",
						DRUGS: "drugs",
						FOOD: "food",
						LOOT_BOX: "loot",
						MISC_CONSUMABLE: "miscellaneous",
						MISC_LOOT: "miscellaneous loot",
						QUEST: "important mission items",
						REEL: "whoring manuals (reels)",
						WEAPON: "weapons"
					};
					const shopSpendings: {lbl: string, amount: number}[] = [];
					let key: Data.ItemType;
					for (key in ms.Shopping) {
						if (ms.Shopping[key] > 0) {
							shopSpendings.push({lbl: shoppingCategories[key], amount: ms.Shopping[key]});
						}
					}
					shopSpendings.sort((a, b) => b.amount - a.amount);

					res += " Your shopping expenses are as follows: <ul>";
					res += shopSpendings.map(s => `<li>${s.amount} coins on ${s.lbl}.</li>`).join('');
					res += "</ul><hr/>";
				}
			}

			const SK = GS.Skills;
			let sexSkillsPrinted = false;

			if (SK.AssFucking && SK.AssFucking.Success + SK.AssFucking.Failure > 0) {
				res += `Your ass was used ${SK.AssFucking.Success + SK.AssFucking.Failure} times, `;
				if (SK.AssFucking.Success > 0) {
					res += `and ${SK.AssFucking.Success} times among them it gave a pleasure!`;
				} else {
					res += "but no one enjoyed that."
				}
				sexSkillsPrinted = true;
				res += "<br/>"
			}

			if (SK.BlowJobs && SK.BlowJobs.Success + SK.BlowJobs.Failure > 0) {
				res += `You've sucked ${SK.BlowJobs.Success + SK.BlowJobs.Failure} dicks, `;
				if (SK.BlowJobs.Success > 0) {
					res += `and satisfied ${SK.BlowJobs.Success} of them!`;
				} else {
					res += "but each time time you've failed to give a pleasure."
				}
				sexSkillsPrinted = true;
				res += "<br/>"
			}

			if (SK.HandJobs && SK.HandJobs.Success + SK.HandJobs.Failure > 0) {
				res += `${SK.HandJobs.Success + SK.HandJobs.Failure} times you've used your hands to make men happy, `;
				if (SK.HandJobs.Success > 0) {
					res += `and ${SK.HandJobs.Success} times it worked quite well!`;
				} else {
					res += "yet nobody enjoyed that."
				}
				sexSkillsPrinted = true;
				res += "<br/>"
			}

			if (SK.TitFucking && SK.TitFucking.Success + SK.TitFucking.Failure > 0) {
				res += `Your tits were fucked ${SK.TitFucking.Success + SK.TitFucking.Failure} times, `;
				if (SK.TitFucking.Success > 0) {
					res += `and ${SK.TitFucking.Success} dicks enjoyed that!`;
				} else {
					res += "and it was nothing but waste of time."
				}
				sexSkillsPrinted = true;
				res += "<br/>"
			}

			if (sexSkillsPrinted) {
				res += "<hr/>";
			}

			const coffin = GS.COFFIN;
			if (coffin.Played > 0) {
				res += `You've played Coffin Dice ${coffin.Played} times.<br>`;
				res += `Matches won: ${coffin.Won}<br>Matches lost: ${coffin.Lost}<br>Matches drawn: ${coffin.Draw}<br>`;
				if (coffin.CoinsWon > 0) {
					res += `You've won ${coffin.CoinsWon} coins`;
					if (coffin.CoinsLost > 0) {
						res += ` and lost ${coffin.CoinsLost} for a net earnings of ${coffin.CoinsWon - coffin.CoinsLost}.<br>`;
					} else {
						res += '.<br>';
					}
				} else if (coffin.CoinsLost > 0) {
					res += `You've lost ${coffin.CoinsLost} coins.<br>`;
				}

				if (coffin.ItemsWon > 0) {
					res += `You've won ${coffin.ItemsWon} items, for an estimated value of ${coffin.ItemsWonValue} coins.<br>`;
				}

				if (coffin.SexPaid > 0) {
					res += `You used your body to pay ${coffin.SexPaid} coins worth of debts.<br>`;
				}
				res += "<hr/>";
			}
		}
		return res;
	}

	export type PassageRef = string | [string, string];

	function passageLinkStrip(passages: PassageRef[], prefix: string, className: string): string {
		if (passages.length === 0) {
			return '';
		}

		return `@@.${className};${prefix}@@: ` + passages.map(p => {
			if (typeof p === 'string') {
				return  p.startsWith('[[') || p.startsWith('<') ? p : `[[${p}]]`; // works for "caption|passage_name" too
			}
			return p[1] === null ? `@@.state-disabled;[${p[0]}]@@` : `[[${p[0]}|${p[1]}]]`; // disabled links are passed in as ["text", null]
		}).join('&thinsp;|&thinsp;');
	}

	export function actionLinkStrip(passages: PassageRef[]): string {
		return passageLinkStrip(passages, 'Action', 'action-general');
	}

	export function pInteractLinkStrip(passages?: PassageRef[], exitCaption = 'Exit'): string {
		let links: PassageRef[] = [];
		if (exitCaption !== null) {
			const gbm = State.variables.GameBookmark;
			if (!_.isEmpty(gbm) && gbm !== passage()) {
				links.push(`[[${exitCaption}|${gbm}]]`);
			}
		}
		if (passages && passages.length) {
			links = links.concat(passages);
		}
		return passageLinkStrip(links, 'Interact', 'action-interact');
	}

	export function pTravelLinkStrip(passages: PassageRef[], caption = "Travel"): string {
		return passageLinkStrip(passages, caption, 'action-travel');
	}

	function isLockedDestinationWithNote(d: Data.Travel.Destination): d is Data.Travel.LockedDestinationWithNote {
		return d.hasOwnProperty('note');
	}

	function resolveDynamicText(player: Entity.Player): undefined;
	function resolveDynamicText(player: Entity.Player, text: Data.Travel.DynamicText): string;
	function resolveDynamicText(player: Entity.Player, text?: Data.Travel.DynamicText): string|undefined {
		if (text === undefined) {
			return undefined;
		}
		return typeof text === "string" ? text : PR.TokenizeString(player, undefined, text(player));
	}

	export function wCustomMenuLink(text: string, action?: string | Entity.NPC | App.Job, passageName?: string): HTMLAnchorElement {
		const res = document.createElement("a");
		res.classList.add("link-internal");
		const targetPassage = passageName ?? text;
		res.append(text);
		res.onclick = () => {
			if (action) {
				State.variables.MenuAction = action;
			}
			if (!tags().includes("custom-menu")) {
				State.variables.GameBookmark = passage();
			}
			Engine.play(targetPassage);
		}
		return res;
	}

	export function wTravels(player: Entity.Player, destinations: Data.Travel.Destinations) {
		let noteDiv: HTMLDivElement | null = null;
		const res = document.createElement("div");
		const travels = appendNewElement("div", res, undefined, ['action-row']);
		const divLabel = appendNewElement("div", travels, undefined, ['action-caption']);
		appendNewElement("span", divLabel, "Travel", ['action-travel']);
		const listDiv = appendNewElement("div", travels, undefined, ['action-links']);
		const links = appendNewElement("ul", listDiv, undefined, ['choices-strip']);
		for (const d of destinations) {
			const link = document.createElement("li");
			if (typeof d === "string") {
				link.append(passageLink(resolveDynamicText(player, Data.Travel.passageDisplayNames[d]) ?? d, d));
				links.append(link);
			} else {
				const available = d.available ? d.available(player) : true;
				if (!available) {
					continue;
				}

				if (isLockedDestinationWithNote(d)) {
					if (!noteDiv) {
						noteDiv = appendNewElement("div", res);
						noteDiv.id = "travel-locked-notification";
					}
					const anchor = appendNewElement("a", link, resolveDynamicText(player, d.text));
					anchor.onclick = () => {
						UI.replace(noteDiv!, resolveDynamicText(player, d.note));
					};
				} else {
					const destPasage = typeof d.destination === "string" ? d.destination : d.destination(player);
					const enabled = d.enabled ? d.enabled(player) : true;
					const dt = d.text;
					const text = dt ? resolveDynamicText(player, dt) : resolveDynamicText(player, Data.Travel.passageDisplayNames[destPasage]) ?? destPasage;
					if (enabled) {
						link.append(passageLink(text, destPasage));
					} else {
						appendNewElement("span", link, text, ['state-disabled']);
					}
				}
				links.append(link);
			}
		}

		// late night teleport
		if (player.Phase >= 4 && !tags().includes("no-teleport")) {
			const lnt = appendNewElement("div", res, " It's getting kind of late… ");
			lnt.append(passageLink("head back to the Mermaid", "Cabin"));
			lnt.style.marginLeft = "0.5em";
		}

		return res;
	}

	export function wNPC(player: Entity.Player, npcTag: string, custom?: PassageRef[]) {
		const npc = player.GetNPC(npcTag);
		const res = makeElement("div", undefined, ["npc-interact"]);
		$(res).wiki(PR.TokenizeString(player, npc, npc.ShortDesc()));
		const npcMenu = appendNewElement("div", res,)
		appendNewElement("span", npcMenu, "NPC Menu", ["npc-menu"]);
		npcMenu.append(": ");

		const npcLinks = appendNewElement("ul", npcMenu, undefined, ['choices-strip'])
		appendNewElement("li", npcLinks).append(wCustomMenuLink("Examine", npcTag, "ExamineNPC"));

		if (setup.jobEngine.HasJobs(player, npcTag)) {
			const liJobs = appendNewElement("li", npcLinks);
			liJobs.append(wCustomMenuLink("Jobs", npcTag));
			if (setup.jobEngine.JobsAvailable(player, npcTag)) {
				appendNewElement("span", liJobs, " (!)", ["task-available"]);
			}
		}
		if (App.Quest.List("any", player, npcTag).length > 0) {
			const liQuests = appendNewElement("li", npcLinks);
			liQuests.append(wCustomMenuLink("Quests", npcTag));
			if (App.Quest.List("available", player, npcTag).length > 0) {
				appendNewElement("span", liQuests, " (!)", ["task-available"]);
			}
			if (App.Quest.List("cancomplete", player, npcTag).length > 0) {
				appendNewElement("span", liQuests, " (!)", ["task-cancomplete"]);
			}
		}

		if (App.StoreEngine.HasStore(npc)) {
			const liShop = appendNewElement("li", npcLinks);
			if (App.StoreEngine.IsOpen(player, npc)) {
				liShop.append(wCustomMenuLink("Shop", npc));
			} else {
				appendNewElement("span", liShop, "Shop", ['state-disabled']);
			}
		}

		if (custom) {
			for (const pr of custom) {
				if (typeof pr === "string") {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr, npcTag, pr));
				} else {
					appendNewElement("li", npcLinks).append(wCustomMenuLink(pr[0], npcTag, pr[1]));
				}
			}
		}
		return res;
	}

	export function wLocationJobs(player: Entity.Player, location: string, brief = false): HTMLDivElement | null {
		const jobList = setup.jobEngine.GetAvailableLocationJobs(player, location);
		if (!jobList.length) {
			return null;
		}
		const res = document.createElement("div");
		if (!brief) {
			appendNewElement("span", res, "Action", ["action-general"]);
			res.append(": ");
		}
		const links = appendNewElement("ul", res, undefined, ['choices-strip'])
		for (const j of jobList) {
			const li = appendNewElement("li", links);
			if (j.Ready(player)) {
				li.append(wCustomMenuLink(j.Title(true), j, "SelfJobs"));
			} else {
				appendNewElement("span", li, j.Title(true), ["state-disabled"]);
			}
			$(li).wiki(j.RatingAndCosts());
		}
		return res;
	}

	export function pJobResults(player: Entity.Player, NPC: Entity.NPC, job: Task): string {
		const start = job.PrintStart(player, NPC);
		const scenes = job.PlayScenes(player, NPC);
		const end = job.PrintEnd(player, NPC);

		let res: string[] = [];
		if (start.length) {
			res.push(start);
		}
		for (const s of scenes) {
			res.push(s.Print());
		}
		if (end.length) {
			res.push(end);
		}

		const results = PR.pTaskRewards(job);
		if (results.length) {
			res.push("@@color:cyan;Your receive some items:@@");
			res = res.concat(results);
		}

		job.CompleteScenes(setup.player);
		return res.join('<br/>');
	}

	export function rBodyScore(player: Entity.Player): HTMLTableElement {
		const res = makeElement("table", undefined, ["body-stats"]);
		res.style.width = "100%";

		const tbody = appendNewElement("tbody", res);

		const statsOrder: BodyStatStr[] = ["Height", "Hair", "Face", "Lips",
			"Bust", "Lactation", "Waist", "Hips", "Ass", "Penis", "Balls"];
		const shownIfPresent: Set<BodyStatStr> = new Set(["Bust", "Lactation", "Penis", "Balls"]);
		const invertedStats:BodyStatStr[] = ["Waist"]

		for (const st of statsOrder) {
			if (player.BodyStats[st] > 0 || !shownIfPresent.has(st)) {
				const tr = appendNewElement("tr", tbody);
				appendNewElement("td", tr, `${st}:`);
				const meter = appendNewElement("td", tr);
				meter.id = st;
				meter.append(rBodyMeter(st, player, invertedStats.includes(st)));
				meter.append(Wikifier.wikifyEval(PR.TokenizeString(player, undefined, ' p' + st.toUpperCase())));
			}
		}
		return res;
	}

	export function rGameScore(): DocumentFragment {
		const res = new DocumentFragment();

		const player = setup.player;

		const slaveName = appendNewElement("div", res, player.SlaveName, ['state-feminity']);

		const dayDiv = appendNewElement("div", res, `Day ${player.Day}, `);
		dayDiv.append(Wikifier.wikifyEval(`${player.GetPhase(false)} ${player.phaseIconStrip()}`));

		const coins = appendNewElement("div", res, "Coins: ", ['item-money']);
		appendNewElement("span", coins, `${player.Money}`).id = "Money";

		if (player.Skills.Courtesan) {
			const tokens = appendNewElement("div", res, "Tokens: ", ['state-sexiness']);
			appendNewElement("span", tokens, `${player.Tokens}`).id = "Tokens";
		}

		const linkHandler = () => {
			const passageObj = Story.get(passage());
			if (!passageObj.tags.contains("custom-menu")) {
				State.variables.GameBookmark = passage();
			}
		};

		const links = generateLinksStrip([
			passageLink("Journal", "Journal", linkHandler),
			passageLink("Skills", "Skills", linkHandler),
			passageLink("Inventory", "Inventory", linkHandler),
		]);

		const linksDiv = appendNewElement("div", res);
		linksDiv.style.textAlign = "center";
		linksDiv.append(links);

		const scoreTable = appendNewElement("table", res);
		scoreTable.style.width = "100%";
		scoreTable.style.marginTop = "0.5ex";
		const scoreTableBody = appendNewElement("tbody", scoreTable);

		const statsOrder: CoreStat[] = [
			CoreStat.Health, CoreStat.Energy, CoreStat.WillPower, CoreStat.Perversion, CoreStat.Nutrition, CoreStat.Femininity
		];

		for (const st of statsOrder) {
			const tr = appendNewElement("tr", scoreTableBody);
			appendNewElement("td", tr, `${st}:`);
			const meterTd = appendNewElement("td", tr);
			meterTd.id = st;
			meterTd.append(rCoreStatMeter(st, setup.player));
		}
		// Toxicity is inverted
		let tr = appendNewElement("tr", scoreTableBody);
		appendNewElement("td", tr, "Toxicity:");
		let meterTd = appendNewElement("td", tr);
		meterTd.id = "Toxicity";
		meterTd.append(rCoreStatMeter(CoreStat.Toxicity, setup.player, true));

		// hormones meter needs a special symbol
		tr = appendNewElement("tr", scoreTableBody);
		appendNewElement("td", tr, "Hormones:");
		meterTd = appendNewElement("td", tr);
		meterTd.id = "Hormones";
		meterTd.append(rCoreStatMeter(CoreStat.Hormones, setup.player));
		meterTd.innerHTML += PR.pHormoneSymbol();

		// update avatar if empty
		if (settings.displayAvatar && document.getElementById("avatarFace")?.children.length === 0) {
			App.Avatar._DrawPortrait();
		}

		// update body score
		if (settings.displayBodyScore) {
			const container = $("#bodyScoreContainer");
			container.empty();
			container.append(rBodyScore(setup.player));
		}

		return res;
	}
}
