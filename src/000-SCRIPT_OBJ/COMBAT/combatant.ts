namespace App.Combat {

	export type MyStatusCallback = (combatant: Combatant) => void;
	export type TheirStatusCallback = (combatant: Combatant) => void;
	export type ChatLogCallback = (message: string, combatabt: Combatant) => void;

	/**
	 * Base combatant class, used by NPC's and inherited by the player class.
	 */
	export class Combatant {
		#data: Data.Combat.Enemy;
		#name: string;
		#title: string;
		#portrait: string;

		#engine: Engines.Generic;
		//Number of turns taken
		#turn = 1;
		//Current delay due to last attack speed
		#weaponDelay = 0;

		#id: string;
		//Combo meter - hidden on enemies
		#combo = 0;

		//Is this object alive or dead?
		#dead = false;

		// FIXME replace with static type App.Combat.Effect
		#allowedEffects = [
			'STUNNED', 'BLINDED', 'GUARDED', 'DODGING', 'BLOODTHIRST', 'SEEKING', 'LIFE LEECH', 'PARRY'
		]

		#effects: Partial<Record<Effect, number>> = {};

		#myStatus: MyStatusCallback;
		#chatLog: ChatLogCallback;

		constructor(data: Data.Combat.Enemy, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			this.#data = clone(data); // don't mangle our dictionary
			//Naming
			if (this.#data.Name.includes('RANDOM_MALE_NAME')) {
				this.#name = this.#data.Name.replace(/RANDOM_MALE_NAME/g, App.PR.GetRandomListItem(App.Data.Names.Male));
			} else if (this.#data.Name.includes('RANDOM_FEMALE_NAME')) {
				this.#name = this.#data.Name.replace(/RANDOM_FEMALE_NAME/g, App.PR.GetRandomListItem(App.Data.Names.Female));
			} else {
				this.#name = this.#data.Name;
			}
			this.#title = this.#data.Title.replace(/NAME/g, this.#name);

			this.#portrait = App.PR.GetRandomListItem(this.#data.Portraits ?? ['pugilist_a']);

			this.ChangeMoveSet(App.Combat.Moves[this.#data.Moves].Engine ?? App.Combat.Engines.Generic, myStatusCB, theirStatusCB, chatLogCB);
		}

		get Name() {return this.#name;}
		get Portrait() {return this.#portrait;}
		get Id() {return this.#id;}
		set Id(n) {this.#id = n;}
		get Title() {return this.#title;}
		get Health() {return this.#data.Health;}
		get MaxHealth() {return this.#data.MaxHealth;}
		get MaxEnergy() {return 10;} // Hardcoded - same as player max energy.
		get Energy() {return this.#data.Energy;}
		get MaxCombo() {return 10;} // Hardcoded
		get Combo() {return this.#combo;}
		//get Skill() {return this.#data.Skill;}

		get Bust() {
			return this.#data.Bust ?? this.Attack;
		}

		get Ass() {
			return this.#data.Ass ?? this.Attack;
		}

		get Engine() {return this.#engine;}
		get Moves() {return App.Combat.Moves[this.Engine.Class].moves;}
		/** @deprecated by definition, type fields may not exist */
		get IsNPC() {return true;}
		get IsDead() {return this.#dead;};
		get MaxStamina() {return this.#data.MaxStamina;}
		get Stamina() {return this.#data.Stamina;}
		get Speed() {return this.#data.Speed;}
		get WeaponDelay() {return this.#weaponDelay;};
		get Turn() {return this.#turn;}
		get Gender() {return this.#data.Gender;}
		get Attack() {return this.#data.Attack;}
		get Defense() {return this.#data.Defense;}
		get AllowedEffects() {return this.#allowedEffects;}
		get Effects() {return this.#effects;}

		HasEffect(e: Combat.Effect) {return this.#effects.hasOwnProperty(e);}

		/**
		 * Add a valid effect
		 */
		AddEffect(e: Effect, d: number) {
			if (this.AllowedEffects.includes(e) == true) {
				this.#effects[e] = Math.abs(this.#effects[e] ?? 0) + d;
			}
		}

		/**
		 * Reduce an effect
		 */
		ReduceEffect(e: Effect, d: number) {
			if (this.#effects.hasOwnProperty(e)) {
				this.#effects[e] = (this.#effects[e] ?? 0) - d;
				if (this.#effects[e] as number <= 0) {
					delete this.#effects[e];
				}
			}
		}

		TakeDamage(n: number) {
			this.#data.Health -= n;
			if (this.#data.Health <= 0) {
				this.#dead = true;
				this.#chatLog("NPC_NAME is defeated!", this);
			}
			this.#myStatus(this);
		}

		RecoverHealth(n: number) {
			this.#data.Health += n;
			if (this.#data.Health >= this.MaxHealth) this.#data.Health = this.MaxHealth;
			this.#myStatus(this);
		}

		UseEnergy(n: number) {
			this.#data.Energy -= n;
			this.#myStatus(this);
		}

		RecoverEnergy(n: number) {
			this.#data.Energy += n;
			if (this.#data.Energy >= this.MaxEnergy) this.#data.Energy = this.MaxEnergy;
			this.#myStatus(this);
		}

		UseStamina(n: number) {
			this.#data.Stamina -= n;
			this.#myStatus(this);
		}

		RecoverStamina(n: number) {
			this.#data.Stamina += n;
			if (this.#data.Stamina >= this.MaxStamina) this.#data.Stamina = this.MaxStamina;
			this.#myStatus(this);
		}

		UseCombo(n: number) {
			this.#combo -= n;
			this.#myStatus(this);
		}

		RecoverCombo(n: number) {
			this.#combo += n;
			if (this.#combo >= this.MaxCombo) this.#combo = this.MaxCombo;
			this.#myStatus(this);
		}

		/**
		 * Call this after using attack, but before ending turn.
		 */
		AddWeaponDelay(n: number) {
			this.#weaponDelay += n;
		}

		StartTurn() {
			this.#weaponDelay = 0;
		}

		EndTurn() {
			this.#turn += 1;
			for (var k in this.Effects) {
				this.ReduceEffect(k as Effect, 1);
			}
		}


		/**
		 * Get the speed for the entity at the current turn, or optional turn in the future
		 * @param n The number of turns to predict.
		 */
		GetTimeline(n: number) {
			var arr: number[] = [];
			for (var i = 1; i <= n; i++) {
				var Time = (this.Speed * (this.Turn + i)) + this.WeaponDelay;
				arr.push(Time);
			}

			return arr;
		}

		/**
		 *
		 * @param Mine My skill value
		 * @param Difficulty to check against
		 * @param Mod bonus / penalty to roll
		 * @return Floating point between 0.1 and 2.0 indicating value of roll.
		 */
		SkillRoll(Mine: number, Difficulty: number, Mod: number) {

			var targetRoll = (100 - Math.max(5, Math.min((50 + (Mine - Difficulty)), 95)));
			var diceRoll = Math.floor(Math.random() * 100) + Mod;

			return Math.max(0.1, Math.min((diceRoll / targetRoll), 2.0)); // Default same as player object
		}

		/**
		 * Simulate an attack roll.
		 * @returns roll - floating point between 0.1 and 2.0
		 */
		AttackRoll() {
			var attack = this.Attack;
			var Mod = 0;

			if (this.HasEffect("BLINDED")) Mod -= 30;
			if (this.HasEffect("SEEKING")) Mod += 30;

			//TODO: Put some checks here to get values from effects
			return this.SkillRoll(attack, 100, Mod);
		}
		/**
		 * Simulate a defensive roll.
		 * @returns roll - floating point between 0.1 and 2.0
		 */
		DefenseRoll() {
			var def = this.Defense;
			var Mod = 0;

			if (this.HasEffect("STUNNED")) Mod -= 30;
			if (this.HasEffect("DODGING")) Mod += 30;

			//TODO: Put some checks here to get values from effects
			return this.SkillRoll(def, 100, Mod);
		}

		/**
		 * Generally only used by players to switch their attack style on the fly.
		 * Called from CombatEngine
		 * @param Engine reference to personal engine class
		 * @param myStatusCB callback to update my status elements in ui
		 * @param theirStatusCB callback to update enemy status elements in ui
		 * @param chatLogCB callback to update chat log in ui
		 */
		ChangeMoveSet(Engine: Engines.EngineConstructor, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			//Callbacks
			this.#myStatus = myStatusCB;
			this.#chatLog = chatLogCB;
			//Moveset and personal combat engine
			this.#engine = new Engine(this, myStatusCB, theirStatusCB, chatLogCB);
		}

		DoAI(Target: Combatant, chatLogCB: ChatLogCallback) {
			if (this.HasEffect("STUNNED")) {
				chatLogCB("NPC_NAME looks dazed and fumbles around.", this);
				return;
			}

			if (this.Stamina < 10) {
				if (this.Energy > 0) {
					this.Engine.Recover();
				} else {
					this.Engine.Defend();
				}
				return;
			}

			//Call Enginef or Specific routine.
			this.Engine.DoAI(Target);
		}

		protected die() {
			this.#dead = true;
		}
	}

	type LooseHandler = (player: Entity.Player) => void;
	type WinHandler = (player: Entity.Player) => void;

	/**
	 * Player combatant class. Just a few tweaks from the base.
	 */
	export class Player extends Combatant {
		#player: Entity.Player;
		#loseHandler: LooseHandler;
		#winHandler: WinHandler;

		/**
		 *
		 * @param {App.Entity.Player} Player
		 * @param {object} data
		 * @param {function} myStatusCB
		 * @param {function} theirStatusCB
		 * @param {function} chatLogCB
		 * @param {function} loseHandler
		 * @param {function} winHandler
		 */
		constructor(Player: Entity.Player, data: Data.Combat.Enemy, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback, loseHandler: LooseHandler, winHandler: WinHandler) {
			super(data, myStatusCB, theirStatusCB, chatLogCB);
			this.#player = Player;
			this.#loseHandler = loseHandler;
			this.#winHandler = winHandler;
		}

		get Name() {return this.#player.SlaveName;}
		get Player() {return this.#player;}
		get IsNPC() {return false;}

		get AvailableMoveset() {
			var a = {"UNARMED": "Unarmed"};

			if (this.#player.IsEquipped('Weapon', true) == true) a["SWASHBUCKLING"] = "Swashbuckling";
			if (this.#player.GetStat('SKILL', 'BoobJitsu') > 0) a["BOOBJITSU"] = "Boob-Jitsu";
			if (this.#player.GetStat('SKILL', 'AssFu') > 0) a["ASSFU"] = "Ass-Fu";

			return a;
		}

		// Player attack depends on what skill they are using.
		get Attack() {
			var val = 0;

			switch (this.Engine.Class) {
				case 'SWASHBUCKLING':
					val = this.Player.GetStat('SKILL', 'Swashbuckling');
					val += this.Player.RollBonus('SKILL', 'Swashbuckling');
					break;
				case 'BOOBJITSU':
					val = this.Player.GetStat('SKILL', 'BoobJitsu');
					val += this.Player.RollBonus('SKILL', 'BoobJitsu');
					break;
				case 'ASSFU':
					val = this.Player.GetStat('SKILL', 'AssFu');
					val += this.Player.RollBonus('SKILL', 'AssFu');
					break;
				default:
					val = 20;
					val += Math.floor(this.Player.GetStat('STAT', 'Fitness') * 0.8); // Cap it at 80% of fitness.
					// no roll bonus for unarmed attacks
					break;
			}

			return val;
		}

		get Defense() {
			var val = 0;

			switch (this.Engine.Class) {
				case 'SWASHBUCKLING':
					val = this.Player.GetStat('SKILL', 'Swashbuckling');
					// Technicaly a private method… but whatever javascript
					val += this.Player.RollBonus('SKILL', 'Swashbuckling');
					break;
				case 'BOOBJITSU':
					val = this.Player.GetStat('SKILL', 'BoobJitsu');
					val += this.Player.RollBonus('SKILL', 'BoobJitsu');
					break;
				case 'ASSFU':
					val = this.Player.GetStat('SKILL', 'AssFu');
					val += this.Player.RollBonus('SKILL', 'AssFu');
					break;
				default:
					val = 20;
					val += Math.floor(this.Player.GetStat('STAT', 'Fitness') * 0.4); // Cap it at 40% of fitness.
					val += Math.floor(this.Player.GetStat('SKILL', 'Dancing') * 0.4); // Cap it at 40% of dancing skill.
					// no roll bonus for unarmed attacks
					break;
			}

			return val;
		}


		// Grant xp to players
		AttackRoll() {
			var mod = super.AttackRoll();
			this.GrantXP(Math.floor(10 * mod));
			return mod;
		}

		DefenseRoll() {
			var mod = super.DefenseRoll();
			this.GrantXP(Math.floor(10 * mod));
			return mod;
		}

		GrantXP(xp: number) {
			switch (this.Engine.Class) {
				case 'SWASHBUCKLING':
					this.Player.AdjustSkillXP('Swashbuckling', xp);
					break;
				case 'BOOBJITSU':
					this.Player.AdjustSkillXP('BoobJitsu', xp);
					break;
				case 'ASSFU':
					this.Player.AdjustSkillXP('AssFu', xp);
					break;
				default:
					this.Player.AdjustStatXP('Fitness', Math.floor(xp / 2));
					this.Player.AdjustSkillXP('Dancing', Math.floor(xp / 2));
					break;
			}
		}

		UseEnergy(n: number) {
			var x = Math.abs(n) * -1; // Always reduce
			this.Player.AdjustStat('Energy', x);
			App.PR.RefreshTwineMeter(CoreStat.Energy);
		}

		RecoverEnergy(n: number) {
			this.Player.AdjustStat('Energy', n);
			App.PR.RefreshTwineMeter(CoreStat.Energy);
		}

		TakeDamage(n: number) {
			var x = Math.abs(n) * -1; // Always reduce
			this.Player.AdjustStat('Health', x);
			App.PR.RefreshTwineMeter(CoreStat.Health);
			// Test for loss -
			if (this.Player.GetStat('STAT', 'Health') <= 0) {
				this.#loseHandler(this.Player);
				this.die();
			}
		}

		RecoverHealth(n: number) {
			this.Player.AdjustStat('Health', n);
			App.PR.RefreshTwineMeter(CoreStat.Health);
		}

		IsEquipped(Slot: Data.ClothingSlot) {
			return this.Player.IsEquipped(Slot, true);
		}

		/**
		 * Attempt to figure out the quality of an equipped weapon.
		 * @returns value 1 through 5.
		 */
		GetWeaponQuality() {
			if (this.IsEquipped('Weapon') == false) return 1;
			var o = this.Player.GetEquipmentInSlot('Weapon');
			if (!o || typeof o === 'undefined' || o == null) return 1;

			if (o.Data.Style == 'COMMON') {
				return 3;
			} else if (o.Data.Style == 'UNCOMMON') {
				return 4;
			} else if (o.Data.Style == 'RARE') {
				return 5;
			} else if (o.Data.Style == 'LEGENDARY') {
				return 6;
			} else {
				return 1;
			}
		}

		GetWeaponBonus() {
			return this.Player.GetWornSkillBonus("Sharpness");
		}

		get Bust() {return this.Player.GetStat('BODY', 'Bust');}

		get Ass() {return this.Player.GetStat('BODY', 'Ass');}
	}
}
