namespace App.Combat.Engines {
	// Generic engine with default behavior
	export class Generic {
		#owner: Combatant;
		#myStatusCB: MyStatusCallback;
		#theirStatusCB: TheirStatusCallback;
		#chatLogCB: ChatLogCallback;
		#attackHistory: string[] = [];

		constructor(owner: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback) {
			this.#owner = owner; // The object that owns this engine.
			this.#myStatusCB = myStatusCB; // Callback for updating a html component
			this.#theirStatusCB = theirStatusCB; // Callback for updating a html component
			this.#chatLogCB = chatLogCB; // Callback for updating an html component

			if (typeof this.#owner === 'undefined') throw new Error("You must associate this engine with an owner.");
		}

		get Class() {return "GENERIC";}

		get Owner() {return this.#owner}

		get LastMove() {
			return this.#attackHistory.length >= 1 ? this.#attackHistory[this.#attackHistory.length - 1] : null;
		}

		/**
		 * Attack the enemy
		 */
		AttackTarget(Target: Combatant, Command: Data.Combat.Move) {
			var roll = this.CalculateHit(Target);

			// Try to hit target
			if (roll > 0) {
				this.Owner.RecoverCombo(this.GenerateCombo(Target, Command, roll));
				this.#attackHistory.push(Command.Name);
				this.ConsumeResources(Command);
				this.ApplyEffects(Target, Command, roll);
				this.DoDamage(Target, Command, roll);
				return true;
			} else {
				this.#attackHistory.push("Miss");
				this.ConsumeResources(Command);
				var Message = this.GetMissMessage(Command.Miss);
				this.PrintMessage(Message, Target);
				return false;
			}
		}

		Recover() {
			// use energy for stamina
			this.Owner.UseEnergy(1);
			this.Owner.RecoverStamina(100);
			this.Owner.AddWeaponDelay(10);
			if (this.Owner.IsNPC == true) {
				this.PrintMessage("<span style='color:lime'>NPC_NAME catches a second wind!</span>", this.Owner);
			} else {
				this.PrintMessage("<span style='color:lime'>You pull deep from your reserves and catch a second wind!</span>", this.Owner);
			}
		}
		Defend() {
			this.Owner.RecoverStamina(10); // Regain some stamina
			this.Owner.AddEffect('GUARDED', 2);
			this.Owner.AddWeaponDelay(10);
			if (this.Owner.IsNPC == true) {
				this.PrintMessage("<span style='color:lime'>NPC_NAME assumes a defensive position.</span>", this.Owner);
			} else {
				this.PrintMessage("<span style='color:lime'>You assume a defensive position and catch your breath.</span>", this.Owner);
			}
		}

		DoAI(_Target) {
			if (this.Owner.IsNPC == true) {
				this.PrintMessage("<span style='color:red'>*BUG*</span> NPC_NAME doesn't know what to do!", this.Owner);
			} else {
				this.PrintMessage("<span style='red'>*BUG* NO AI routine implemented for player attack.", this.Owner);
			}
		}

		ConsumeResources(Command: Data.Combat.Move) {
			this.Owner.UseStamina(Command.Stamina);
			this.Owner.UseCombo(Command.Combo);
			this.Owner.AddWeaponDelay(Command.Speed);
		}

		CheckCommand(Command: Data.Combat.Move) {
			if (Command.Unlock(this.Owner) == false) return false;
			if (Command.Stamina > this.Owner.Stamina) return false;
			if (Command.Combo > this.Owner.Combo) return false;

			return true;
		}

		/**
		 * Calculate if an attack hits.
		 * @param Target  Entity that you are attacking
		 */
		CalculateHit(Target: Combatant) {
			const MyRoll = this.Owner.AttackRoll(); //Includes getting attack buffs
			const TheirRoll = Target.DefenseRoll(); //Includes getting defense buffs
			return (MyRoll - TheirRoll);
		}

		/**
		* @param {string} Message Miss array from attack definition.
		* @param {App.Combat.Combatant|App.Combat.Player} Target  Object we are attacking.
		*/
		PrintMessage(Message: string, Target: Combatant) {
			if (typeof this.#chatLogCB === 'function') {
				if (this.Owner.IsNPC == true) {
					this.#chatLogCB(Message, this.Owner);
				} else {
					this.#chatLogCB(Message, Target);
				}
			}
		}

		DoDamage(Target: Combatant, Command: Data.Combat.Move, roll: number) {
			var dmg = this.CalculateDamage(Target, Command, roll);
			// Apply effect bonuses
			if (this.Owner.HasEffect('BLOODTHIRST')) dmg = Math.ceil(dmg * 1.5);
			if (Target.HasEffect('GUARDED')) dmg = Math.floor(dmg * 0.7);

			if (Target.HasEffect('PARRY')) {
				dmg = 0; // block all damage.
				Target.ReduceEffect('PARRY', 1); // Reduce parry counter.
				this.#attackHistory.push("Miss"); // We missed. Sadface.
				if (this.Owner.IsNPC == true) {
					this.PrintMessage("You parry NPC_NAME's attack!", Target);
				} else {
					this.PrintMessage("NPC_NAME parries your attack!", Target);
				}
			} else {

				if (Target instanceof Combat.Player) {
					var dmgMod = Math.min(Target.Player.GetWornSkillBonus("Damage Resistance"), 90) / 100; //capped
					var dmg = (dmg - Math.ceil(dmg * dmgMod));
				}

				this.PrintHit(Command.Hit, Target, roll, dmg);
				Target.TakeDamage(dmg);

			}

			if (this.Owner.HasEffect('LIFE LEECH')) {
				var heal = Math.ceil(dmg * 0.5);
				this.Owner.RecoverHealth(heal);
				if (this.Owner.IsNPC == true) {
					this.PrintMessage("NPC_NAME heals <span style='color:lime'>" + heal + "</span> damage.", Target);
				} else {
					this.PrintMessage("You heal <span style='color:lime'>" + heal + "</span> damage.", Target);
				}
			}
		}

		CalculateDamage(_Target: Combatant, _Command: Data.Combat.Move, _roll: number) {
			return 1;
		}

		ApplyEffects(_Attack, _Target, _Effects) {

		}

		GenerateCombo(_Target: Combatant, _Command: Data.Combat.Move, _roll: number) {
			return 0;
		}

		PrintHit(Attacks, Target: Combatant, Roll: number, Damage) {
			var len = Math.floor(Math.max(0, Math.min((Attacks.length * Roll), (Attacks.length - 1))));
			var msg = (typeof this.Owner.IsNPC !== 'undefined' && this.Owner.IsNPC == true) ? Attacks[len][1] : Attacks[len][0];
			msg += " <span style='color:red'>(" + Damage + ")</span>";
			this.PrintMessage(msg, Target);
		}

		/**
		 *
		 * @param Arr Message to show to chat log for misses.
		 */
		GetMissMessage(Arr: string[][]) {
			const MissMessage = App.PR.GetRandomListItem(Arr);

			return (typeof this.Owner.IsNPC !== 'undefined' && this.Owner.IsNPC == true) ? MissMessage[1] : MissMessage[0];
		}

	}

	// Unarmed combat class
	export class Unarmed extends Generic {

		get Class() {return "UNARMED";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @param Target
		 * @param Command
		 * @param Roll
		 * @returns Damage
		 */
		CalculateDamage(Target: Combatant, Command: Data.Combat.Move, _roll: number) {
			var base = 1;

			const owner = this.Owner;
			if (owner instanceof App.Combat.Player) {
				base = 1 + (Math.random() * 3) + Math.max(1, Math.min((owner.Player.GetStat('STAT', 'Fitness') / 25), 4));
				base = Math.floor(base);
			} else {
				base = base + Math.floor(owner.Attack / 20);
			}

			if (Command.Name != "Knee") {
				base = Math.floor(base * Command.Damage); // Add damage mod
			} else {
				var mod = Target.Gender == 1 ? 4.0 : 2.0; // Knee attack does more damage on male enemies
				base = Math.floor(base * mod);
			}

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			if ((command.Name == "Punch" && this.LastMove == "Kick") ||
				(command.Name == "Kick" && this.LastMove == "Punch")) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 * @param Target
		 * @param Command
		 * @param Roll
		 */
		ApplyEffects(Target: Combatant, Command: Data.Combat.Move, roll: number) {

			if (Command.Name == 'Haymaker') {
				var chance = Math.max(10, Math.min((100 * roll), 100));
				if (chance >= Math.floor(Math.random() * 100)) {
					Target.AddEffect('STUNNED', 2);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", Target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", Target);
					}
				}
			}
		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Knee"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Knee"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Haymaker"].Combo && Math.floor(Math.random() * 100) >= 60) {
				this.AttackTarget(Target, this.Owner.Moves["Haymaker"]);
			} else if (this.LastMove == "Kick") {
				this.AttackTarget(Target, this.Owner.Moves["Punch"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Kick"]);
			}
		}

	};

	//Swashbuckling Class
	export class Swashbuckling extends Generic {

		get Class() {return "SWASHBUCKLING";}

		/**
		 * Calculate the damage of an swashbuckling attack
		 * @returns Damage
		 */
		CalculateDamage(target: Combatant, command: Data.Combat.Move, _roll: number) {
			var base = 1;
			const owner = this.Owner;
			if (owner instanceof App.Combat.Player) {
				var weaponQuality = owner.GetWeaponQuality();
				var bonus = owner.GetWeaponBonus();
				var skill = owner.Attack;
				var fitness = owner.Player.GetStat('STAT', 'Fitness');
				var mod = 1 + (skill / 100) + (fitness / 100);
				base = Math.ceil(weaponQuality * mod) + bonus;
			} else {
				base += Math.floor(owner.Attack / 10);
			}

			if (command.Name == 'Riposte') { // Converts combo points into extra damage.
				// Drain all combo points.
				var combo = this.Owner.Combo;
				this.Owner.UseCombo(combo);

				base = base + (combo * 2); // bonus base damage from combo points.
			}

			if (command.Name == 'Behead') { // Chance to do massive damage against enemies at low health
				if (target.Health / target.MaxHealth < 0.5) {
					var chance = (65 - Math.floor((100 * (target.Health / target.MaxHealth))));
					if (chance >= Math.floor(Math.random() * 100)) {
						base = target.Health;
					}
				}
			}

			base = Math.floor(base * command.Damage); // Add damage mod

			return base;
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {

			if (
				(command.Name == "Slash" && this.LastMove == "Stab") ||
				(command.Name == "Stab" && this.LastMove == "Slash") ||
				(command.Name == 'Slash' && this.LastMove == 'Riposte') ||
				(command.Name == 'Parry')
			) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(_target: Combatant, command: Data.Combat.Move, _roll: number) {

			if (command.Name == 'Parry') {
				this.Owner.AddEffect('GUARDED', 2);
				this.Owner.AddEffect('PARRY', 2);
			} else if (command.Name == 'Stab' && this.LastMove == 'Riposte') {
				this.Owner.AddEffect('SEEKING', 3);
			}

		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Behead"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Behead"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Cleave"].Combo && Math.floor(Math.random() * 100) >= 60) {
				this.AttackTarget(Target, this.Owner.Moves["Cleave"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Riposte"].Combo && this.LastMove == 'Parry') {
				this.AttackTarget(Target, this.Owner.Moves["Riposte"]);
			} else if ((this.LastMove != 'Riposte' && this.LastMove != 'Parry') && Math.floor(Math.random() * 100) >= 70) {
				this.AttackTarget(Target, this.Owner.Moves['Parry']);
			} else if (this.LastMove == "Stab") {
				this.AttackTarget(Target, this.Owner.Moves["Slash"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Stab"]);
			}
		}
	};

	//Boob-jitsu Class
	export class Boobjitsu extends Generic {

		get Class() {return "BOOBJITSU";}

		CalculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			var base = Math.floor(this.Owner.Bust / 10) + Math.floor(this.Owner.Attack / 10);
			return Math.floor(base * command.Damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {

			if ((command.Name == "Jiggle" && this.LastMove == "Wobble") ||
				(command.Name == "Wobble" && this.LastMove == "Jiggle")) {
				return 1;
			}

			if (command.Name == 'Bust Out' &&
				(this.LastMove == 'Twirl' || this.LastMove == 'Boob Quake' || this.LastMove == 'Titty Twister')) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(target: Combatant, command: Data.Combat.Move, _roll: number) {

			if (command.Name == 'Jiggle') {
				target.AddEffect('BLINDED', 3);
			}

			if (command.Name == 'Wobble') {
				this.Owner.AddEffect('BLOODTHIRST', 3);
			}

			if (command.Name == 'Bust Out') {
				this.Owner.AddEffect('SEEKING', 1);
			}

			if (command.Name == 'Twirl') {
				this.Owner.AddEffect('LIFE LEECH', 1);
			}

			if (command.Name == 'Boob Quake') {
				if (Math.floor(100 * Math.random()) <= (this.Owner.Attack / 2)) {
					target.AddEffect('STUNNED', 3);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.Name == 'Titty Twister') {
				if (Math.floor(100 * Math.random()) <= this.Owner.Attack) {
					target.AddEffect('STUNNED', 4);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Titty Twister"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Titty Twister"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Boob Quake"].Combo && Math.floor(Math.random() * 100) >= 60) {
				this.AttackTarget(Target, this.Owner.Moves["Boob Quake"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Twirl"].Combo && Math.floor(Math.random() * 100) >= 80) {
				this.AttackTarget(Target, this.Owner.Moves["Twirl"]);
			} else if (this.LastMove == 'Titty Twister' || this.LastMove == 'Boob Quake' || this.LastMove == 'Twirl') {
				this.AttackTarget(Target, this.Owner.Moves['Bust Out']);
			} else if (this.LastMove == "Wobble") {
				this.AttackTarget(Target, this.Owner.Moves["Jiggle"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Wobble"]);
			}
		}
	};

	//Ass-fu Class
	export class Assfu extends Generic {

		get Class() {return "ASSFU";}

		CalculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			var base = Math.floor(this.Owner.Ass / 10) + Math.floor(this.Owner.Attack / 10);
			return Math.floor(base * command.Damage); // Add damage mod
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {

			if ((command.Name == "Shake It" && this.LastMove == "Booty Slam") ||
				(command.Name == "Booty Slam" && this.LastMove == "Shake It")) {
				return 1;
			}

			if (command.Name == 'Twerk' &&
				(this.LastMove == 'Ass Quake' || this.LastMove == 'Thunder Buns' || this.LastMove == 'Buns of Steel')) {
				return 2;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(target: Combatant, command: Data.Combat.Move, _roll: number) {

			if (command.Name == 'Shake It') {
				target.AddEffect('BLINDED', 3);
			}

			if (command.Name == 'Twerk') {
				this.Owner.AddEffect('DODGING', 3);
			}

			if (command.Name == 'Ass Quake') {
				if (Math.floor(100 * Math.random()) <= (this.Owner.Attack / 3)) {
					target.AddEffect('STUNNED', 3);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.Name == 'Thunder Buns') {
				if (Math.floor(100 * Math.random()) <= this.Owner.Attack / 2) {
					target.AddEffect('STUNNED', 4);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}
			}

			if (command.Name == 'Buns of Steel') {
				if (Math.floor(100 * Math.random()) <= this.Owner.Attack) {
					target.AddEffect('STUNNED', 4);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
					}
				}

				this.Owner.AddEffect('GUARDED', 4);
			}

		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Buns of Steel"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Buns of Steel"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Thunder Buns"].Combo && Math.floor(Math.random() * 100) >= 60) {
				this.AttackTarget(Target, this.Owner.Moves["Thunder Buns"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Ass Quake"].Combo && Math.floor(Math.random() * 100) >= 80) {
				this.AttackTarget(Target, this.Owner.Moves["Ass Quake"]);
			} else if (this.LastMove == 'Buns of Steel' || this.LastMove == 'Thunder Buns' || this.LastMove == 'Ass Quake') {
				this.AttackTarget(Target, this.Owner.Moves['Twerk']);
			} else if (this.LastMove == "Booty Slam") {
				this.AttackTarget(Target, this.Owner.Moves["Shake It"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Booty Slam"]);
			}
		}
	};

	// MONSTER CLASSES
	// ====================================================

	// Kraken
	export class Kraken extends Generic {

		get Class() {return "KRAKEN";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @override
		 * @returns Damage
		 */
		CalculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number): number {
			if (command.Name == 'Ejactulate1' || command.Name == 'Ejaculate2') return 0;

			return Math.ceil(Math.max(2, (5 * Math.random())) * command.Damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, _command: Data.Combat.Move, _roll: number): number {
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(Target: Combatant, Command: Data.Combat.Move, _roll: number) {
			if (Target instanceof Combat.Player) { // No effects on non-player characters.
				const player = Target.Player;
				if (Command.Name == 'Grab') {
					this.Owner.AddEffect('SEEKING', 4);

				} else if (Command.Name == 'Ejaculate1') {
					player.AdjustStatXP('Perversion', 20);
					player.AdjustStatXP('WillPower', -20);
					player.AdjustBodyXP('Lips', 100);
					player.AdjustStatXP('Hormones', 100);
					player.AdjustBodyXP('Bust', 100);

				} else if (Command.Name == 'Ejaculate2') {
					player.AdjustStatXP('Perversion', 20);
					player.AdjustStatXP('WillPower', -20);
					player.AdjustBodyXP('Hips', 100);
					player.AdjustStatXP('Hormones', 100);
					player.AdjustBodyXP('Ass', 100);
				}
			}
		}

		DoAI(Target: Combatant) {

			if (this.Owner.Stamina < 30 && (50 >= (100 * Math.random()))) {
				this.Defend();
				return;
			}

			switch (this.LastMove) {
				case 'Ejaculate2':
				case 'Ejaculate1':
					this.AttackTarget(Target, this.Owner.Moves["Strangle"]);
					break;
				case 'Ass':
					this.AttackTarget(Target, this.Owner.Moves['Ejaculate2']);
					break;
				case 'Mouth':
					this.AttackTarget(Target, this.Owner.Moves["Ejaculate1"]);
					break;
				case 'Strangle':
					if (100 * Math.random() >= 50) {
						this.AttackTarget(Target, this.Owner.Moves['Ass']);
					} else {
						this.AttackTarget(Target, this.Owner.Moves['Mouth']);
					}
					break;
				case 'Grab':
					this.AttackTarget(Target, this.Owner.Moves['Strangle']);
					break;
				default:
					this.AttackTarget(Target, this.Owner.Moves['Grab']);
			}
		}

	};

	// Siren
	export class Siren extends Generic {

		get Class() {return "SIREN";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		CalculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			return Math.ceil(Math.max(1, ((this.Owner.Attack / 10) * Math.random())) * command.Damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			if ((command.Name == 'Touch' && this.LastMove == 'Toss') ||
				(command.Name == 'Toss' && this.LastMove == 'Touch') ||
				(this.LastMove == 'Miss')) {
				return 1;
			}
			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(target: Combatant, command: Data.Combat.Move, _roll: number) {
			if (target.IsNPC == true) return; // No effects on non-player characters.

			if (command.Name == 'Scream') {
				target.AddEffect('STUNNED', 2);
				this.Owner.AddEffect('BLOODTHIRST', 2);
				if (this.Owner.IsNPC) {
					this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
				} else {
					this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
				}
			} else if (command.Name == 'Drown') {
				target.AddEffect('BLINDED', 3);

			}
		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Drown"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Drown"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Scream"].Combo && Math.floor(Math.random() * 100) >= 75) {
				this.AttackTarget(Target, this.Owner.Moves["Scream"]);
			} else if (this.LastMove == "Toss") {
				this.AttackTarget(Target, this.Owner.Moves["Touch"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Toss"]);
			}

		}

	};

	// Boobpire
	export class Boobpire extends Generic {

		get Class() {return "BOOBPIRE";}

		/**
		 * Calculate the damage of an unarmed attack
		 * @returns Damage
		 */
		CalculateDamage(_target: Combatant, command: Data.Combat.Move, _roll: number) {

			return Math.ceil(Math.max(1, ((this.Owner.Attack / 10) * Math.random())) * command.Damage);
		}

		/**
		 * Generate any combo points
		 * @returns number of combo points to grant
		 */
		GenerateCombo(_target: Combatant, command: Data.Combat.Move, _roll: number) {
			if ((command.Name == 'Touch' && this.LastMove == 'Toss') ||
				(command.Name == 'Toss' && this.LastMove == 'Touch')) {
				return 1;
			}

			return 0;
		}

		/**
		 * Apply effects to enemy
		 */
		ApplyEffects(target: Combatant, command: Data.Combat.Move, _roll: number) {
			if (target instanceof App.Combat.Player) { // No effects on non-player characters.
				if (command.Name == 'Bite') {
					target.AddEffect('STUNNED', 2);
					this.Owner.AddEffect('BLOODTHIRST', 2);
					if (this.Owner.IsNPC) {
						this.PrintMessage("<span style='color:yellow'>NPC_NAME stuns you!</span>", target);
					} else {
						this.PrintMessage("<span style='color:yellow'>You stun NPC_NAME!</span>", target);
						this.PrintMessage("<span style='color:red'>Your chest feels hot!</span>", target);
						// Drain breast xp
						target.Player.AdjustBodyXP('Bust', Math.ceil((50 * Math.random()) * -1.0));
					}
				}
			}
		}

		DoAI(Target: Combatant) {
			if (this.Owner.Combo >= this.Owner.Moves["Claw"].Combo) {
				this.AttackTarget(Target, this.Owner.Moves["Claw"]);
			} else if (this.Owner.Combo >= this.Owner.Moves["Bite"].Combo && Math.floor(Math.random() * 100) >= 50) {
				this.AttackTarget(Target, this.Owner.Moves["Bite"]);
			} else if (this.LastMove == "Toss") {
				this.AttackTarget(Target, this.Owner.Moves["Touch"]);
			} else {
				this.AttackTarget(Target, this.Owner.Moves["Toss"]);
			}

		}
	};

	export interface EngineConstructor {
		new(combatant: Combatant, myStatusCB: MyStatusCallback, theirStatusCB: TheirStatusCallback, chatLogCB: ChatLogCallback): Generic;
	}
}
