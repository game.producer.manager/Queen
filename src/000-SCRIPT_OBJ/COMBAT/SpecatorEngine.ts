namespace App.Combat {
	/**
	 * This class is used for spectating and betting on fights.
	 * TODO: Allow the player to input the amount they want to bet.
	 * TODO: Calculate odds of match up and pay out on them.
	 */
	export class SpectatorEngine {

		#OP_A: Combatant;
		#OP_B: Combatant;
		#BET_A: number;
		#BET_B: number;
		#BetOn = null;
		#ChatLog: string[] = [];
		#LootBuffer = [];
		#MaxBet = 0;
		#Club: string;
		#CurrentOpponent:Combatant;


		get OpponentA() {return this.#OP_A;}
		get OpponentB() {return this.#OP_B;}
		get BetOn() {return this.#BetOn;}
		get Club() {return this.#Club;}
		get MaxBet() {return this.#MaxBet;}
		get CA() {return this.#CurrentOpponent;}
		get CT() {return this.CA.Id == 'A' ? this.OpponentB : this.OpponentA;}
		get Winner() {
			if (this.OpponentA == null && this.OpponentB == null) return "no one";
			return this?.OpponentA?.IsDead == true ? this.OpponentB?.Name : this.OpponentA?.Name;
		}

		get IWon() {
			var winner = this.OpponentA?.IsDead == true ? this.OpponentB : this.OpponentA;
			return (winner?.Id == this.BetOn);
		}

		get PayoutStr() {
			if (this.BetOn == null) return "";

			if (this.IWon) {
				return "You win @@.item-money;" + this.MaxBet + " coins@@!";
			} else {
				return "You lose @@.item-money;" + this.MaxBet + " coins@@!";
			}
		}
		LoadEncounter(Club: string, MaxBet: number) {

			sessionStorage.setItem('QOS_FIGHTCLUB_NAME', Club);
			sessionStorage.setItem('QOS_FIGHTCLUB_MAXBET', MaxBet.toString());

			this.#BET_A = 0;
			this.#BET_B = 0;
			this.#ChatLog = [];
			this.#LootBuffer = [];
			this.#Club = Club;
			this.#MaxBet = MaxBet;

			this.#OP_A = this._AddEnemy(App.PR.GetRandomListItem(App.Combat.ClubBetData[Club]));
			this.#OP_A.Id = 'A';

			// Don't allow the same "name" to fight each other. Useful for unique encounters.
			while (true) {
				this.#OP_B = this._AddEnemy(App.PR.GetRandomListItem(App.Combat.ClubBetData[Club]));
				this.#OP_B.Id = 'B';
				if (this.OpponentA?.Name != this.OpponentB?.Name) break;
			}

			this.#CurrentOpponent = this.#OP_A;

		}

		DrawUI() {

			if (this.Club == null) { // Reload from session state if browser refreshed.
				this.LoadEncounter(sessionStorage.getItem('QOS_FIGHTCLUB_NAME') as string,
					parseInt(sessionStorage.getItem('QOS_FIGHTCLUB_MAXBET') as string));
			}

			$(document).one(":passageend", this._DrawUI.bind(this));
		}

		DrawResults() {
			$(document).one(":passageend", this._DrawWinLog.bind(this));
		}

		BetLink(Target) {
			var str = "<<link 'Bet " + this.MaxBet + " coins on " + Target.Title + "' 'FightBetOverUI'>> \
        <<run setup.player.NextPhase(1);>>\
        <<run setup.Spectator.PlaceBet('"+ Target.Id + "')>><</link>>";

			return str;
		}

		WatchLink() {
			var str = "<<link 'Just watch' 'FightBetOverUI'>> \
        <<run setup.player.NextPhase(1);>>\
        <<run setup.Spectator.SpectateFight();>><</link>>";

			return str;
		}

		BetALink() {
			return this.BetLink(this.OpponentA);
		}

		BetBLink() {
			return this.BetLink(this.OpponentB);
		}

		PlaceBet(TargetId) {
			this.#BetOn = TargetId;
			this._SimulateCombat();
			this._DoPayout();
		}

		SpectateFight() {
			this.#BetOn = null;
			this._SimulateCombat();
		}

		_DrawUI() {
			this._DrawEnemyContainers();
		}

		_DrawEnemyContainers() {
			var root = $('#EnemyGUI');
			root.empty();
			this._DrawPortrait(this.OpponentA);
			var vs = $('<div>').attr('id', 'versusTxt').addClass('EnemyContainer').text('VS');
			root.append(vs);
			this._DrawPortrait(this.OpponentB);
		}

		_DrawPortrait(Fighter) {
			var root = $('#EnemyGUI');
			var container = $('<div>').attr('id', 'EnemyContainer' + Fighter.Id).addClass('EnemyContainer');
			container.append('<div>').addClass('EnemyTitle').text(Fighter.Title);
			var frame = $('<div>').attr('id', 'EnemyPortrait' + Fighter.Id).addClass('EnemyPortrait');
			frame.addClass(Fighter.Portrait);
			container.append(frame);
			root.append(container);
		}

		_DrawWinLog() {
			var root = $('#WinDiv');
			var log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (var i = 0; i < this.#ChatLog.length; i++) {
				log.append("<P class='ChatLog'>" + this.#ChatLog[i] + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		/**
		 * Add an enemy object to the encounter.
		 * @param e enemy object data
		 */
		private _AddEnemy(e: string) {
			const d = App.Combat.EnemyData[e];
			var enemy = new App.Combat.Combatant(d,
				this._UpdateNPCStatusCB.bind(this),
				this._UpdatePlayerStatusCB.bind(this),
				this._ChatLogCB.bind(this));
			return enemy;
		}

		private _FormatMessage(m: string, o: Combatant) {
			if (_.isNil(o)) {
				return m;
			}

			var npc = setup.player.GetNPC("Dummy"); // Fake NPC
			npc.Data.Name = o.Name; // Swapsie name
			// Custom replacer(s)
			var that = this;
			/*
			m = m.replace(/(ENEMY_([0-9]+))/g, function (m, f, n) {
				return "<span style='color:cyan'>" + that.#enemies[n].Name + "</span>";
			});*/

			m = m.replace(/NPC_PRONOUN/g, () => o.Gender == Gender.Male ? "his" : "her");

			m = App.PR.TokenizeString(setup.player, npc, m);
			return m;
		}


		/**
		 * Cheap and dirty hack to re-use combat messages.
		 */
		private _SubstituteYou(m: string, o: Combatant) {
			var target = (o.Id === this.OpponentA.Id ? this.OpponentB : this.OpponentA) as Combatant;
			m = m.replace(/((\s)(you)([^a-zA-z]))/g, (_m, _p1, p2, _p3, p4) => p2 + target.Name + p4);

			m = m.replace(/^You/g, target.Name);
			return m;
		}

		private _WriteMessage(m, o) {
			m = this._SubstituteYou(m, o);
			m = this._FormatMessage(m, o);
			this.#ChatLog.push(m);
		}

		// Dummy callbacks

		private _ChatLogCB(m, o) {
			this._WriteMessage(m, o);
		}

		private _UpdatePlayerStatusCB(_m) {
		}

		private _UpdateNPCStatusCB(_npc) {

		}

		private _NextRound() {
			this.#CurrentOpponent = this.CA.Id == 'A' ? this.OpponentB : this.OpponentA;
		}

		_SimulateCombat() {
			while (this.OpponentA?.IsDead != true && this.OpponentB?.IsDead != true) {
				this.CA.StartTurn();
				this.CA.DoAI(this.CT as Combatant, this._ChatLogCB.bind(this));
				this.CA.EndTurn();
				this._NextRound();
			}
		}

		_DoPayout() {
			if (this.IWon == true) {
				setup.player.earnMoney(this.MaxBet, Entity.Income.Betting);
			} else {
				setup.player.spendMoney(this.MaxBet, Entity.Spending.Betting);
			}
		}
	}
}
