namespace App.Combat {
	/**
	 * Make sure to instantiate this object to a namespace that will not be cleared during
	 * normal play. Prior to drawing the UI with the <<combatUI>> widget, make sure that you
	 * call this.InitializeScene() to clear variables and then this.LoadEncounter({}) from the
	 * PREVIOUS passage inside a <<link>> macro, so that the state of this object will not be
	 * overwritten if the player goes into another menu screen while playing.
	 */
	export class CombatEngine {
		#target:Combatant | null = null;
		#encounterData: Data.Combat.EncounterDesc | null = null;
		#enemies: Combatant[] = [];
		#player: Player | null = null;
		#timeline: Combatant[] = [];
		#UI_MaxInitiative = 13;
		#hpBars = {};
		#staminaBars = {};
		#chatLog: string[] = [];
		#lastSelectedStyle = "UNARMED"; // Don't overwrite
		#flee = 0;
		#noWeapons = false;
		#fleePassage = 'Cabin';
		#lootBuffer:string[] = [];
		#introChat: string | null = null;

		get LastSelectedStyle() {
			return this.#lastSelectedStyle;
		}

		set LastSelectedStyle(s) {
			this.#lastSelectedStyle = s;
			sessionStorage.setItem('QOS_ENCOUNTER_COMBAT_STYLE', this.#lastSelectedStyle);
		}

		get Enemies() {return this.#enemies;}

		get DuelMode() {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.Fatal;
		}

		get WinPassage() {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.WinPassage;
		}
		get LosePassage() {
			assertIsDefined(this.#encounterData);
			return this.#encounterData.LosePassage;
		}

		/**
		 * Call before load encounter.
		 * @param {*} opts option object
		 */
		InitializeScene(opts: {flee?: number, fleePassage?: string, noWeapons?: boolean, intro?: string}) {
			this.#encounterData = null;
			this.#enemies = [];
			this.#player = null;
			this.#timeline = [];
			this.#hpBars = {};
			this.#staminaBars = {};
			this.#chatLog = [];
			this.#lootBuffer = [];
			this.#noWeapons = false;

			if (typeof opts !== 'undefined') {
				if (!_.isUndefined(opts.flee)) this.#flee = opts.flee;
				if (!_.isUndefined(opts.fleePassage)) this.#fleePassage = opts.fleePassage;
				if (!_.isUndefined(opts.noWeapons)) this.#noWeapons = opts.noWeapons;
				if (!_.isUndefined(opts.intro)) {
					this.#introChat = opts.intro;
					sessionStorage.setItem('QOS_ENCOUNTER_INTRO', this.#introChat);
				}
			}

			sessionStorage.setItem('QOS_ENCOUNTER_FLEE', this.#flee.toString());
			sessionStorage.setItem('QOS_ENCOUNTER_FLEE_PASSAGE', this.#fleePassage);
			sessionStorage.setItem('QOS_ENCOUNTER_NO_WEAPONS', this.#noWeapons ? "true" : "false");
		}

		/**
		 * Load an encounter to the engine.
		 * Come back later and serialize the enemy stats and player stats to save to sessionStorage
		 * and then reconstitute them on reload of page.
		 * @param Encounter encounter data record
		 */
		LoadEncounter(Encounter: string) {
			sessionStorage.setItem('QOS_ENCOUNTER_KEY', Encounter);
			const eqData = clone(App.Combat.EncounterData[Encounter]);
			if (!eqData) {
				throw "Encounter name invalid";
			}
			this.#encounterData = eqData;
			for (const e of eqData.Enemies) this._AddEnemy(e);
			this._AddPlayer(setup.player);
			if (this.#introChat && this.#introChat != null && this.#introChat != 'null') {
				this._AddChat(this.#introChat);
			} else if (typeof eqData.Intro !== "undefined") {
				this._AddChat(eqData.Intro);
			}
		}

		DrawUI() {
			// Reconstitute some data from the session storage if the player reloaded the page to be a cheat.
			if (this.#encounterData == null) {
				this.#lastSelectedStyle = sessionStorage.getItem('QOS_ENCOUNTER_COMBAT_STYLE') as string;
				this.#flee = parseInt(sessionStorage.getItem('QOS_ENCOUNTER_FLEE') as string);
				this.#fleePassage = sessionStorage.getItem('QOS_ENCOUNTER_FLEE_PASSAGE') as string;
				this.#noWeapons = (sessionStorage.getItem('QOS_ENCOUNTER_NO_WEAPONS') === "true");
				this.LoadEncounter(sessionStorage.getItem('QOS_ENCOUNTER_KEY') as string);
				this.#introChat = sessionStorage.getItem('QOS_ENCOUNTER_INTRO');
			}

			$(document).one(":passageend", this._DrawUI.bind(this));
		}

		DrawResults() {
			$(document).one(":passageend", this._DrawWinLog.bind(this));
		}

		StartCombat() {
			this._StartRound();
		}

		//UI COMPONENTS

		private _DrawUI() {
			// Main UI Components.
			this._DrawInitiativeBar();
			this._DrawEnemyContainers();
			this._DrawChatLog();
			this._DrawStyles();
			this._DrawCombatButtons();

			// Default Control buttons
			this._AddDefaultCommands();

			//Status for players.
			this._UpdatePlayerComboBar();
			this._UpdatePlayerStaminaBar();
			//Start the fight.
			this._StartRound();
		}

		private _DrawCombatButtons() {
			var selectedStyle = $('#combatStyles').children("option:selected").val() as string;
			var root = $('#CombatCommands');
			root.empty();

			var d = App.Combat.Moves[selectedStyle].moves;

			for (const prop in d) {
				const m = d[prop];
				var container = $('<div>').addClass("CombatButtonContainer");
				var span = $('<span>').addClass("CombatToolTip").html(
					"<span style='color:yellow'>" + m.Name + "</span><br>" + m.Description);
				var button = $('<div>').attr('id', 'combatButton' + prop.replace(/ /g, '_')).addClass("CombatButton");
				button.addClass(m.Icon);
				if ((this.#player as Player).Engine.CheckCommand(m)) {
					button.on("click", {cmd: prop}, this._CombatCommandHandler.bind(this));
					button.addClass("CombatButtonEnabled");
				} else {
					button.addClass("CombatButtonDisabled");
				}
				container.append(span);
				container.append(button);
				root.append(container);
			}
		}

		private _UpdateCombatButtons() {
			var selectedStyle = $('#combatStyles').children("option:selected").val() as string;
			var d = App.Combat.Moves[selectedStyle].moves;

			for (var prop in d) {
				var button = $("#combatButton" + prop.replace(/ /g, '_'));
				button.off('click');
				assertIsDefined(this.#player);
				if (this.#player.Engine.CheckCommand(d[prop])) {
					button.on("click", {cmd: prop}, this._CombatCommandHandler.bind(this));
					button.removeClass("CombatButtonDisabled");
					button.addClass("CombatButtonEnabled");
				} else {
					button.removeClass("CombatButtonEnabled");
					button.addClass("CombatButtonDisabled");
				}
			}
		}

		private _AddDefaultCommands() {
			var defendBtn = $('#cmdDefend');
			defendBtn.on("click", {cmd: 'defend'}, this._CombatCommandHandler.bind(this));
			var fleeBtn = $('#cmdFlee');
			fleeBtn.on("click", {cmd: 'flee'}, this._CombatCommandHandler.bind(this));
			var restoreBtn = $('#cmdRestoreStamina');
			restoreBtn.on("click", {cmd: 'restoreStamina'}, this._CombatCommandHandler.bind(this));
		}

		private _UpdateDefaultCommands() {

		}

		private _DrawStyles() {
			var root = $('#combatStyles');
			assertIsDefined(this.#player);
			var styles = this.#player.AvailableMoveset;
			if (Object.keys(styles).includes(this.LastSelectedStyle) == false) {
				this.LastSelectedStyle = 'UNARMED';
			}

			for (var prop in styles) {
				if (this.#noWeapons == true && prop == 'SWASHBUCKLING') continue; // Don't allow weapon combat.
				var opt = $('<option>').attr('value', prop).text(styles[prop]);
				if (prop == this.LastSelectedStyle) opt.attr('selected', 'selected');
				root.append(opt);
			}

			root.on("change", this._CombatStyleChangeHandler.bind(this));

		}

		private _DrawChatLog() {
			var root = $('#EnemyGUI');
			var log = $('<div>').attr('id', 'CombatChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<P class='ChatLog'>" + clm + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		private _DrawWinLog() {
			var root = $('#WinDiv');
			var log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (const clm of this.#chatLog) {
				log.append("<P class='ChatLog'>" + clm + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		private _DrawEnemyContainers() {
			var root = $('#EnemyGUI');
			root.empty();

			for (var i = 0; i < this.#enemies.length; i++) {
				var container = $('<div>').attr('id', 'EnemyContainer' + i).addClass('EnemyContainer');
				container.append('<div>').addClass('EnemyTitle').text(this.#enemies[i].Title);
				var frame = $('<div>').attr('id', 'EnemyPortrait' + i).addClass('EnemyPortrait');
				frame.addClass("EnemySelectEnabled");
				frame.addClass(this.#enemies[i].Portrait);
				frame.on("click", {index: i}, this._SelectEnemyHandler.bind(this));
				container.append(frame);
				root.append(container);
				this._DrawStatus(frame, i);
				this._UpdateHPBar(this.#enemies[i]);
				this._UpdateStaminaBar(this.#enemies[i]);
				this._UpdateEnemyPortraits();
			}
		}

		private _DrawStatus(container, enemyIndex) {
			var frame = $('<div>').attr('id', 'StatusFrame' + enemyIndex).addClass('EnemyStatusFrame');

			var hp = $('<div>').attr('id', 'HpHolder' + enemyIndex).addClass('EnemyHPHolder');
			var hpBar = $('<div>').attr('id', 'EnemyHpBar' + enemyIndex).addClass('EnemyHpBar');
			var hpCover = $('<div>').attr('id', 'EnemyHpBarCover' + enemyIndex).addClass('EnemyHpBarCover');
			hp.append(hpBar);
			hpBar.append(hpCover);
			frame.append(hp);
			this.#hpBars[this.#enemies[enemyIndex].Id] = hpCover;

			var stamina = $('<div>').attr('id', 'StaminaHolder' + enemyIndex).addClass('EnemyStaminaHolder');
			var staminaBar = $('<div>').attr('id', 'EnemyStaminaBar' + enemyIndex).addClass('EnemyStaminaBar');
			var staminaCover = $('<div>').attr('id', 'EnemyStaminaBarCover' + enemyIndex).addClass('EnemyStaminaBarCover');

			stamina.append(staminaBar);
			staminaBar.append(staminaCover);
			frame.append(stamina);
			this.#staminaBars[this.#enemies[enemyIndex].Id] = staminaCover;

			container.append(frame);

		}

		private _UpdateEnemyPortraits() {
			for (var i = 0; i < this.#enemies.length; i++) {
				var frame = $("#EnemyPortrait" + i);
				frame.removeClass("ActivePortraitFrame");
				frame.removeClass("InactivePortraitFrame");

				if (this.#enemies[i].IsDead) {
					frame.css("opacity", 0.5);
					frame.addClass("InactivePortraitFrame");
					frame.removeClass("EnemySelectEnabled");
					if (this.#enemies[i] == this.#target) this.#target = null;
					frame.off("click");
				} else {
					if (this.#enemies[i] == this.#target) {
						frame.addClass("ActivePortraitFrame");
					} else {
						frame.addClass("InactivePortraitFrame");
					}
				}
			}
		}

		private _UpdateHPBar(obj: Combatant) {
			var element = this.#hpBars[obj.Id];
			var x = Math.ceil(190 * (obj.Health / obj.MaxHealth));
			element.css('width', x + "px");
		}

		private _UpdateStaminaBar(obj: Combatant) {
			var element = this.#staminaBars[obj.Id];
			var x = Math.ceil(190 * (obj.Stamina / obj.MaxStamina));
			element.css('width', x + "px");
		}

		private _UpdatePlayerStaminaBar() {
			assertIsDefined(this.#player);

			var element = $('#PlayerStaminaBar');
			var x = Math.ceil(120 * (this.#player.Stamina / this.#player.MaxStamina));
			element.css('width', x + "px");
		}

		private _UpdatePlayerComboBar() {
			assertIsDefined(this.#player);

			var element = $('#PlayerComboBar');
			var x = Math.ceil(120 * (this.#player.Combo / this.#player.MaxCombo));
			element.css('width', x + "px");
		}

		private _DrawInitiativeBar() {
			this._GenerateTimeLine(); // Generate data structure

			var root = $('#InitiativeBar');
			root.empty(); // Clear all elements that already exist.

			//Title
			root.append($('<div>').addClass('InitiativePortrait').text('TURN'));

			var max = this.#timeline.length >= this.#UI_MaxInitiative ? this.#UI_MaxInitiative : this.#timeline.length;
			// Create portrait frames.
			for (var i = 0; i < max; i++) {
				var frame = $('<div>').attr('id', 'InitiativePortrait' + i).addClass('InitiativePortrait');

				if (i == 0) {
					frame.addClass('ActiveInitiativeFrame');
				} else {
					frame.addClass('InactiveInitiativeFrame');
				}
				frame.text(this.#timeline[i].Name);
				root.append(frame);
			}


		}
		// OTHER INTERNAL METHODS

		private _AddChat(m: string) {
			m = this._FormatMessage(m);
			this.#chatLog.push(m);
		}

		private _FormatMessage(m: string, o?: Combatant) {
			const enemy = o ?? this.#enemies[0];

			var npc = setup.player.GetNPC("Dummy"); // Fake NPC
			npc.Data.Name = enemy.Name; // Swapsie name
			// Custom replacer(s)
			m = m.replace(/(ENEMY_([0-9]+))/g, (_m, _f, n) => `<span style='color:cyan'>"${this.#enemies[n].Name}</span>`);
			m = m.replace(/NPC_PRONOUN/g,  () => enemy.Gender == 1 ? "his" : enemy.Gender == -1 ? "its" : "her");

			m = App.PR.TokenizeString(setup.player, npc, m);
			return m;
		}

		private _WriteMessage(m: string, o?: Combatant) {
			m = this._FormatMessage(m, o);
			this.#chatLog.push(m);
			var log = $('#CombatChatLog');
			log.append("<P class='ChatLog'>" + m + "</P>");
			log.animate({scrollTop: log.prop("scrollHeight")}, 500);
		}

		/**
		 * Add an enemy object to the encounter.
		 * @param e enemy object data
		 */
		private _AddEnemy(e: string) {
			const d = App.Combat.EnemyData[e];
			var enemy = new App.Combat.Combatant(d,
				this._UpdateNPCStatusCB.bind(this),
				this._UpdatePlayerStatusCB.bind(this),
				this._ChatLogCB.bind(this));
			enemy.Id = "ENEMY_" + this.#enemies.length;
			this.#enemies.push(enemy);
		}

		/**
		 * Add the player to the encounter.
		 */
		private _AddPlayer(p: Entity.Player) {
			var data: Data.Combat.Enemy = {
				Name: p.SlaveName,
				Title: 'NAME',
				Health: p.GetStat('STAT', 'Health'),
				MaxHealth: 100,
				Energy: p.GetStat('STAT', 'Energy'),
				MaxStamina: 100,
				Stamina: 100,
				Speed: 50,
				Moves: 'UNARMED',
				// FIXME Player should not inherit base enemy
				Attack: -1,
				Defense: -1,
				Gender: NaN,
			};

			this.#player = new App.Combat.Player(p, data,
				this._UpdatePlayerStatusCB.bind(this),
				this._UpdateNPCStatusCB.bind(this),
				this._ChatLogCB.bind(this),
				this._LoseFight.bind(this),
				this._WinFight.bind(this));
			this.#player.Id = "PLAYER";

			if (this.#noWeapons == true && this.LastSelectedStyle == 'SWASHBUCKLING') {
				this._SwitchMoveSet('UNARMED');
			} else if (this.LastSelectedStyle == 'SWASHBUCKLING' && !this.#player.IsEquipped('Weapon')) {
				this._SwitchMoveSet('UNARMED');
			} else {
				this._SwitchMoveSet(this.LastSelectedStyle);
			}


		}

		private _SwitchMoveSet(Name: string) {
			assertIsDefined(this.#player);

			var MoveSet = App.Combat.Moves[Name];
			this.#player.ChangeMoveSet(MoveSet.Engine, this._UpdatePlayerStatusCB.bind(this),
				this._UpdateNPCStatusCB.bind(this),
				this._ChatLogCB.bind(this));
			this.LastSelectedStyle = Name;
		}

		/**
		 * Creates an array of the predicted order of turns.
		 */
		private _GenerateTimeLine() {
			assertIsDefined(this.#player);

			this.#timeline = [];
			var Timeline = {};
			this._PopulateTimeline(this.#player, Timeline);
			for (var i = 0; i < this.#enemies.length; i++) {
				if (this.#enemies[i].IsDead == true) continue;
				this._PopulateTimeline(this.#enemies[i], Timeline);
			}

			// Navigate timeline. Rely on the expected behavior of first set keys being retrieved first.
			// If this doesn't work then get keys as array, sort, navigate that. Dumb but whatever.
			var position = 0;
			for (var prop in Timeline) {
				if (position >= this.#UI_MaxInitiative) break; // Abort out of range of the portraits.

				var arr = Timeline[prop];
				for (var x = 0; x < arr.length; x++) {
					if (position >= this.#UI_MaxInitiative) break; // Abort nested loop
					this.#timeline.push(Timeline[prop][x]);
					position++;
				}
			}

		}

		private _PopulateTimeline(o: Combatant, Timeline) {
			var Time = o.GetTimeline(5); // Get 5 turns.
			for (var i = 0; i < Time.length; i++) {
				var t = Time[i];
				if (Timeline.hasOwnProperty(t)) {
					Timeline[t].push(o);
				} else {
					Timeline[t] = [o];
				}
			}
		}

		private _StartRound() {
			//Who's turn is it?
			if (this._GetCombatant().IsNPC) {
				this._EnemyTurn();
			} else {
				this._PlayerTurn();
			}
		}

		private _NextRound() {
			this._DrawInitiativeBar();
			this._StartRound();
		}

		private _GetCombatant() {
			return this.#timeline[0];
		}

		private _EnemyTurn() {
			assertIsDefined(this.#player);

			this._GetCombatant().StartTurn();
			this._GetCombatant().DoAI(this.#player, this._ChatLogCB.bind(this));
			this._GetCombatant().EndTurn();

			if (!this.#player.IsDead)
				setTimeout(this._NextRound.bind(this), 500); // Don't attack if PC is defeated.
		}

		private _PlayerTurn() {
			assertIsDefined(this.#player);

			if (this.#player.IsDead) return; // Don't do anything if defeated.

			if (this.#player.HasEffect('STUNNED')) {
				this._WriteMessage("<span style='color:red'>You are stunned!</span>", this.#player);
				this._GetCombatant().StartTurn();
				this._GetCombatant().EndTurn();
				this._NextRound();
			}

			if (this.#enemies.filter(function (o) {return o.IsDead == false}).length == 0) {
				this._WriteMessage("You have defeated all your foes!", this.#player);
				setTimeout(this._WinFight.bind(this), 500);
			}
			//this._WriteMessage("It is your turn!", this.#player);
		}

		// CALLBACKS

		private _CombatCommandHandler(e) {
			if (this._GetCombatant() !== this.#player) return;

			if (e.data.cmd == 'flee') {

				if (this.#flee == 0) {
					this._WriteMessage("<span style='color:red'>You cannot flee!</span>", this.#player);
					return;
				}
				if (Math.floor(Math.random() * 100) > this.#flee) {
					this._WriteMessage("<span style='color:red'>You attempt to flee, but fail!</span>", this.#player);
					this._GetCombatant().StartTurn();
					this._GetCombatant().AddWeaponDelay(10);
					this._GetCombatant().EndTurn();
					this._NextRound();
					return;
				} else {
					assertIsDefined(this.#encounterData);
					if (!_.isUndefined(this.#encounterData.FleeHandler))
						this.#encounterData.FleeHandler();

					Engine.play(this.#fleePassage);
				}
				return;
			}

			if (e.data.cmd == 'defend') {
				this._GetCombatant().StartTurn();
				this._GetCombatant().Engine.Defend();
				this._GetCombatant().EndTurn();
				this._NextRound();
				return;
			}

			if (e.data.cmd == 'restoreStamina') {
				if (SugarCube.setup.player.CoreStats.Energy <= 0) {
					this._WriteMessage("<span style='color:red'>You do not have enough energy!</span>", this.#player);
					return;
				} else {
					this._GetCombatant().StartTurn();
					this._GetCombatant().Engine.Recover();
					this._GetCombatant().EndTurn();
					this._NextRound();
					return;
				}
			}

			if (this.#target == null) {
				this._WriteMessage("<span style='color:red'>Select a target first!</span>");
				return;
			}

			var Command = this._GetCombatant().Moves[e.data.cmd];
			this._GetCombatant().StartTurn();
			this._GetCombatant().Engine.AttackTarget(this.#target, Command);
			this._GetCombatant().EndTurn();
			this._DrawInitiativeBar();
			this._StartRound();
		}

		private _CombatStyleChangeHandler() {
			var list = $('#combatStyles');
			var style = list.val() as string;
			this._SwitchMoveSet(style);
			this._DrawCombatButtons();
		}

		private _SelectEnemyHandler(e) {
			var obj = this.#enemies[e.data.index];
			if (obj.IsDead != true) {
				this.#target = obj;
			}

			this._UpdateEnemyPortraits();
		}

		/**
		 * call back for writing to the chat log window
		 */
		private _ChatLogCB(m: string, o: Combatant) {
			this._WriteMessage(m, o);
		}

		private _UpdatePlayerStatusCB() {
			this._UpdatePlayerComboBar();
			this._UpdatePlayerStaminaBar();
			this._UpdateCombatButtons();
		}

		private _UpdateNPCStatusCB(npc: Combatant) {
			this._UpdateHPBar(npc);
			this._UpdateStaminaBar(npc);
			this._UpdateEnemyPortraits();
		}

		private _LoseFight(Player: Entity.Player) {
			assertIsDefined(this.#encounterData);
			const lh = this.#encounterData.LoseHandler;
			if (lh)
				lh();

			if (this.DuelMode == true) {
				Player.CoreStats.Health = 10; // Don't kill them…
				Engine.play(this.LosePassage);
			} else {
				// you ded.
				setup.eventEngine.PassageOverride = this.LosePassage;
				Engine.play(this.LosePassage);
			}
		}

		private _WinFight() {

			const loot = this.#encounterData?.Loot;
			if (loot !== undefined) {

				//Generate loot.
				for (const l of loot) {
					this._GrantLoot(l);
				}

				if (this.#lootBuffer.length > 0) {
					const lootMessage = this.#encounterData?.LootMessage;
					if (lootMessage !== undefined) {
						this._AddChat("<span style='color:yellow'>" + lootMessage + "</span>");
					} else {
						this._AddChat("<span style='color:yellow'>You find some loot…</span>")
					}

					for (const l of this.#lootBuffer)
						this._AddChat("<span style='color:yellow'>&check; </span>" + l);
				}
			}

			if (this.#encounterData?.WinHandler !== undefined)
				this.#encounterData.WinHandler();

			Engine.play(this.WinPassage);
		}

		private _GrantLoot(data: Data.Combat.EncounterLoots.Any) {
			if (data.Chance == 100 || data.Chance <= Math.floor(Math.random() * 100)) {
				const amt = random(data.Min, data.Max);
				if (data.Type == 'Coins') {
					Entity.Player.instance.earnMoney(amt, Entity.Income.Loot);
					this.#lootBuffer.push(amt + " coins");
				} else if (data.Type == 'Random') {
					const item = App.Items.PickItem(['FOOD', 'DRUGS', 'COSMETICS'], {price: amt});
					if (item != null) {
						Entity.Player.instance.AddItem(item.cat, item.tag, 0);
						this.#lootBuffer.push(item.desc);
					}
				} else {
					// Specific loot.
					const item = Entity.Player.instance.AddItem(data.Type, data.Tag, amt);
					if (amt < 2) {
						this.#lootBuffer.push(item.Description);
					} else {
						this.#lootBuffer.push(item.Description + " x" + amt);
					}
				}
			}
		}

		// Somewhat hackish stuff to support fight club
	}
}
