namespace App{
	export class EventEngine {
		#passageOverride: string | null = null;
		#fromPassage: string | null = null;
		#toPassage: string | null = null;

		constructor() {
		}

		get PassageOverride() {return this.#passageOverride;}
		set PassageOverride(n) {this.#passageOverride = n;};

		get FromPassage() {
			if (this.#fromPassage == null) this._LoadState();
			return this.#fromPassage;
		}
		get ToPassage() {
			if (this.#toPassage == null) this._LoadState();
			return this.#toPassage;
		}

		NextPassage() {
			if (this.#toPassage == null) this._LoadState();
			Engine.play(this.#toPassage as string);
		}

		BackPassage() {
			if (this.#fromPassage == null) this._LoadState();
			Engine.play(this.#fromPassage as string);
		}

		/**
		 * Checks a dictionary of destinations (including 'any') for conditions to process.
		 * On a successful evaluation return a string of a twine passage to the initiating handler
		 * that overrides the characters navigation to that passage. Also, save both the calling
		 * passage and the original destination passage for later use by the passage we are
		 * redirecting to.
		 * @param Player
		 * @param FromPassage
		 * @param ToPassage
		 * @returns OverridePassage
		 */
		CheckEvents(Player: Entity.Player, FromPassage: string, ToPassage: string): string | null {

			// Init the audio engine on passage click to get around browsers blocking auto play
			setup.Audio.Init();
			setup.Audio.Transition(ToPassage);

			this._d("From: " + FromPassage + ",To:" + ToPassage);

			//One time override
			if (this.PassageOverride != null) {
				var tmp = this.PassageOverride;
				this.PassageOverride = null;
				return tmp;
			}

			// Check gameover conditions first. Hardcoded just because they are rare.
			if (Entity.Player.instance.GetStat("STAT", "Health") <= 0) {
				this._d("Player died event.");
				//State.temporary.followup = passageName;
				return "DeathEnd";
			}

			if (Entity.Player.instance.GetStat("STAT", "WillPower") <= 15) {
				this._d("Player lost too much willpower event.");
				//State.temporary.followup = passageName;
				return "WillPowerEnd";
			}

			// Look for forced events. We need to fire them off regardless.
			var event = this._FindForcedEvent(FromPassage, ToPassage);
			if (event != null) {
				this._setFlags(Player, event["ID"]);
				this._SaveState(FromPassage, ToPassage);
				return event.PASSAGE;
			}

			// For now, let's throttle events to 1 per day.
			if (Player.QuestFlags.hasOwnProperty("LAST_EVENT_DAY") && Player.QuestFlags["LAST_EVENT_DAY"] == Player.Day) return null;

			// Location specific events get checked first.
			var validEvents = this._FilterEvents(Player, FromPassage, ToPassage);

			//todo: have to add quest flags to character when event fires off.
			if (validEvents.length > 0) {
				const ev = this._SelectEvent(validEvents);
				if (ev["CHECK"](Player) == true) {
					this._setFlags(Player, ev["ID"]);
					this._SaveState(FromPassage, ToPassage);
					return ev["PASSAGE"];
				}
			}

			validEvents = this._FilterEvents(Player, FromPassage, "Any");

			if (validEvents.length > 0) {
				const ev = this._SelectEvent(validEvents);
				if (ev["CHECK"](Player) == true) {
					this._setFlags(Player, ev["ID"]);
					this._SaveState(FromPassage, ToPassage);
					return ev["PASSAGE"];
				}
			}

			return null;
		}

		private _SaveState(FromPassage: string, ToPassage: string) {
			this.#fromPassage = FromPassage;
			this.#toPassage = ToPassage;
			sessionStorage.setItem('QOS_EVENT_FROM_PASSAGE', this.#fromPassage);
			sessionStorage.setItem('QOS_EVENT_TO_PASSAGE', this.#toPassage);
		}

		private _LoadState() {
			this.#fromPassage = sessionStorage.getItem('QOS_EVENT_FROM_PASSAGE');
			this.#toPassage = sessionStorage.getItem('QOS_EVENT_TO_PASSAGE');
		}

		/**
		 * Filter events down to only potentially valid ones.
		 * @param Player
		 * @param FromPassage
		 * @param ToPassage
		 */
		private _FilterEvents(Player: Entity.Player, FromPassage: string, ToPassage: string) {
			if (App.Data.Events.hasOwnProperty(ToPassage) == false || App.Data.Events[ToPassage].length < 1) return [];
			return App.Data.Events[ToPassage].filter(function (o) {
				return ((o.FROM == 'Any' || o.FROM == FromPassage) && (Player.Day >= o.MIN_DAY)
					&& (o.PHASE.includes(Player.Phase))
					&& (o.MAX_DAY == 0 ? true : Player.Day <= o.MAX_DAY)
					&& (o["MAX_REPEAT"] == 0 ? true :
						(Player.QuestFlags.hasOwnProperty("EE_" + o["ID"] + "_COUNT") ? Player.QuestFlags["EE_" + o["ID"] + "_COUNT"] as number < o["MAX_REPEAT"] : true))
					&& (Player.QuestFlags.hasOwnProperty("EE_" + o["ID"] + "_LAST") ? Player.QuestFlags["EE_" + o["ID"] + "_LAST"] as number + o["COOL"] < Player.Day : true)
				);
			});
		}

		/**
		 * Select an event from the available ones.
		 */
		private _SelectEvent(eventArray: Data.EventDesc[]): Data.EventDesc {
			this._d("_SelectEvent:");
			this._d(eventArray);
			var events = eventArray.filter(o =>
				o.hasOwnProperty('FORCE') &&
				o.FORCE == true &&
				o.CHECK(setup.player) == true);

			if (events.length > 0) return events[0];

			return eventArray[Math.floor(Math.random() * eventArray.length)];
		}

		/**
		 * Check to see if we need to force an event for quests.
		 * They should all be single fire events otherwise things might get weird. Use with caution.
		 * @param FromPassage
		 * @param ToPassage
		 * @returns Event data
		 */
		private _FindForcedEvent(FromPassage: string, ToPassage: string) {
			if (App.Data.Events.hasOwnProperty(ToPassage) == false || App.Data.Events[ToPassage].length == 0) return null;

			var events = App.Data.Events[ToPassage].filter(o =>
				(o.FROM == 'Any' || o.FROM == FromPassage) &&
				o.hasOwnProperty('FORCE') &&
				o.FORCE == true &&
				(o.MAX_REPEAT == 0 ? true :
					(setup.player.QuestFlags.hasOwnProperty("EE_" + o["ID"] + "_COUNT") ?
						setup.player.QuestFlags["EE_" + o["ID"] + "_COUNT"] as number < o.MAX_REPEAT : true)) &&
				o.CHECK(setup.player) == true);

			return events.length > 0 ? events[0] : null;
		}

		/**
		 * Helper Method to set quest flags in player state for event tracking.
		 * @param Player
		 * @param Key
		 */
		private _setFlags(Player: Entity.Player, Key: string) {
			var countKey = "EE_" + Key + "_COUNT";
			var lastKey = "EE_" + Key + "_LAST";

			Player.QuestFlags["LAST_EVENT_DAY"] = Player.Day;
			Player.QuestFlags[lastKey] = Player.Day;

			Player.QuestFlags[countKey] = Player.QuestFlags.hasOwnProperty(countKey) ?
				Player.QuestFlags[countKey] = Player.QuestFlags[countKey] as number + 1 : 1;
		}

		/**
		 * @param Player
		 * @param ID Event Id
		 * @returns Last Day event was fired. 0 if never fired
		 */
		EventFired(Player: Entity.Player, ID: string) {
			var lastKey = "EE_" + ID + "_LAST";
			if (Player.QuestFlags.hasOwnProperty(lastKey) == false) return 0;
			return Player.QuestFlags[lastKey];
		}

		/**
		 * Manually trigger an event and ignore checks, coolsdowns, etc.
		 * @param Passage
		 * @param ID
		 */
		FireEvent(Passage: string, ID: string) {
			var event = App.Data.Events[Passage].filter(function (o) {return o.ID == ID;})[0];
			console.log(event);
			this._setFlags(setup.player, event["ID"]);
			this._SaveState(event["FROM"], Passage);
			Engine.play(event["PASSAGE"]);
		}

		private _d(m: any) {
			if (setup.player.debugMode == true) console.log(m);
		}
	}
}
