declare module "twine-sugarcube" {
	export interface SugarCubeSetupObject {
		player: App.Entity.Player;
		jobEngine: App.JobEngine;
		eventEngine: App.EventEngine;
		CoffinGame: App.Gambling.Coffin;
		Combat: App.Combat.CombatEngine;
		Spectator: App.Combat.SpectatorEngine;
		Notifications: App.Notifications.Engine;
		Loot: App.Loot;
		Audio: App.Audio;
	}

	export interface SugarCubeSettingVariables {
		displayAvatar: boolean;
		displayNPC: boolean;
		displayBodyScore: boolean;
		fastAnimations: boolean;
		bgmVolume: number;
		displayMeterNumber: boolean;
		units: 'Imperial' | 'Metric';
		inlineItemDetails: boolean;
		alternateControlForRogue: boolean;
		autosaveAtSafePlaces: boolean;
		theme: string;
	}

	export interface SugarCubeStoryVariables {
		GameBookmark: string;
		MenuAction: string | App.Entity.NPC | App.Job;
		PlayerState: App.Entity.PlayerState;
		WhoreLink: string;
	}
}

export {}
