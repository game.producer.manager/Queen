namespace App {
	export const enum CoreStat {
		Health = "Health",
		Nutrition = "Nutrition",
		WillPower = "WillPower",
		Perversion = "Perversion",
		Femininity = "Femininity",
		Fitness = "Fitness",
		Toxicity = "Toxicity",
		Hormones = "Hormones",
		Energy = "Energy",
		Futa = "Futa",
	}

	export type CoreStatStr = keyof typeof CoreStat;

	export const enum BodyStat {
		Face = "Face",
		Lips = "Lips",
		Bust = "Bust",
		BustFirmness = "BustFirmness",
		Ass = "Ass",
		Waist = "Waist",
		Hips = "Hips",
		Penis = "Penis",
		Hair = "Hair",
		Height = "Height",
		Balls = "Balls",
		Lactation = "Lactation"
	}

	export type BodyStatStr = keyof typeof BodyStat;

	export const enum DerivedBodyStat {
		AssFirmness = "AssFirmness",
	}

	export type DerivedBodyStatStr = keyof typeof DerivedBodyStat;

	export const enum Skill {
		HandJobs = "HandJobs",
		TitFucking = "TitFucking",
		BlowJobs = "BlowJobs",
		AssFucking = "AssFucking",
		Dancing = "Dancing",
		Singing = "Singing",
		Seduction = "Seduction",
		Courtesan = "Courtesan",
		Cleaning = "Cleaning",
		Cooking = "Cooking",
		Serving = "Serving",
		Swashbuckling = "Swashbuckling",
		Sailing = "Sailing",
		Navigating = "Navigating",
		Styling = "Styling",
		BoobJitsu = "BoobJitsu",
		AssFu = "AssFu"
	}

	export type SkillStr = keyof typeof Skill;

	export const enum StatType {
		STAT = "STAT",
        SKILL = "SKILL",
        BODY = "BODY"
	}

	export type StatTypeStr = keyof typeof StatType;

	export interface StatTypeMap {
		[StatType.STAT]: CoreStat;
		[StatType.SKILL]: Skill;
		[StatType.BODY]: BodyStat | DerivedBodyStat;
	}

	export interface StatTypeStrMap {
		"STAT": CoreStatStr;
		"SKILL": SkillStr;
		"BODY": BodyStatStr | DerivedBodyStatStr;
	}

	export type QuestStage = "INTRO" | "MIDDLE" | "FINISH" | "JOURNAL_ENTRY" | "JOURNAL_COMPLETE";
	export type QuestState = "cancomplete" | "available" | "unavailable" | "completed" | "active";

	export type DayPhase = 0 | 1 | 2 | 3 | 4; // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night

	export const enum Phase {
		Morning = 0,
		Afternoon = 1,
		Evening = 2,
		Night = 3,
		LateNight = 4
	}

	export interface ItemTypeMap {
		"DRUGS": App.Items.Consumable,
		"FOOD": App.Items.Consumable,
		"COSMETICS": App.Items.Consumable,
		"MISC_CONSUMABLE": App.Items.Consumable,
		"MISC_LOOT": App.Items.Consumable,
		"CLOTHES": App.Items.Clothing,
		"WEAPON": App.Items.Clothing,
		"QUEST": App.Items.QuestItem,
		"LOOT_BOX": App.Items.Consumable,
		"REEL": App.Items.Reel,
	}

	export interface InventoryItemTypeMap {
		"DRUGS": App.Items.Consumable,
		"FOOD": App.Items.Consumable,
		"COSMETICS": App.Items.Consumable,
		"MISC_CONSUMABLE": App.Items.Consumable,
		"MISC_LOOT": App.Items.Consumable,
		"QUEST": App.Items.QuestItem,
		"LOOT_BOX": App.Items.Consumable,
		"REEL": App.Items.Reel,
	}

	export namespace Entity {
		export interface ItemTypeInventoryTypeMap {
			"DRUGS":App.Entity.InventoryManager,
			"FOOD": App.Entity.InventoryManager,
			"COSMETICS": App.Entity.InventoryManager,
			"MISC_CONSUMABLE": App.Entity.InventoryManager,
			"MISC_LOOT": App.Entity.InventoryManager,
			"CLOTHES": App.Entity.ClothingManager,
			"WEAPON": App.Entity.ClothingManager,
			"QUEST": App.Entity.InventoryManager,
			"LOOT_BOX": App.Entity.InventoryManager,
			"REEL": App.Entity.InventoryManager,
		}

		export interface StoreInventoryItem { // TODO shares common base with StoreInventoryItemDesc
			CATEGORY: "COMMON" | "RARE";
			TYPE: Data.ItemTypeStr;
			QTY: number;
			MAX: number;
			PRICE: number;
			MOOD: number;
			LOCK: 0 | 1;
			TAG: string;
		}

		export interface StoreInventory {
			LAST_STOCKED: number;
			INVENTORY: StoreInventoryItem[];
			RARE: StoreInventoryItem[];
		}
	}

	export namespace Combat {
		export type Effect = 'STUNNED' | 'BLINDED' | 'GUARDED' | 'DODGING' | 'BLOODTHIRST' | 'SEEKING' | 'LIFE LEECH' | 'PARRY';
	}
}
