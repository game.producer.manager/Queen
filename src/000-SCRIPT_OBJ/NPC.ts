namespace App.Entity {

	interface NPCQuestFlag {
		Value: string | number;
		TMP: number;
	}

	export type NPCStat = "Mood" | "Lust";

	type NPCStats = {[key in NPCStat]: number};
	export interface NPCState extends NPCStats {
		QuestFlags: {[x: string]: NPCQuestFlag};
	}

	/**
	 * NPC Entity
	 */
	export class NPC {
		private readonly _data: App.Data.NPCDesc;
		private _state: App.Entity.NPCState;
		private _nameOverride: string | undefined;

		constructor(Data: App.Data.NPCDesc, state: App.Entity.NPCState) {
			this._data = Data;
			this._state = state;
		}

		get Name(): string {return this._nameOverride ?? this._data.Name;}
		set Name(v: string) {this._nameOverride = v;}

		pName(): string {return "@@.npc;" + this.Name + "@@";}
		get Mood() {return this._state.Mood;}
		pMood() {return App.PR.GetRating("Mood", this.Mood, true);}
		get Lust() {return this._state.Lust;}
		pLust() {return App.PR.GetRating("Lust", this.Lust, true);};
		Title() {
			return this._data.Title.replace("{NAME}", this.Name);
		}

		ShortDesc() {return this.Title() + " (" + this.pMood() + ", " + this.pLust() + ")";}
		LongDesc() {return this._data.LongDesc.replace("{NAME}", this.Name);}
		HasStore() {return (typeof this._data.Store !== 'undefined');}
		StoreName() {return this._data.Store;}

		GetFlag(Flag: string) {
			if (typeof this._state.QuestFlags[Flag] === 'undefined') return 0;
			return this._state.QuestFlags[Flag]["Value"];
		}

		SetFlag(Flag: string, Value: string | number, Tmp: number) {
			if (typeof Tmp === 'undefined') Tmp = 0;
			this._state.QuestFlags[Flag] = {"Value": Value, "TMP": Tmp};
		}

		ClearFlag(Flag: string) {
			if (typeof this._state.QuestFlags[Flag] === 'undefined') return;
			delete this._state.QuestFlags[Flag];
		}

		ResetFlags() {
			for (var prop in this._state.QuestFlags) {
				if (!this._state.QuestFlags.hasOwnProperty(prop)) continue;
				if (this._state.QuestFlags[prop]["TMP"] != 0) delete this._state.QuestFlags[prop];
			}
		}
		/**
		 * Adjust an NPC statistic (dictionary value)
		 */
		AdjustStat(stat: NPCStat, shift: number) {
			this._state[stat] = Math.ceil(Math.max(1, Math.min((this._state[stat] + shift), 100)));
		}

		/**
		 * Get the value of an NPC stat.
		 */
		GetStat(Stat: NPCStat): number {
			return this._state[Stat];
		}

		AdjustFeelings() {
			this.AdjustStat("Mood", this._data.DailyMood);
			this.AdjustStat("Lust", this._data.DailyLust);
		}

		get Data(): Data.NPCDesc {
			return this._data;
		}
	};

}
