namespace App {
	export namespace EventHandlers {

		export function Init() {
			console.log("Initializing Event Handlers…");
		}

		/**
		 * Do we have a player state and a player object?
		 */
		export function HasPlayerState(): boolean {
			return (exists("SugarCube.setup.player") == true && exists("SugarCube.State.variables.PlayerState") == true);
		}

		function exists(namespace: string): boolean {
			var tokens = namespace.split('.');
			return tokens.reduce(function (prev, curr) {
				return (typeof prev == "undefined") ? prev : prev[curr];
			}, globalThis) != undefined;
		}

		export function onLoad(save: TwineSugarCube.SaveObject) {
			function findItemId(Name: string, Rank?: Data.ReelRankStr) {

				function tryFindInClass<T>(cl: Record<string, T>, Name: string, NameProp: string) {
					for (var prop in cl) {
						if (!cl.hasOwnProperty(prop)) continue;
						if (cl[prop][NameProp] == Name) return prop;
					}
					return undefined;
				}

				function tryFindReelInClass(cl: Record<string, Data.ReelDesc>, Name: string, Rank?: Data.ReelRankStr) {
					for (var prop in cl) {
						if (!cl.hasOwnProperty(prop)) continue;
						if (cl[prop]["NAME"] == Name && cl[prop]["RANK"] == Rank) return prop;
					}
					return undefined;
				}

				var nm = tryFindInClass(App.Data.Drugs, Name, "Name");
				if (nm) return ["DRUGS", nm];
				nm = tryFindInClass(App.Data.Food, Name, "Name");
				if (nm) return ["FOOD", nm];
				nm = tryFindInClass(App.Data.Cosmetics, Name, "Name");
				if (nm) return ["COSMETICS", nm];
				nm = tryFindInClass(App.Data.Misc, Name, "Name");
				if (nm) return ["MISC_CONSUMABLE", nm];
				nm = tryFindInClass(App.Data.Clothes, Name, "Name");
				if (nm) return ["CLOTHES", nm];
				//?? if (Type == "WEAPON") return App.Data.Clothes;
				nm = tryFindInClass(App.Data.Stores, Name, "Name");
				if (nm) return ["STORE", nm];
				nm = tryFindInClass(App.Data.NPCS, Name, "Name");
				if (nm) return ["NPC", nm];
				nm = tryFindInClass(App.Data.QuestItems, Name, "Name");
				if (nm) return ["QUEST", nm];
				nm = tryFindInClass(App.Data.LootBoxes, Name, "Name");
				if (nm) return ["LOOT_BOX", nm];
				//nm = tryFindInClass(App.Data.Slots, Name, "NAME");
				nm = tryFindReelInClass(App.Data.Slots, Name, Rank);
				if (nm) return ["REEL", nm];
			}

			if (save.id.indexOf("queen-of-the-seas") != 0) {
				throw new Error("Save from another game");
			}

			if (save.version == undefined) save.version = 0.0;
			if (save.version < 0.08) {
				console.log("Migrating inventory format…");
				// migrating inventory and wardrobe
				var oldInv = save.state.history[0].variables.Player.Inventory; // array of items
				var newInv = {};
				for (const oi of oldInv) {
					const id = findItemId(oi.Name(), oi?.Rank())
					if (id === undefined) {
						console.error(`Could not find item for name ${oi.Name()}`);
						continue;
					}
					if (!newInv.hasOwnProperty(id[0])) newInv[id[0]] = {};
					if (!newInv[id[0]].hasOwnProperty(id[1])) newInv[id[0]][id[1]] = 0;
					if (id[0] == "REEL") {
						newInv[id[0]][id[1]] = newInv[id[0]][id[1]] + 1;
						console.log("Item " + oi.Name() + " of id " + id + " has 1 charge");
					} else {
						newInv[id[0]][id[1]] = oi.Charges();
						console.log("Item " + oi.Name() + " of id " + id + " has " + oi.Charges() + " charges");
					}
				}

				console.log("New Inventory:"); console.log(newInv);
				if (!save.state.history[0].variables.hasOwnProperty("PlayerState")) {
					save.state.history[0].variables["PlayerState"] = new App.Entity.PlayerState();
				}

				save.state.history[0].variables.PlayerState.Inventory = newInv;

				var oldSlots = save.state.history[0].variables.Player._Slots;
				console.log("Old slots: "); console.log(oldSlots);
				var newSlots = {};
				for (var prop in oldSlots) {
					if (!oldSlots.hasOwnProperty(prop)) continue;
					if (oldSlots[prop] != null) {
						var reelId = findItemId(oldSlots[prop].Name(), oldSlots[prop].Rank());
						if (reelId === undefined) {
							console.error(`Could not find reel for ${oldSlots[prop].Name()}`);
							continue;
						}
						newSlots[prop] = reelId[1];
						save.state.history[0].variables.PlayerState.Inventory[reelId[1]] -= 1;
						if (save.state.history[0].variables.PlayerState.Inventory[reelId[1]] <= 0)
							delete save.state.history[0].variables.PlayerState.Inventory[reelId[1]];
					} else {
						newSlots[prop] = null;
					}
				}

				console.log("New slots:"); console.log(newSlots);
				save.state.history[0].variables.PlayerState.Slots = newSlots;

				var oldWb = save.state.history[0].variables.Player.Wardrobe;
				console.log("Old wardrobe:"); console.log(oldWb);

				var newWb: string[] = [];
				for (var i = 0; i < oldWb.length; ++i) {
					const oi = oldWb[i];
					const id = findItemId(oi.Name());
					if (id == undefined) {
						console.error(`Could not find clothes item named ${oi.Name()}`);
						continue;
					}
					newWb.push(App.Items.MakeId(id[0] as Data.ItemTypeStr, id[1]));
				}

				save.state.history[0].variables.PlayerState.Wardrobe = newWb;
				console.log("New wardrobe:"); console.log(newWb);

				var oldEquip = save.state.history[0].variables.Player.Equipment;
				console.log("Old equipment:"); console.log(oldEquip);
				var newEquip = {};
				for (var prop in oldEquip) {
					if (!oldEquip.hasOwnProperty(prop)) continue;
					if (oldEquip[prop] != 0) {
						var oi = oldEquip[prop];
						var id = findItemId(oi.Name());
						if (id == undefined) {
							console.error(`Could not find item named ${oi.Name()}`);
							continue;
						}
						newEquip[prop] = {ID: App.Items.MakeId(id[0] as Data.ItemTypeStr, id[1]), Locked: oi.IsLocked()};
					} else {
						newEquip[prop] = 0;
					}
				}
				save.state.history[0].variables.PlayerState.Equipment = newEquip;
				console.log("New equipment:"); console.log(newEquip);
				// Copy variables from old player object.
				var newP:Entity.PlayerState = save.state.history[0].variables.PlayerState;
				var oldP = save.state.history[0].variables.Player;

				newP.OriginalName = oldP.OriginalName;
				newP.SlaveName = oldP.SlaveName;
				newP.GirlfriendName = oldP.GirlfriendName;
				newP.NickName = oldP.NickName;
				newP.HairColor = oldP.HairColor;
				newP.HairStyle = oldP.HairStyle;
				newP.HairBonus = oldP.HairBonus;
				newP.MakeupStyle = oldP.MakeupStyle;
				newP.MakeupBonus = oldP.MakeupBonus;
				newP.EyeColor = oldP.EyeColor;
				newP.Money = oldP.Money;
				newP.SailDays = oldP.SailDays;
				newP.LastUsedMakeup = oldP.LastUsedMakeup;
				newP.LastUsedHair = oldP.LastUsedHair;
				newP.LastQuickWardrobe = oldP.LastQuickWardrobe;
				newP.debugMode = oldP.debugMode;
				newP.difficultySetting = oldP.difficultySetting;
				newP.WhoreTutorial = oldP.WhoreTutorial;
				newP.JobFlags = oldP.JobFlags;
				newP.VoodooEffects = oldP.VoodooEffects;
				newP.QuestFlags = oldP.QuestFlags;
				newP.History = oldP.History;
				newP.Day = oldP.Day;
				newP.Phase = oldP.Phase;
				newP.CoreStats = oldP.CoreStats;
				newP.CoreStatsXP = oldP.CoreStatsXP;
				newP.BodyStats = oldP.BodyStats;
				newP.BodyXP = oldP.BodyXP;
				newP.Skills = oldP.Skills;
				newP.SkillsXP = oldP.SkillsXP;
				newP.StoreInventory = oldP.StoreInventory;
				newP.NPCS = oldP.NPCS;
				newP.CurrentSlots = oldP._CurrentSlots;

				delete save.state.history[0].variables.Player; // Clear old player object after copy.
			}

			// adding new stats, skills, XP attributes
			/**
			 * Copies new properties from source to target
			 * @param target
			 * @param source
			 * @param blacklist properties to ignore
			 */
			function deepUpdate(target: object, source: object, blacklist: string[]) {
				function isObject(o: any) {
					return (o !== undefined && typeof o === 'object' && !Array.isArray(o));
				}
				if (isObject(target) && isObject(source)) {
					for (const key in source) {
						if (!source.hasOwnProperty(key) || blacklist.contains(key)) {continue;}
						if (isObject(source[key])) {
							if (!target.hasOwnProperty(key)) {target[key] = {};}
							deepUpdate(target[key], source[key], []);
						} else {
							if (!target.hasOwnProperty(key)) {
								target[key] = source[key];
							}
						}
					}
				}
			}

			console.log('Adding new stats and skills to player state…');
			deepUpdate(save.state.history[0].variables.PlayerState, new App.Entity.PlayerState(), [
				"Wardrobe", "Inventory", "InventoryFavorites", "Equipment", "StoreInventory"
			]);

			if (save.version < 0.10) {
				console.log("Replacing '0' with 'null' in empty slots…");
				let eq = save.state.history[0].variables.PlayerState.Equipment;
				// replace '0' with 'null' for empty slots
				for (let slot in eq) {
					if (!eq.hasOwnProperty(slot)) continue;
					if (eq[slot] === 0) eq[slot] = null;
				}
			}

			if (save.version < 0.122) {
				let ps = save.state.history[0].variables.PlayerState;
				if (ps.Inventory.hasOwnProperty('MISC_CONSUMABLE')) {
					for (var k in ps.Inventory['MISC_CONSUMABLE']) {
						if (k == 'broken rune' || k == 'old arrowhead' || k == 'glowing crystal' || k == 'stone tablet') {
							console.log(ps.Inventory['MISC_CONSUMABLE']);
							if (ps.Inventory.hasOwnProperty('MISC_LOOT') == false) ps.Inventory['MISC_LOOT'] = {};
							ps.Inventory['MISC_LOOT'][k] = ps.Inventory['MISC_CONSUMABLE'][k];
							delete ps.Inventory['MISC_CONSUMABLE'][k];
						}
					}
				}
			}

			if (save.version < 0.130) {
				const ps = save.state.history[0].variables.PlayerState;
				console.log("migrating money earnings stats");
				const earnings = ps.GameStats.MoneyEarned;
				ps.GameStats.MoneyEarned = {
					Jobs: 0,
					SexualJobs: 0,
					Whoring: 0,
					Unknown: earnings
				};
			}

			if (save.version > App.Data.Game.Version) {
				console.log("Version out of range on save. Loaded : " + save.version + ", above expected:" + App.Data.Game.Version);
				/* Invalidates saves outside of legal scope */
				throw new Error("The save you're attempting to load is incompatible with the current game. Please download the latest game version.");
			}

			save.id = Config.saves.id; // the game title includes version, hence this overwrite

			SugarCube.setup.player.SaveLoaded();
			App.Avatar.DrawPortrait();
		}

		export function onSave(save: TwineSugarCube.SaveObject, details: TwineSugarCube.SaveDetails) {
			const gameSate = save.state.history[0].variables as TwineSugarCube.SugarCubeStoryVariables;
			switch (details.type) {
				case "autosave":
					save.title = `Day ${gameSate.PlayerState.Day}, ${App.PR.phaseName(gameSate.PlayerState.Phase)}`;
					break;
				default:
					save.title = `${save.state.history[0].title}, day ${gameSate.PlayerState.Day}, ${App.PR.phaseName(gameSate.PlayerState.Phase)}`;
					break;
			}
		}
	}
}
