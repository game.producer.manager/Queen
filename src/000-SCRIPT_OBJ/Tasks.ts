namespace App {

	export type TaskFlag = boolean | number | string | undefined;

	/**
	 * Represents a basic task for the player. Could be either a job or quest.
	 */
	export abstract class Task {
		private _TaskData: App.Data.Tasks.Task;
		private _SceneBuffer: App.Scene[];
		private _missingRequirements: string[]

		constructor(Data: App.Data.Tasks.Task) {
			this._TaskData = Data;
			this._SceneBuffer = [];
			this._missingRequirements = [];
		}

		get TaskData(): App.Data.Tasks.Task {
			return this._TaskData;
		}

		get Scenes(): App.Scene[] {
			return this._SceneBuffer;
		}

		private static _gte(X: number, Y: number) {return (X >= Y);}
		private static _lte(X: number, Y: number) {return (X <= Y);}
		private static _gt(X: number, Y: number) {return (X > Y);}
		private static _lt(X: number, Y: number) {return (X < Y);}
		private static _eq(X: number, Y: number) {return (X == Y);}
		private static _ne(X: number, Y: number) {return (X != Y);}

		ID() {return this.TaskData.ID;}
		Title() {return this.TaskData.TITLE;}
		get Giver() {return this.TaskData.GIVER;}
		Hidden(): boolean {return this.TaskData.HIDDEN ?? false;}

		/**
		 * Check to see if the player meets the other requirements to take the task. Usually skill, stat, body related or quest flags.
		 * @param Player
		 * @param NPC
		 * @param Requirements
		 * @param MissingRequirements Will be filled with textual description of missing requirements
		 */
		static CheckRequirements(Player: App.Entity.Player, NPC: App.Entity.NPC | string,
			Requirements: App.Data.Tasks.Requirements.Any[], MissingRequirements?: string[]): boolean {
			let StatusFlag = true;
			let Result = true;
			if (MissingRequirements === undefined) MissingRequirements = [];

			for (const r of Requirements) {
				// const {TYPE: Type, NAME: Name, VALUE: Value, CONDITION: Condition, OPT: Option} = r;
				let ReqString = "";

				switch (r.TYPE) {
					case "STAT":
						if (cmp(Player.GetStat(r.TYPE, r.NAME), r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = App.PR.ColorizeString(r.VALUE, r.NAME + " stat is too high");
							} else {
								ReqString = App.PR.ColorizeString(r.VALUE, r.NAME + " stat is too low");
							}
						}
						break;
					case "SKILL":
						if (cmp(Player.GetStat(r.TYPE, r.NAME), r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = App.PR.ColorizeString(r.VALUE, r.NAME + " skill is too high");
							} else {
								ReqString = App.PR.ColorizeString(r.VALUE, r.NAME + " skill is too low");
							}
						}
						break;
					case "BODY":
						if (cmp(Player.GetStat(r.TYPE, r.NAME), r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = App.PR.ColorizeString(r.VALUE, "Too much " + r.NAME);
							} else {
								ReqString = App.PR.ColorizeString(r.VALUE, "Not enough " + r.NAME);
							}
						}
						break;
					case "META":
						let pStat = 0;
						if (r.NAME == 'BEAUTY') pStat = Player.Beauty();
						if (cmp(pStat, r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = App.PR.ColorizeString(r.VALUE, "Too much " + r.NAME.toLowerCase());
							} else {
								ReqString = App.PR.ColorizeString(r.VALUE, "Not enough " + r.NAME.toLowerCase());
							}
						}
						break;
					case "MONEY":
						if (cmp(Player.Money, r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							ReqString = "@@.attention;Need Money (" + (r.VALUE - Player.Money) + ")";
						}
						break;
					case "TOKENS":
						if (cmp(Player.Tokens, r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							ReqString = "@@.attention;Need Tokens (" + (r.VALUE - Player.Tokens) + ")";
						}
						break;
					case "NPC_STAT":
						if (typeof NPC === "string") NPC = Player.GetNPC(NPC);
						if (typeof r.OPTION !== 'undefined') NPC = Player.GetNPC(r.OPTION);
						if (typeof NPC !== 'undefined') {
							if (cmp(NPC.GetStat(r.NAME as App.Entity.NPCStat), r.VALUE, r.CONDITION) == false) {
								StatusFlag = false;
								if (r.CONDITION == "lt" || r.CONDITION == "lte") {
									ReqString = App.PR.ColorizeString(r.VALUE, NPC.Name + "'s " + r.NAME + " is too high");
								} else {
									ReqString = App.PR.ColorizeString(r.VALUE, NPC.Name + "'s " + r.NAME + " is too low");
								}
							}
						}
						break;
					case "DAYS_PASSED":
						const offset = r.NAME ? Quest.GetFlag(Player, r.NAME) as number : 0;
						StatusFlag = cmp(Player.Day, r.VALUE + offset, r.CONDITION);
						if (!StatusFlag) {
							const diffDays = Player.Day - r.VALUE - offset;
							const waitConditions: Data.Tasks.Condition[] = ["eq", "gt", "gte"];
							if (waitConditions.includes(r.CONDITION)) {
								ReqString = `wait ${diffDays} days`;
							} else {
								ReqString = diffDays > 0 ? `${diffDays} days left` : `expired ${0 - diffDays} days ago`;
							}
						}
						break;
					case "QUEST":
						if (!cmpAny(Quest.ByTag(r.NAME).state(Player), r.VALUE, r.CONDITION)) {
							// TODO "cancomplete" is not grammatically correct
							ReqString = `@@.attention;Quest '${App.Data.Quests[r.NAME].TITLE}' not ${r.VALUE}`;
							StatusFlag = false;
						}
						break
					case "QUEST_FLAG":
						if (cmpAny(Player.QuestFlags[r.NAME], r.VALUE, r.CONDITION ?? "eq") === false) {
							if (App.Data.Quests[r.NAME] !== undefined) {
								ReqString = `@@.attention;Quest '${App.Data.Quests[r.NAME].TITLE}' not complete@@`;
							}
							StatusFlag = false;
						}
						break;
					case "JOB_FLAG":
						if (cmpAny(Player.JobFlags[r.NAME], r.VALUE, r.CONDITION ?? "eq") == false) {
							StatusFlag = false;
						}
						break;
					case "ITEM":
						{
							const itm = Player.GetItemById(r.NAME);
							if (itm === undefined) {
								StatusFlag = false;
								ReqString = "@@.state-negative;Missing item '" + r.NAME + "' &times;" + r.VALUE + "@@";
							} else {
								if (!(itm instanceof Items.Clothing)) {
									if (cmp(itm.Charges(), r.VALUE, r.CONDITION) == false) {
										StatusFlag = false;
										ReqString = "@@.state-negative;Missing item '" + r.NAME + "' &times;" + r.VALUE + "@@";
									}
								} else {
									// this is clothing
									// TODO sort out quantity and name printout:
									// What if CLOTHING/ specify VALUE greater than 1?
									StatusFlag = (r.VALUE) === 1;
									if (!StatusFlag) {
										ReqString = `@@.state-negative;Missing clothing ${r.NAME}@@`;
									}
								}
							}
						}
						break;
					case "EQUIPPED":
						if (!Player.IsEquipped(r.NAME)) {
							StatusFlag = false;
							ReqString = "@@.state-negataive;Must have '" + r.NAME + "' equipped.@@";
						}
						break;
					/* TODO: Refactor this to check also for wearing specific items. **/
					case "IS_WEARING": {
						const val = r.VALUE ?? true;
						if (r.SLOT !== undefined) {
							StatusFlag = (Player.GetEquipmentInSlot(r.SLOT)?.Name === r.NAME) === val;
						} else {
							StatusFlag = Player.IsEquipped(r.NAME, false) === val;
						}
					}
						break;
					case "STYLE_CATEGORY":
						if (cmp(Player.GetStyleSpecRating(r.NAME), r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = PR.ColorizeString(r.VALUE, "Too much style '" + r.NAME + "'");
							} else {
								ReqString = PR.ColorizeString(r.VALUE, "Not enough style '" + r.NAME + "'");
							}
						}
						break;
					case "STYLE":
						if (cmp(Player.Style(), r.VALUE, r.CONDITION) == false) {
							StatusFlag = false;
							if (r.CONDITION == "lt" || r.CONDITION == "lte") {
								ReqString = PR.ColorizeString(r.VALUE, "Style and Grooming too high");
							} else {
								ReqString = PR.ColorizeString(r.VALUE, "Style and Grooming too low");
							}
						}
						break;
					case "HAIR_STYLE":
						if ((Player.GetHairStyle() == r.NAME) != r.VALUE) {
							StatusFlag = false;
							ReqString = "@@.state-negative;Need '" + r.NAME + "' hair style@@";
						}
						break;
					case "HAIR_COLOR":
						if ((Player.GetHairColor() == r.NAME) != r.VALUE) {
							StatusFlag = false;
							ReqString = "@@.state-negative;Need '" + r.NAME + "' hair color@@";
						}
						break;
					case "PORT_NAME":
						if ((Player.GetShipLocation().Title == r.NAME) != r.VALUE) {
							StatusFlag = false;
							ReqString = "@@.state-negative;Need Port'" + r.NAME + "'@@";
						}
						break;
					case "IN_PORT":
						if (Player.IsInPort(r.VALUE) != r.CONDITION) {
							StatusFlag = false;
						}
						break;
					case "TRACK_CUSTOMERS":
						var flag = Player.QuestFlags["track_" + r.NAME] as number;
						var count = Player.GetHistory("CUSTOMERS", r.NAME);
						if (count - flag < r.VALUE) StatusFlag = false;
						break;
					case "TRACK_PROGRESS":
						StatusFlag = cmp(App.Quest.GetProgressValue(Player, r.NAME), r.VALUE, r.CONDITION);
						break;

				}

				if (ReqString != "") MissingRequirements.push(ReqString);
				if (!StatusFlag && Result) {Result = false;}
			}

			return Result;
		}

		protected abstract _AvailableScenes(_Player: App.Entity.Player, _NPC: App.Entity.NPC): App.Scene[];

		protected get MissingRequirements() {
			return this._missingRequirements;
		}

		protected clearMissingRequirements() {
			this._missingRequirements = [];
		}
		/**
		 * Play the selected task scenes.
		 * @virtual
		 */
		PlayScenes(player:App.Entity.Player, NPC: App.Entity.NPC): App.Scene[] {
			const results: Data.Tasks.CheckResults = {};

			Task.Debug("PlayScenes", "Started");
			this._SceneBuffer = [];

			const scenes = this._AvailableScenes(player, NPC);
			for (const s of scenes) {
				s.CalculateScene(results);
				this._SceneBuffer.push(s);
			}

			Task.Debug("PlayScenes", "Ended");
			return this._SceneBuffer.filter(s => s.Triggered());
		}

		CompleteScenes(_Player: App.Entity.Player) {
			this._SceneBuffer.filter(s => s.Triggered()).forEach(s => {s.CompleteScene();})
		}

		/**
		 * Match a result to a string and return the colorized version.
		 */
		private _MatchResult(Tag: string, Result: number, Value: number): string {
			var Output = "";
			var Percent = Math.floor((Result / Value) * 100);

			var Colorize = function (a:string ): string {return (typeof a !== 'undefined') ? App.PR.ColorizeString(Percent, a.slice(2, -2), 100) : "";};

			for (var i = 0; i < this.TaskData["JOB_RESULTS"].length; i++)
				if (Percent <= this.TaskData["JOB_RESULTS"][i][Tag])
					return this.TaskData["JOB_RESULTS"][i]["TEXT"].replace(/@@((?!color:).)+?@@/g, Colorize);

			return Output;
		}

		/**
		 * Print the "job results" at the end of a job.
		 */
		protected _PrintJobResults(): string {
			var Checks = this._SceneBuffer[this._SceneBuffer.length - 1].Results();
			var cKeys = Object.keys(Checks);
			var Results: string[] = [];

			for (var i = 0; i < cKeys.length; i++)
				Results.push(this._MatchResult(cKeys[i], Checks[cKeys[i]]["RESULT"], Checks[cKeys[i]]["VALUE"]));
			Results = Results.filter(function (r) {return r != "";});
			return Results.length > 0 ? " " + Results.join(" ") : Results.join("");
		}

		/**
		 * Calculate the total pay from all scenes for the task.
		 */
		protected _PrintPay(): string {
			var Pay = this.TaskData["PAY"];
			if (Pay === undefined) Pay = 0;
			for (const sb of this._SceneBuffer) {
				Pay += sb.RewardItems().Pay;
			}
			return (Pay > 0) ? `<span class='item-money'>${Pay}</span>` : "";
		}

		private _PrintTokens(): string {
			var Tokens = this.TaskData["TOKENS"];
			if (Tokens === undefined) Tokens = 0;
			for (const sb of this._SceneBuffer) {
				Tokens += sb.RewardItems().Tokens;
			}
			return (Tokens > 0) ? `<span class='item-courtesan-token'>${Tokens}</span>` : "";
		}

		/**
		 * Print all of the items earned in this task
		 */
		private _SummarizeTask(): string {
			var items: string[] = [];
			var Pay = this.TaskData["PAY"];
			var Tokens = this.TaskData["TOKENS"];

			if (Pay === undefined) Pay = 0;
			if (Tokens === undefined) Tokens = 0;

			for (const sb of this._SceneBuffer) {
				Pay += sb.RewardItems().Pay;
				Tokens += sb.RewardItems().Tokens;

				for (const ri of sb.RewardItems().Items) {
					var n = Items.SplitId(ri["Name"]);
					var oItem = Items.Factory(n.Category, n.Tag);
					items.push(oItem.Description + " x " + ri["Value"]);
				}
			}

			if (Tokens > 0) {
				items.unshift(`<span class='item-courtesan-token'>${Tokens} courtesan tokens</span>\n`);
			}

			if (Pay > 0) {
				items.unshift(`<span class='item-money'>${Pay} coins</span>\n`);
			}

			if (items.length > 0 || Pay > 0 || Tokens > 0) {
				items.unshift("<span style='color:cyan'>Your receive some items:</span>");
			}

			if (items.length > 0) return items.join("\n") + "\n";
			return "";
		}

		/**
		 * Tokenize a string and return result.
		 */
		protected _Tokenize(Player: App.Entity.Player, NPC: App.Entity.NPC, String?: string): string {
			if (typeof String == 'undefined') return "";
			return App.PR.TokenizeString(Player, NPC, String);
		}

		/**
		 * Print the intro to a task.
		 */
		Intro(Player: App.Entity.Player, Npc: App.Entity.NPC): string {
			return App.PR.TokenizeString(Player, Npc, this.TaskData["INTRO"]);
		}

		/**
		 * Print the "start" scene of a job.
		 * @param {App.Entity.Player} Player
		 * @param {App.Entity.NPC} NPC
		 * @returns {string}
		 */
		PrintStart(Player: App.Entity.Player, NPC: App.Entity.NPC): string {
			return this.TaskData["START"] == "" ? "" : this._Tokenize(Player, NPC, this.TaskData["START"]) + "\n";
		}

		/**
		 * Print the "end" scene of a job.
		 */
		PrintEnd(Player: App.Entity.Player, NPC: App.Entity.NPC): string {
			var JobEnd = this.TaskData["END"] == "" ? "" : this._Tokenize(Player, NPC, this.TaskData["END"]) + "\n";
			// if (this._SummarizeTask() != "") JobEnd += this._SummarizeTask();
			return JobEnd;
		}

		static get _Debug() {
			return Task._debug;
		}

		static set _Debug(v: boolean) {
			Task._debug = v;
		}

		static Debug(Fun: string, String: string) {
			if (Task._Debug == true)
				console.debug(Fun + ":" + String + "\n");
		}

		private static _debug: boolean;
	}

	interface RewardItem {
		Name: string;
		Value: number;
		Opt?: "RANDOM" | "WEAR";
	}

	interface ItemChoiceElement {
		Name: string;
		Value: number;
	}
	export class SceneRewards {
		Pay: number;
		Tokens: number;
		//ScaledValues: number[];
		Items: (RewardItem|number)[];
		ItemChoices: ItemChoiceElement[];
		ChosenItem: number;
		SlotUnlockCount: number;
		constructor() {
			this.Pay = 0;
			this.Tokens = 0;
			//this.ScaledValues = [];
			this.Items = [];
			this.ItemChoices = [];
			this.ChosenItem = -1;
			this.SlotUnlockCount = 0;
		}
	}

	/**
	 * Stores and plays a "scene" from a job.
	*/
	export class Scene {
		private static _debug = false;
		protected _player: App.Entity.Player;
		private _npc: App.Entity.NPC;
		private _sceneData: App.Data.Tasks.JobScene;
		private _triggered: boolean;
		protected _strBuffer: string;
		private _results: App.Data.Tasks.CheckResults;
		private _rewardItems: SceneRewards;

		constructor(player: App.Entity.Player, npc: App.Entity.NPC, sceneData: App.Data.Tasks.JobScene) {
			this._player = player;
			this._npc = npc;
			this._sceneData = sceneData;
			this._triggered = false;
			this._strBuffer = "";
			this._results = {};
			this._rewardItems = new SceneRewards();
		}

		Id() {return this._sceneData.ID;}
		Triggered() {return this._triggered;}
		Print() {return (this._strBuffer !== "") ? this._strBuffer + "\n" : "";}
		Results() {return this._results;}
		RewardItems() {return this._rewardItems;}

		/**
		 * Tokenize a string
		 */
		protected _Tokenize(str: string): string {
			return App.PR.TokenizeString(this._player, this._npc, str);
		}
		/**
		 * Process a Trigger rule and return true/false state on if it fires.
		 */
		private _ProcessTrigger(t: Data.Tasks.Triggers.Any, Checks: Data.Tasks.CheckResults): boolean {
			Scene.Debug('_ProcessTrigger', `(Type=${t.TYPE},Name=${t['NAME']},Value=${t['VALUE']},Condition=${t['CONDITION']},Opt=${t['OPT']}`);

			switch (t.TYPE) {
				case "NPC_STAT":
					return cmp(this._npc.GetStat(t.NAME), t.VALUE, t.CONDITION);
				case "RANDOM-100":
					return cmp(Math.ceil((100 * Math.random()) + 1), t.VALUE, t.CONDITION);
				case "COUNTER":
					if (this._player.JobFlags.hasOwnProperty(t.NAME) == false) this._player.JobFlags[t.NAME] = 0;
					if (t.OPT === "RANDOM")
						return cmp(this._player.JobFlags[t.NAME] as number, Math.ceil(t.VALUE * Math.random()), t.CONDITION);
					return cmp(this._player.JobFlags[t.NAME] as number, t.VALUE, t.CONDITION);
				case "STAT_CORE":
					if (t.OPT === "RANDOM")
						return cmp(this._player.GetStatPercent("STAT", t.NAME), Math.ceil((100 * Math.random()) + 1), t.CONDITION);
					return cmp(this._player.GetStatPercent("STAT", t.NAME), t.VALUE, t.CONDITION);
				case "STAT_BODY":
					if (t.OPT === "RANDOM")
						return cmp(this._player.GetStatPercent("BODY", t.NAME), Math.ceil((100 * Math.random()) + 1), t.CONDITION);
					return cmp(this._player.GetStatPercent("BODY", t.NAME), t.VALUE, t.CONDITION);
				case "STAT_SKILL":
					if (t.OPT === "RANDOM")
						return cmp(this._player.GetStatPercent("SKILL", t.NAME), Math.ceil((100 * Math.random()) + 1), t.CONDITION);
					return cmp(this._player.GetStatPercent("SKILL", t.NAME), t.VALUE, t.CONDITION);
				case "FLAG_SET":
					return t.VALUE ? this._player.JobFlags.hasOwnProperty(t.NAME) : !this._player.JobFlags.hasOwnProperty(t.NAME)
				case "FLAG":
					return cmp(this._player.JobFlags[t.NAME] as number, t.VALUE, t.CONDITION);
				case "TAG":
					if (Checks.hasOwnProperty(t.NAME) == false) return true;
					var Percent = Math.ceil((Checks[t.NAME].RESULT / Checks[t.NAME]["VALUE"]) * 100);
					return cmp(Percent, t.VALUE, t.CONDITION);
				case 'HAS_ITEM':
					// Format like CATEGORY/NAME
					return (typeof this._player.GetItemById(t.NAME) !== 'undefined');
				case "TRACK_PROGRESS":
					return cmp(t.VALUE, App.Quest.GetProgressValue(this._player, t.NAME), t.CONDITION);

			}
		}

		/**
		 * Check the status of each trigger in a group and return true/false on if the scene fires
		 */
		private _ProcessTriggers(triggers: Data.Tasks.Triggers.Any[], checks: Data.Tasks.CheckResults) {
			const anyFailed = triggers.some(t => {
				const r = this._ProcessTrigger(t, checks);
				Scene.Debug("_ProcessTriggers", `Trigger result: ${r}`);
				return !r;
			});
			return !anyFailed;
		}

		/**
		 * Check the status of each trigger in a group and return true/false on if the scene fires
		 */
		private _ProcessTriggersAny(triggers: Data.Tasks.Triggers.Any[], checks: Data.Tasks.CheckResults) {
			return triggers.some(t => {
				const r = this._ProcessTrigger(t, checks);
				Scene.Debug("_ProcessTriggersAny", `Trigger result: ${r}`);
				return r;
			});
		}

		/**
		 * Process a reward rule from a scene and fill _RewardItems and _RewardObjects
		 */
		private _CalculateReward(post: Data.Tasks.Posts.Any, checks: Data.Tasks.CheckResults) {
			Scene.Debug("_CalculateReward", `${post.TYPE}, ${post['NAME']}, ${post['VALUE']}, ${post['OPT']}, ${checks}`);

			let val: number | undefined = undefined;

			switch (post.TYPE) {
				case "MONEY":
					val = Tasks.postNumericValue(post, checks);
					this._rewardItems.Pay += Math.floor(val);
					Scene.Debug("_CalculateReward", "Pay +=" + this._rewardItems.Pay.toString());
					break;
				case "TOKENS":
					val = Tasks.postNumericValue(post, checks);
					this._rewardItems.Tokens += Math.floor(val);
					Scene.Debug("_CalculateReward", "Tokens +=" + this._rewardItems.Tokens.toString());
					break;
				case "ITEM": // any item by ID
					val = Tasks.postNumericValue(post, checks);
					Scene.Debug("_CalculateReward", "Items.push(" + {"Name": post.NAME, "Value": val, "Opt": post.OPT}.toString() + ")");
					this._rewardItems.Items.push({Name: post.NAME, Value: val, Opt: undefined});
					break;
				case "FOOD":
				case "DRUGS":
				case "COSMETICS":
				case "LOOT_BOX":
					val = Tasks.postNumericValue(post, checks);
					Scene.Debug("_CalculateReward", "Items.push(" + {"Name": Items.MakeId(post.TYPE, post.NAME), "Value": val}.toString() + ")");
					this._rewardItems.Items.push({Name: Items.MakeId(post.TYPE, post.NAME), Value: val});
					break;
				case "CLOTHES":
				case "WEAPON":
					val = Tasks.postNumericValue(post, checks);
					if (this._player.OwnsWardrobeItem({TYPE: post.TYPE, TAG: post.NAME})) {
						var clothCost = Math.floor(Items.CalculateBasePrice(post.TYPE, post.NAME) * 0.3);
						this._rewardItems.Pay += clothCost;
						this._rewardItems.Items.push(clothCost);
						break;
					} else {
						this._rewardItems.Items.push({Name: Items.MakeId(post.TYPE, post.NAME), Value: val, Opt: post.WEAR});
					}
					break;
				case "PICK_ITEM":
					var item = App.Items.PickItem(post.NAME, post.VALUE);
					if (item != null) {
						if (item.cat == 'CLOTHES' && this._player.OwnsWardrobeItem(item.cat, item.tag)) {
							var itemCost = Math.floor(Items.CalculateBasePrice("CLOTHES", item.tag) * 0.3);
							this._rewardItems.Pay += itemCost;
							this._rewardItems.Items.push(itemCost);
						} else {
							this._rewardItems.Items.push({Name: Items.MakeId(item.cat, item.tag), Value: 1});
						}
					}
					break;
				case "ITEM_CHOICE":
					this._rewardItems.ItemChoices.push({Name: post.NAME, Value: Tasks.postNumericValue(post, checks)});
					break;
				case "SLOT":
					this._rewardItems.SlotUnlockCount += 1;
					break;
			}
		}

		/**
		 * Process a reward rule from a scene.
		 */
		private _ApplyReward(r: Data.Tasks.Posts.Any, checks: Data.Tasks.CheckResults) {
			Scene.Debug("_ApplyReward", `Type=${r.TYPE},Name=${r['NAME']},Value=${r['VALUE']},Factor=${r['FACTOR']}`);

			switch (r.TYPE) {
				case "MONEY":
				case "TOKENS":
					// The total scene pay is handled upstream
					break;
				case "ITEM": // any item by ID
				case "FOOD":
				case "DRUGS":
				case "COSMETICS":
				case "LOOT_BOX":
				case "CLOTHES":
				case "PICK_ITEM":
					const itemRec = this._rewardItems.Items.shift();
					if (_.isUndefined(itemRec)) {
						throw `Exhausted reward items array while applying rewards for scene ${this.Id}`;
					}
					if (typeof (itemRec) !== 'number') { // if item was converted to money, its value was added to the _RewardItems.Pay
						var n = Items.SplitId(itemRec.Name);
						if (itemRec.Value > 0) {
							this._player.AddItem(n.Category, n.Tag, r.VALUE, r['WEAR']);
						} else {
							this._player.TakeItem(itemRec.Name, 0 - itemRec.Value);
						}
					}
					break;
				case "ITEM_CHOICE":
					if (this._rewardItems.ChosenItem >= 0 && this._rewardItems.ItemChoices.length > 0) {
						const val = Tasks.postNumericValue(r, checks);
						var chosenItemRec = this._rewardItems.ItemChoices[this._rewardItems.ChosenItem];
						var nameSplit = Items.SplitId(chosenItemRec["Name"]);
						if (val >= 0) {
							this._player.AddItem(nameSplit.Category, nameSplit.Tag, val);
						} else {
							this._player.TakeItem(chosenItemRec["Name"], 0 - val);
						}
						this._rewardItems.ItemChoices = [];
					}
					break;
				case "STAT_XP":
				case "BODY_XP":
				case "SKILL_XP":
					if (r.NAME == 'WillPower') { // Override and apply corruption effect.
						this._player.CorruptWillPower(Tasks.postNumericValue(r, checks), 75); // default scaling for jobs.
					} else {
						this._player.AdjustXP(r.TYPE.slice(0, -3) as StatTypeStr, r.NAME, Tasks.postNumericValue(r, checks));
					}
					break;
				case "CORRUPT_WILLPOWER":
					var Difficulty = 20;
					if (r.NAME.toLowerCase() == 'medium') {
						Difficulty = 50;
					} else if (r.NAME.toLowerCase() == 'high') {
						Difficulty = 75;
					}
					this._player.CorruptWillPower(r.VALUE, Difficulty);
					break;
				case "STAT":
					this._player.AdjustStat(r.NAME, Tasks.postNumericValue(r, checks));
					break;
				case "BODY":
					this._player.AdjustBody(r.NAME, Tasks.postNumericValue(r, checks));
					break;
				case "SKILL":
					this._player.AdjustSkill(r.NAME, Tasks.postNumericValue(r, checks));
					break;
				case "QUEST":
					let q = new App.Quest(App.Data.Quests[r.NAME]);
					if (r.VALUE === "START") q.Accept(this._player);
					if (r.VALUE === "COMPLETE") q.Complete(this._player);
					break;
				case "JOB_FLAG":
				case "QUEST_FLAG": {
					const flags = r.TYPE === "JOB_FLAG" ? this._player.JobFlags : this._player.QuestFlags;
					if (r.VALUE == undefined) {
						delete flags[r.NAME];
					} else if (r.OP === "ADD") {
						this._player.increaseFlagValue(r.TYPE === "JOB_FLAG" ? Entity.FlagType.Job : Entity.FlagType.Quest, r.NAME, r.VALUE as number);
					} else {
						flags[r.NAME] = r.VALUE;
					}
				}
					break;
				case "COUNTER":
					if (r.VALUE !== undefined) {
						this._player.increaseFlagValue(Entity.FlagType.Job, r.NAME, r.VALUE);
					} else {
						this._player.deleteFlag(Entity.FlagType.Job, r.NAME);
					}
					break;
				case "STORE":
					if (this._player.StoreInventory[r.NAME]["INVENTORY"].length < 1) {
						App.StoreEngine.OpenStore(this._player, this._npc);
					}

					if (r.OPT == "LOCK") App.StoreEngine.ToggleStoreItem(this._player, r.NAME, r.VALUE, 1);
					if (r.OPT == "UNLOCK") App.StoreEngine.ToggleStoreItem(this._player, r.NAME, r.VALUE, 0);
					break;
				case "RESET_SHOP":
					if (this._player.StoreInventory[r.NAME]["INVENTORY"].length > 1) {
						this._player.StoreInventory[r.NAME]["LAST_STOCKED"] = 0;
					}
					break;
				case "NPC_STAT":
					(r.NPC ? npcByTag(r.NPC) : [this._npc]).forEach(npc => npc.AdjustStat(r.NAME, Tasks.postNumericValue(r, checks)));
					break;
				case "SAIL_DAYS":
					this._player.AdvanceSailDays(Tasks.postNumericValue(r, checks));
					break;
				case "SET_CLOTHING_LOCK":
					this._player.Clothing.SetLock(r.SLOT, r.VALUE);
					break;
				case "SLOT":
					this._player.UnlockSlot();
					break;
				case "TRACK_CUSTOMERS":
					// Let's set a tag in the player to start tracking their history
					App.Quest.SetFlag(this._player, "track_" + r.NAME, this._player.GetHistory("CUSTOMERS", r.NAME));
					break;
				case "TRACK_PROGRESS": {
					const curVal = App.Quest.GetProgressValue(this._player, r.NAME);
					const act = r.OP ?? "ADD";
					let newVal = curVal;
					let value = r.VALUE ?? 1;
					if (r.FACTOR) {
						value *= checks[r.FACTOR].MOD;
					}
					switch (act) {
						case "ADD":
							newVal += value
							break;
						case "MULTIPLY":
							newVal *= value;
							break;
						case "SET":
							newVal = value;
							break;
					}
					App.Quest.SetProgressValue(this._player, r.NAME, newVal);
				}
					break;
				case "BODY_EFFECT":
					if (r.VALUE) {
						this._player.AddBodyEffect(r.NAME);
					} else {
						this._player.RemoveBodyEffect(r.NAME);
					}
					break;
				case "EFFECT":
					Data.EffectLib[r.NAME].FUN(this._player, null);
					break;
				case "GAME_STATE":
					switch (r.NAME) {
						case "DAY_PHASE":
							this._player.NextPhase(Tasks.postNumericValue(r, checks) - this._player.Phase);
							break;
					}
					break;
			}

			function npcByTag(tag: string | string[], npcs?: Entity.NPC[]): Entity.NPC[] {
				const res: Entity.NPC[] = npcs ?? [];
				if (Array.isArray(tag)) {
					tag.forEach(t => npcByTag(t, res));
				} else {
					if (tag.startsWith('@')) { // npc group
						App.Data.NPCGroups[tag.slice(1)].forEach(t => {res.push(Entity.Player.instance.GetNPC(t));});
					} else {
						res.push(Entity.Player.instance.GetNPC(tag));
					}
				}
				return res;
			}
		}

		/**
		 * Process a check rule on a a scene, store results in this._Checks[Tag]
		 */
		private _ProcessCheck(check: Data.Tasks.Checks.Any, results: Data.Tasks.CheckResults) {
			const scaling = (check.OPT != "NO_SCALING");
			const returnRawValue = check.OPT === "RAW";
			let value = check.VALUE ?? 100;
			let result = 0;

			if (returnRawValue) {
				switch (check.TYPE) {
					case "BODY":
					case "STAT":
					case "SKILL":
						result = this._player.GetStat(check.TYPE, check.NAME);
						break;
					case "FUNC":
						result = check.FUN(this._player, this, results);
						value = 1;
						break;
					case "META":
						switch (check.NAME) {
							case "BEAUTY":
								result = this._player.Beauty();
								break;
							case "DANCE_STYLE":
								result = (this._player.GetStyleSpecRating("Sexy Dancer") + this._player.GetStyleSpecRating(JobEngine.instance.GetDance())) / 2;
								break;
						}
				}
			} else {
				switch (check.TYPE) {
					case "STAT":
					case "BODY":
						result = this._player.StatRoll(check.TYPE, check.NAME, check.DIFFICULTY, value, scaling);
						break;
					case "SKILL":
						// This skill is inoperable until you get some points in it.
						if (check.NAME == 'Courtesan' && this._player.GetStat('SKILL', 'Courtesan') == 0) {
							result = 0;
							break;
						}
						result = this._player.SkillRoll(check.NAME, check.DIFFICULTY, value, scaling);
						break;
					case "META":
						switch (check.NAME) {
							case "BEAUTY":
								result = this._player.GenericRoll(this._player.Beauty(), check.DIFFICULTY, value, scaling);
								break;
							case "DANCE_STYLE":
								let defaultStyleResult = this._player.GenericRoll(
									this._player.GetStyleSpecRating("Sexy Dancer"), check.DIFFICULTY, value, scaling);
								let specStyleResult = this._player.GenericRoll(
									this._player.GetStyleSpecRating(JobEngine.instance.GetDance()), check.DIFFICULTY, value, scaling);
								result = (defaultStyleResult + specStyleResult) / 2;
								break;
						}
						break;
					case "FUNC":
						result = check.FUN(this._player, this, results);
						break;
				}
			}
			let r: Data.Tasks.CheckResult = {
				"RESULT": result,
				"VALUE": value,
				"MOD": result / value
			};
			Scene.Debug("_ProcessCheck", `${check.TAG}: ${JSON.stringify(r)}`);
			results[check.TAG] = r;
			this._results[check.TAG] = r;
		}

		/**
		 * Process all the checks in a scene.
		 */
		private _ProcessChecks(Checks: Data.Tasks.Checks.Any[], Results: Data.Tasks.CheckResults) {
			for (const check of Checks)
				this._ProcessCheck(check, Results);
		}

		/**
		 * Process all the "POST" flags in a scene. Generally they give rewards/set status flags.
		 * @param Posts
		 * @param {object} Checks
		 */
		private _ProcessPost(posts: Data.Tasks.Posts.Any[], checks: Data.Tasks.CheckResults) {
			Scene.Debug("_ProcessPost", JSON.stringify(checks));
			posts.forEach(p => {this._CalculateReward(p, checks)});
		}

		/**
		 * Iterate through all of the text fragments, filter them basing on check results for a scene, and then combine them into a string.
		 */
		private _sceneText(checks: Data.Tasks.CheckResults): string {
			const text = this._sceneData.TEXT ?? "";
			if (typeof text === "string") {
				return text;
			}

			const cKeys = Object.keys(checks);
			let results: string[] = [];
			const usedTags: Set<string> = new Set();
			let percent = 0;
			const calcCheckValue = (check: Data.Tasks.CheckResult) => check.VALUE > 1 ? Math.floor(check.MOD * 100) : check.RESULT;
			const colorize =  (a: string) => (typeof a !== 'undefined') ? App.PR.ColorizeString(percent, a.slice(2, -2), 100) : "";

			for (const textFragment of text) {
				if (typeof textFragment === "string") {
					results.push(textFragment);
					usedTags.clear(); // allows to process the next set of choices
				} else {
					const tags = Object.keys(textFragment).filter(k => k !== "TEXT");
					const combinedTag = tags.join('&');
					if (!usedTags.has(combinedTag)) {
						let pass = true;
						for (const t of tags) {
							console.assert(checks.hasOwnProperty(t) && typeof (textFragment[t]) === "number",
								`Wrong tag reference in the TEXT property of scene ${this._sceneData.ID} for tag ${t}`);
							const checkValue = calcCheckValue(checks[t]);
							if (checkValue > textFragment[t]) {
								pass = false;
								break;
							} else {
								if (checks[t].VALUE > 1) {
									percent = checkValue;
								}
							}
						}
						if (pass) {
							usedTags.add(combinedTag);
							results.push(textFragment.TEXT.replace(/@@((?!color:).)+?@@/g, colorize)); // use the last computed percent for the colorizing
						}
					}
				}
			}

			results = results.filter(function (r) {return r != "";});
			return results.length > 0 ? " " + results.join(" ") : "";
		}

		/**
		 * Process all the rules in a scene and store the results in the object.
		 */
		CalculateScene(Checks: Data.Tasks.CheckResults) {
			Scene.Debug(this.Id() + ":CalculateScene", "Started");

			this._results = Checks;
			const triggers = this._sceneData.TRIGGERS ?? [];
			if ((triggers.length > 0) && !this._ProcessTriggers(triggers, Checks)) return;
			const triggersAny = this._sceneData.TRIGGERS_ANY ?? [];
			if ((triggersAny.length > 0) && !this._ProcessTriggersAny(triggersAny, Checks)) return;

			this._triggered = true;
			Scene.Debug(this.Id() + ":CalculateScene", 'Scene triggered');

			this._ProcessChecks(this._sceneData.CHECKS ?? [], Checks);
			const posts = this._sceneData.POST ?? [];
			if (posts.length > 0) this._ProcessPost(posts, Checks);
			this._strBuffer = this._Tokenize(this._sceneText(Checks));

			Scene.Debug(this.Id() + ":CalculateScene", "Ended");
		}

		CompleteScene() {
			Scene.Debug(this.Id() + ":CompleteScene", "Started");
			this._player.earnMoney(this._rewardItems.Pay, Entity.Income.Jobs);
			this._player.AdjustTokens(this._rewardItems.Tokens);
			const posts = this._sceneData.POST ?? [];
			for (const p of posts) {
				this._ApplyReward(p, this._results);
			}
			Scene.Debug(this.Id() + ":CompleteScene", "Ended");
		}

		static get _Debug() {
			return Scene._debug;
		}

		static set _Debug(v: boolean) {
			Scene._debug = v;
		}

		static Debug(Fun: string, String: string) {
			if (Scene._Debug)
				console.debug(Fun + ":" + String + "\n");
		}
	};

	// --------------------- Jobs ----------------------------
	/**
	 * Represents a job.
	 */
	export class Job extends Task {
		private _checkResults: Data.Tasks.CheckResult[] = [];

		constructor(Data: Data.Tasks.Job) {
			super(Data);
		}

		get TaskData(): Data.Tasks.Job {
			return super.TaskData as Data.Tasks.Job;
		}

		Pay() {return this.TaskData.PAY ?? 0;}
		Tokens() {return this.TaskData.TOKENS ?? 0;}
		Phases() {return this.TaskData.PHASES;}

		/**
		 * Check to see if the player meets the other requirements to take the job. Usually skill, stat, body related or quest flags.
		 */
		Requirements(Player: Entity.Player, NPC: Entity.NPC | string): boolean {
			this.clearMissingRequirements();
			return Task.CheckRequirements(Player, NPC, this._Req(), this.MissingRequirements);
		}

		RatingAndCosts(): string {
			let res = "";
			for (var i = 0; i < this.TaskData.RATING; i++) res += " &#9733;";
			res = App.PR.ColorizeString(this.TaskData.RATING, res, 5);

			const Time = this._GetCost("TIME");
			const Energy = this._GetCost("STAT", "Energy");
			const Money = this._GetCost("MONEY");
			const Tokens = this._GetCost("TOKENS");
			const strings: string[] = [];

			if (Time != 0) strings.push("@@.item-time;Time " + Time + "@@");
			if (Energy != 0) strings.push("@@.item-energy;Energy " + Energy + "@@");
			if (Money != 0) strings.push("@@.item-money;Money " + Money + "@@");
			if (Tokens != 0) strings.push("@@.item-courtesan-token;Tokens " + Tokens + "@@");

			if (strings.length != 0) res += ` [${strings.join("&nbsp;")}]`;
			return res;
		}

		/**
		 * Print the title of a job.
		 * @param brief print brief description
		 */
		Title(brief: boolean = false): string {
			var Output = this.TaskData.TITLE;
			if (brief) return Output;

			return `${Output} ${this.RatingAndCosts()}`;
		}

		/**
		 * Calculate cost of a job
		 */
		private _GetCost(Type:Data.Tasks.Costs.CostType, Name?: string) {
			var Check: Data.Tasks.Costs.Any[] = [];
			if (typeof Name !== 'undefined') {
				Check = $.grep(this._Cost(), function (c) {
					return c.TYPE == Type && c['NAME'] == Name;
				});
			} else {
				Check = $.grep(this._Cost(), function (c) {
					return c.TYPE == Type;
				});
			}
			if (typeof Check !== 'undefined' && Check.length > 0) return Check[0].VALUE;
			return 0;
		}

		GetEnergyCost() {
			return this._GetCost("STAT", "Energy");
		}

		GetTimeCost() {
			return this._GetCost("TIME");
		}

		/**
		 * Check to see if the player meets the "Cost" requirement for a job. Usually energy as this
		 * is subtracted from their status upon taking the job.
		 */
		Cost(player: Entity.Player): boolean {
			if (typeof this._Cost() === 'undefined') return true;

			const checkFail = (c: Data.Tasks.Costs.Any) => {
				switch (c.TYPE) {
					case "STAT":
					case "SKILL":
					case "BODY":
						return player.GetStat(c.TYPE, c.NAME) < c.VALUE;
					case "MONEY":
						return player.Money < c.VALUE;
					case "TOKENS":
						return player.Tokens < c.VALUE;
					default:
						return false;
				}
			};
			return this._Cost().some(checkFail) ? false : true;
		}

		/**
		 * Deduct the costs of the Job from the player (usually energy, but could be other stats)
		 */
		private _DeductCosts(Player: Entity.Player) {
			if (typeof this._Cost() === 'undefined') return;
			for (const c of this._Cost()) {
				Job.Debug("_DeductCosts", `Type=${c.TYPE},Name=${c['NAME']},VALUE=${c['VALUE']}`);
				switch (c.TYPE) {
					case "STAT":
						Player.AdjustStat(c.NAME, Math.floor(c.VALUE * -1.0));
						break;
					case "SKILL":
						Player.AdjustSkill(c.NAME, Math.floor(c.VALUE * -1.0));
						break;
					case "BODY":
						Player.AdjustBody(c.NAME, Math.floor(c.VALUE * -1.0));
						break;
					case "MONEY":
						Player.spendMoney(Math.floor(c.VALUE), Entity.Spending.Jobs);
						break;
					case "TOKENS":
						Player.AdjustTokens(Math.floor(c.VALUE * -1.0));
						break;
					case "ITEM":
						Player.Inventory.RemoveItem(c.NAME);
						break;
					case "TIME":
						Player.NextPhase(c.VALUE);
						break;
				}
			}
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity and that there
		 * is enough time in the day to complete it.
		 */
		private _CheckTime(Player: Entity.Player) {
			return (($.inArray(Player.Phase, this.Phases()) != -1) && (this._GetCost("TIME") + Player.Phase <= 4));
		}

		/**
		 * Check to see if the time cost falls within the open/allowed phases of the activity
		 */
		private _CheckReady(player: Entity.Player): boolean;
		private _CheckReady(player: Entity.Player, opt: false): boolean;
		private _CheckReady(player: Entity.Player, opt: true): number;
		private _CheckReady(player: Entity.Player, opt = false): boolean | number {
			var Key = this.ID() + "_LastCompleted";
			var FlagValue = player.JobFlags[Key] ? player.JobFlags[Key] as number : 0;
			if (opt) return (FlagValue + this.TaskData.DAYS - player.Day);
			return ((FlagValue == 0) || (FlagValue + this.TaskData.DAYS <= player.Day));
		}

		/**
		 * Set's the last completed flag for the job.
		 */
		private _SetLastCompleted(Player: Entity.Player) {
			const Key = this.ID() + "_LastCompleted";
			Player.JobFlags[Key] = Player.Day;
		}

		/**
		 * Check to see if this job is available to be used.
		 */
		Available(Player: Entity.Player, NPC: Entity.NPC): boolean {
			Job.Debug("Job.Available:", this.ID());
			Job.Debug("Cost:", "" + this.Cost(Player));
			Job.Debug("Requirements:", "" + this.Requirements(Player, NPC));
			Job.Debug("_CheckTime:", "" + this._CheckTime(Player));
			Job.Debug("_CheckReady:", "" + this._CheckReady(Player));

			return ((this.Cost(Player) == true) && (this.Requirements(Player, NPC) == true) && (this._CheckTime(Player) == true) && (this._CheckReady(Player) == true));
		}

		/**
		 * Print out the requirements (missing) string for a job.
		 */
		ReqString(player: Entity.Player, NPC: Entity.NPC): string {
			let output: string[] = [];

			const coolDown = this._CheckReady(player, true);
			if (this.OnCoolDown(player))
				output.push("@@.state-neutral;Available in " + coolDown + " day" + (coolDown > 1 ? "s@@" : "@@"));

			if (this.OnCoolDown(player) == false && this._CheckTime(player) == false) {
				output.push("@@.state-neutral;Only Available@@");
				for (var i = 0; i < this.Phases().length; i++)
					output.push(Entity.Player.GetPhaseIcon(this.Phases()[i]));
			}
			if (!this.Requirements(player, NPC)) {
				output.push(UI.pWithTooltip("(Requirements not met)", this.MissingRequirements, ["state-negative"]));
			}
			return output.join(' ');
		}

		/**
		 * Check to see if a job is ready (if it's known about).
		 */
		Ready(player: Entity.Player): boolean {
			return this.Cost(player) && this._CheckTime(player) && this._CheckReady(player);
		}

		/**
		 * Return if the job is on cool down.
		 */
		OnCoolDown(Player: Entity.Player): boolean {
			Job.Debug("OnCoolDown", JSON.stringify(Player.JobFlags));
			return (this._CheckReady(Player) != true);
		}

		CompleteScenes(Player: Entity.Player): void {
			Player.earnMoney(this.Pay(), Entity.Income.Jobs);
			Player.AdjustTokens(this.Tokens());
			this._DeductCosts(Player);
			this._SetLastCompleted(Player);
			super.CompleteScenes(Player);
		}

		/**
		 * @override
		 */
		protected _AvailableScenes(Player: App.Entity.Player, NPC: App.Entity.NPC): Scene[] {
			return this.TaskData.SCENES.map(function (x) {return new JobScene(Player, NPC, x);});
		}

		/**
		 * Tokenize a string and return result.
		 * @override
		 */
		protected _Tokenize(Player:App.Entity.Player, NPC: App.Entity.NPC, String: string): string {
			if (typeof String == 'undefined') return "";
			String = String.replace(/JOB_PAY/g, this._PrintPay());

			if (String.indexOf("JOB_RESULTS") != -1) { // Kludge because replace evaluates function even if no pattern match. Dumb.
				String = String.replace(/JOB_RESULTS/g, this._PrintJobResults());
			}
			return super._Tokenize(Player, NPC, String);
		}

		private _Cost() {return this.TaskData.COST ?? [];};
		private _Req() {return this.TaskData.REQUIREMENTS ?? [];};
	};

	/**
	 * Stores and plays a "scene" from a job.
	*/
	class JobScene  extends Scene {
		constructor(Player: Entity.Player, NPC: Entity.NPC, SceneData: Data.Tasks.JobScene) {
			super(Player, NPC, SceneData);
		}

		/**
		 * Tokenize a string
		 * @override
		 */
		protected _Tokenize(String: string) {
			//String = String.replace(/JOB_PAY/g, this.Pay());
			return super._Tokenize(String);
		}
	}

	// ---------------------  Quests -------------------------
	export class Quest extends Task {
		/**
		 * Returns a list of quest entries that fit the criteria on the "flag" option.
		 * * cancomplete - Player can possibly complete these now.
		 * * available - Player can accept these quests.
		 * * completed - Quests the player has completed.
		 * * active - Quests that are currently active.
		 * * any - return all quests that match the above criteria.
		 * @param Flag
		 * @param Player
		 * @param NPC - The NPC ID of the quest giver. Optional.
		 */
		static List(state: QuestState| QuestState[] | "any", player: App.Entity.Player, NPC?: string): App.Quest[] {
			const res: Quest[] = [];
			const allQuests = Object.values(App.Data.Quests).filter(qd => NPC === undefined || qd.GIVER === NPC).map(qd => new Quest(qd));
			const npc = NPC !== undefined ? player.GetNPC(NPC) : undefined;

			if (state === "any") { // TODO clarify "any" and "unavailable" relation
				return allQuests.filter(q => q.state(player, npc) !== "unavailable");
			}

			const states = Array.isArray(state) ? state : [state];

			return allQuests.filter(q => states.includes(q.state(player, npc)));
		}

		/**
		 * Create quest by tag
		 */
		static ByTag(Tag: string) {
			return new App.Quest(App.Data.Quests[Tag]);
		}

		/**
		 * Creates a quest object that operates a virtual quest
		 *
		 * The virtual quest does not have a definition (in App.Data.Quests)
		 * and its only property is the quest ID. Thus it can be used only for
		 * testing or setting quest flags.
		 */
		static VirtualById(Id: string) {
			return new App.Quest({"ID": Id} as Data.Tasks.Quest);
		}

		/**
		 * Retrieve a quest related flag.
		 */
		static GetFlag(Player: Entity.Player, Flag: string): TaskFlag {
			if ((typeof Player.QuestFlags[Flag] === 'undefined')) return undefined;
			return Player.QuestFlags[Flag];
		}

		/**
		 * Set a quest flag value.
		 */
		static SetFlag(Player: Entity.Player, Flag: string, Value: TaskFlag) {
			if (typeof Value === 'undefined') {
				delete Player.QuestFlags[Flag];
			} else {
				Player.QuestFlags[Flag] = Value;
			}
		}

		/**
		 * Helper function to read progress value
		 * @param Player - player being checked.
		 * @param Key - key from check entry.
		 */
		static GetProgressValue(Player: Entity.Player, Key: string): number {
			var res = App.Quest.GetFlag(Player, "progress_" + Key);
			return res === undefined ? 0. : res as number;
		}

		/**
		 * Helper function to read progress value
		 * @param Player - player being checked.
		 * @param Key - key from check entry.
		 * @param Value
		 */
		static SetProgressValue(Player: Entity.Player, Key: string, Value: number) {
			Quest.SetFlag(Player, "progress_" + Key, Value);
		}

		/**
		 * Handles time-dependent changes for active quest
		 */
		static NextDay(Player: Entity.Player) {
			Quest.List('active', Player).forEach(q => q.NextDay(Player));
		}

		/**
		 * @param Player
		 * @param id Quest id
		 */
		static IsActive(Player: Entity.Player, id: string): boolean {
			return Player.QuestFlags[id] === 'ACTIVE';
		}

		/**
		 * @param Player
		 * @param id Quest id
		 */
		static IsCompleted(Player: Entity.Player, id: string): boolean {
			return Quest.GetFlag(Player, id) === 'COMPLETED';
		}


		static allMissingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			const res: Record<Data.ItemNameTemplateAny, number> = {};
			return Quest.List("active", player)
				.map(q => q.missingItems(player))
				.reduce((acc, qr) => {
					let i: Data.ItemNameTemplateAny;
					for (i in qr) {
						const ex = acc[i];
						if (!ex) {
							acc[i] = qr[i];
						} else {
							acc[i] = ex + qr[i];
						}
					}
					return acc;
				 });
		}

		constructor(Data: Data.Tasks.Quest) {
			super(Data);
		}

		get TaskData(): Data.Tasks.Quest {
			return super.TaskData as Data.Tasks.Quest;
		}

		get JournalEntry() {return this.TaskData.JOURNAL_ENTRY;}
		get JournalCompleteEntry() {return this.TaskData.JOURNAL_COMPLETE;}
		get Checks() {return this.TaskData.CHECKS;}

		/**
		 * Maps scene
		 */
		_MakeSceneData(sceneId: string): Data.Tasks.JobScene {
			var res: Data.Tasks.JobScene = {
				"ID": sceneId,
				"TEXT": this.TaskData[sceneId],
			};
			const addToPost = (item: Data.Tasks.Posts.Any) => {
				if (!res.POST) {
					res.POST = [clone(item)];
				} else {
					res.POST.push(clone(item))
				}
			};

			switch (sceneId) {
				case "INTRO":
					this.TaskData.ON_ACCEPT?.forEach(addToPost);
					break;
				case "MIDDLE":
					break;
				case "FINISH":
					if (this.TaskData.POST) {
						res.POST = clone(this.TaskData.POST);
					}
					if (this.TaskData.REWARD) {
						this.TaskData.REWARD.forEach(addToPost);
					}

					if (this.TaskData.CHECKS) {
						const itemsToConsume = this.TaskData.CHECKS.filter(function (o) {
							return o.TYPE === 'ITEM' &&
								typeof o["NAME"] !== 'undefined' &&
								!o["NAME"].startsWith("CLOTHES/");
						});
						for (const itemRule of itemsToConsume) {
							addToPost({TYPE: "ITEM", VALUE: 0 - itemRule['VALUE'], NAME: itemRule['NAME']});
						}
					}
					break;
			}
			return res;
		}

		/**
		 * @override
		 */
		_AvailableScenes(Player:App.Entity.Player, NPC:App.Entity.NPC): App.Scene[] {
			var scenes: App.Scene[] = [];
			if (this.IsAvailable(Player, NPC)) {
				scenes.push(new QuestIntroScene(Player, NPC, this._MakeSceneData('INTRO'), this));
			} else if (this.CanComplete(Player, NPC)) {
				scenes.push(new QuestFinishingScene(Player, NPC, this._MakeSceneData('FINISH'), this));
			} else if (this.IsActive(Player)) {
				scenes.push(new QuestMiddleScene(Player, NPC, this._MakeSceneData('MIDDLE'), this));
			}
			return scenes;
		}

		IsAvailable(Player:App.Entity.Player, NPC?: App.Entity.NPC): boolean {
			if (this.IsCompleted(Player) || this.IsActive(Player)) return false;
			if (NPC === undefined) NPC = Player.GetNPC(this.Giver);
			return Task.CheckRequirements(Player, NPC, this.TaskData.PRE ?? []);
		}

		IsActive(Player: App.Entity.Player): boolean {
			return Quest.IsActive(Player, this.ID());
		}

		CanComplete(Player: App.Entity.Player, NPC?:App.Entity.NPC): boolean {
			if ((typeof Player.QuestFlags[this.ID()] === 'undefined')) return false;
			if (Player.QuestFlags[this.ID()] == 'COMPLETED') return false; // Should never happen eh??
			if (NPC === undefined) NPC = Player.GetNPC(this.Giver);
			return Task.CheckRequirements(Player, NPC, this.TaskData.CHECKS ?? []);
		}

		IsCompleted(Player: App.Entity.Player): boolean {
			return Quest.IsCompleted(Player, this.ID());
		}

		state(player: Entity.Player, npc?: Entity.NPC): QuestState {
			if (Quest.IsCompleted(player, this.ID())) {
				return "completed";
			}
			// npc is required for all further tests
			const lNPC = npc ? npc : player.GetNPC(this.Giver);
			const questFlag = player.QuestFlags[this.ID()];
			if (questFlag === undefined) { // available or unavailabel
				return (Task.CheckRequirements(player, lNPC, this.TaskData.PRE ?? [])) ? "available" : "unavailable";
			}
			return Task.CheckRequirements(player, lNPC, this.TaskData.CHECKS ?? []) ? "cancomplete" : "active"
		}

		missingItems(player: Entity.Player): Record<Data.ItemNameTemplateAny, number> {
			const res: Record<Data.ItemNameTemplateAny, number> = {};
			for (const c of this.Checks ?? []) {
				switch (c.TYPE) {
					case "ITEM":
						if (c.HIDDEN) {
							continue;
						}
						const invItem = player.GetItemById(c.NAME);
						if (invItem === undefined) {
							res[c.NAME] = c.VALUE;
						} else if (invItem instanceof App.Items.Consumable && invItem.Charges() < c.VALUE) {
							res[c.NAME] = c.VALUE - invItem.Charges();
						}
						break;
				}
			}
			return res;
		}

		/**
		 * Accepts the quest
		 *
		 * This method is not meant to be used for "Accept" button in the quest dialog because
		 * it creates its own copy of the INTRO scene.
		 */
		Accept(Player: Entity.Player) {
			let scene = new QuestIntroScene(Player, Player.GetNPC(this.Giver), this._MakeSceneData('INTRO'), this);
			let checks = {};
			scene.CalculateScene(checks);
			scene.CompleteScene();
		}

		/**
		 * Completes the quest
		 */
		Complete(Player: Entity.Player) {
			var scene = new QuestFinishingScene(Player, Player.GetNPC(this.Giver), this._MakeSceneData('FINISH'), this);
			var Checks = {};
			scene.CalculateScene(Checks);
			scene.CompleteScene();
		}

		/**
		 * Marks quest as completed without processing the finishing scene
		 */
		MarkAsCompleted(Player: Entity.Player) {
			App.Quest.SetFlag(Player, this.ID(), "COMPLETED");
			App.Quest.SetFlag(Player, this.ID() + "_CompletedOn", Player.Day);
		}

		/**
		 * Handles time-dependent changes for active quest
		 */
		NextDay(player: Entity.Player) {
			this.TaskData.ON_DAY_PASSED?.call(this, player);
		}

		/**
		 * Day when the quest was accepted by player
		 */
		AcceptedOn(Player: Entity.Player) {
			let r = Quest.GetFlag(Player, this.ID() + "_AcceptedOn");
			return typeof r === 'number' ? r : 1;
		}

		/**
		 * Day when the quest was completed by player
		 */
		CompletedOn(Player: Entity.Player) {
			let r = Quest.GetFlag(Player, this.ID() + "_CompletedOn");
			return typeof r === 'number' ? r : 1;
		}
	};

	class QuestScene extends Scene {
		protected _quest: Quest;
		private _sceneName: string;

		constructor(Player: Entity.Player, NPC: Entity.NPC, SceneData: Data.Tasks.JobScene, Quest: Quest, SceneName: string) {
			super(Player, NPC, SceneData);

			this._quest = Quest;
			this._sceneName = SceneName;
		}

		Id() {
			return this._sceneName;
		}
	}


	class QuestIntroScene extends QuestScene {
		constructor(Player: Entity.Player, NPC: Entity.NPC, SceneData: Data.Tasks.JobScene, Quest: Quest) {
			super(Player, NPC, SceneData, Quest, "INTRO");
		}

		/**
		 * Accept the quest and process triggers.
		 */
		CompleteScene() {
			super.CompleteScene();
			App.Quest.SetFlag(this._player, this._quest.ID(), "ACTIVE");
			App.Quest.SetFlag(this._player, this._quest.ID() + "_AcceptedOn", this._player.Day);
		}
	}

	class QuestMiddleScene extends QuestScene {
		constructor(Player: Entity.Player, NPC: Entity.NPC, SceneData: Data.Tasks.JobScene, Quest: Quest) {
			super(Player, NPC, SceneData, Quest, "MIDDLE");
		}

		/**
		 * Process all the rules in a scene and store the results in the object.
		 */
		CalculateScene(Checks: Data.Tasks.CheckResults) {
			super.CalculateScene(Checks);
			this._strBuffer += "<br/><br/>" + App.PR.pQuestRequirements(this._quest.ID(), this._player);
		}
	}

	class QuestFinishingScene extends QuestScene {
		constructor(Player: Entity.Player, NPC: Entity.NPC, SceneData: Data.Tasks.JobScene, Quest: Quest) {
			super(Player, NPC, SceneData, Quest, "FINISH");
		}

		/**
		 * Completes the quest.
		 */
		CompleteScene() {
			super.CompleteScene();
			this._quest.MarkAsCompleted(this._player);
		}
	}

	/**
	 * Helper function - runs comparisons.
	 */
	function cmp(A: number, B: number, C?: App.Data.Tasks.Condition): boolean;
	function cmp(A: string, B: string, C?: "eq" | "ne" | "==" | "!=" | boolean): boolean;
	function cmp(A: TaskFlag, B: TaskFlag, C?: App.Data.Tasks.Condition): boolean;
	function cmp(A: number|string, B: number|string, C: App.Data.Tasks.Condition = true) {
		switch (C) {
			case "gte":
			case ">=":
				return A >= B;
			case "lte":
			case "<=":
				return A <= B;
			case "gt":
			case ">":
				return A > B;
			case "lt":
			case "<":
				return A < B;
			case "eq":
			case "==":
				return A == B;
			case "ne":
			case "!=":
				return A != B;
			default:
				return (A == B) === C;
		}
	}

	export function cmpAny(test: TaskFlag, val: TaskFlag | TaskFlag[], c?: Data.Tasks.Condition): boolean {
		if (Array.isArray(val)) {
			return val.some(v => cmp(test, v, c));
		}
		return cmp(test, val, c);
	}

	namespace Tasks {
		export function postNumericValue(p: Data.Tasks.Posts.BaseNumeric, checks: Data.Tasks.CheckResults): number {
			let res = p.VALUE;
			if (p.OPT === "RANDOM") {
				res = Math.floor((res * State.random()) + 1);
			}
			// value refers to a result of a previous check
			if (p.FACTOR) {
				// Retrieve the result of another check and modify the value of this reward by that scaling percentage.
				const check = checks[p.FACTOR];
				if (check === undefined) {
					throw new Error(`Could not find check named '${p.FACTOR}'`);
				}
				res *= checks[p.FACTOR].MOD;
			}
			return res;
		}
	}
}
