namespace App.UI.Passages {
	export class StoryCaption extends DOMPassage {
		constructor() {
			super("StoryCaption");

			$('<div id="my-story-caption"></div>').appendTo($('#ui-bar-body'));
			// Setup some divs that will not get refreshed on passage navigation.
			if ($('#extended-story-stats').length == 0) {
				const extendedStoryStats = $("<div id='extended-story-stats'></div>").appendTo($('#ui-bar-body'));
				const container = $("<div id='avatarContainer'></div>").appendTo(extendedStoryStats);
				$("<div id='avatarFace'></div").appendTo(container);
				$("<div id='bodyScoreContainer'></div>").appendTo(extendedStoryStats);

				const menu = makeElement("nav");
				menu.id = "menu";

				// repeat SugarCube ids and classes to apply its CSS
				const menuList = appendNewElement("ul", menu);
				menuList.id = "menu-core";

				const savesItem = appendNewElement("li", menuList);
				savesItem.id = "menu-item-saves";
				appendNewElement("a", savesItem, "Saves").onclick = () => {SugarCube.UI.saves();};

				const settingsItem = appendNewElement("li", menuList);
				settingsItem.id = "menu-item-settings";
				appendNewElement("a", settingsItem, "Settings").onclick = () => {SugarCube.UI.settings();};

				const helpItem = appendNewElement("li", menuList);
				helpItem.id = "menu-item-help";
				const helpAnchor = appendNewElement("a", helpItem, "Help");
				helpAnchor.setAttribute("data-passage", "Help");
				helpAnchor.onclick = (ev) => {Engine.play((ev.target as HTMLElement).getAttribute("data-passage")!)}
				helpAnchor.className = "link-internal"

				const restartItem = appendNewElement("li", menuList);
				restartItem.id = "menu-item-restart";
				appendNewElement("a", restartItem, "Settings").onclick = () => {SugarCube.UI.restart();};

				$(menu).insertAfter(extendedStoryStats);
			}
		}

		public static updateGameScore(): void {
			const d = $("#game-score");
			d.empty();
			d.append(rGameScore());
		}

		render() {
			const res = new DocumentFragment();
			if (State.variables.PlayerState) {
				const gameScore = appendNewElement("div", res);
				gameScore.id = "game-score";
				gameScore.append(UI.rGameScore());
			}

			return res;
		}
	}
}

new App.UI.Passages.StoryCaption();
