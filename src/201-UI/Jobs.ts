namespace App.UI.Passages {
	export class Jobs extends App.UI.DOMPassage {
		constructor() {
			super("Jobs");
		}

		private jobButton(job: Job, isAvailabe: boolean, player: Entity.Player, npc: Entity.NPC) {
			const res = document.createDocumentFragment();
			const btn = appendNewElement("button", res, undefined, ["job-button"]);
			if (isAvailabe) {
				btn.textContent = "Accept";
				btn.onclick = () => {
					App.UI.replace("#JobUI", App.UI.pJobResults(player, npc, job));
				};
			} else {
				btn.classList.add("job-button-disabled");
				if (job.OnCoolDown(player)) {
					btn.textContent = "COOLDOWN";
				} else {
					btn.textContent = "LOCKED";
				}
				$(res).wiki(' ' + job.ReqString(player, npc));
			}
			return res;
		}

		render() {
			const res = document.createDocumentFragment();
			const player = setup.player;

			const npcName = State.variables.MenuAction;
			if (typeof npcName !== "string") {
				throw "Expect $MenuAction to be an NPC name";
			}

			const npc = setup.player.GetNPC(npcName);
			const JA = setup.jobEngine.GetAvailableJobs(player, npcName);
			const JU = setup.jobEngine.GetUnavailableJobs(player, npcName);

			if (settings.displayNPC) {
				const npcDiv = appendNewElement("div", res, undefined, ["npc-portrait-small"]);
				$(npcDiv).wiki(`<<DrawPortraitNPC "${npcName}">>`); // TODO replace with JS call
			}

			const jobUI = appendNewElement("span", res);
			jobUI.id = "JobUI";

			for (const j of JA) {
				const jobDiv = appendNewElement("div", jobUI, undefined, ["job-intro"]);
				$(jobDiv).wiki(j.Title() + "<br/>" + j.Intro(player, npc));
				appendNewElement("br", jobDiv);
				jobDiv.append(this.jobButton(j, true, player, npc));
				appendNewElement("br", jobDiv);
			}

			for (const j of JU) {
				const jobDiv = appendNewElement("div", jobUI, undefined, ["job-intro"]);
				$(jobDiv).wiki(j.Title() + "<br/>" + j.Intro(player, npc));
				appendNewElement("br", jobDiv);
				jobDiv.append(this.jobButton(j, false, player, npc));
				appendNewElement("br", jobDiv);
			}

			appendNewElement("br", res);
			appendNewElement("br", res);
			appendNewElement("span", res, "Interact: ", ["action-interact"]);
			res.append(UI.passageLink("Exit", State.variables.GameBookmark));

			return res
		}
	}
}

new App.UI.Passages.Jobs();
