namespace App {
	registerPassageDisplayNames({
		GreatMarket: "Great Market",
		PR_BackAlley: "Back Alley",
		PortRoyale: "The Docks",
		FortuneTeller: "Madame Blanche's",
		PR_Tavern: "Grey Peacock",
		PR_CheapBrothel: "Lusty Lass",
		EntertainmentDistrict: "The Entertainment District",
		PR_GuardHouse: "Guard House",
		PR_BottlecorkLane: "Bottlecork Lane",
		PR_PublicHouse: "Long's Public House",
		PR_DarkAlley: "Darkened Alleyway",
		PR_Levant: "Le Maison Du Soleil Levant",
		PR_LevantBackRoom: "Back Room",
		PR_Jail: "Jail House",
		WealthyDistrict: "The Wealthy District",
		CourtesansGuild: "Lady Selene's School for Courtesans",
		PR_Clothier: "Veronica's Secret",
		PR_RareGoods: "David Jone's Locker",
		PR_GovernorsMansion: "The Governor's Mansion",
		Casino: "The Gaming House",
		PR_DanceHall: "Jezebelle's",
		PR_FancyTavern: "Green Gryphon",
		XXXShow: "Sindee Rella's Sexorium and Slut Show",
		GiftShop: "Curio Shop",
		PR_BackStage: "Backstage"
	});

	registerTravelDestination({
		PortRoyale: [
			{
				text: "Back to The Mermaid",
				destination: "Deck"
			},
			"ShipWright", "GreatMarket", "PR_BackAlley"
		],
		ShipWright: ["PortRoyale"],
		GreatMarket: ["PortRoyale", "FortuneTeller", "PR_Tavern", "PR_CheapBrothel", "EntertainmentDistrict", "PR_GuardHouse"],
		PR_BackAlley: ["PortRoyale", "PR_BottlecorkLane"],
		PR_BottlecorkLane: [
			{
				text: "Entrance",
				destination: "PR_BackAlley"
			},
			"PR_PublicHouse", "PR_DarkAlley"
		],
		PR_PublicHouse: ["PR_BottlecorkLane"],
		PR_DarkAlley: ["PR_BottlecorkLane", "PR_Levant"],
		PR_Levant: ["PR_DarkAlley",
			{
				available: (player) => !Quest.IsCompleted(player, "LEVANT_PLACEHOLDER"),
				destination: "PR_LevantBackRoom"
			}
		],
		PR_LevantBackRoom: [{text: "Le Maison Du Soleil Levant - Front Room", destination: "PR_Levant"}],
		FortuneTeller: ["GreatMarket"],
		PR_Tavern: ["GreatMarket"],
		PR_CheapBrothel: ["GreatMarket"],
		EntertainmentDistrict: ["GreatMarket", "Casino", "PR_DanceHall", "PR_FancyTavern", "XXXShow", "GiftShop"],
		PR_GuardHouse: ["GreatMarket", "PR_Jail", "WealthyDistrict"],
		PR_Jail: ["PR_GuardHouse"],
		WealthyDistrict: ["PR_GuardHouse", "CourtesansGuild", "PR_RareGoods", "PR_Clothier", "PR_GovernorsMansion"],
		CourtesansGuild: ["WealthyDistrict"],
		PR_RareGoods: ["WealthyDistrict"],
		PR_Clothier: ["WealthyDistrict"],
		PR_GovernorsMansion: ["WealthyDistrict"],
		Casino: ["EntertainmentDistrict"],
		PR_DanceHall: ["EntertainmentDistrict", "PR_BackStage"],
		PR_StudyCrowd: ["PR_BackStage"],
		PR_FancyTavern: ["EntertainmentDistrict"],
		XXXShow: ["EntertainmentDistrict"],
		GiftShop: ["EntertainmentDistrict"],
	})
}
